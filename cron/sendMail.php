<?php
    header("Content-Type: text/html; charset=utf-8");
    // Шаблон для крон файлов
    include_once dirname(__FILE__).'/../core/initialization.php';
    include_once dirname(__FILE__).'/../core/cms.php';
    include_once dirname(__FILE__).'/../core/classes/sendMail.php';
    $cms = new Cms(); // тольк в кронах
    $sendMail->cron();
    $sendMail->unsubscribe();

    // Пример использования
    /*$sendMail->get(['unic'      => 'test',            // уникальное имя рассылки, лучше использовать с id записи
                    //'user'      => 'all',             // or [1,2 ...] // только для email
                    'email'     => ['test@test.ru'],    // вариант при регистрации или если нет bk_user
                    // 'phone'     => ['79220000000'],  // для sms рассылки если имеется, можно совместно с блоком 'email'
                    'subject'   => 'title',             // заголовок письма
                    'message'   => 'message'            // ntrcn gbcmvf
                    ]);*/
?>

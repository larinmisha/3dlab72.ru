<!-- Модальное окно -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog modal_dialog">
        <div class="modal-content modal_content">

        </div>
      </div>
    </div>
<!-- ^^^Модальное окно -->
<script type="text/x-handlebars-template" id="js__scan_qr">
<div class="modal fade" id="myModal_user__login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class='modal-header'>
                <button type='button' class='close' data-dismiss='modal'><span aria-hidden='true'>&times;</span></button>
                <h3 class='modal-title' id='myModalLabel'>Сканер QR-code</h3>
            </div>
            <div class='modal-body modal_body'>
                <!-- <div class='mb1' style='color: #253387;font-size: 18px;'>наведите на любой QR-code представленный на выставке</div> -->
                <video muted playsinline id="qr-video">
                </video>
                <small>PWA технология довольно молода, и не все устройства предоставляют доступ к камере. Если у вас не отображается QR-сканер, зайдите на сайт Тюменской марки через браузер на телефоне (например Google Chrome) и вы сможете использовать дунную функцию.</small>
            </div>
            <div class='modal-footer'>

            </div>
        </div>
    </div>
</div>
</script>

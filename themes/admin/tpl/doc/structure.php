<div class="panel panel-primary">
    <div class="panel-heading">Описание структур папок:</div>
    <div class="panel-body">
        <ol>
            <li>
                <p>Структура ядра:</p>
                 <ul class="list-unstyled">
                    <li>
                        <ul>
                            <li><p><b><span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span> &nbsp; cache </b> - Используется для хранения кеша и скомпелированного css из less. </p></li>
                            <li><p><b><span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span> &nbsp; core </b> - ядро cms системы;</p>
                                <ul>
                                    <li><p><b><span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span> &nbsp; classes </b> - Дополнительные классы ядра;</p>
                                        <ul>
                                            <li><p><b><span class="glyphicon glyphicon-file" aria-hidden="true"></span> &nbsp; api.php </b> - Базовый класс для api;</p></li>
                                            <li><p><b><span class="glyphicon glyphicon-file" aria-hidden="true"></span> &nbsp; base.php </b> - Базовый класс с функциями;</p></li>
                                            <li><p><b><span class="glyphicon glyphicon-file" aria-hidden="true"></span> &nbsp; bootstrap__nav.php </b> - Используется совместно с bootstrap.php;</p></li>
                                            <li><p><b><span class="glyphicon glyphicon-file" aria-hidden="true"></span> &nbsp; designer_form.php </b> - Конструктор форм. Используется в админке;</p></li>
                                            <li><p><b><span class="glyphicon glyphicon-file" aria-hidden="true"></span> &nbsp; PDOTester.php </b> - Оболочка для pdo, позволяет выводить запросы;</p></li>

                                        </ul>
                                    </li>
                                    <li><p><b><span class="glyphicon glyphicon-file" aria-hidden="true"></span> &nbsp; cms.php </b> - Основной файл ядра cms;</p></li>
                                    <li><p><b><span class="glyphicon glyphicon-file" aria-hidden="true"></span> &nbsp; conf.local.php </b> - Настройки ядра (для local.sity.ru);</p></li>
                                    <li><p><b><span class="glyphicon glyphicon-file" aria-hidden="true"></span> &nbsp; conf.php </b> - Настройки ядра (для sity.ru);</p></li>
                                    <li><p><b><span class="glyphicon glyphicon-file" aria-hidden="true"></span> &nbsp; initialization.php </b> - Обёртка для всех точек вхожа, вызывать этот файл;</p></li>
                                    <li><p><b><span class="glyphicon glyphicon-file" aria-hidden="true"></span> &nbsp; minify.php </b> - Уменьшение размеров файлов css, js. Объединение всех файлов в один файл;</p></li>
                                </ul>

                            </li>
                            <li><p><b><span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span> &nbsp; cache </b> - Используется для хранения cache файлов. </p></li>
                            <li><p><b><span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span> &nbsp; files </b> - Используется для хранения файлов модулей. </p></li>
                            <li><p><b><span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span> &nbsp; library </b> - Компоненты js или библиотеками, которые нельзя скачать через bower. </p></li>
                            <li><p><b><span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span> &nbsp; modules </b> - Модули системы.</p>
                                <ul>
                                    <li><p><b><span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span> &nbsp; admin_cms </b> - Папка для модулей ядра (системные);</p>
                                        <ul>
                                            <li><p><b><span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span> &nbsp; admin </b> - Модули для админ-панели(api);</p></li>
                                            <li><p><b><span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span> &nbsp; captcha </b> - Код с картинки;</p></li>
                                            <li><p><b><span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span> &nbsp; debug </b> - Модуль для тестирования api;</p></li>
                                            <li><p><b><span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span> &nbsp; page </b> - Дополнительные страницы(404, badbrowser);</p></li>
                                            <li><p><b><span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span> &nbsp; tree_node </b> - Навигация (вывод вложенных разделов);</p></li>
                                            <li><p><b><span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span> &nbsp; user </b> - Модуль для работы с авторизацией(in, out);</p></li>
                                        </ul>
                                    </li>
                                    <li><p><b><span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span> &nbsp; feedback </b> - Модуль обратной связи;</p></li>
                                    <li><p><b><span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span> &nbsp; informer </b> - Информер;</p></li>
                                    <li><p><b><span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span> &nbsp; main_page </b> - Модуль для главной страницы;</p></li>
                                    <li><p><b><span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span> &nbsp; poligon </b> - Модуль для зарисовки полигонов;</p></li>
                                    <li><p><b><span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span> &nbsp; ... </b> - в каждой папке свой модуль;</p></li>
                                </ul>
                            </li>
                            <li><p><b><span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span> &nbsp; themes </b> - Темы для сайта.</p>
                                <ul>
                                    <li><p><b><span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span> &nbsp; admin </b> - Тема для админ-панели;</p></li>
                                    <li><p><b><span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span> &nbsp; default </b> - Тема по умолчанию для front;</p></li>
                                    <li><p><b><span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span> &nbsp; ... </b> - Любая другая тема, может быть подключена для разных страниц;</p></li>
                                </ul>
                            </li>
                            <li><p><b><span class="glyphicon glyphicon-file" aria-hidden="true"></span> &nbsp; .gitignore </b> - Описываются какие файлы и папки надо игнорировать и не заливать в git репозиторий.</p></li>
                            <li><p><b><span class="glyphicon glyphicon-file" aria-hidden="true"></span> &nbsp; package.json </b> - Описывает все js библиотеки которые нужны, загружается через yarn</p></li>
                            <li><p><b><span class="glyphicon glyphicon-file" aria-hidden="true"></span> &nbsp; composer.json </b> - Описывает все php библиотеки которые нужны, загружается через composer</p></li>
                            <li><p><b><span class="glyphicon glyphicon-file" aria-hidden="true"></span> &nbsp; index.php </b> -Стартовый файл ядра, обходных путей нет</p></li>
                            <li><p><b><span class="glyphicon glyphicon-file" aria-hidden="true"></span> &nbsp; robots.txt </b> - Описывает правила индексации для роботов. Необходимо удалить, перез запуском в сеть</p></li>
                            <li><p><b><span class="glyphicon glyphicon-file" aria-hidden="true"></span> &nbsp; tesseract.sql </b> - Талицы БД для ядра</p></li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li>
                <p>Структура папки themes, на примере одного шаблона:</p>
                <ul class="list-unstyled">
                    <li>
                        <ul>
                            <li><p><b><span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span> &nbsp; img </b> - Картинок для темы(только дизайн header и footer)</p></li>
                            <li><p><b><span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span> &nbsp; js </b> - js функции используемые более чем в одном модуле</p>
                                <ul>
                                    <li><p><b><span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span> &nbsp; js.js </b> - Основной файл js для темы, уже подключен</p></li>
                                    <li><p><b><span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span> &nbsp; *.js </b> - Можно написать свой js файл, но подключать вручную</p></li>
                                </ul>
                            </li>
                            <li><p><b><span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span> &nbsp; less </b> - less для описания стилей</p>
                                <ul>
                                    <li><p><b><span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span> &nbsp; ... </b> - Разные настройки bootstrap</p></li>
                                    <li><p><b><span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span> &nbsp; style.less </b> - Основной файл куда писать less стили для темы</p></li>
                                </ul>
                            </li>

                            <li><p><b><span class="glyphicon glyphicon-file" aria-hidden="true"></span> &nbsp; html.php </b> - Разметка сайта;</p></li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li>
                <p>Структура папки modules, на примере одного шаблона:</p>
                <ul class="list-unstyled">
                    <li>
                        <ul>
                            <li><p><b><span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span> &nbsp; img </b> - Картинок для модуля(только дизайн)</p></li>
                            <li><p><b><span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span> &nbsp; js </b> - js функции используемые более чем в одном модуле</p>
                                <ul>
                                    <li><p><b><span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span> &nbsp; js.js </b> - js для модуля, используется на front</p></li>
                                    <li><p><b><span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span> &nbsp; admin.js </b> - js для модуля, используется на admin</p></li>
                                    <li><p><b><span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span> &nbsp; *.js </b> - Можно написать свой js файл, но подключать вручную</p></li>
                                </ul>
                            </li>
                            <li><p><b><span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span> &nbsp; less </b> - less для описания стилей</p>
                                <ul>
                                    <li><p><b><span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span> &nbsp; ... </b> - Разные настройки bootstrap</p></li>
                                    <li><p><b><span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span> &nbsp; style.less </b> - Основной файл куда писать less стили для front темы</p></li>
                                    <li><p><b><span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span> &nbsp; admin.less </b> - Основной файл куда писать less стили для admin темы</p></li>
                                </ul>
                            </li>
                            <li><p><b><span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span> &nbsp; collections </b> - Коллекции модуля</p></li>
                            <li><p><b><span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span> &nbsp; tpl </b> - оболочки для front которые берут данные из src</p></li>

                            <li><p><b><span class="glyphicon glyphicon-file" aria-hidden="true"></span> &nbsp; admin.php </b> - Стартовый файл для admin панели выбранного модуля;</p></li>
                            <li><p><b><span class="glyphicon glyphicon-file" aria-hidden="true"></span> &nbsp; main.php </b> - Стартовый файл для запросов с front;</p></li>
                            <li><p><b><span class="glyphicon glyphicon-file" aria-hidden="true"></span> &nbsp; api.php </b> - Основная точка схода для api запросов;</p></li>
                            <li><p><b><span class="glyphicon glyphicon-file" aria-hidden="true"></span> &nbsp; tables.sql </b> - Таблицы БД для модуля, инсталлируются автоматически один раз;</p></li>
                        </ul>
                        <p>Стоит отметить что предлагаемая структура папки может быть изменена, в зависимости от модуля и способа его реализации. Файлы и папки могут как присутствовать и отсутствовать, система сама определит их наличие и подключит в случае необходимости</p>
                    </li>
                </ul>
            </li>
        </ol>
    </div>
</div>

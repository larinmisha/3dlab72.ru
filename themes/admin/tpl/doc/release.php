<div class="panel panel-primary">
    <div class="panel-heading">Действия необходимые перед запуском проекта</div>
    <div class="panel-body">
        <ol>
            <li>
                <p>Перед запуском в релиз необходимо удалить:</p>
                <ul class="list-unstyled">
                    <li> <p><b>/robots.txt</b> - Если необходима индексация сайта поисковиками;</p></li>
                    <li> <p><b>/tesseract.sql</b> - Талицы БД для ядра;</p></li>
                    <li> <p><b>/modules/***/*.sql</b> - Талицы БД для модулей;</p></li>
                </ul>
                <br>
            </li>
        </ol>
    </div>
</div>

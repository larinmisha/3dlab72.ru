<div class="panel panel-primary">
    <div class="panel-heading">Всё о работе с пользователями:</div>
    <div class="panel-body">
        <ol>
            <li>
                <p>Пользователи (параметры модуля смотрите в разделе <b><a href="<?php echo $this->structure_page(1, 'chpy'); ?>doc-start">Установка</a></b>)</p>
                <ul class="list-unstyled">
                    <li>
                        <p>Доступ к функциям работы с пользователями реализован через js класс, которая в свою очередь работает с модулем user.</p>
                        <p>Для подключения, необходимо прописать нужный класс к активному блоку</p>
                        <ul>
                            <li> </lip>Авторизация: <pre>.js-user__login</pre></li></li>
                            <li> </lip>Выход из учётной записи: <pre>.js-user__logout</pre></li></li>
                            <li> </lip>Регистрация: <pre>.js-user__reg</pre></li></li>
                            <li> </lip>Обновление ФИО, смена пароля: <pre>.js-user__update_profile</pre></li></li>
                            <li> </lip>Все шаблоны форм можно править: <pre>modules\admin_cms\user\tpl</pre></li></li>
                        </ul>
                        <p><b>Если нужны дополнительные параметры в учётной записи пользователя, то их можно добавить в необходимом шаблоне, и их сохранение прописать в самом модуле user</b></p>
                        <p>За что отвечают файлы в модуле:</p>
                        <ul>
                            <li> <p>api.php - точка входа для http и api протокола</p></li>
                            <li> <p>api-in.php - в случае использования внутренней авторизации</p></li>
                            <li> <p>api-out.php - в случае использования внешней авторизации</p></li>
                            <li> <p>esia.php - вход через ЕСИА, настраивается отдельно</p></li>
                            <li> <p>loginza.php - вход через соц.сети</p></li>
                        </ul>
                        <pre>themes\admin\js\auth.js<br>modules\admin_cms\user\***</pre>
                    </li>
                </ul>
            </li>
            <li>
                <p>При ошибках авторизации:</p>
                <p>В базе установлена система от перебора связки логин-пароль, она активна всегда и её невозможно отключить.</p>
                <p>В случае если в определённого <b>ip</b>., за один день будет <b>>100</b> попыток входа, то первые 100 ещё будут пытаться авторизоваться в базе, но после сотой попытки, всегда будет выдаваться сообщение о том что логин и пароль не верные. Даже если и введут правильно.</p>
                <p>данный счётчик можно сбросить вручную, удалив лишние (или все) строки в таблице <b>bk_security</b>
                </p>
            </li>
            <li>
                <p>При успешной авторизации:</p>
                <ul class="list-unstyled">
                    <li><p>Создаётся под массив, который хранит все данные пользователя(все поля из таблицы <b>bk_structure</b>).</p>
                         <pre>$_SESSION['user'][]</pre>
                    </li>
                </ul>
            </li>
            <li>
                <p>Работа с группами доступа:</p>
                <ul class="list-unstyled">
                    <li><p>Для проверки группы доступа пользователь должен быть авторизован.<br>
                        Основная задача этой функции проверка доступа в админке.</p>
                         <pre>$this->is_access($id, $tip);</pre>
                        <p>Где, $tip - это тип проверки, а $id - это идентификатор раздела (bk_structure)<br>
                        Возможные варианты параметры $tip:</p>
                        <ul>
                            <li> <p>dir - проверка на доступ к разделу</p></li>
                            <li> <p>mod_edit - можно открывать модули</p></li>
                            <li> <p>mod_add - можно добавлять модули</p></li>
                            <li> <p>mod_delete - можно удалять модули</p></li>
                            <li> <p>mod_display - можно скрывать модули</p></li>
                            <li> <p>mod_list - можно открывать разделы и просматривать список модулей</p></li>
                            <li> <p>structure_edit - можно редактировать разделы ЧПУ</p></li>
                            <li> <p>structure_add - можно добавлять разделы </p></li>
                            <li> <p>structure_delete - можно удалять разделы </p></li>
                            <li> <p>structure_sort - можно сортировать разделы </p></li>
                        </ul>
                        <p>Важно! Советую использовать только первый тип. Все остальные будут переписаны и уже используются на уровне ядра, так что нет смысла их использовать где то ещё.</p>
                    </li>
                </ul>
            </li>
            <li>
                <p>Дополнительная защита в админку:</p>
                <ul class="list-unstyled">
                    <li><p>Если активирован параметр <b>conf::$get_admin</b>, тогда доступ в админку по следующему сценарию:</p>
                        <ul>
                            <li> <p>1. При попытке зайти в раздел <b>/admin/</b> будет выдаваться 404 ошибка</p></li>
                            <li> <p>2. Для активации одноразового доступа в админ панель, необходимо ввести <b>/admin/param</b>, где '<b>param</b>' это изменяемый параметр хранящийся в переменной:</p>
                                <pre>conf::$get_admin = 'param';</pre>
                                при этом входе с параметром будет так же 404 ошибка, но произойдёт одноразовая активация для доступа в админку.
                            </li>
                            <li> <p>3. Затем убираем дополнительный параметр и заходим напрямую в админку <b>/admin/</b>, в свою очередь там появиться форма для вода логина и пароля</p></li>
                            <li> <p>4. Если обновить страницу (без успешной авторизации), то одноразовый доступ в админку закроется и снова выдастся 404 ошибка.</p></li>
                            <li> <p>4. Если необходимо снова зайти в админку, тогда надо заново повторить все пункты с 2.</p></li>
                        </ul>
                    </li>
                </ul>
            </li>
        </ol>
    </div>
</div>

<div class="panel panel-primary">
    <div class="panel-heading">Доступные переменные:</div>
    <div class="panel-body">
        <ol>
            <li> <p><b>$this->open_page</b> - Хранит id открытой страницы из таблицы bk_structure.<br>Использовать в крайней необходимости, есть замена $modules['fk_structure'];</p></li>
            <li> <p><b>$modules</b> - Доступен только в папке modules. Хранит информацию о модуле и разделе откуда был запущен;</p>
                <pre><?php print_R(['id' => 'id модуля из таблицы bk_modules',
                                    'fk_structure' => 'id страницы из таблицы bk_structure',
                                    'dirrect' => 'название папки с модулем, где он хранится',
                                    'type_script' => 'тип модуля',
                                    'mid' => 'id модуля из таблицы bk_modul',
                                    'dir' => 'Полный путь на сервере до папки с модулем',
                                    'http' => 'http путь на сервере до папки с модулем, используется для получения картинок из папки img',
                                    'chpu' => 'ЧПУ до раздела(front)',
                                    'chpu_admin' => 'ЧПУ до раздела(admin)']); ?></pre>

            <p>Пример:</p>
                <pre><?php print_R(['id' => 1,
                                    'fk_structure' => 1,
                                    'dirrect' => 'main_page',
                                    'type_script' => 0,
                                    'mid' => 10,
                                    'dir' => 'D:/web_work/tesseract/modules/main_page/',
                                    'http' => 'http://local.tesseract.ru/modules/main_page/',
                                    'chpu' => 'http://local.tesseract.ru/',
                                    'chpu_admin' => 'http://local.tesseract.ru/admin/']); ?></pre>
            </li>
            <li> <p><b>$_SESSION['user']</b> - Хранит все поля пользователя(при успешной авторизации);</p>
                <pre>if ($_SESSION['user']['id']>0) {} - Так можно проверить сам факт авторизации(произвеён вход)</pre></li>
            <li> <p><b>conf::$ip();</b> - Получить ip пользователя</p></li>
            <li> <p><b>conf::$informer['yandex'];</b> - Хранит блок на сайт для метрики, на локальной версии не выводится</p></li>
        </ol>
    </div>
</div>

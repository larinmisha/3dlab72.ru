<div class="panel panel-primary">
    <div class="panel-heading">Краткая инструкция по настройке проекта:</div>
    <div class="panel-body">

        <p><b>Все команды выполняются через терминал из папки проекта.</b></p>
        <ol>
            <li>
                <p>Настроить базу данных:</p>
                <ul class="list-unstyled">
                    <li>- Развернуть базу данных из файла <b>tesseract.sql</b>;</li>
                    <li>- Создать нового пользователя, сгенерировать пароль;</li>
                    <li>- Настроить подключение к бд в файле <b>classes/conf.php</b>;</li>
                    <li>- Настроить подключение к бд в файле <b>classes/conf.local.php</b>;</li>
                </ul>
                <br>
            </li>
            <li>
                <p>Скачать сторонние php библиотеки в папку <b>vendor</b>:</p>
                <ul class="list-unstyled">
                    <li>$ <b>php</b> composer.phar install;</li>
                </ul>
                <br>
            </li>
            <li>
                <p>Скачать сторонние js библиотеки в папку <b>bower_components</b>:</p>
                <ul class="list-unstyled">
                    <li>$ <b>yarn;</b></li>
                </ul>
                <br>
            </li>
            <li>
                <p>Изменить <b>права на запись</b> в папках cache, modules, themes:</p>
                <ul class="list-unstyled">
                    <li>$ <b>chmod</b> 775 .;</li>
                    <li>$ <b>chmod</b> -R 777 cache;</li>
                </ul>
                <br>
            </li>
            <li>
                <p>Либо одной командой</b> (пункт 2,3,4):</p>
                <ul class="list-unstyled">
                    <li>$ <b>bash</b> install</li>
                </ul>
                <br>
            </li>
        </ol>

        <p><b>Ссылки:</b></p>
        <ul class="">
            <li><a href="https://nodejs.org/en/download/" class="link link-primary" target="_blank">Установка Node.js;</a></li>
            <li><a href="https://bower.io/#install-bower" class="link link-primary" target="_blank">Установка Bower;</a></li>
            <li><a href="https://getcomposer.org/download/" class="link link-primary" target="_blank">Установка Composer.php;</a></li>
            <li><a href="https://www.sublimetext.com/3" class="link link-primary" target="_blank">Установка Sublime Text 3;</a></li>
            <li><a href="https://www.sourcetreeapp.com/" class="link link-primary" target="_blank">Установка SourceTree;</a></li>
            <li><a href="http://www.ampps.com/download" class="link link-primary" target="_blank">Установка Ampps;</a></li>
        </ul>

        <p><b>Настройки конфигуратора</b></p>
        <ul>
            <li>
                <p><b>$get_admin</b> - дополнительный параметр для входа в админку <b>(<a href="<?php echo $this->structure_page(1, 'chpy'); ?>doc-group">подробнее</a>)</b></p>
                <p><b>$lessphp</b> - включить встроенный less компилятор</p>
                <p><b>$minify</b> - включить минификацию js и css файлов</p>
                <p><b>$path</b> - папка с названием сайта. В случае если сайт лежит не в корне</p>
                <p><b>$informer</b> - Хранит массив js кода для счётчиков(например метрика). позволяет на локальной версии не  выводить счётчики</p>
                <ul class="list-unstyled">
                    <li>
                        <ul>
                            <li> <p>start - описывает тип авторизации. "api-in" - внутри сайта, "api-out" - использует единый сервер авторизации</p></li>
                            <li> <p>url - Используется только в "api-out", указывает ссылку на единый сервер (может быть сайтом, но не желательно)</p></li>
                            <li> <p>key - используется только для разработки мобильных версий, передаёт заголовки говорящие о том что в ответ пришел json</p></li>
                            <li> <p>redirect - Используется для ЕСИА, в связке с "api-out"</p></li>
                            <li> <p>Пример:</p>
                            </li>
                                <?php echo "<pre>";print_r(["start" => "api-in",
                                                            "url"   => "http://user.tesseract.ru/api-user",
                                                            "key" => "123da52ed126447d359e70c05721a8aa",
                                                            "redirect"  => ['pref'=>'http://sity.ru/']]);echo "</pre>"; ?>
                        </ul>
                    </li>
                </ul>
            </li>
        </ul>

        <p><b>Установка БД на сервер</b></p>
        <ul>
            <li> <p>База данных <b>ядра</b> расположена в файле "tesseract.sql", расположенный в корне сайта</p></li>
            <li> <p>в каждом модуле может находиться файл "table.sql", который устанавливается всего один раз в момент первого использования модуля</p></li>
        </ul>
    </div>
</div>

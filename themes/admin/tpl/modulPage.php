<?php base::test(); ?>
<script id="js__modulPage" type="text/x-handlebars-template">
    <div class="input-group input-group-sm pb0_25">
        <span class="input-group-btn">
            <button type="submit"
                    class="btn js-create_modul btn-default"
                    >
                <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span>
                Добавить модуль
            </button>
            <button type="submit"
                    class="btn js-page_info btn-default"
                    >
                <span class="glyphicon glyphicon-wrench" aria-hidden="true"></span>
                Параметры раздела
            </button>
            <button type="submit"
                    class="btn js-page_add btn-default"
                    >
                <span class="glyphicon glyphicon-leaf" aria-hidden="true"></span>
                Добавить подраздел
            </button>
        </span>
    </div>
    <div class="pb0_25">
        {{#if breadcrumb}}
            {{breadcrumb}}
        {{/if}}
    </div>
    {{#each row}}
        <div class="input-group input-group-sm pb0_25">
            <span class="input-group-addon" id="sizing-addon1">
                {{title}}
            </span>

            <input class="form-control" placeholder="Комментарий к модулю" value="{{info}}">

            <span class="input-group-btn">
                <!-- <button type="submit"
                        class="btn js-modul__save_title btn-default"
                        data-id="{{id}}"
                        >
                    <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                </button> -->

                <button type="submit"
                        class="btn js-modul__open {{#if event.activ}} btn-active {{else}} btn-default {{/if}}"
                        data-id="{{id}}"
                        {{#if event.open}} disabled="disabled" {{/if}}
                        >
                        {{#if event.activ}}
                            <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                        {{else}}
                            <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                        {{/if}}
                        {{#if dirrect}}
                            <span style="width: 100px;display: inline-block;">
                                ({{dirrect}}, id: {{id}})
                            </span>
                        {{/if}}
                </button>

                <button type="submit"
                        class="btn js-modul__see {{#if event.see}} btn-default {{else}} btn-warning {{/if}}"
                        data-id="{{id}}"
                        >
                    <span class="glyphicon glyphicon-eye-{{glyphicon}}" aria-hidden="true"></span>
                </button>

                <button type="submit"
                        class="btn btn-default js-modul__sort"
                        data-id="{{id}}"
                        data-order="ASC"
                        >
                    <span class="glyphicon glyphicon-save" aria-hidden="true"></span>
                </button>
                <button type="submit"
                        class="btn btn-default js-modul__sort"
                        data-id="{{id}}"
                        data-order="DESC"
                        >
                    <span class="glyphicon glyphicon-open" aria-hidden="true"></span>
                </button>

                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li>
                        <a href="JavaScript:" class="js-modul__delete" data-id="{{id}}">Подтвердить удаление</a>
                    </li>
                </ul>
            </span>
        </div>
    {{/each}}
</script>


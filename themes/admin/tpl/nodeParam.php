<?php base::test(); ?>
<script id="js__form-nodeParams" type="text/x-handlebars-template">
    <form method="POST" id="node_element">
        <input type="hidden" name="id_tree" value='{{id}}'>
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Редактировать/добавить раздел</h3>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label for="name_title">Название</label>
                    <input type="text" class="form-control js-node__translit_title" id="name_title" name="title" value='{{title}}'>
                    <small>
                        <input type="checkbox" id='js-node__translit_ch' {{#if new}}checked{{/if}} > Автозаполение utl из поля "Названия"
                    </small>
                </div>
                <div class="form-group">
                    <label for="name_url">url</label>
                    <input type="text" class="form-control js-node__translit_url" id="name_url" name="url" value='{{url}}'>
                </div>
                <div class="form-group">
                    <label for="name_description">Описание</label>
                    <input type="text" class="form-control" id="name_description" name="description" value="{{description}}">
                </div>
                <div class="form-group">
                    <label for="name_keywords">Ключевые слова</label>
                    <input type="text" class="form-control" id="name_keywords" name="keywords" value="{{keywords}}">
                </div>
                <div class="form-group">
                    <label>Раздел открыт</label>
                    &nbsp;
                    {{#each open_row}}
                        &nbsp; &nbsp;
                        <input type="radio" id="name_open" name="open" value="{{id}}" {{#if checked}}checked=""{{/if}}>
                        <span>{{text}}</span>
                    {{/each}}
                </div>
                <div class="form-group">
                    <label>Отображение узлов</label>
                    <select name="tree_node">
                        {{#each tree_node}}
                            <option value="{{id}}" {{#if checked}}selected{{/if}} >{{text}}</option>
                        {{/each}}
                    </select>
                </div>
                <br>
                {{#if new}}
                    <div name="btn_work_update" class="btn btn-default btn-xs js-node__insert">
                        <span class="glyphicon glyphicon-floppy-save" aria-hidden="true"></span> Добавить раздел
                    </div>
                {{else}}
                    <div name="btn_work_update" class="btn btn-default btn-xs js-node__update">
                        <span class="glyphicon glyphicon-floppy-save" aria-hidden="true"></span> Внести изменения в раздел
                    </div>
                    &nbsp;
                    &nbsp;
                    &nbsp;
                    <div onclick="$('#b_del_').toggle();" class="btn btn-default btn-xs">
                        <span class="glyphicon glyphicon-trash" aria-hidden="true">
                        </span> Удалить
                    </div>
                    <span id="b_del_" style="display:none;">
                        -&gt; вы уверены? -&gt;
                        <button name="btn_work_del" class="btn btn-default btn-xs js-node__delete" type="button">
                            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Подтвердить
                        </button>
                    </span>
                {{/if}}
            </div>
        </div>
    </form>
</script>


<?php base::test(); ?>
<script id="js__form-modulesParams" type="text/x-handlebars-template">
    <form method="POST" id="modul_element">
        <input type="hidden" id="id_tree" name="id_tree" value="{{node}}">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h4 class="pb0_5">Добавляем модуль в раздел: {{breadcrumb}}</h4>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <div>Поле с информацией по модулю для админов</div>
                    <input type="text" class="form-control" id="name_info" name="info" placeholder="" value="">
                </div>
                {{#each modul}}
                    <div class="form-group">
                        {{tip}}:
                        {{#each row}}
                            <div class="radio">
                                <label>
                                    <input type="radio" id="name_id_modul" name="id_modul" value="{{id}}">
                                    <span>{{title}}</span>
                                </label>
                            </div>
                        {{/each}}
                    </div>
                {{/each}}
                <div class="btn btn-default btn-xs js-modul__list">
                    <span class="glyphicon glyphicon-floppy-save" aria-hidden="true"></span> Сохранить
                </div>
            </div>
        </div>
    </form>
</script>

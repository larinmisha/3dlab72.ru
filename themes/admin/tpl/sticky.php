<?php base::test(); ?>
<?php if ($this->is_access(1)) { ?>
    <?php base::minify_import("library/jquery.sticky.js"); ?>
    <?php base::minify_import("themes/admin/js/sticky.js"); ?>
    <?php
        $fio = $_SESSION['user']['f'];
        if ($_SESSION['user']['i']!= '') {
            $fio .= ' '.mb_substr($_SESSION['user']['i'], 0, 1).'.';
        }
        if ($_SESSION['user']['o']!= '') {
            $fio .= ' '.mb_substr($_SESSION['user']['o'], 0, 1).'.';
        }
    ?>
    <div class="js-header sticky">
        <div class="item">
            <?php echo file_get_contents(__DIR__."/../img/tesseract_fff.svg"); ?>
        </div>
        <div class="flex2">
            <div class="item">
                <?php echo $fio; ?> /
                <?php if ($this->admin) { ?>
                    <a href="<?php echo $this->url(null, null, 1) ?>" role="button">На сайт</a>
                <?php } else { ?>
                    <a href="<?php echo $this->url(null, 1, 1) ?>" role="button">Admin</a>
                <?php } ?>
                <?php if ($this->group_cms['full'] == 1) { ?>
                    / <a href="/admin/?doc=start" role="button">doc</a>
                <?php } ?>
            </div>
            <div class="item">
                <a href="<?php echo $this->url() ?>?logout">
                    <svg width="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg" >
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M4.1665 3.33329C3.94549 3.33329 3.73353 3.42109 3.57725 3.57737C3.42097 3.73365 3.33317 3.94561 3.33317 4.16663V15.8333C3.33317 16.0543 3.42097 16.2663 3.57725 16.4225C3.73353 16.5788 3.94549 16.6666 4.1665 16.6666H7.49984C7.96007 16.6666 8.33317 17.0397 8.33317 17.5C8.33317 17.9602 7.96007 18.3333 7.49984 18.3333H4.1665C3.50346 18.3333 2.86758 18.0699 2.39874 17.6011C1.9299 17.1322 1.6665 16.4963 1.6665 15.8333V4.16663C1.6665 3.50358 1.9299 2.8677 2.39874 2.39886C2.86758 1.93002 3.50346 1.66663 4.1665 1.66663H7.49984C7.96007 1.66663 8.33317 2.03972 8.33317 2.49996C8.33317 2.9602 7.96007 3.33329 7.49984 3.33329H4.1665Z" fill="#F3F5F7"/>
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M12.7441 5.24408C13.0695 4.91864 13.5972 4.91864 13.9226 5.24408L18.0893 9.41074C18.4147 9.73618 18.4147 10.2638 18.0893 10.5893L13.9226 14.7559C13.5972 15.0814 13.0695 15.0814 12.7441 14.7559C12.4186 14.4305 12.4186 13.9028 12.7441 13.5774L16.3215 10L12.7441 6.42259C12.4186 6.09715 12.4186 5.56951 12.7441 5.24408Z" fill="#F3F5F7"/>
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M6.6665 9.99996C6.6665 9.53972 7.0396 9.16663 7.49984 9.16663H17.4998C17.9601 9.16663 18.3332 9.53972 18.3332 9.99996C18.3332 10.4602 17.9601 10.8333 17.4998 10.8333H7.49984C7.0396 10.8333 6.6665 10.4602 6.6665 9.99996Z" fill="#F3F5F7"/>
                    </svg>
                </a>
            </div>
        </div>
    </div>
<?php } ?>

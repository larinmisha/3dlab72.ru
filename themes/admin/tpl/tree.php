<script id="js__tree-node" type="text/x-handlebars-template">
    <ul>
        {{#each node}}
            <li class="li-{{id}}" data-id="{{id}}">
                <div class="opened js__tree-opened" data-id="{{id}}" data-url="{{url}}">
                    {{#if node}}
                        <span class="glyphicon glyphicon-triangle-right" aria-hidden="true"></span>
                    {{/if}}
                </div>
                <a href="{{url}}">
                    <div class="item bg-offline">
                        <div class="move__block bg-offline js-node__item-{{id}} js-draggable" draggable="true" data-id="{{id}}"></div>
                        {{#if open}}
                            <span>{{#if access}}[{{id}}] {{/if}}{{title}}</span>
                        {{else}}
                            <span>{{#if access}}[{{id}}] {{/if}}<del>{{title}}</del></span>
                        {{/if}}
                    </div>
                </a>
                {{> node}}
            </li>
        {{/each}}
    </ul>
</script>

<?php base::test(); ?>

<?php if ($_GET['doc']=='') $_GET['doc'] = 'start'; ?>

<ul class="nav nav-tabs">
    <li class="<?php if ($_GET['doc']=='start')    echo 'active'; ?>"><a href="<?php echo $this->structure_page(1, 'chpy'); ?>?doc=start"    >Установка</a></li>
    <li class="<?php if ($_GET['doc']=='structure') echo 'active'; ?>"><a href="<?php echo $this->structure_page(1, 'chpy'); ?>?doc=structure" >Структура</a></li>
    <li class="<?php if ($_GET['doc']=='library')  echo 'active'; ?>"><a href="<?php echo $this->structure_page(1, 'chpy'); ?>?doc=library"  >Библиотеки</a></li>
    <li class="<?php if ($_GET['doc']=='group')    echo 'active'; ?>"><a href="<?php echo $this->structure_page(1, 'chpy'); ?>?doc=group"    >Пользователи/доступ</a></li>
    <li class="<?php if ($_GET['doc']=='var')      echo 'active'; ?>"><a href="<?php echo $this->structure_page(1, 'chpy'); ?>?doc=var"      >Переменные</a></li>
    <li class="<?php if ($_GET['doc']=='url')      echo 'active'; ?>"><a href="<?php echo $this->structure_page(1, 'chpy'); ?>?doc=url"      >ЧПУ/get/api</a></li>
    <li class="<?php if ($_GET['doc']=='release')  echo 'active'; ?>"><a href="<?php echo $this->structure_page(1, 'chpy'); ?>?doc=release"  >Релиз</a></li>
</ul>

<?php include dirname(__FILE__).'/doc/'.$_GET['doc'].'.php'; ?>


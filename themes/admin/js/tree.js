var template_tree;

function node(id, lavel='') {
    $.ajax({
        type: "POST",
        url: '/api/admin/tree/node/json',
        data: {node: id, lavel: lavel}
    }).done(function( json ) {
        if (lavel=='') {
            $('.js__tree').empty();
        }
        if ((json!=null)&&(json.data!=null)) {
            var html = template_tree(json.data);
            if (lavel=='') {
                $('.js__tree').append(html);
                $('.js-node__item-'+id).addClass('selected');
            } else {
                $('.js__tree .li-'+id+' ul').remove();
                $('.js__tree .li-'+id).append(html);
                $('.js-node__item-'+id).addClass('selected');
            }
        }
        refresh_modul();
    });
}
$(function() {

    if ($("#js__tree-node").html()!=undefined) {
        template_tree = Handlebars.compile($("#js__tree-node").html());
        Handlebars.registerPartial("node", $("#js__tree-node").html());
        node(NODE);
    }

    $('body').on('click', '.js__tree-opened', function(e) {
        node($(this).data('id'), 1);
    });

    move_open_start = 0;
    move_tip = '';
    $('body').on('dragover', '.js-draggable', function(e) {
        event.preventDefault();
        $(".js__tree .js-new__li-top").remove();
        $(".js__tree .js-new__li-bottom").remove();
        $(".js__tree .js-new__li-center").remove();

        if (e.offsetY < e.target.clientHeight/4) {
            tpl = $('.js-tree__item-tpl-add .js-new__li-top').clone();
            $(event.target).closest(".item").before(tpl);
            move_open_start = 0;
            move_tip = 'top';
        } else if (e.offsetY > e.target.clientHeight*3/4) {
            tpl = $('.js-tree__item-tpl-add .js-new__li-bottom').clone();
            $(event.target).closest(".item").after(tpl);
            move_open_start = 0;
            move_tip = 'bottom';
        } else {
            tpl = $('.js-tree__item-tpl-add .js-new__li-center').clone();
            $(event.target).closest(".item").before(tpl);
            move_tip = 'center';
            if (move_open_start == 0) {
                move_open_start = $.now();
            }
            if ((move_open_start+500 <= $.now())&&(move_open_start>0)) {
                move_open_start = -1;
                node($(this).data('id'), 1);
            }
        }
    });
    $('body').on('dragleave', '.js-draggable', function(e) {

    });
    $('body').on('dragstart', '.js-draggable', function(e) {
        event.dataTransfer.setData("text", $(event.target).data('id')); // трансфер

        var crt = this.cloneNode(true);
        crt.style.backgroundColor = "red";
        event.dataTransfer.setDragImage(crt, 0, 0);
    });
    $('body').on('drop', '.js-draggable', function(e) {
        event.preventDefault();
        var data = event.dataTransfer.getData("text");

        if (move_tip == 'top') {
            prev_id = $('.li-'+$(event.target).closest(".js-draggable").data('id')).prev('li').data('id');
            new_parent = $(this).closest('ul').closest('li').data('id');
        } else if (move_tip == 'bottom') {
            prev_id = $(event.target).closest(".js-draggable").data('id');
            new_parent = $(this).closest('ul').closest('li').data('id');
        } else {
            prev_id = 0;
            new_parent = $(event.target).closest(".js-draggable").data('id');
        }
        if ($(this).closest('.li-'+data).data('id') == undefined) {
            if (confirm("Переместить?")) {
                $.ajax({
                    type: "POST",
                    url: '/api/admin/tree/move_nod/json',
                    data: { my_id: data,
                            prev_id: prev_id,
                            new_parent: new_parent}
                }).done(function( json ) {
                    location.href = json.data.url
                });
            }
        } else {
            modal_doc_max('Ошибка','Невозможно переместить страницу внутрь своей структуры','sm','');
        }
        $(".js__tree .js-new__li-top").remove();
        $(".js__tree .js-new__li-bottom").remove();
        $(".js__tree .js-new__li-center").remove();
    });
});

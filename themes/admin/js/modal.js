/* Функция вывода модальных окон */
function modal_doc_max(title,content,size,type) {
	if (title === '') {
		title = "Уведомление";
	}
	if (type == 'google') {
		content = "<iframe src=http://docs.google.com/viewer?url="+content+"&amp;embedded=true width='100%' height='700px' style='border: none;'></iframe>";
	}
	if (type == 'text') {
		content = "<p>"+content+"<p>";
	}
	if (type == 'youtube') {
		content = "<iframe src='https://www.youtube.com/embed/"+content+"?autoplay=1' class='embed-responsive-item' width='100%' height='700px'></iframe>";
	}

	$('.modal_content').html("<div class='modal-header'>"
									+"<button type='button' class='close' data-dismiss='modal'><span aria-hidden='true'>&times;</span>"
									+"<h4 class='modal-title' id='myModalLabel'>"+title+"</h4>"
								+"</div>"
								+"<div class='modal-body modal_body' >"
									+content
								+"</div>"
								+"<div class='modal-footer'>"
									+"<button type='button' class='btn btn-default' data-dismiss='modal'>Закрыть</button>"
								+"</div>"
							);

	$('.modal_dialog').removeClass('modal-xs modal-sm modal-md modal-lg');

	if (size == 'xs') {
		$('.modal_dialog').addClass('modal-xs');
	}
	else if (size == 'sm') {
		$('.modal_dialog').addClass('modal-sm');
	}
	else if (size == 'md') {
		$('.modal_dialog').addClass('modal-md');
	}
	else if (size == 'lg') {
		$('.modal_dialog').addClass('modal-lg');
	}
	else {
		$('.modal_dialog').addClass('modal-md');
	}

	$('#myModal').modal();
}
$(function() {
	if ($('*').hasClass("js-gallery")) {
		$(".js-gallery").lightGallery({selector: 'a.js-item'}); // запускаем галлереи
	}
	$('body').on('DOMNodeInserted', function () {
		if ($('*').hasClass("js-gallery")) {
            $(".js-gallery").lightGallery({selector: 'a.js-item'}); // запускаем галлереи
        }
	});
	// отслежиаем появление новых элементов

	// удалить контент, после закрытия модального окна
	$('body').on('hidden.bs.modal', '#myModal', function () {
	    $('.modal_content', '#myModal').html('');
	});
	// отслежиаем появление новых элементов и делаем их квадратными (для фото)
	$('body').on('DOMNodeInserted', function () {
		$('.js-sq').each(function() {
			if ($(this).data('sq') != undefined) {
				$(this).css('height', $(this).css('width').slice(0, -2)*$(this).data('sq'));
			} else {
				$(this).css('height', $(this).css('width').slice(0, -2)*0.66);
			}
		});
	});
	// при повороте экрана
	$(window).resize(function () {
		$('.js-sq').each(function() {
			if ($(this).data('sq') != undefined) {
				$(this).css('height', $(this).css('width').slice(0, -2)*$(this).data('sq'));
			} else {
				$(this).css('height', $(this).css('width').slice(0, -2)*0.66);
			}
		});
	});
	$('.js-sq').each(function() {
		if ($(this).data('sq') != undefined) {
			$(this).css('height', $(this).css('width').slice(0, -2)*$(this).data('sq'));
		} else {
			$(this).css('height', $(this).css('width').slice(0, -2)*0.66);
		}
	});
	$('.js-sq__int').each(function() {
	});
});
$.urlParam = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null){
       return null;
    }
    else{
       return results[1] || 0;
    }
}
/* Функция вывода модальных окон */


var tpl_nodeParams;
var tpl_modulesParams;
var tpl_modulPage;
$(function() {
    TinyMCE();

    tpl_nodeParams    = Handlebars.compile($("#js__form-nodeParams").html());
    tpl_modulesParams = Handlebars.compile($("#js__form-modulesParams").html());
    tpl_modulPage 	  = Handlebars.compile($("#js__modulPage").html());
});

function loader(class_name = '.js-loader') {
    var opts = {
                  lines: 13             // The number of lines to draw
                , length: 38            // The length of each line
                , width: 10             // The line thickness
                , radius: 32            // The radius of the inner circle
                , scale: 0.2            // Scales overall size of the spinner
                , corners: 1            // Corner roundness (0..1)
                , color: '#fff'         // #rgb or #rrggbb or array of colors
                , opacity: 0.25         // Opacity of the lines
                , rotate: 0             // The rotation offset
                , direction: 1          // 1: clockwise, -1: counterclockwise
                , speed: 1              // Rounds per second
                , trail: 60             // Afterglow percentage
                , fps: 20               // Frames per second when using setTimeout() as a fallback for CSS
                , zIndex: 2e9           // The z-index (defaults to 2000000000)
                , className: 'spinner'  // The CSS class to assign to the spinner
                , top: '50%'            // Top position relative to parent
                , left: '50%'           // Left position relative to parent
                , shadow: false         // Whether to render a shadow
                , hwaccel: false        // Whether to use hardware acceleration
                , position: 'absolute'  // Element positioning
                }
    var spinner = new Spinner(opts).spin();
    tpl = $(class_name);
    tpl.html(spinner.el);
    tpl.show();
    return tpl;
    // http://fgnass.github.io/spin.js/#! - описание
}

function TinyMCE() {
    tinymce.remove();

    tinymce.init({
        selector: "textarea.js-tinymce",
        language : "ru",
        language_url: '/library/tinymce/langs/ru.js',
        paste_as_text : true, // вставить как текст
        statusbar: false,
        plugins: [
            "advlist autolink lists link image charmap print preview visualblocks code fullscreen media table paste" // jbimages
        ],
        menubar: false,
        setup: function (editor) {
            editor.on('change', function () {
                tinymce.triggerSave();
            });
        },
        font_formats: '', // шрифты отключил
        paste_word_valid_elements: "b,strong,i,h1,h2, table, tr, td, th, p",
        contextmenu: 'link cut copy paste',
        // external_plugins: {
        //     'jbimages': '/library/tinymce/plugins/jbimages/plugin.min.js'
        // },
        // toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link | fullscreen",
        toolbar: "undo redo | link table image media | bullist numlist | fullscreen code preview",
        relative_urls: false
    });
    $('#myModal').on('shown.bs.modal', function() {
        $(document).off('focusin.modal');
    });

}

function cyr2lat(what) { // транслит
	str = $(what).val();
	var cyr2latChars = new Array(
		['а', 'a'], ['б', 'b'], ['в', 'v'], ['г', 'g'],
		['д', 'd'],  ['е', 'e'], ['ё', 'yo'], ['ж', 'zh'], ['з', 'z'],
		['и', 'i'], ['й', 'y'], ['к', 'k'], ['л', 'l'],
		['м', 'm'],  ['н', 'n'], ['о', 'o'], ['п', 'p'],  ['р', 'r'],
		['с', 's'], ['т', 't'], ['у', 'u'], ['ф', 'f'],
		['х', 'h'],  ['ц', 'c'], ['ч', 'ch'],['ш', 'sh'], ['щ', 'shch'],
		['ъ', ''],  ['ы', 'y'], ['ь', ''],  ['э', 'e'], ['ю', 'yu'], ['я', 'ya'],

		['А', 'a'], ['Б', 'b'],  ['В', 'v'], ['Г', 'g'],
		['Д', 'd'], ['Е', 'e'], ['Ё', 'yo'],  ['Ж', 'zh'], ['З', 'z'],
		['И', 'i'], ['Й', 'y'],  ['К', 'k'], ['Л', 'l'],
		['М', 'm'], ['Н', 'n'], ['О', 'o'],  ['П', 'p'],  ['Р', 'r'],
		['С', 's'], ['Т', 't'],  ['У', 'u'], ['Ф', 'f'],
		['Х', 'h'], ['Ц', 'c'], ['Ч', 'ch'], ['Ш', 'sh'], ['Щ', 'shch'],
		['Ъ', ''],  ['Ы', 'y'],
		['Ь', ''],
		['Э', 'e'],
		['Ю', 'yu'],
		['Я', 'ya'],

		['a', 'a'], ['b', 'b'], ['c', 'c'], ['d', 'd'], ['e', 'e'],
		['f', 'f'], ['g', 'g'], ['h', 'h'], ['i', 'i'], ['j', 'j'],
		['k', 'k'], ['l', 'l'], ['m', 'm'], ['n', 'n'], ['o', 'o'],
		['p', 'p'], ['q', 'q'], ['r', 'r'], ['s', 's'], ['t', 't'],
		['u', 'u'], ['v', 'v'], ['w', 'w'], ['x', 'x'], ['y', 'y'],
		['z', 'z'],

		['A', 'a'], ['B', 'b'], ['C', 'c'], ['D', 'd'],['E', 'e'],
		['F', 'f'],['G', 'g'],['H', 'h'],['I', 'i'],['J', 'j'],['K', 'k'],
		['L', 'l'], ['M', 'm'], ['N', 'n'], ['O', 'o'],['P', 'p'],
		['Q', 'q'],['R', 'r'],['S', 's'],['T', 't'],['U', 'u'],['V', 'v'],
		['W', 'w'], ['X', 'x'], ['Y', 'y'], ['Z', 'z'],

		[' ', '-'],['0', '0'],['1', '1'],['2', '2'],['3', '3'],
		['4', '4'],['5', '5'],['6', '6'],['7', '7'],['8', '8'],['9', '9'],
		['-', '-']
	);

	var newStr = new String();

	for (var i = 0; i < str.length; i++) {
		ch = str.charAt(i);
		var newCh = '';
		for (var j = 0; j < cyr2latChars.length; j++) {
			if (ch == cyr2latChars[j][0]) {
				newCh = cyr2latChars[j][1];
			}
		}
		newStr += newCh;
	}
	return newStr;
}

// начать повторы с интервалом 2 сек
var timerId = setInterval(function() {
	$.each($('.js-message__list .sms'), function(index, field) {
		if ($(this).data('time')>1) {
			$(this).data('time', Number($(this).data('time'))-1);
			$(this).find('.time').html("00:0"+Number($(this).data('time')));
		} else {
			$(this).remove();
		}
    });
}, 1000);

// через 5 сек остановить повторы
// setTimeout(function() {
//   clearInterval(timerId);
// }, 5000);

jQuery.ajaxSetup({
    dataFilter: function (json, type) {
        // message(JSON.parse(json)); // надо tpl шаблоны обрабатывать отдельно или передавать тип json
        return json;
    }
});

function message(json) {
    if (json) {
        if (json.message) {
            if ((json.message.true)&&(json.message.true.length>0)) {
                $.each(json.message.true, function(e) {
                    div = $('<div/>').addClass('sms')
                                     .data('time',10)
                                     .append(this)
                                     .append($('<div/>').addClass('time').html('00:04'));
                    $('.js-message__list').append(div);
                });
            }
            if ((json.message.false)&&(json.message.false.length>0)) {
                $.each(json.message.false, function(e) {
                    div = $('<div/>').addClass('sms error')
                                     .data('time',10)
                                     .append(this)
                                     .append($('<div/>').addClass('time').html('00:04'));
                    $('.js-message__list').append(div);
                });
            }
            if (json.message.refresh) {
                location.reload();
            }
        }
    }
}

$('body').on('click', '.js-modul__list', function(e) {
    var formData = main.fetchformData('#modul_element');
    id = $('#id_tree').val();
    main.ajaxRequest('/api/admin/modul/insert/json', formData)
        .done(function(json) {
            if ((json.message.true)&&(json.message.true.length>0)) {
                location.reload();
            }
            // NODE = json.data.node;
            // refresh_modul();
        });
});

$('body').on('keyup', '.js-node__translit_title', function(e) {
    if ($('#js-node__translit_ch:checked').val() == 'on') {
        $('.js-node__translit_url').val(cyr2lat('.js-node__translit_title'));
    }
});

$('body').on('click', '.js-node__insert', function(e) {
    var formData = main.fetchformData('#node_element');
    $('.menu_structure').html("<img src='/themes/admin/img/ajax-loader.gif'>");
    main.ajaxRequest('/api/admin/tree/insert/json', formData)
        .done(function(json) {
            if ((json.message.true)&&(json.message.true.length>0)) {
                window.location.href = json.data.url;
            }
        });
});

$('body').on('click', '.js-node__update', function(e) {
    var formData = main.fetchformData('#node_element');
    main.ajaxRequest('/api/admin/tree/update/json', formData)
        .done(function(json) {
            $('.menu_structure').html('');
            NODE = json.data.node;
            node(json.data.node);
            refresh_modul();
        });
});

$('body').on('click', '.js-node__delete', function(e) {
    var formData = main.fetchformData('#node_element');
    main.ajaxRequest('/api/admin/tree/delete/json', formData)
        .done(function(json) {
            $('.menu_structure').html('');
            NODE = json.data.node;
            node(json.data.node);
            refresh_modul();
        });
});

$('body').on('click', '.js-modul__delete', function(e) {
    if (confirm("Удалить модуль?")) {
        var formData = new FormData();
        formData.append('id', $(this).data('id'));
        main.ajaxRequest('/api/admin/modul/delete/json', formData)
            .done(function(json) {
                if ((json.message.true)&&(json.message.true.length>0)) {
                    location.reload();
                }
            });
    }
});

$('body').on('click', '.js-modul__see', function(e) {
    var formData = new FormData();
    formData.append('id', $(this).data('id'));
    main.ajaxRequest('/api/admin/modul/see/json', formData)
        .done(function(json) {
            refresh_modul();
        });
});

$('body').on('click', '.js-modul__sort', function(e) {
    var formData = new FormData();
    formData.append('id', $(this).data('id'));
    formData.append('order', $(this).data('order'));
    main.ajaxRequest('/api/admin/modul/sort/json', formData)
        .done(function(json) {
            refresh_modul();
        });
});
// <button data-text="123" class="js-copy__buffer">пример</button>
$('body').on('click', '.js-copy__buffer', function(e) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($(this).data('text')).select();
    document.execCommand("copy");
    $temp.remove();
    message({data: [{detail: "Сохранено в буфер"}]});
});

$('body').on('click', '.js-create_modul', function(e) {
    var formData = new FormData();
    formData.append('node', NODE);
    main.ajaxRequest('/api/admin/modul/setings/json', formData)
    .done(function(json) {
        var html = tpl_modulesParams(json.data);
        $('.menu_structure').html(html);
    });
});
$('body').on('click', '.js-page_add', function(e) {
    var formData = new FormData();
    formData.append('id', NODE);
    formData.append('new', true);
    main.ajaxRequest('/api/admin/tree/setings/json', formData)
    .done(function(json) {
        var html = tpl_nodeParams(json.data);
        $('.menu_structure').html(html);
    });
});
$('body').on('click', '.js-page_info', function(e) {
    var formData = new FormData();
    formData.append('id', NODE);
    main.ajaxRequest('/api/admin/tree/setings/json', formData)
    .done(function(json) {
        var html = tpl_nodeParams(json.data);
        $('.menu_structure').html(html);
    });
});

$('body').on('click', '.js-modul__open', function(e) {
    refresh_modul($(this).data('id'));
});

function refresh_modul(modules) {
    $('.js__block-loader').show();
    var formData = new FormData();
    formData.append('node', NODE);
    formData.append('modules', modules);
    main.ajaxRequest('/api/admin/modul/modulPage/json', formData)
        .done(function(json) {
            html = tpl_modulPage(json.data);
            $('.js__block-loader').fadeOut(400);
            $('.js-modulPage__load').html(html);
            if ($('div').hasClass("js-gallery")) {
                $(".js-gallery").lightGallery(); // запускаем галлереи
            }
            if (modules != undefined) {
                location.reload();
            }
        });
}



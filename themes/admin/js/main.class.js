function mainClass() {
    $this     = this;
    this.host = location.hostname;
    this.url  = location.href;
    this.ajax = {};
    $(function() {
        $this.init();
    });
}

    /**
     * Функция вызываемая после создания объекта
     * @return {null}
     */
    mainClass.prototype.init = function() {
        $('body').on('click', '.js-recaptcha', function(e) {
            mainClass.prototype.recaptcha();
        });
    };

    /**
     * Функция для обновлние капчи
     * @return {null}
     */
    mainClass.prototype.recaptcha = function() {
        var rand = Math.floor(Math.random() * 2147483647);
        var url = window.location.origin;
        $(".js-captcha__input").val('');
        $(".js-captcha").attr('src', url + '/api/captcha/?r=' + rand);
    };

    /**
     * Отправить Ajax запрос
     * @param  {string} dataURL — url адрес для выполнения ajax запроса
     * @param  {object} data
     * @param  {object} options - параметры запроса
     * @return {null}
     */
    mainClass.prototype.ajaxRequest = function(dataURL, data, options) {
        $('.js__block-loader').show();
        if (data == undefined) {
            data = new FormData();
        }
        data.append('node', NODE);
        data.append('mod', MOD);
        // опции по умолчанию
        var settings = $.extend({
            data: data || {},
            type: 'POST',
            url: dataURL,
            // для работы с FormData
            contentType: !(data instanceof FormData) ? 'application/x-www-form-urlencoded; charset=UTF-8' : false, // set content type header
            processData: !(data instanceof FormData), // to send a DOMDocument or other non-processed data, set this option to false
        }, options);

        var request = $.ajax(settings);
        $('.js__block-loader').fadeOut(400);
        return request;
    };

    /**
     * Прочитать данные со всех полей формы
     * @param  {string} targetName
     * @return {Object} Набор пар, представляющий поля формы и их значения
     *
     * @todo: изменить входной параметр targetName со строки на что-нибудь другое (id, class, object)
     */
    mainClass.prototype.fetchformData = function(targetName) {
        var data = new FormData();
        var fields = $(targetName).serializeArray();
        $.each(fields, function(index, field) {
            data.append(field.name, field.value);
        });
        if ($(".js-file__input").length) {
            $.each($('.js-file__input')[0].files, function( index, value ) {
                data.append('file_'+index, value);
            });
        }
        if ($(".js-file__logo").length) {
            $.each($('.js-file__logo')[0].files, function( index, value ) {
                data.append('file_'+index, value);
            });
        }
        return data;
    };

var main = new mainClass();

// Костыль для IE. Необходим для поддержки наследования
// Можно заменить на библиотеку es5-shim.js
$(function() {
    if (!Object.create) {
        Object.create = function(proto) {
            function F() {}
            F.prototype = proto;
            return new F();
        };
    }
});

$.fn.getData = function (dataName) {
    return $(this).data(dataName || 'id');
};


/* ===================================================== */

// Пример наследования класса
function testClass() {
    this.init();
}
    // Наследование от класса mainClass
    testClass.prototype = Object.create(mainClass.prototype);
    testClass.prototype.constructor = testClass;

    // Инициализация
    testClass.prototype.init = function () {
    };

var test = new testClass();

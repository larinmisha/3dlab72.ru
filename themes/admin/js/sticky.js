$(function() {
    $(".js-header").sticky({ topSpacing: 0, zIndex: 100 });
    $('.js-header').on('sticky-start', function() {});
    $('.js-header').on('sticky-end', function() {});
    $('.js-header').on('sticky-update', function() { /*console.log("Update");*/ });
    $('.js-header').on('sticky-bottom-reached', function() { /*console.log("Bottom reached");*/ });
    $('.js-header').on('sticky-bottom-unreached', function() { /*console.log("Bottom unreached");*/ });
});

<?php base::test(); ?>
<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo $this->meta_title(1); ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"> <!-- , user-scalable=no -->
        <meta name='robots' content='noindex,nofollow'>

        <link rel="apple-touch-icon" sizes="57x57" href="/themes/default/img/favicon/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="/themes/default/img/favicon/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="/themes/default/img/favicon/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="/themes/default/img/favicon/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="/themes/default/img/favicon/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="/themes/default/img/favicon/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="/themes/default/img/favicon/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="/themes/default/img/favicon/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="/themes/default/img/favicon/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="/themes/default/img/favicon/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/themes/default/img/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="/themes/default/img/favicon/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/themes/default/img/favicon/favicon-16x16.png">
        <!-- <link rel="manifest" href="manifest.json"> -->
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">

        <?php base::minify_import("node_modules/spin.js/spin.min.js"); ?>
        <?php base::minify_import("node_modules/handlebars/dist/handlebars.min.js"); ?>

        <?php base::minify_import("node_modules/lightgallery/dist/css/lightgallery.min.css"); ?>
        <?php base::minify_import("node_modules/lightgallery/dist/js/lightgallery.min.js"); ?>
        <?php base::minify_import("node_modules/lg-thumbnail/dist/lg-thumbnail.min.js"); ?>

        <?php
            // календарик https://github.com/t1m0n/air-datepicker
            // http://t1m0n.name/air-datepicker/docs/index-ru.html
            base::minify_import("node_modules/air-datepicker/dist/css/datepicker.min.css");
            base::minify_import("node_modules/air-datepicker/dist/js/datepicker.js");
        ?>


        <link href="/core/minify.php?css=<?php echo base::minify_encode($this->meta['minifyCss']); ?>&type=css&template=admin" rel="stylesheet" type="text/css">

        <script src="/node_modules/jquery/dist/jquery.min.js"></script>
        <script src="/node_modules/tinymce/tinymce.min.js"></script>

        <script src="/themes/admin/js/bootstrap_modal.js"></script>
        <script src="/themes/admin/js/bootstrap_dropdown.js"></script>

        <script src="//api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
    </head>
    <body>
        <?php include __DIR__."/../admin/tpl/sticky.php"; ?>
        <main>
            <div class="container-fluid">
                <div class="menu_grid">
                    <div>
                        <div class="js__tree treeview"></div>
                        <div class="hidden js-tree__item-tpl-add">
                            <div class='js-new__li-top move__cursor-top'>
                                <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
                            </div>
                            <div class='js-new__li-bottom move__cursor-bottom'>
                                <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
                            </div>
                            <div class='js-new__li-center move__cursor-center'>
                                <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="block-modul js-modulPage__load mb0_5 pb0_5"></div>
                        <div class="page-title menu_structure">
                            <?php
                                if (array_key_exists('doc', $_GET)) {
                                    include "tpl/doc.php";
                                } else {
                                    if ($this->is_access('', 'mod_edit')) {
                                        $result_=$this->pdo->prepare("  SELECT  ms.id, ms.fk_structure, m.dirrect, m.type_script, m.title
                                                                        FROM    bk_modules as ms,
                                                                                bk_modul as m
                                                                        WHERE   1=1
                                                                                and ms.fk_modul=m.id
                                                                                and ms.id= :idm
                                                                                and ms.fk_structure= :ids
                                                                                and ms.del=0
                                                                        ORDER BY sort ASC");
                                        $result_->execute([ ':idm' => $_SESSION['last_open_modul'],
                                                            ':ids' => $this->open_page
                                                            ]);
                                        if (($result_->rowCount() == 0)&&(count($_POST)==0)) {
                                            $row_ = $this->pdo->prepare("SELECT  ms.id, m.dirrect, m.type_script
                                                                        FROM    bk_modules as ms,
                                                                                bk_modul as m
                                                                        WHERE   ms.fk_modul=m.id and
                                                                                ms.fk_structure= :node and
                                                                                ms.del=0
                                                                        ORDER BY sort ASC");
                                            $row_->execute([':node' => $this->open_page]);
                                            if ($row_->rowCount() > 0) {
                                                while ($row = $row_->fetch()) {
                                                    if(is_file("modules/".(($row['type_script']==2)?"/admin_cms":"").$row['dirrect']."/admin.php")){
                                                        $_SESSION['last_open_modul'] = $row['id'];
                                                        break;
                                                    }
                                                }
                                                $result_->execute([ ':idm' => $_SESSION['last_open_modul'],
                                                                    ':ids' => $this->open_page
                                                                    ]);
                                            }
                                        }
                                        if ($result_->rowCount()>0) {

                                            $modules=$result_->fetch();

                                            if ($modules['type_script'] == 2) { // Модули адинки
                                                $modules['dir'] = "modules/admin_cms/".$modules['dirrect']."/";
                                            } else {
                                                $modules['dir'] = "modules/".$modules['dirrect']."/";
                                            }

                                            $include = $modules['dir']."admin.php";
                                            $this->modules = $modules;
                                            if(is_file($include)){
                                                include $include;
                                            }
                                        }
                                    }
                                }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </main>

        <footer>
            <div class="container">
                <div class="block">
                    <a href="mailto:larinMisha@ya.ru">info@tesseract24.ru</a>
                    <div class=' block-loader js__block-loader'>
                        <div id="loader" class="loader"></div>
                    </div>
                </div>
            </div>
        </footer>
        <div class="message_list js-message__list"><!--для уведомлений--></div>

        <?php include "themes/admin/default_html.php"; ?>
        <?php include "themes/admin/tpl/nodeParam.php"; ?>
        <?php include "themes/admin/tpl/modulParam.php"; ?>
        <?php include "themes/admin/tpl/modulPage.php"; ?>
        <?php include "themes/admin/tpl/tree.php"; ?>

        <?php base::minify_import('modules/admin_cms/user/js/auth.js'); ?>
        <?php include "modules/admin_cms/user/tpl/login.php"; ?>
        <?php include "modules/admin_cms/user/tpl/registration.php"; ?>
        <?php include "modules/admin_cms/user/tpl/profile.php"; ?>
        <?php include "modules/admin_cms/user/tpl/message.php"; ?>
        <div class="js-modal__block"><!--для модальных окон--></div>

        <script>
            var open_page="<?php echo $this->open_page; // deprecated, use NODE constant ?>";
            var NODE = "<?php echo $this->open_page; ?>";
            var MOD = "<?php echo $_SESSION['last_open_modul']; ?>";
        </script>

        <?php base::minify_import($this->tpl['path']['admin'] . 'js/main.class.js'); ?>
        <?php base::minify_import($this->tpl['path']['admin'] . 'js/modal.js'); ?>
        <?php base::minify_import($this->tpl['path']['admin'] . 'js/tree.js'); ?>

        <script src="/core/minify.php?js=<?php echo base::minify_encode($this->meta['minifyJs']); ?>&type=js&template=admin"></script>

    </body>
</html>

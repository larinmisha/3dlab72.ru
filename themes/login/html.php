<?php base::test(); ?>
<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo $this->meta_title(); ?></title>
        <meta name="keywords" content="<?php echo $this->meta['keywords']; ?>">
        <meta name="description" content="<?php echo $this->meta['description']; ?>">
        <meta name='robots' content='<?php echo $this->meta['robots']; ?>'>

        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"> <!-- , user-scalable=no -->
        <meta name="yandex-verification" content="ea2440ad95dd6fa6" />
        <meta name="cmsmagazine" content="a28bd4e374b530aae2995a7887068d1d" />
        <meta name="tesseract" content="https://tesseract24.ru/"/>

        <link rel="apple-touch-icon" sizes="57x57" href="/themes/default/img/favicon/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="/themes/default/img/favicon/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="/themes/default/img/favicon/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="/themes/default/img/favicon/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="/themes/default/img/favicon/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="/themes/default/img/favicon/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="/themes/default/img/favicon/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="/themes/default/img/favicon/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="/themes/default/img/favicon/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="/themes/default/img/favicon/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/themes/default/img/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="/themes/default/img/favicon/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/themes/default/img/favicon/favicon-16x16.png">
        <!-- <link rel="manifest" href="manifest.json"> -->
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">

        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500&display=swap" rel="stylesheet">

        <?php base::minify_import("node_modules/handlebars/dist/handlebars.min.js"); ?>

        <link href="/core/minify.php?css=<?php echo base::minify_encode($this->meta['minifyCss']); ?>&mod=<?php echo @implode($this->meta['modul'], ','); ?>&type=css&template=<?php echo $this->structure_page('','theme'); ?>" rel="stylesheet" type="text/css">

        <noscript><meta http-equiv="refresh" content="0; URL=/badbrowser/"></noscript>

        <script src="/node_modules/jquery/dist/jquery.min.js"></script>

        <link rel="stylesheet" href="/node_modules/swiper/css/swiper.min.css">
        <script src="/node_modules/swiper/js/swiper.min.js"></script>
        <script src="/themes/admin/js/bootstrap_modal.js"></script>

        <?php if (($this->admin)&&($_SESSION['user']['id'] == '')) { // авторизация для админки ?>
            <input type="hidden" class="js-open_form_login" value="1">
        <?php } ?>
    </head>
    <body>
        <?php if (!is_numeric($_SESSION['user']['id'])) { ?>
            <input type="hidden" class="js-open_form_login" value="1">
        <?php } else { ?>
            <div class="mt3">
                <center>
                    <h1>
                        Доступ в раздел закрыт
                        <br>
                        <small>Администратор сайта еще не выдал вам доступ на данный раздел</small>
                        <br>
                        <small>Перейти на главную <a href="/">страницу</a> сайта</small>
                    </h1>
                </center>
            </div>
        <?php } ?>

        <script>
            var URL  = "<?php echo $this->structure_page('','chpu'); ?>";
            var NODE = "<?php echo $this->open_page; ?>";
            var MOD = "";
            <?php if (is_numeric($_GET['userhash'])&&($_GET['userhash']!=$_SESSION['userhash'])) { ?>
                <?php $_SESSION['userhash'] = $_GET['userhash']; ?>
                $(document).ready(function () {
                    auth.hash('<?php echo $_GET['userhash']; ?>');
                });
            <?php } ?>
            <?php if (base::is_email($_GET['subscribe_visit'])) { ?>
                $(document).ready(function () {
                    auth.subscribe_visit('<?php echo $_GET['subscribe_visit']; ?>');
                });
            <?php } ?>
        </script>

        <?php base::minify_import('modules/admin_cms/user/js/auth.js'); ?>
        <?php include "modules/admin_cms/user/tpl/login.php"; ?>
        <?php include "modules/admin_cms/user/tpl/registration.php"; ?>
        <?php include "modules/admin_cms/user/tpl/profile.php"; ?>
        <?php include "modules/admin_cms/user/tpl/message.php"; ?>
        <div class="js-modal__block"><!--для модальных окон--></div>

        <script src="/core/minify.php?js=<?php echo base::minify_encode($this->meta['minifyJs']); ?>&mod=<?php echo @implode($this->meta['modul'], ','); ?>&type=js&template=<?php echo $this->structure_page('','theme'); ?>"></script>

    </body>
</html>

<?php base::test(); ?>
<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo $this->meta_title(); ?></title>
        <meta name="keywords" content="<?php echo $this->meta['keywords']; ?>">
        <meta name="description" content="<?php echo $this->meta['description']; ?>">
        <meta name='robots' content='<?php echo $this->meta['robots']; ?>'>

        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"> <!-- , user-scalable=no -->
        <meta name="yandex-verification" content="ea2440ad95dd6fa6" />
        <meta name="cmsmagazine" content="a28bd4e374b530aae2995a7887068d1d" />
        <meta name="tesseract" content="https://tesseract24.ru/"/>

        <meta property="og:type" content="website">
        <meta property="og:site_name" content="<?php echo $this->structure_page('1','title'); ?>" />
        <meta property="og:title" content="<?php echo $this->structure_page('1','title'); ?>" />
        <meta property="og:image" content="<?php echo conf::$http; ?>themes/default/img/logo.jpg" />
        <meta property="og:url" content="<?php echo $this->url(); ?>" />
        <meta property="og:description" content="<?php echo $this->meta_title(); ?>" />
        <meta name="twitter:title" content="<?php echo $this->structure_page('1','title'); ?>">
        <meta name="twitter:image:src" content="<?php echo conf::$http; ?>themes/default/img/og-image.jpg">

        <link rel="apple-touch-icon" sizes="57x57" href="/themes/default/img/favicon/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="/themes/default/img/favicon/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="/themes/default/img/favicon/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="/themes/default/img/favicon/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="/themes/default/img/favicon/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="/themes/default/img/favicon/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="/themes/default/img/favicon/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="/themes/default/img/favicon/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="/themes/default/img/favicon/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="/themes/default/img/favicon/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/themes/default/img/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="/themes/default/img/favicon/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/themes/default/img/favicon/favicon-16x16.png">
        <!-- <link rel="manifest" href="manifest.json"> -->
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">

        <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700&display=swap&subset=cyrillic" rel="stylesheet">

        <?php base::minify_import("node_modules/lightgallery/dist/css/lightgallery.min.css"); ?>
        <?php base::minify_import("node_modules/lightgallery/dist/js/lightgallery.min.js"); ?>
        <?php base::minify_import("node_modules/lg-thumbnail/dist/lg-thumbnail.min.js"); ?>

        <?php base::minify_import("node_modules/handlebars/dist/handlebars.min.js"); ?>

        <link href="/core/minify.php?css=<?php echo base::minify_encode($this->meta['minifyCss']); ?>&mod=<?php echo @implode($this->meta['modul'], ','); ?>&type=css&template=<?php echo $this->structure_page('','theme'); ?>" rel="stylesheet" type="text/css">

        <noscript><meta http-equiv="refresh" content="0; URL=/badbrowser/"></noscript>

        <script src="/node_modules/jquery/dist/jquery.min.js"></script>

        <link rel="stylesheet" href="/node_modules/swiper/css/swiper.min.css">
        <script src="/node_modules/swiper/js/swiper.min.js"></script>
        <script src="/themes/admin/js/bootstrap_modal.js"></script>

        <?php if (($this->admin)&&($_SESSION['user']['id'] == '')) { // авторизация для админки ?>
            <input type="hidden" class="js-open_form_login" value="1">
        <?php } ?>
    </head>
    <?php
        include_once dirname(__FILE__)."/../../modules/admin_cms/tree_node/collections/defult.php";
        $menu        = $defultNode->menu(['nones' => '2, 9, 14, 21, 24, 25']);
        $menu_footer = $defultNode->menu(['nones' => '2, 9, 14, 21']);
    ?>
    <body>
        <?php include __DIR__."/../admin/tpl/sticky.php"; ?>
        <script>
            $(function() {
                $('.js-nav__burger').click(function (e) {
                    $('.js-nav__burger, .js-nav__menu').toggleClass('active');
                    if ($('.js-nav__burger').hasClass('active')) {
                        $('body').css('overflow', 'hidden');
                    } else {
                        $('body').css('overflow', '');
                    }
                })
                $('.js-nav__onen').click(function (e) {
                    if ($(this).hasClass('active')) {
                        $('.js-nav__onen').removeClass('active');
                        $('.js-nav__onen').closest("li").find('.lavel').removeClass('active');
                    } else {
                        $('.js-nav__onen').removeClass('active');
                        $(this).addClass('active');
                        $('.js-nav__onen').closest("li").find('.lavel').removeClass('active');
                        $(this).closest("li").find('.lavel').addClass('active');
                    }
                })
            });
        </script>
        <header class="mt1_5 mb0_25">
            <div class="container">
                <nav class="nav hidden-xs">
                    <ul>
                        <li>
                            <a class="<?php if ($this->open_page == 1) echo "open"; ?>" href="<?php echo $this->structure_page(1,'chpu'); ?>">
                                Главная
                            </a>
                        </li>
                        <?php if (count($menu) > 0) { ?>
                            <?php foreach ($menu as $row) { ?>
                                <li>
                                    <a class="<?php if ($row['active']) echo "open"; ?>" href="<?php echo $row['url']; ?>">
                                        <?php echo $row['title']; ?>
                                    </a>
                                    <?php if (count($row['node']) > 0) { ?>
                                        <div class="lavel mt0_75">
                                            <?php foreach ($row['node'] as $node) { ?>
                                                <div class="mb0_5">
                                                    <a href="<?php echo $node['url']; ?>" class="mb0_5 <?php if ($node['active']) echo "open"; ?>">
                                                        <?php echo $node['title']; ?>
                                                    </a>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    <?php } ?>
                                </li>
                            <?php } ?>
                        <?php } ?>
                    </ul>
                    <div class="bottom hidden">
                        <?php if(isset($_SESSION['user']) && !empty($_SESSION['user'])){ ?>
                            <li class="js-user__logout">
                                <a href="JavaScript:">Выход</a>
                            </li>
                        <?php } else { ?>
                            <li>
                                <a href="/?esia">
                                    &nbsp;Вход
                                </a>
                            </li>
                        <?php } ?>
                    </div>
                </nav>


                <div class="nav-mobile">
                    <div class="logo">
                        <a href="/"  class="logo">
                            <?php // echo file_get_contents(__DIR__."/img/logo.svg"); ?>
                        </a>
                        <h1>
                            <a href="/">
                                Фирма
                            </a>
                        </h1>
                    </div>
                    <nav class="menu js-nav__menu">
                        <div class="top">
                            <div class="name">Меню</div>
                        </div>
                        <div class="overflow">
                            <ul>
                                <?php if ($_SESSION['user']['id'] > 0) { ?>
                                    <li>
                                        <a class="<?php if ($row['active']) echo "open"; ?>" href="<?php echo $this->structure_page(84,'chpu'); ?>">
                                            <?php echo $this->structure_page(84,'title'); ?>
                                        </a>
                                    </li>
                                <?php } ?>
                                <?php if (count($menu) > 0) { ?>
                                    <?php foreach ($menu as $row) { ?>
                                        <li>
                                            <div class="item">
                                                <a class="<?php if ($row['active']) echo "open"; ?>" href="<?php echo $row['url']; ?>">
                                                    <?php echo $row['title']; ?>
                                                </a>
                                                <?php if (count($row['node']) > 0) { ?>
                                                    <div class="more js-nav__onen <?php if ($row['active']) echo "active"; ?>">
                                                        <svg width="14" height="8" viewBox="0 0 14 8" fill="#757575" xmlns="http://www.w3.org/2000/svg">
                                                            <path fill-rule="evenodd" clip-rule="evenodd" d="M0.292893 0.292893C0.683417 -0.0976311 1.31658 -0.0976311 1.70711 0.292893L7 5.58579L12.2929 0.292893C12.6834 -0.0976311 13.3166 -0.0976311 13.7071 0.292893C14.0976 0.683417 14.0976 1.31658 13.7071 1.70711L7.70711 7.70711C7.31658 8.09763 6.68342 8.09763 6.29289 7.70711L0.292893 1.70711C-0.0976311 1.31658 -0.0976311 0.683417 0.292893 0.292893Z"/>
                                                        </svg>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                            <?php if (count($row['node']) > 0) { ?>
                                                <div class="lavel <?php if ($row['active']) echo "active"; ?>">
                                                    <?php foreach ($row['node'] as $node) { ?>
                                                        <a href="<?php echo $node['url']; ?>" class="<?php if ($node['active']) echo "open"; ?>">
                                                            <?php echo $node['title']; ?>
                                                        </a>
                                                    <?php } ?>
                                                </div>
                                            <?php } ?>
                                        </li>
                                    <?php } ?>
                                <?php } ?>
                            </ul>
                            <div class="info">
                                <div class="mb1 title">Контакты</div>
                                <div class="mb1">
                                    Россия, 625036, г. Тюмень,<br>
                                    ул. Первомайская, 20, каб. 224
                                </div>
                                <div class="mb1">
                                    тел.: <a href="tel:83452464429">(3452) 46-44-29</a>, <a href="tel:83452297915">29-79-15</a><br>
                                    факс: <a href="tel:83452297915">(3452) 29-79-15</a>
                                </div>
                                <div>
                                    Электронная почта: <a href="mailto:izbirkom@tyumen-city.ru">izbirkom@tyumen-city.ru</a>
                                </div>
                            </div>
                        </div>
                    </nav>
                    <div class="burger js-nav__burger"><div class="block"><span></span></div></div>
                </div>
            </div>
            <?php if ($this->open_page > 1) { ?>
                <div class="container hidden">
                    <?php $breadcrumb = $this->breadcrumb($this->open_page); ?>
                    <?php if (count($breadcrumb) > 0) { ?>
                        <ol class="breadcrumb mb0_75 pl0 hidden-xs">
                            <?php foreach ($breadcrumb as $b) { ?>
                                <?php if ($b['chpu'] == '/') {$b['title'] = 'Главная'; } ?>
                                <li><a href="<?php echo $b['chpu']; ?>"><?php echo $b['title']; ?></a></li>
                            <?php } ?>
                            <?php if ($this->meta['title'] != '') { ?>
                                <li><?php echo $this->meta['title']; ?></li>
                            <?php } ?>
                        </ol>
                    <?php } ?>
                </div>
            <?php } ?>
            <?php if (count($_SESSION['user']['message']) > 0) { ?>
                <div class="container">
                    <?php foreach ($_SESSION['user']['message'] as $message) { ?>
                        <div class="alert alert-<?php echo $message['type']; ?> alert-dismissible fade in mb2" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <strong><?php echo $message['title']; ?></strong> <?php echo $message['message']; ?>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>
        </header>

        <main class="pb1_75 pt1_75">
            <?php if ($this->open_page > 1) { ?>
                <div class="container">
                    <?php if ($this->meta['title'] != '') { ?>
                        <div class='tpl__breadcrumb_new pb1'>
                            <div class="grid">
                                <div class="left">
                                    <?php if ($this->meta['title'] != '') { ?>
                                        <?php $url = $this->structure_page('', 'chpu'); ?>
                                    <?php } else { ?>
                                        <?php $url = $this->structure_page($this->structure_page('', 'fk_structure'), 'chpu'); ?>
                                    <?php }  ?>
                                    <a href="<?php echo $url; ?>">
                                        <svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M2.29289 12.2929C1.90237 12.6834 1.90237 13.3166 2.29289 13.7071L8.65685 20.0711C9.04738 20.4616 9.68054 20.4616 10.0711 20.0711C10.4616 19.6805 10.4616 19.0474 10.0711 18.6569L4.41421 13L10.0711 7.34314C10.4616 6.95262 10.4616 6.31946 10.0711 5.92893C9.68054 5.53841 9.04738 5.53841 8.65686 5.92893L2.29289 12.2929ZM23 12L3 12L3 14L23 14L23 12Z" fill="#757575"/>
                                        </svg>
                                    </a>
                                </div>
                                <div class="right">
                                    <?php echo $this->meta['title']; ?>
                                </div>
                            </div>
                        </div>
                    <?php } else { ?>
                        <h2 class="pb1_75"><?php echo $this->structure_page('', 'title'); ?></h2>
                    <?php } ?>
                </div>
            <?php } ?>
            <?php echo $this->content; ?>
            <?php include "modules/admin_cms/tree_node/main.php"; // блок с картой сайта + список категорий ?>
        </main>

        <footer class="pt2 pt1_75 pb1_75">
            <div class="container">
                <div class="row mb1">
                    <div>

                    </div>
                    <?php if (count($menu_footer) > 0) { ?>
                        <?php foreach ($menu_footer as $row) { ?>
                            <div class="menu visible-lg">
                                <a href="<?php echo $row['url']; ?>" class="mb0_5">
                                    <?php echo $row['title']; ?>
                                </a>
                                <div class="node">
                                    <?php foreach ($row['node'] as $node) { ?>
                                        <a href="<?php echo $node['url']; ?>" class="mb0_5">
                                            <?php echo $node['title']; ?>
                                        </a>
                                    <?php } ?>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>
                <div class="box">
                    <div class="info">
                        Copyright © 2020 «___»
                    </div>
                    <div class="tesseract">
                        <a href="https://tesseract24.ru/" target="_blank">
                            <?php echo file_get_contents(__DIR__."/img/tesseract.svg"); ?>
                        </a>
                    </div>
                </div>
            </div>
        </footer>
        <?php include_once($this->tpl['path']['admin'] . "default_html.php"); ?>

        <?php echo conf::$informer['yandex']; ?>

        <script>
            var URL  = "<?php echo $this->structure_page('','chpu'); ?>";
            var NODE = "<?php echo $this->open_page; ?>";
            var MOD = "";
            <?php if (is_numeric($_GET['userhash'])&&($_GET['userhash']!=$_SESSION['userhash'])) { ?>
                <?php $_SESSION['userhash'] = $_GET['userhash']; ?>
                $(document).ready(function () {
                    auth.hash('<?php echo $_GET['userhash']; ?>');
                });
            <?php } ?>
            <?php if (base::is_email($_GET['subscribe_visit'])) { ?>
                $(document).ready(function () {
                    auth.subscribe_visit('<?php echo $_GET['subscribe_visit']; ?>');
                });
            <?php } ?>
        </script>

        <?php base::minify_import($this->tpl['path']['admin'] . 'js/main.class.js'); ?>
        <?php base::minify_import($this->tpl['path']['admin'] . 'js/modal.js'); ?>

        <?php base::minify_import('modules/admin_cms/user/js/auth.js'); ?>
        <?php include "modules/admin_cms/user/tpl/login.php"; ?>
        <?php include "modules/admin_cms/user/tpl/registration.php"; ?>
        <?php include "modules/admin_cms/user/tpl/profile.php"; ?>
        <?php include "modules/admin_cms/user/tpl/message.php"; ?>
        <div class="js-modal__block"><!--для модальных окон--></div>

        <script src="/core/minify.php?js=<?php echo base::minify_encode($this->meta['minifyJs']); ?>&mod=<?php echo @implode($this->meta['modul'], ','); ?>&type=js&template=<?php echo $this->structure_page('','theme'); ?>"></script>

    </body>
</html>

<?php
    // echo "<pre>";print_r(dirname(__FILE__).'/../.env');echo "</pre>";
    // $envs = parse_ini_file(dirname(__FILE__).'/../.env');
    // echo "<pre>";print_r($envs);echo "</pre>";
    // foreach ($envs as $key => $value) {
    //     $_ENV[$key] = $value;
    // }

    // set_include_path(__DIR__);

    include_once dirname(__FILE__).'/vendor/autoload.php'; // composer autoload

    include_once dirname(__FILE__)."/core/initialization.php";
    include_once dirname(__FILE__).'/core/classes/sendMail.php';

    include_once dirname(__FILE__)."/core/classes/designer_form.php"; // Класc - фабрика элементов форм для админки
    include_once dirname(__FILE__)."/core/classes/api.php";  // Класс для работы api
    include_once dirname(__FILE__)."/core/cms.php";

    $cms = new designer_office();
    $cms->designer($_SERVER['REQUEST_URI']);
?>

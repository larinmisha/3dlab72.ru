<?php
    // изменить права по умолчанию
    // для создаваемых файлов и папок на 775
    umask(0);

    include '../vendor/autoload.php'; // composer autoload

    include "initialization.php"; // тут основные классы, в том числе PDO подключенное к БД

    if (!in_array($_GET['template'],['admin','default'])) {
        $row_ = $pdo->prepare("SELECT * FROM bk_structure where theme = :theme");
        $row_->execute([':theme'=>$_GET['template']]);
        if ($row_->rowCount()==0) {
            die();
        }
    } else if (($_GET['template'] == 'admin')&&($_GET['type']=='js')) {
        $row_ = $pdo->prepare(" SELECT  u.id
                                FROM    bk_users as u,
                                        bk_access_group_user as g
                                WHERE   1=1
                                        and u.id = g.fk_users
                                        and u.id = :id");
        $row_->execute([':id'=>$_SESSION['user']['id']]);
        if (($row_->rowCount()==0)||($_SESSION['user']['id'] == '')) {
            $_GET['template'] = 'default';
        }
    }

    // создать папку для Less-компилятора
    $cache = "../cache/";
    if (!file_exists($cache)) {
        mkdir($cache, 0775, true);
    }
    // exec("rm -rf {$cache}"); // очистить кеш

    // компилятор Less файлов
    $options = array(
        'cache_method' => 'php',
        'compress' => true,
        'cache_dir' => $cache,
        'import_dirs' => [
            'themes/default/less/' => '',
            'themes/admin/less/' => '',
        ],
    );

    $parser = new Less_Parser($options);

    $dir['template'] = "../themes/".$_GET['template']."/";
    if ($_GET['type']=='css') {

        $dir['css'] = $cache."css/themes-".$_GET['template']."/";
        if (!file_exists($dir['css'])) {
            mkdir($dir['css'], 0775, true);
        }

        if (conf::$lessphp && is_file($dir['template']."less/style.less")) {
            Less_Cache::Get([
                $dir['template']."less/style.less" => ""
            ], [
                'output' => $dir['css']."style.css"
            ]);
        }

        $css['themes'] = $dir['css']."style.css";
    } else if ($_GET['type']=='js') {
        $js['themes'] = $dir['template']."js/js.js";
    }

    if ($_GET['template']=='admin') {
        $file = ['css'=>'admin.css', 'less'=>'admin.less'];
        $row_ = $pdo->query("SELECT * FROM bk_modul");

    } else {
        $file = ['css'=>'style.css', 'less'=>'style.less', 'js'=>'js.js'];
        if ($_GET['mod']!='') {
            $row_ = $pdo->query("SELECT * FROM bk_modul WHERE  ( id = ".implode(" or id = ", explode(',',$_GET['mod'])).")");
        }
    }
    if ($row_) {
        while($row = $row_->fetch()) {
            $dir['module'] = "../modules".(($row['type_script']==2)?"/admin_cms":"")."/".$row['dirrect']."/";
            if ($_GET['type']=='css') {

                $dir['css'] = $cache."css/modul-".$row['dirrect']."/";
                if (!file_exists($dir['css'])) {
                    mkdir($dir['css'], 0775, true);
                }
                if (conf::$lessphp && is_file($dir['module']."less/".$file['less'])) {
                    Less_Cache::Get([
                        $dir['module']."less/".$file['less'] => ""
                    ], [
                        'output' => $dir['css'].$file['css']
                    ]);
                }
                $css[$row['dirrect']] = $dir['css'].$file['css'];
            } else if ($_GET['type']=='js') {
                if ($_GET['template'] != 'admin') {
                    $js[$row['dirrect']] = $dir['module']."js/".$file['js'];
                }
            }
        }
    }

    // минификация css
    if ($_GET['type'] == 'css') {
        foreach ($css as $mod => $url) {
            if (is_file($url)) { // Потом переписать через админку
                $css[$mod] = preg_replace("/^\.{2}/",'',$url);
            } else {
                unset($css[$mod]);
                // $error[] = "Missing path: ".$url;
            }
        }

        if (isset($_GET['css']) && !empty($_GET['css'])) {
            $get_url = explode(",", base::minify_decode($_GET['css']));
            foreach ($get_url as $mod => $url) {
                if (is_file("../".$url)) { // Потом переписать через админку
                    $css[$url] = $url;
                } else {
                    $error[] = "Missing path: ".$url;
                }
            }
        }
        $f = implode($css, ',');
    } else if ($_GET['type'] == 'js') { // минификация js
        foreach ($js as $mod => $url) {
            if (is_file($url)) { // Потом переписать через админку
                $js[$mod] = preg_replace("/^\.{2}/",'',$url);
            } else {
                unset($js[$mod]);
                // $error[] = "Missing path: ".$url;
            }
        }

        if (isset($_GET['js']) && !empty($_GET['js'])) {
            $get_url = explode(",", base::minify_decode($_GET['js']));
            foreach ($get_url as $mod => $url) {
                if (is_file("../".$url)) { // Потом переписать через админку
                    $js[$url] = $url;
                } else {
                    $error[] = "Missing path: ".$url;
                }
            }
        }
        $f = implode($js, ',');
    }
    if (count($error)==0) {
        if ($_GET['test'] == '') {
            unset($_GET);
            $_GET['f'] = $f;
            // только конкатенация
            $min_serveOptions['concatOnly'] = !conf::$minify;

            $min_serveController = new Minify_Controller_MinApp();
            Minify::serve($min_serveController, $min_serveOptions);
        } else {
            echo "<pre>";print_r($f);echo "</pre>";
        }
    } else {
        echo "Error:<pre>";print_r($error);echo "</pre>";
    }
?>

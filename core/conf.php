<?php

    // $_SERVER['DOCUMENT_ROOT'] = "/var/www/u0530832/data/www/marka72.tesseract24.ru/";

    if (array_key_exists('SERVER_NAME', $_SERVER)) {
        session_name(md5($_SERVER['SERVER_NAME']));
        session_start();
    }

    error_reporting(0);
    ini_set('display_errors', 0);

    date_default_timezone_set('Asia/Yekaterinburg');
    include_once(dirname(__FILE__) . '/../vendor/autoload.php'); // composer autoload

    class conf { // Клас - фундамент, хранит все необходимые константные функции, словари.
        static $sender_mail;
        /* Ядро */
            static $url_dev = 'apiDev'; // 'apiDev', если пусть то тесты отключены

            static $auth = [
                            // в настройках vk надо указать адресс сайта ***/?auth=vk
                            'vk'       => ['id' => '', 'secret' => ''],
                            'facebook' => ['id' => '', 'secret' => '']
                            ];

            static $yaTurboKey = 'AgAAAAACMpjfAAUEegwbiAiJfEwNj9z5XFdP2L0'; // Турбо страницы

            static $vip_structur = 536; // id vip раздела

            static $auto_login = false; // true - без авторизации не пускать в систему

            static $log_limit = 0; // количество дней для хранения логов

            static $get_admin = ''; // get параметр для доступа в админку. если пусто то без него

            static $lessphp = true; // включить встроенный less компилятор

            static $minify = true;  // включить минификацию js и css файлов

            /* Настройки БД */
            static $db = [  'server'    => 'db',
                            'name'      => 'tesseract.ru',
                            'user'      => 'root',
                            'pass'      => 'root'];
            /* счётчики сайта */
            static $informer = ["yandex" => ''];

            /* Переменые которые формируются сами, но используются везде как конфигурационные константы */
                static $http;
                static $ip;             // ip пользователя
                static $badbrowser = 0; // старый браузер
            /* Переменые которые формируются сами, но используются везде как конфигурационные константы */
        /* Ядро */

        static function ip() { // функция вывода массивов
            if (self::$ip=='') {
                self::$ip=$_SERVER['REMOTE_ADDR'];
            }
            return self::$ip;
        }

        public function __construct() {  // По умолчанию, инициализация

            $protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,5))=='https'? 'https' : 'http';
            if($_SERVER["SERVER_PORT"] == 443)
                $protocol = 'https';
            elseif (isset($_SERVER['HTTPS']) && (($_SERVER['HTTPS'] == 'on') || ($_SERVER['HTTPS'] == '1')))
                $protocol = 'https';
            elseif (!empty($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https' || !empty($_SERVER['HTTP_X_FORWARDED_SSL']) && $_SERVER['HTTP_X_FORWARDED_SSL'] == 'on')
                $protocol = 'https';

            self::$http = $protocol."://".$_SERVER['HTTP_HOST']."/"; // использовать только во вне сайта
            self::$sender_mail = 'webkoder@'.$_SERVER['SERVER_NAME'];
            // проверка версии браузера
            // http://chrisschuld.com/projects/browser-php-detecting-a-users-browser-from-php.html
            // мин. версии выбраны по свойству box-sizing http://www.w3schools.com/cssref/css3_pr_box-sizing.asp

            $browser = new Browser();

            if($browser->getBrowser() == Browser::BROWSER_IE && $browser->getVersion() < 11) {
                self::$badbrowser = 1;
            }

            else if($browser->getBrowser() == Browser::BROWSER_CHROME && $browser->getVersion() < 40) {
                self::$badbrowser = 1;
            }

            else if($browser->getBrowser() == Browser::BROWSER_SAFARI && $browser->getVersion() < 10) {
                self::$badbrowser = 1;
            }

            else if($browser->getBrowser() == Browser::BROWSER_OPERA && $browser->getVersion() < 11) {
                self::$badbrowser = 1;
            }

            else if($browser->getBrowser() == Browser::BROWSER_FIREFOX && $browser->getVersion() < 44) {
                self::$badbrowser = 1;
            }
        }
    }
    $conf = new conf;
?>

<?php
    include dirname(__FILE__)."/classes/base.php";

    // для загрузки локального конифга, url должен содержать либо порт, либо local
    if (array_key_exists('HTTP_HOST', $_SERVER) && strpos($_SERVER['HTTP_HOST'], 'local') !== false) {
        include dirname(__FILE__)."/conf.local.php";

        include dirname(__FILE__)."/classes/PDOTester.php";
        try {
            $pdo = new PDOTester("mysql:host=".conf::$db['server'].";dbname=".conf::$db['name'].";charset=utf8", conf::$db['user'], conf::$db['pass'], array(
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
                // PDO::MYSQL_ATTR_INIT_COMMAND => "SET time_zone = 'Asia/Yekaterinburg';",
            ));
        } catch(PDOException $e) {
            echo $e->getMessage();
            exit;
        }
    } else {
        include dirname(__FILE__)."/conf.php";

        include dirname(__FILE__)."/classes/PDOTester.php";

        try {
            $pdo = new PDOTester("mysql:host=".conf::$db['server'].";dbname=".conf::$db['name'].";charset=utf8", conf::$db['user'], conf::$db['pass'], array(
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
                // PDO::MYSQL_ATTR_INIT_COMMAND => "SET time_zone = 'Asia/Yekaterinburg';",
            ));
        } catch(PDOException $e) {
            echo $e->getMessage();
            exit;
        }
    }

?>

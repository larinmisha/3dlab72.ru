<?php
    class PDOTester extends PDO {
        public function __construct($dsn, $username = null, $password = null, $driver_options = array()) {
            parent::__construct($dsn, $username, $password, $driver_options);
            $this->setAttribute(PDO::ATTR_STATEMENT_CLASS, array('PDOStatementTester', array($this)));
        }

        public function prepare($statement = '', $driver_options = array()) {
            return parent::prepare($statement, $driver_options);
        }

        public function query($statement = '', $debug = 0) {
            $row_ = self::prepare($statement);
            $row_->execute([],$debug);
            return $row_;
        }

        public function getArray($statement) {
            $r = parent::query($statement);
            return $r->fetchAll();
        }
    }

    class PDOStatementTester extends PDOStatement {
        const NO_MAX_LENGTH = -1;
        protected $connection;
        protected $bound_params = array();

        protected function __construct(PDO $connection) {
            $this->connection = $connection;
        }

        public function bindParam($paramno, &$param, $type = PDO::PARAM_STR, $maxlen = null, $driverdata = null) {
            $this->bound_params[$paramno] = array('value' => &$param,
                'type' => $type,
                'maxlen' => (is_null($maxlen)) ? self::NO_MAX_LENGTH : $maxlen,
                            // ignore driver data
                );
            $result = parent::bindParam($paramno, $param, $type, $maxlen, $driverdata);
        }

        public function bindValue($parameter, $value, $data_type = PDO::PARAM_STR) {
            $this->bound_params[$parameter] = array(
                'value' => $value,
                'type' => $data_type,
                'maxlen' => self::NO_MAX_LENGTH
                );
            parent::bindValue($parameter, $value, $data_type);
        }
        public function execute($input_parameters = NULL, $debug=0) { // 0 - обычный режим, 1 - вывести запрос, 2 - вывести запрос без выполнения
            global $pdo, $cms;

            $log_sql = $this->getSQL($input_parameters);
            if (conf::$log_limit>0) {
                if ((stripos($log_sql,'select')===false)&&($debug!=-1)) {
                    $q = $pdo->prepare("INSERT INTO bk_log(`fk_structure`, `fk_users`, `query`) VALUES (:fks, :fku, :q)");
                    $q->execute([   ':fks'=>$cms->open_page,
                                    ':fku'=>$_SESSION['user']['id'],
                                    ':q'=>$log_sql
                                ],-1);
                    $q = $pdo->prepare("DELETE FROM bk_log WHERE timestamp < DATE_SUB(NOW(), INTERVAL :d DAY)");
                    $q->execute([':d'=>conf::$log_limit],-1);
                }
            }

            if ($debug >  0) {
                echo "<pre>";print_r($log_sql);echo "</pre>";
            }
            if ($debug <= 1) {
                if (count($input_parameters) > 0) {
                    foreach ($input_parameters as $key => $value) {
                        if ($input_parameters[$key] == '') {
                            $input_parameters[$key] = null;
                        }
                    }
                    parent::execute($input_parameters);
                } else {
                    parent::execute();
                }

            }
        }
        public function getSQL($values = array()) {
            $sql = $this->queryString;
            if (sizeof($values) > 0) {
                foreach ($values as $key => $value) {
                    $sql = str_replace($key, $this->connection->quote($value), $sql);
                }
            }

            if (sizeof($this->bound_params)) {
                foreach ($this->bound_params as $key => $param) {
                    $value = $param['value'];
                    if (!is_null($param['type'])) {
                        $value = self::cast($value, $param['type']);
                    }
                    if ($param['maxlen'] && $param['maxlen'] != self::NO_MAX_LENGTH) {
                        $value = self::truncate($value, $param['maxlen']);
                    }
                    if (!is_null($value)) {
                        $sql = str_replace($key, $this->connection->quote($value), $sql);
                    } else {
                        $sql = str_replace($key, 'NULL', $sql);
                    }
                }
            }
            return $sql;
        }

        static protected function cast($value, $type) {
            switch ($type) {
                case PDO::PARAM_BOOL:
                return (bool) $value;
                break;
                case PDO::PARAM_NULL:
                return null;
                break;
                case PDO::PARAM_INT:
                return (int) $value;
                case PDO::PARAM_STR:
                default:
                return $value;
            }
        }

        static protected function truncate($value, $length) {
            return substr($value, 0, $length);
        }
    }
?>

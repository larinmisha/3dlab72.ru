<?php
    /*
        Клас - фундамент, хранит все необходимые константные функции, словари.
        Сюда    писать только те функции которые постоянно используются в ЦМС(во всех).
        Сюда НЕ писать функции которые уникальные для сайта, для этого другой класс
    */
    class base {
        public static function test() {} // Функция защиты от запуска файлов без вызова класса

        /**
         * Функция для добавления скриптов и стилей в minify через массив $cms->meta
         * @param  string $file Путь к файлу (от корня)
         * @return null   изменяет массив $cms->meta
         */
        public function minify_import($file) {
            global $cms;

            $ext  = pathinfo($file, PATHINFO_EXTENSION);

            if ($ext == 'css') {
                $cms->meta['minifyCss'][] = $file;
            }
            else if ($ext == 'js') {
                $cms->meta['minifyJs'][] = $file;
            }

        }

        /**
         * Кодирование строки (спрятать пути к файлам)
         * @param  array  $array массив путей
         * @param  string $key   ключ шифрования
         * @return string        зашифрованная строка
         */
        function minify_encode($array, $key='') {
            $key = ($key) ? $key : md5(date('d.m.Y'));
            $string = implode(',', array_unique($array));
            return urlencode(base64_encode(base::xor_string($string, $key)));
        }

        /**
         * Декодирование строки
         * @param  array  $array массив путей
         * @param  string $key   ключ шифрования
         * @return string        расшифрованная строка
         */
        function minify_decode($string, $key='') {
            $key = ($key) ? $key : md5(date('d.m.Y'));
            return base::xor_string(base64_decode($string), $key);
        }

        /**
         * Функция для кодирования строк
         * @param  string $string строка
         * @param  string $key    ключ шифрования
         * @return string         зашифрованная строка
         */
        function xor_string($string, $key='') {

            // Our plaintext/ciphertext
            $text = $string;

            // Our output text
            $outText = '';

            // Iterate through each character
            for($i=0; $i<strlen($text); ) {
                for($j=0; ($j<strlen($key) && $i<strlen($text)); $j++, $i++) {
                    $outText .= $text{$i} ^ $key{$j};
                    //echo 'i=' . $i . ', ' . 'j=' . $j . ', ' . $outText{$i} . '<br />'; // For debugging
                }
            }

            return $outText;
        }

        public function registerScriptFile($file) {
            global $cms;

            $file = '<script type="text/javascript" src="'.$file.'"></script>';

            $cms->meta['vendorJs'][] = $file;
        }

        public function registerCssFile($file) {
            global $cms;

            $file = '<link rel="stylesheet" href="'.$file.'" />';

            $cms->meta['vendorCss'][] = $file;
        }

        public static function log_err($e) { // функция вывода массивов
            echo "<pre>";
            print_r("<b>Fatal error</b><br><b>{$e->getMessage()}</b><br>in  file: <b>{$e->getFile()}</b> line: <b>{$e->getLine()}</b><br>trace: <br>{$e->getTraceAsString()}");
            echo "</pre>";
        }
        public static function file_dir($arr) {
            $path = __DIR__."/../../";
            array_unshift($arr, 'files');
            foreach ($arr as $row) {
                $path .= $row."/";
                if(!is_dir($path)){
                    mkdir($path);       // создаем папку
                    chmod($path, 0755); // выставляем нужные права на эту папку
                    // file_put_contents($path.'index.php', 'black hole');
                }
            }
            return $path;
        }

        public static function curl_api($post, $section) { //  вызов api функций через curl
            foreach($post as $k => $v){
                $post_line .= "&{$k}={$v}";
            }
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $section);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS,$post_line);
            $result = curl_exec($ch);
            curl_close($ch);
            // echo $result;
            return json_decode($result,true);
        }

        public static function text_min ($text, $col){ // обрезка текста
            $text=str_replace("\'","'",$text);
            $text=str_replace('\"','"',$text);

            $text=strip_tags($text);
            while(($col<strlen($text))and(mb_substr($text,$col-1,1,'UTF-8')<>' ')){
                $col=$col+1;
            }
            if(mb_substr($text,$col-1,1,'UTF-8')==' ')$col=$col-1;
            $d='';
            if($col<strlen($text))$d='...';
            $text=mb_substr($text,0,$col,'UTF-8').$d;

            return $text;
        }
        public static function is_email($email) { // проверка на e-mail
            return preg_match("/^([a-zA-Z0-9])+([\.a-zA-Z0-9_-])*@([a-zA-Z0-9_-])+(\.[a-zA-Z0-9_-]+)*\.([a-zA-Z]{2,6})$/", $email);
        }
        public static function is_phone($phone) { // проверка на phone
            $phoneNumber = preg_replace('/\s|\+|-|\(|\)/','', $phone); // удалим пробелы, и прочие не нужные знаки
            if ((is_numeric($phoneNumber))&&(in_array(strlen($phoneNumber),array(10,11)))&&($phoneNumber[0]==9)) {
                return $phoneNumber;
            } else {
                return FALSE;
            }
        }
        function getCaseWord($num, $cases) { // Возвращает единицу измерения с правильным окончанием
            // $IDEA_case = getCaseWord($colIDEA, array('nom' => 'Идея', 'gen' => 'Идеи', 'plu' => 'Идей'));
            $num = abs($num);
            $word = '';
            if (strpos($num, '.')) {
                $word = $cases['gen'];
            } else {
                $word = (
                    (($num % 10 == 1) && ($num % 100 != 11))
                        ? $cases['nom']
                        : (
                            (($num % 10 >= 2) && ($num % 10 <= 4) && (($num % 100 < 10) || ($num % 100 >= 20)))
                            ? $cases['gen']
                            : $cases['plu']
                        )
                );
            }
            return $word;
        }
        /**
         * Функция для определения правильного окончания слова, в зависимости от численности
         * @param  string $n     number rows to ending determine
         * @param  array  $forms nouns or endings words for (1, 4, 5)
         * @return string
         */
        public function pluralize($n, $forms=['рубль', 'рубля', 'рублей']) {
          return $n%10==1&&$n%100!=11?$forms[0]:($n%10>=2&&$n%10<=4&&($n%100<10||$n%100>=20)?$forms[1]:$forms[2]);
        }
            /* alias for base::pluralize() */
            public function plural($n, $forms) {
              return base::pluralize($n, $forms);
            }

    }
    $base = new base;
    // print_r(conf::$ip);
?>

<?php base::test();

    class Api {

        private $action = '';
        private $format;
        private $params = [];
        private $result = [];

        function __construct() {
            $this->init();
        }

        /**
         * записать свойство
         * @param string $format
         * @return string
         */
        public function setAction ($action) {
            return $this->action = $action;
        }

        /**
         * прочитать свойство
         * @return string
         */
        public function getAction () {
            return $this->action;
        }


        /**
         * записать свойство
         * @param string $action
         * @return string
         */
        public function setFormat ($format) {
            return $this->format = $format;
        }

        /**
         * прочитать свойство
         * @return string
         */
        public function getFormat () {
            return $this->format;
        }


        /**
         * записать свойство
         * @param string $params
         * @return string
         */
        public function setParams ($params) {
            return $this->params = $params;
        }

        /**
         * прочитать свойство
         * @return string
         */
        public function getParams () {
            return $this->params;
        }


        /**
         * записать свойство
         * @param string $result
         * @return string
         */
        public function setResult ($result) {
            return $this->result = $result;
        }

        /**
         * прочитать свойство
         * @return string
         */
        public function getResult () {
            return $this->result;
        }

        /**
         * инициализация класса
         * @return object вернуть объект
         */
        private function init() {
            global $cms;

            $this->setFormat($cms->api['format']); // определить формат

            $this->setAction($cms->api["action"]); // определить метод
            // если внутри коллекции вызываешь другую этот параметр мешает там,
            // вторая коллекция думает что ее вызвали из вне и выдаёт лишние ковычки
            unset($cms->api["action"]);

            // получить/обработать переменные
            if (isset($_REQUEST)) {
                $this->setParams($_REQUEST);
            }

            return $this;
        }

        /**
         * выполнить метод
         * @return object вернуть объект
         */
        public function query() {
            global $cms;

            $action = $this->getAction();
            $params = $this->getParams();

            // проверка существования метода
            if (method_exists($this, $action)) {
                // выолнить метода
                try {
                    $this->setResult($this->{$action}($params));
                } catch (Exception $e) {
                    return $this;
                }
            } else {
                $mas['developer']['message'][] = "Method Not Allowed";
                http_response_code(405);
            }

            return $this;
        }

        /**
         * получить результат выполнения
         * @return string результат выполнения
         */
        public function fetch($format=false) {
            global $cms;

            $format = ($format) ? $format : $this->getFormat();
            $result = $this->getResult();
            if (is_array($result)) {
                if ((isset($result['developer']))&&(count($result['developer']) > 0)) {
                    if ($cms->api['developer']) {
                        foreach ($result['developer'] as $key => $value) {
                            $result[$key] = $value;
                        }
                    }
                    unset($result['developer']);
                }
            }

            if ($format == 'pre') {
                echo "<pre>";print_r($result);echo "</pre>";
            } else if ($format == 'json') {
                header('Content-Type: application/json');
                return json_encode($result, JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT|JSON_NUMERIC_CHECK);
            } else if ($format == 'row') { // по умолчанию т.к. если вызывать из php нужен чистый поток
                return $result;
            }
        }

    }

?>

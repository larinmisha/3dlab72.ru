<?php
    base::test();

    class SendMail {
        /**
         * Получение данных
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function status($params = null) {
            global $cms;
            $all_ = $cms->pdo->query("  SELECT  *
                                        FROM    bk_sendMail
                                        WHERE   1=1
                                                and time_send is NULL");
            $text = "В очереди на рассылку ".$all_->rowCount()." писем. ";
            if ($params['unic'] != '') {
                $unic_ = $cms->pdo->prepare("SELECT  *
                                            FROM    bk_sendMail
                                            WHERE   1=1
                                                    and time_send is NULL
                                                    and unic=:unic");
                $unic_->execute([':unic' => $params['unic']]);
                $text .= "По выбранной рассылке ".$unic_->rowCount()." писем.";
            }
            return $text;
        }

        /**
         * Письма счастья
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function unsubscribe($params = null) {
            global $cms;
            $tpl_ = $cms->pdo->query("  SELECT  *
                                        FROM    bk_sendMail_tpl
                                        WHERE   1=1
                                                and tip like 'unsubscribe' ");
            $tpl = $tpl_->fetch();

            $row_ = $cms->pdo->query("  SELECT  id, email
                                        FROM    bk_users
                                        WHERE   1=1
                                                and subscribe = 1
                                                and last_visit <= NOW() - INTERVAL 12 month
                                        LIMIT 10");
            if ($row_->rowCount() >0) {
                $ins_ = $cms->pdo->prepare("INSERT INTO bk_sendMail (unic, fk_users, subject, message)
                                                VALUES (:unic, :fk_users, :subject, :message)");
                while ($row = $row_->fetch()) {
                    $message = str_replace("{{url}}", conf::$http."?subscribe_visit=".$row['email'], $tpl['text']);
                    $message = str_replace("{{title}}",  $cms->structure_page(1, 'title'), $message);

                    $title = str_replace("{{title}}",  $cms->structure_page(1, 'title'), $tpl['title']);

                    $ins_->execute([':unic'     => NULL,
                                    ':fk_users'  => $row['id'],
                                    ':subject'  => $title,
                                    ':message'  => $message
                                ]);

                    $upd_ = $cms->pdo->prepare("   UPDATE  bk_users
                                                    SET     subscribe = -1
                                                    WHERE   id = :id
                                                    LIMIT 1");
                    $upd_->execute([":id"=>$row['id']]);
                }
            }
            return 1;
        }

        /**
         * Получение данных
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function get($params = null) {
            global $cms;
            set_time_limit(0);
            $count = 0;
            if ($params['unic'] != '') {
                $row_ = $cms->pdo->prepare("SELECT  *
                                            FROM    bk_sendMail
                                            WHERE   unic = :unic ");
                $row_->execute([':unic'=> $mas['unic']]);
                if ($row_->rowCount() > 0) { // дубль не рассылаем
                    unset($params);
                }
            } else {
                $params['unic'] = NULL;
            }

            $params['message'] = preg_replace("/(href=[\'\"])(\/)/", "$1".conf::$http, $params['message']);

            if ($params['user'] == 'all') { // собираем всех в БД
                $row_ = $cms->pdo->query("  SELECT  id, email
                                            FROM    bk_users
                                            WHERE   1=1
                                                    and email != ''
                                                    and subscribe = 1
                                                    and last_visit IS NOT NULL
                                            GROUP BY email
                                            ORDER BY email");
                if ($row_->rowCount() >0) {
                    $ins_ = $cms->pdo->prepare("INSERT INTO bk_sendMail (unic, fk_users, subject, message)
                                                VALUES (:unic, :fk_users, :subject, :message)");
                    while ($row = $row_->fetch()) {
                        $ins_->execute([':unic'     => $params['unic'],
                                        ':fk_users'  => $row['id'],
                                        ':subject'  => $params['subject'],
                                        ':message'  => $params['message']
                                    ]);
                        $count++;
                    }
                }
            } else if (count($params['user'])>0) {
                $count += count($params['user']);
                $ins_ = $cms->pdo->prepare("INSERT INTO bk_sendMail (unic, fk_users, subject, message)
                                                VALUES (:unic, :fk_users, :subject, :message)");
                foreach ($params['user'] as $user) {
                    $ins_->execute([':unic'     => $params['unic'],
                                    ':fk_users'  => $user,
                                    ':subject'  => $params['subject'],
                                    ':message'  => $params['message']
                                ]);
                }
            }

            if (count($params['email'])>0) {
                $count += count($params['email']);
                foreach ($params['email'] as $email) {
                    $ins_ = $cms->pdo->prepare("INSERT INTO bk_sendMail (unic, email, subject, message)
                                                VALUES (:unic, :email, :subject, :message)");
                    $ins_->execute([':unic'     => $params['unic'],
                                    ':email'    => $email,
                                    ':subject'  => $params['subject'],
                                    ':message'  => $params['message']
                                ]);
                }
            }
            if (count($params['phone'])>0) {
                $count += count($params['phone']);
                foreach ($params['phone'] as $phone) {
                    $ins_ = $cms->pdo->prepare("INSERT INTO bk_sendMail (unic, phone, subject, message)
                                                VALUES (:unic, :phone, :subject, :message)");
                    $ins_->execute([':unic'     => $params['unic'],
                                    ':phone'    => $phone,
                                    ':subject'  => $params['subject'],
                                    ':message'  => $params['message']
                                ]);
                }
            }
            $cms->pdo->query("DELETE FROM bk_sendMail WHERE timestamp < DATE_SUB(NOW(), INTERVAL 3 month)");
            $this->cron(['max_send' => 1]);
            return $count;
        }

        /**
         * Функция для крон рассылки
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function cron($params = null) {
            global $cms;
            $start = microtime(true);
            $dat = date("Y-m-d H:i:s");
            $row_ = $cms->pdo->query("  SELECT  id
                                        FROM    bk_sendMail
                                        WHERE   1=1
                                                and time_send is NULL
                                        ORDER BY id");
            if ($row_->rowCount()>0) {
                if (!is_numeric($params['max_second'])) {
                    $params['max_second'] = 55;
                }
                if ((!is_numeric($params['max_send']))||($params['max_send'] >= 25)) {
                    $params['max_send'] = 25;
                }
                $count = min($params['max_send'], $row_->rowCount());
                for ($i=1; $i <= $count; $i++) {
                    $row_ = $cms->pdo->query("  SELECT  e.id, e.subject, e.message, e.phone, e.email as em, u.email as um
                                                FROM    bk_sendMail AS e left join bk_users AS u on e.fk_users=u.id
                                                WHERE   1=1
                                                        and e.time_send is NULL
                                                ORDER BY e.id DESC
                                                LIMIT   1");
                    if ($row_->rowCount()>0) {
                        $row = $row_->fetch();
                        if ($row['um'] != '') {
                            $row['email'] = $row['um'];
                        } else if ($row['em'] != '') {
                            $row['email'] = $row['em'];
                        }

                        $row2_ = $cms->pdo->prepare("   UPDATE  bk_sendMail
                                                        SET     time_send = NOW()
                                                        WHERE   id = :id
                                                        LIMIT 1");
                        $row2_->execute([":id"=>$row['id']]);


                        $headers  = "MIME-Version: 1.0\r\n";
                        $headers .= "Content-type: text/html;charset=UTF-8\r\n";
                        $headers .= "From: ".mb_encode_mimeheader($cms->structure_page(1, 'title'), 'UTF-8')." <".conf::$sender_mail.">\r\n";
                        mb_send_mail(
                            $row['email'],
                            $row['subject'],
                            $row['message'],
                            $headers
                        );
                        if ($row['phone'] != '') {
                            $ch = curl_init();
                            curl_setopt($ch, CURLOPT_URL, 'http://10.223.16.17/sms_megafon/smsapi.php');
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
                            curl_setopt($ch, CURLOPT_POST, true);
                            curl_setopt($ch, CURLOPT_POSTFIELDS, ['phone' => $row['phone'], 'sms' => $row['message']]);
                            $result = curl_exec($ch);
                            curl_close($ch);
                        }
                        if ($i < $count) {
                            sleep(1);
                        }
                    } else {
                        break;
                    }
                    if (round(microtime(true) - $start, 4) >= $params['max_second']) {
                        echo "<pre>";print_r("За max_second: ".$params['max_second']." (по факту: ".round(microtime(true) - $start, 4)."), было отправлено ".$i." писем, максимальный лимит сообщений был: ".$params['max_send']);echo "</pre>";
                        break;
                    }
                }
            }
            return 1;
        }
    }

    $sendMail = new SendMail();

    // if ($sendMail->getAction()!='') { // вызов через api, иначе вызов напрямую через php
    //     echo $sendMail->query()->fetch();
    // }

?>

<?php

    class form { // Клас - фабрика элементов форм для админки
        public static function info($text1='', $text2='', $type="danger") { // Вывод информации
            // ($type: success-зелёный, info-синий, warning-желтый. danger-красный)
            return "<div class='alert alert-{$type}' role='alert'><strong>{$text1}</strong> {$text2}</div>";
        }
        public static function start($action='', $method='POST', $name='') { // заголоок формы
            return "<form method='{$method}' action='{$action}' name='{$name}' id='name_{$name}' enctype='multipart/form-data'>";
        }
        public static function end() { // заголоок формы
            return "</form>";
        }

        public static function header($but='', $text='Редактор модуля') {
            for ($i=0;$i<=3;$i++) {
                if (isset($but['but_'.$i])) {
                    if ($but['but_'.$i][type] != 'false')
                    $b[$i] = "<input class='btn btn-default form-control' type='{$but['but_'.$i][type]}' name='{$but['but_'.$i][name]}' value='{$but['but_'.$i][value]}' OnClick=\"{$but['but_'.$i][OnClick]}\" >";
                }
            }
            return "<div class='panel panel-primary'>
                        <div class='panel-heading'>
                            <div class='row'>
                                <div class='col-xs-4 col-sm-4'>
                                    <h3 class='panel-title'>{$text}</h3>
                                </div>
                                <div class='col-xs-2 col-sm-2'>
                                    {$b[3]}
                                </div>
                                <div class='col-xs-2 col-sm-2'>
                                    {$b[2]}
                                </div>
                                <div class='col-xs-2 col-sm-2'>
                                    {$b[1]}
                                </div>
                                <div class='col-xs-2 col-sm-2'>
                                    {$b[0]}
                                </div>
                            </div>
                        </div>
                        <div class='panel-body'>";
        }

        public static function footer() {
            return "    </div>
                    </div>";
        }

        public static function hidden($name='', $id='', $class='', $value='') {
            return "<input type='hidden' class='{$class}' id='{$id}' name='{$name}' value='{$value}'>";
        }

        public static function textarea($title='', $name='', $text='', $placeholder='', $rows=20) {
            return "<div class='form-group'><label for='name_{$name}'>{$title}</label>
                    <textarea class='form-control js-tinymce' id='name_{$name}' name='{$name}' rows='{$rows}' placeholder='{$placeholder}'>{$text}</textarea></div>";
        }

        public static function text($title='', $name='', $text='', $placeholder='', $events='') {
            return "<div class='form-group'><label for='name_{$name}'>{$title}</label>
                    <input type='text' class='form-control' id='name_{$name}' name='{$name}' placeholder='{$placeholder}' value='{$text}' {$events}></div>";
        }
        public static function dat($title='', $name='', $text='', $placeholder='') {
            return "<div class='form-group'><label for='name_{$name}'>{$title}</label>
                    <input type='text' class='form-control' id='name_{$name}' name='{$name}' placeholder='{$placeholder}' value='{$text}'></div>";
        }
        public static function file($title='', $name='', $img='', $multiple='') {
            if ($multiple!='') {
                $multiple = " multiple='true' ";
                $m_arr = "[]";
            }
            $val ='1'; // признак того что есть поле выбора файла
            if ($img!='') {
                $img = "<a href='javascript:'  class='name_{$name}' OnClick=\"$('.name_{$name}').toggle();$('.{$name}_h').val('1');\">
                    <div>Заменить файл<br><br></div>
                </a>";
                $file_dis = "style='display:none;'";
                $val ='0'; // признак того что Нет поля выбора файла
            }
            return "<div class='form-group'><label for='name_{$name}'>{$title}</label>
                        {$img}
                    ".form::hidden($name."_h",$name."_h",$name."_h",$val)."
                    <input type='file' class='form-control name_{$name}' id='name_{$name}{$m_arr}' name='{$name}{$m_arr}' {$file_dis} {$multiple}></div>";
        }
        public static function file_img($title='', $name='', $img='', $multiple='') {
            if ($multiple!='') {
                $multiple = " multiple='true' ";
                $m_arr = "[]";
            }
            $val ='1'; // признак того что есть поле выбора файла
            if ($img!='') {
                $img = "<br><img src='{$img}' style='width: 250px;'><br>
                <a href='javascript:'  class='{$name}_h' OnClick=\"$('.name_{$name}').toggle();$('.{$name}_h').val('1');\">
                    <div>Заменить файл<br><br></div>
                </a>";
                $file_dis = "style='display:none;'";
                $val ='0'; // признак того что Нет поля выбора файла
            }

            return "<div class='form-group'><label for='name_{$name}'>{$title}</label>
                    {$img}
                    ".form::hidden($name."_h",$name."_h",$name."_h",$val)."
                    <input type='file' class='form-control name_{$name}' id='name_{$name}{$m_arr}' name='{$name}{$m_arr}' {$file_dis} {$multiple}></div>";
        }
        public static function select($title='', $name='', $option='', $checked='-1', $js='') {
            if ($option) {
                foreach ($option as $k=>$v) {
                    if($checked==$k) $selected = ' selected'; else $selected = "";
                    $option_ .= "<option value='{$k}' {$selected}>{$v}</option>";
                }
            }
            return "<div class='form-group'><label for='name_{$name}'>{$title}</label>
                    <select id='name_{$name}' name='{$name}' class='form-control' {$js}>{$option_}</select></div>";

        }
        public static function checkbox($title='', $name='', $option='', $checked='-1', $js='') {
            if ($option) {
                foreach ($option as $key => $val) {
                    if($checked==$key) $selected = 'checked'; else $selected = "";
                    $option_ .= "<div class='checbox'>
                                    <label>
                                        <input type='checkbox' id='name_{$name}' name='{$name}' value='{$key}' {$selected}>
                                        <span>{$val}</span>
                                    </label>
                                </div>
                                ";
                }
            }
            if ($title!='') $title = "<label>{$title}</label>";
            return "<div class='form-group'><div class='checkbox'>{$title}{$option_}</div></div>";
        }
        public static function radio($title='', $name='', $option='', $checked='-1', $js='') {
            if ($option) {
                foreach ($option as $key => $val) {
                    if($checked==$key) $selected = 'checked'; else $selected = "";
                    $option_ .= "<div class='radio'>
                                    <label>
                                        <input type='radio' id='name_{$name}' name='{$name}' value='{$key}' {$selected}>
                                        <span>{$val}</span>
                                    </label>
                                </div>
                                ";
                }
            }
            if ($title!='') $title = "<label>{$title}</label>";
            return "<div class='form-group'>{$title}{$option_}</div>";

        }
        public static function btn($title='', $name='', $img='') {
            return "<button type='submit' name='{$name}' class='btn btn-default btn-xs'>
                        <span class='glyphicon {$img}' aria-hidden='true'></span>
                        {$title}
                    </button>";

        }
        public static function h($title='') {
            return "<h6>{$title}</h6>";
        }
    }
    // $df = new designer_form;
    // print_r(conf::$ip);
?>

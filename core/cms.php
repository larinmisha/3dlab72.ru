<?php
    // Когда я начинал это писать, только Бог и я понимали, что я делаю
    // Сейчас остался только Бог

    //
    // Дорогой друг:
    //
    // Когда ты закончишь «оптимизировать» это ядро
    // и поймешь, насколько большой ошибкой было делать это,
    // пожалуйста, увеличь счетчик внизу как предупреждение
    // для следующих глупцов:
    //
    // total_hours_wasted_here = 1
    //

    // public - разрешен отовсюду
    // protected - наследуемым и родительским классам
    // private - где объявлен сам элемент
    // ----
    // self - в классе (внутри вызванного класса)
    // parent - обращение к родителю
    // http://www.kiev-market.com/article/OOP1.htm

    class cms  { // класс обработчика
        public $meta      = [];     // Хранит meta данные для head
        public $tpl       = [];     // Хранит пути к теме
        public $structure = [];     // Хранит данные из таблицы structure, именно private чтобы не кто не трогал!

        public $get       = [];     // Хранит обработанный $_GET
        public $get_      = [];     // Хранит сырой $_GET массив
        public $post      = [];     // Хранит обработанный $_POST
        public $post_     = [];     // Хранит сырой $_POST массив
        public $request   = [];     // Хранит обработанный $_REQUEST
        public $request_  = [];     // Хранит сырой $_REQUEST массив

        public $content   = '';     // Хранит содержимое контента
        public $api       = [];     // массив параметров для api
        public $group_cms = [];     // Хранит группы доступа
        public $open_page;          // Хранит id открытой страницы
        public $pdo;                // Хранит группы доступа
        public $region    = '';     // Хранит select региона
        public $modules   = [];     // Хранит параметры текущего модуля
        public $admin     = false;  // Статус доступа (front/admin)

        public function __construct() {
            global $pdo;
            // parent::__construct('0'); // По умолачнию цепляем 0 Базу(defult для $this)
            $this->pdo = $pdo;
            set_exception_handler("base::log_err"); // вызов функции для перехвата ошибок
        }
        public function __destruct() {  // По умолчанию, инициализация
            // parent::__destruct();
        }
        public function auto_login() {  // Проверка авторизации и данных о пользователе
            include "modules/admin_cms/user/loginza.php";
            include "modules/admin_cms/user/collections/user_cms.php";
            $user_cms->get_user_token();
            if (is_numeric($_SESSION['user']['id'])) {
                /* Работа с группами доступами для ЦМС */
                $this->group_cms['id']=[];     // Хранит ip групп доступа, т.к. может быть несколько
                $this->group_cms['full']=0;    // Если 1 то все права доступа (меняться может только через делатизацию(см. ниже)
                $this->group_cms['detal']=[];  // Хранит данные о дополнительных параметрах групп доступа, можно расширять и исключать

                /* Выставляем параметры на все группы доступа, если такие имеются, т.е. на все разделы сразу */
                $row_ = $this->pdo->prepare("SELECT g.*
                                             FROM   bk_access_group_user as u,
                                                    bk_access_group as g
                                            WHERE   1=1
                                                    AND u.fk_access_group = g.id
                                                    AND u.fk_users = :uid ");
                $row_->execute([':uid' => $_SESSION['user']['id']]);    // Фиксируем время последнего действия
                while($row_g=$row_->fetch()) {
                    $this->group_cms['id'][]=$row_g['id'];
                    if ($row_g['full']==1)  $this->group_cms['full']    =1;
                }
                /*
                    Выставляем параметры на отдельные страницы, можно блокировать(исключением)
                    Например: Выставить полные права доступа на full но заблокирвоать только некоторые разделы
                */
                if (count($this->group_cms['id'])>0) {
                    $row_ = $this->pdo->query(" SELECT  *
                                                FROM    bk_access_group_structure
                                                WHERE   (fk_access_group = ".implode(" or fk_access_group = ", $this->group_cms['id']).")");
                    while($row_g=$row_->fetch()) {
                        $this->group_cms['detal'][$row_g['fk_structure']] = []; // говорим о том что какие то права будут
                        foreach ($row_g as $k=>$v) {
                            if (($row_g[$k]==1)&&($k!='fk_structure')) {
                                $this->group_cms['detal'][$row_g['fk_structure']][$k] = $v;
                            }
                        }
                    }
                }
            }
        }
        // Проверка существования авторизации
        public function is_user() {
            if (is_numeric($_SESSION['user']['id']) && $_SESSION['user']['id'] > 0) {
                return true;
            } else  {
                return false;
            }
        }
        // Проверка прав доступа
        public function is_access($id='', $tip='', $dirrect = NULL) {
            if ($this->is_user()) {
                if ($id == '') $id = $this->open_page;
                $this->structure_page($id); // иначе права доступа не проставятся на раздел
                if ($this->group_cms['full']==1) {
                    return true;
                } else if (array_key_exists($id, $this->group_cms['detal'])) {
                    if ((array_key_exists($tip, $this->group_cms['detal'][$id])) || ($tip == '')) {
                        if ($dirrect != '') {
                            return $this->is_modules($dirrect);
                        } else {
                            return true;
                        }
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
        public function is_modules($dirrect = null) {
            $row_ = $this->pdo->prepare("SELECT  ms.id
                                        FROM    bk_modules as ms,
                                                bk_modul   as m
                                        WHERE   1=1
                                                and ms.fk_modul         = m.id
                                                and ms.del              = 0
                                                and ms.id               = :mod
                                                and ms.fk_structure     = :node
                                                and m.dirrect           = :dirrect
                                        ");
            $row_->execute([':node'     => $_POST['node'],
                            ':mod'      => $_POST['mod'],
                            ':dirrect'  => $dirrect
                            ]);
            if ($row_->rowCount()>0) {
                return true;
            } else {
                $mas['developer']['message'][] = "modul is not valid";
                http_response_code(401);
                return false;
            }
        }
        public function check_chpu($chpu) { // Парсим ЧПУ
            $chpu=htmlspecialchars($chpu,ENT_QUOTES); // Получаем строку ЧПУ
            if (stripos($chpu, "?")) { // избавляемся от ?{переменные}
                $chpu = substr($chpu, 0, stripos($chpu, "?"));
            }
            if ((stripos($chpu, '/api/')!==false)&&(substr($chpu, -1)!='/')) {
                $chpu .= '/';
            }
            if (!empty(conf::$url_dev)) {
                if ((stripos($chpu, '/'.conf::$url_dev.'/')!==false)&&(substr($chpu, -1)!='/')) {
                    // $chpu .= '/'; // мне кажется это надо было когда был хитрый гет. Удалить если ошибок не найдется!
                }
            }

            $url=explode("/", $chpu); // Разбиваем строку на массив, разделителем считаем знак "/"
            if ($url[0]=='')          array_shift($url); // удаляем пустой элемент
            // if ($url[0]=='page')      array_shift($url); // удаляем page
            array_pop($url); // странная штука
            /* GET / POST */
            // $get_mas = array_filter(explode("_", array_pop($url))); // Получаем параметры get для модуля, и удаляем из массива;
            // $get_mas = array_values($get_mas); // сбросить ключи
            // foreach ($get_mas as $buf) {
            //     $get_detal = explode("-", $buf,2);
            //     $this->get[array_shift($get_detal)]=array_shift($get_detal);
            // }
            // $this->get = array_merge_recursive($_GET, $this->get);
            // $this->post=$_POST;

            // $this->request  = array_merge($this->get , $this->post);
            $this->auto_login(); // Проверка авторизации и данных о пользователе (попробовал перенести сюда, причина - нужен post массив для loginza
            $this->open_page = $this->mas_structure($this->pdo->query("SELECT * FROM bk_structure WHERE fk_structure=0 LIMIT 1")->fetch());
            if ($url[0]=='admin') {
                if ((conf::$get_admin!='')&&(array_key_exists(conf::$get_admin, $_GET))) {
                    $_SESSION['get_admin'] = conf::$get_admin;
                    $this->api = ['modules'   => 'page'];
                    $_GET['tpl'] = '404';
                } else if ($this->is_access() || ($_SESSION['get_admin'] == conf::$get_admin)) { // проверяем права доступа на раздел админа
                    $this->admin = true;
                    unset($_SESSION['get_admin']);
                } else {
                    $this->api = ['modules'   => 'page'];
                    $_GET['tpl'] = '404';
                }
                array_shift($url); // оставляем чистую ЧПУ и работаем уже с ней
            } else if (in_array($url[0], ['api', conf::$url_dev])) { // Магия. Не трогать. (потом убрать)
                $this->api = [  'developer' => (($url[0] == conf::$url_dev)?1:0),
                                'modules'   => $url[1],
                                'collection'=> $url[2],
                                'action'    => $url[3],
                                'format'    => $url[4]];
                unset($url); // полностью удаляем ЧПУ, точка доступа в api одна, а get уже обработали
            }

            $this->meta['description']  = htmlspecialchars($this->structure_page('', 'description'));
            $this->meta['keywords']     = htmlspecialchars($this->structure_page('', 'keywords'));
            if ((conf::$badbrowser == true)&&(!$this->admin)) {
                $this->api = ['modules'   => 'page'];
                $_GET['tpl'] = 'badbrowser';
            } else if (count($this->api)==0) {
                for ($i=0;$i<=count($url)-1;$i++) {
                    if (!$this->admin) $open = "and open=1";
                    $row_ = $this->pdo->prepare("SELECT * FROM bk_structure WHERE fk_structure= :fk_s and url= :url and del=0  {$open} LIMIT 1");
                    $row_->execute([':fk_s' => $this->structure_page('','id'),':url' => $url[$i]]);
                    $row = $row_->fetch();
                    if (is_numeric(conf::$vip_structur)) {
                        if ( ($row['id'] == conf::$vip_structur) && (count($this->group_cms['id']) == 0) ) {
                            include "themes/login/html.php";
                            die();
                        }
                    }
                    if ($row['id']>0) {
                        $this->open_page=$this->mas_structure($row);
                        $this->meta['description']  = htmlspecialchars($this->structure_page('', 'description'));
                        $this->meta['keywords']     = htmlspecialchars($this->structure_page('', 'keywords'));
                    } else { //Если раздела нет или он закрыт - открываем страницу с ошибкой 404
                        if (!$this->admin) {
                            $this->api = ['modules'   => 'page'];
                            $_GET['tpl'] = '404';
                            break;
                        }
                    }
                }
                if (($this->structure_page('','fk_structur_redirect')!='')&&(!$this->admin)) {
                    if (is_numeric($this->structure_page('','fk_structur_redirect'))) {
                        echo '<meta http-equiv="refresh" content="0;url='.$this->structure_page($this->structure_page('','fk_structur_redirect'),'chpu').'">';
                    } else {
                        echo '<meta http-equiv="refresh" content="0;url='.$this->structure_page('','fk_structur_redirect').'">';
                    }
                    die();
                }
            }
        }
        public function meta_title($admin = null) {
            $breadcrumb = array_reverse($this->breadcrumb($this->open_page, $admin));
            foreach ($breadcrumb as $row) {
                $buf[] = $row['title'];
            }
            if ((array_key_exists('title', $this->meta))&&($this->meta['title'] != 'none')) {
                return $this->meta['title'].' / '.implode($buf, ' / ');
            } else {
                return implode($buf, ' / ');
            }
        }

        // Собираем массив из таблицы structure - использовать только в ядре и только в исключительных случаях для ускорения!
         public function mas_structure($row) {
            if (!$this->structure[$row['id']]) {
                if ($row['id'] > 0) {
                    $this->structure[$row['id']]=$row;

                    if (!isset($this->structure[$row['fk_structure']]['id'])) { // если НЕ существует
                        $this->structure_page($row['fk_structure'],'id');
                    }

                    $this->structure[$row['id']]['chpu'] = '/';
                    $this->structure[$row['id']]['chpu_admin'] = '/admin/';
                    if ($row['url'] != '') { // так отсеиваем главный узел, чтобы там не писать /
                        if (($this->structure_page($row['id'], 'fk_structur_redirect') != '')&&(!$this->admin)) {
                            if (is_numeric($this->structure_page($row['id'],'fk_structur_redirect'))) {
                                $this->structure[$row['id']]['chpu']         = $this->structure_page($row['id'], 'chpu');
                            } else {
                                $this->structure[$row['id']]['chpu']         = $row['fk_structur_redirect'];
                            }
                        } else {
                            $this->structure[$row['id']]['chpu']         = $this->structure[$row['fk_structure']]['chpu'].$row['url']."/";
                        }
                        // если нет темы на узле, то берем с верхнего уровня
                        if ($this->structure[$row['id']]['theme'] == '') {
                            $this->structure[$row['id']]['theme'] = $this->structure[$row['fk_structure']]['theme'];
                        }
                        $this->structure[$row['id']]['chpu_admin']   = $this->structure[$row['fk_structure']]['chpu_admin'].$row['url']."/";
                    }

                    // если есть галочка передачи прав, то передаём на нихний уровень
                    if ($this->group_cms['detal'][$this->structure[$row['id']]['fk_structure']]['recursion']==1) {
                        if (!array_key_exists($this->structure[$row['id']]['id'], $this->group_cms['detal'])) {
                            $this->group_cms['detal'][$this->structure[$row['id']]['id']] = $this->group_cms['detal'][$this->structure[$row['id']]['fk_structure']];
                        }
                    }
                }
            }
            return $row['id'];
        }
        public function structure_page($id='', $atr='') { // запрос к таблице со структурой
            if (!(is_numeric($id))) $id = $this->open_page;
            if (!isset($this->structure[$id]['id'])) {
                $this->mas_structure($this->pdo->query("SELECT * FROM bk_structure WHERE id='{$id}' and del=0 LIMIT 1")->fetch());
            }
            if ($atr!='') {
                return $this->structure[$id][$atr];
            } else {
                return $this->structure[$id];
            }
        }
        public function url($id = '', $type = '', $get = '') {
            if (($type == 1)&&($this->is_access($params['node']))) {
                $type = 'chpu_admin';
            } else {
                $type = 'chpu';
            }
            $url = $this->structure_page($id, $type);
            if (($get == 1)&&($_GET)) {
                $url .= '?'.http_build_query($_GET);
            }
            return $url;
        }

        // Рекурсией собираем все подразделы, раздела. На выходе: '18','19',...
        public function list_id_tree_structure($id, $implode=true, $admin=false) {
            // Чтобы понять рекурсию, прочтите комментарий в конце файла.
            if ($id =='')  {$id=$this->open_page;}
            $mas[] = $id; // то что пришло. то и записали, так мы берём ещё и сам раздел! (это важно!)
            if ($admin==false) $w = " and open=1  and maps=1";
            $result=$this->pdo->prepare("SELECT * FROM bk_structure WHERE fk_structure= :fk_s {$w} and del=0 ORDER BY sort ASC");
            $result->execute([':fk_s' => $id]);
            while($row_lm=$result->fetch()) {
                $this->mas_structure($row_lm);
                if ($row_lm['fk_structure']>0) {
                    $buf = $this->list_id_tree_structure($row_lm[id], $implode, $admin);
                    if ($buf!='') {
                        if ($this->admin) { // если в админке
                            if ($this->is_access($row_lm['id'])) {
                                $mas[] = $buf;
                            }
                        } else {
                            $mas[] = $buf;
                        }
                    }
                }
            }
            if ($implode) {
                return implode(',', $mas);
            } else {
                return $mas;
            }
            // Чтобы понять рекурсию, прочтите комментарий в начале файла.
        }
        public function include_ob($php,$mas=[]) { // $mas - скорее всего используется в инклудах
            ob_start();
            include $php;
            $content = ob_get_contents();
            ob_end_clean();
            return $content;
        }

        public function breadcrumb($id=null, $admin=null) {
            if ($id > 0) {
                $node = self::breadcrumb($this->structure_page($id, 'fk_structure'), $admin);
                $title = $this->structure_page($id, 'title');
                if ($admin == '1') {
                    $title .= " [".$id."]";
                }
                $node[] = ['title' => $title,
                           'chpu'  => $this->url($id, $admin)
                          ];
            }
            return $node;
        }
    }
    class designer_office extends cms { // Конструктор Сайта
        public $s_time; // Хранит время начала выполнения скрипта
        public function __construct() {
            parent::__construct();
            $this->s_time=microtime(true);
        }
        public function designer($url) {
            header('Content-type: text/html; charset=UTF-8');
            $this->meta['minifyCss']    = [];
            $this->meta['minifyJs']     = [];

            $this->check_chpu($url);    // Парсим ЧПУ и проверяем н правильноть

            $this->tpl['name']          = $this->structure_page('','theme');               // название шаблона
            $this->tpl['folder']        = $this->structure_page('','theme');               // название папки шаблона
            $this->tpl['path']['admin'] = "themes/admin/";                                 // путь     до      шаблона  админки
            $this->tpl['path']['tpl']   = "themes/".$this->structure_page('','theme')."/"; // путь     до      текущего шаблона

            if (count($this->api)>0) {
                $this->api_office();
            } else {
                if (($this->admin)&&($this->is_access(1))) {
                    $this->back_office();
                } else {
                    if (!is_numeric($_SESSION['user']['id']) && (array_key_exists('login', $_GET) || conf::$auto_login)) {
                        include "themes/login/html.php";
                    } else {
                        $this->front_office();
                    }
                }
            }
        }
        public function back_office() {
            // отключаем кеширование в админке
            header("Pragma-directive: no-cache");
            header("Cache-directive: no-cache");
            header("Cache-control: no-cache");
            header("Pragma: no-cache");
            header("Expires: 0");
            $this->meta['modul'] = []; // на бужущее, если нуэно и в админку передаать модули через заголовок
            include "themes/admin/html.php";
        }
        public function front_office() {
            $this->meta['robots'] = "all";

            ob_start();
            $result=$this->pdo->prepare("   SELECT  ms.id, ms.fk_structure, m.dirrect, m.type_script, m.id as mid
                                            FROM    bk_modules as ms,
                                                    bk_modul as m
                                            WHERE   1=1
                                                    AND ms.fk_modul=m.id
                                                    AND ms.fk_structure= :fk_s
                                                    AND ms.open=1
                                                    AND ms.del=0
                                            ORDER BY sort ASC");
            $result->execute([':fk_s' => $this->open_page]);
            while($this->modules=$result->fetch()) {
                $path = "modules".(($this->modules['type_script']==2)?"/admin_cms":"")."/{$this->modules['dirrect']}/";
                $this->modules['dir'] = $path;
                $this->modules['http']= '/'.$path;
                $this->modules['chpu']= $this->structure_page($this->modules['fk_structure'],'chpu');
                $this->modules['chpu_admin']= $this->structure_page($this->modules['fk_structure'],'chpu_admin');
                $this->meta['modul'][$this->modules['dirrect']] = $this->modules['mid'];
                $modules = $this->modules;
                if (is_file($this->modules['dir']."main.php")) {
                    include $this->modules['dir']."main.php";
                }
            }
            $this->content = ob_get_contents();
            ob_end_clean();

            include "themes/".$this->structure_page('','theme')."/html.php";
        }
        public function api_office() {
            header('Access-Control-Allow-Origin: *');
            if ($_POST['key']!='') header('Content-Type: application/json'); // это для мобильной версии, заголовок обязательный

            if ($_REQUEST['rzd'] > 0) { // если передали раздел
                $row_=$this->pdo->prepare("SELECT   ms.id, ms.fk_structure, m.type_script, m.dirrect
                                            FROM    bk_modules as ms,
                                                    bk_modul as m
                                            WHERE   1=1
                                                    AND ms.fk_modul = m.id
                                                    AND ms.fk_structure= :rzd
                                                    AND m.dirrect= :dirrect
                                                    AND open=1
                                                    AND del=0
                                            ORDER BY sort ASC");
                $row_->execute([':dirrect' => $this->api['modules'], ':rzd' => $_REQUEST['rzd']]);
                $this->modules = $row_->fetch();
            } else if (($_REQUEST['mod'] > 0)&&($this->api['modules'] != 'admin')) { // если передали модуль
                $row_=$this->pdo->prepare("SELECT   ms.id, ms.fk_structure, m.type_script, m.dirrect
                                            FROM    bk_modules as ms,
                                                    bk_modul as m
                                            WHERE   1=1
                                                    AND ms.fk_modul = m.id
                                                    AND ms.id= :mod
                                                    AND m.dirrect= :dirrect
                                                    AND open=1
                                                    AND del=0
                                            ORDER BY sort ASC");
                $row_->execute([':dirrect' => $this->api['modules'], ':mod' => $_REQUEST['mod']]);
                $this->modules = $row_->fetch();
            } else { // если передали только тип модуля
                $row_=$this->pdo->prepare("SELECT * FROM bk_modul WHERE dirrect= :dirrect");
                $row_->execute([':dirrect' => $this->api['modules']]);
                $this->modules = $row_->fetch();
            }
            if ($this->modules['id']>0) {
                $path = "modules".(($this->modules['type_script']==2)?"/admin_cms":"")."/".$this->modules['dirrect']."/";
                $this->modules['dir'] = $path;
                $this->modules['http']= '/'.$path;
                $this->modules['chpu']= $this->structure_page($this->modules['fk_structure'],'chpu');
                $this->modules['chpu_admin']= $this->structure_page($this->modules['fk_structure'],'chpu_admin');
                $modules = $this->modules;
                include $this->modules['dir']."api.php";
                if ($this->api['format'] == 'html') {
                    $this->front_office();
                }
            }
        }
        public function __destruct() {  // По умолчанию, инициализация
            parent::__destruct();
            if ((!$this->api)&&($this->is_access(1))) {
                echo "<!-- Сгенерирована за ".round(microtime(true)-$this->s_time,4)." сек. Запросов к базе: ".$this->col_query." -->";
            }
            // Иногда мне кажется, что компилятор игнорирует все мои комментарии
        }
    }
?>

<?php base::test(); ?>
<?php
    // include "modules/informer/collections/main.php";
    // $news = $informerMain->select(['mid' => 3, 'pageNum' => 1, 'pageLimit' => 3]);
?>
<div class="tesseract_cube">
    <div class="block">
        <div class="shape">
            <div class="cube outer">
                <div class="side left"></div>
                <div class="side right"></div>
                <div class="side top"></div>
                <div class="side bottom"></div>
                <div class="side front"></div>
                <div class="side back"></div>

                <div class="cube">
                    <div class="side left"></div>
                    <div class="side right"></div>
                    <div class="side top"></div>
                    <div class="side bottom"></div>
                    <div class="side front"></div>
                    <div class="side back"></div>
                </div>
            </div>
        </div>
    </div>
</div>

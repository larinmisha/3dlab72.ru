$( document ).ready(function() {
    function list(page, date) {
        if (page == 1) {
            history.pushState(null, null, "//"+location.hostname+URL+"?page="+page);
        } else {
            history.pushState(null, null, "//"+location.hostname+URL+"?page="+page);
        }
        $('.js__block-loader').show();

        var formData = new FormData();
        formData.append('pageLimit',9);
        $('.js__filter.active').each(function(i,elem) {
            formData.delete('category_'+$(this).data('filter'));
            if (($(this).data('filter') > 0)&&($(this).data('item') > 0)) {
                formData.append('category_'+$(this).data('filter'), $(this).data('item'));
            }
        });

        formData.append('pageNum',  page);
        main.ajaxRequest('/api/informer/main/select/json', formData)
            .done(function( json ) {
                if (json!=null) {
                    $('.js-container__reestrs').html(template(json));
                }
                $('.js__block-loader').fadeOut(400);
            });
    }
    if ($("#js-template__reestrs").html()!=undefined) {
        var template = Handlebars.compile($("#js-template__reestrs").html());
        if ($.urlParam('page') > 0) {
            list($.urlParam('page'));
        } else {
            list(1);
        }
    }
    $('body').on('click', '.js__table-refresh', function(e) {
        if ($(this).data('page') > 0) {
            list($(this).data('page'));
        }
    });
    $('body').on('click', '.js__filter', function(e) {
        $('.js__filter').removeClass('active');
        $(this).addClass('active');
        list(1);
    });
});

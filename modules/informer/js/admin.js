$(function() {
    var active_page;

    var locationInfo = document.getElementById('location');
    function init () {
        $.each($('.js-maps'), function( index, value ) {
            field = $(this).data('field');
            if ($('#'+field).val() != '') {
                mark  = JSON.parse($('#'+field).val());
            } else {
                mark = [57.147902, 65.531925];
            }

            DG.then(function () {
                var map,
                    marker;

                map = DG.map('map_'+field, {
                    center: mark,
                    zoom: 15
                });

                marker = DG.marker(mark, {
                    draggable: true
                }).addTo(map);

                marker.on('drag', function(e) {
                    var lat = e.target._latlng.lat.toFixed(3),
                        lng = e.target._latlng.lng.toFixed(3);
                    $('#'+field).val('['+lat+','+lng+']');
                });
            });
        });
    }
    function menu() {
        var formData = new FormData();
        formData.append('id', $(this).data('id'));
        main.ajaxRequest('/api/informer/tpl/select/json', formData)
            .done(function( json ) {
                if (json != null) {
                    $('.js-container__menu').html(template_menu(json));
                    if (json.action != null) {
                        if ($.urlParam('view') > 0) {
                            card($.urlParam('view'));
                        } else {
                            if ($.urlParam('page') > 0) {
                                list($.urlParam('page'));
                            } else {
                                list(active_page);
                            }
                        }
                    }
                }
            });
    }
    function list(page) {
        if (page == undefined) {
            page = 1
        }
        active_page = page;
        var formData = new FormData();
        formData.append('page', page);
        main.ajaxRequest('/api/informer/admin/select/json', formData)
            .done(function( json ) {
                if (json != null) {
                    $('.js-container__reestr').html(template(json));
                }
                if (page > 1) {
                    history.pushState(null, null, location.origin+location.pathname+"?page="+page);
                } else {
                    history.pushState(null, null, location.origin+location.pathname);
                }
            });
    }
    function card(id) {
        var template_card = Handlebars.compile($("#js-template__card").html());

        var formData = new FormData();
        formData.append('id', id);
        main.ajaxRequest('/api/informer/admin/record/json', formData)
            .done(function( json ) {
                if (json != null) {
                    $('.js-container__reestr').html(template_card(json));
                    TinyMCE();
                    ymaps.ready(init);
                    $('.js-upload').each(function(i,elem) {
                        designer.init(this);
                    });
                    $('.js-informer__datepicker').datepicker({dateFormat: 'yyyy-mm-dd', timeFormat: 'hh:ii:00'}).each(function() {
                        if ($(this).val() != '') {
                            $(this).data('datepicker').selectDate(new Date($(this).val()));
                        } else if ($(this).data('empty') == true) {
                            $(this).data('datepicker').selectDate(new Date());
                        }
                    });
                    if (id > 0) {
                        history.pushState(null, null, location.origin+location.pathname+"?view="+id);
                    } else {
                        history.pushState(null, null, location.origin+location.pathname);
                    }
                }
            });
    }
    if ($("#js-template__reestr").html()!=undefined) {
        var template_menu = Handlebars.compile($("#js-template__menu").html());
        var template = Handlebars.compile($("#js-template__reestr").html());
        menu();
    }
    $('body').on('click', '.js__table-refresh', function(e) {
        list($(this).data('page'));
    });

    $('body').on('click', '.js-informer__api', function(e) {
        this_ = this;
        url = $(this).data('url');
        if (url != '') {
            var formData = new FormData();
            for (var i in $(this).data()) {
                if (i != 'url') {
                    formData.append(i, $(this).data(i));
                }
            }

            main.ajaxRequest(url, formData)
                .done(function( json ) {
                    if (url == '/api/informer/uploader/delete/json') {
                        $(this_).parents('.js-block').hide('slow', function(){
                            $(this).remove();
                        });
                    } else {
                        list(active_page);
                    }

                });
        } else {
            console.log('отсутствует URL');
        }
    });
    /*$('body').on('click', '.js-modal__move_save', function(e) {
        var formData = new FormData();
        formData.append('id', $(this).data('id'));
        main.ajaxRequest('/api/informer/admin/move_save/json', formData)
            .done(function( json ) {
                list(active_page);
            });
    });*/


    $('body').on('click', '.js__btn-send-email', function(e) {
        if (confirm("Отправить email рассылку подписчикам?")) {
            var formData = new FormData();
            formData.append('id', $(this).data('id'));
            main.ajaxRequest('/api/informer/admin/send_email/json', formData)
                .done(function( json ) {
                    list(active_page);
                });
        }
    });

    $('body').on('click', '.js__btn-email-status', function(e) {
        var formData = new FormData();
        formData.append('id', $(this).data('id'));
        main.ajaxRequest('/api/informer/admin/email_status/json', formData)
            .done(function( json ) {
                modal_doc_max("Статус рассылки", json, "min", "");
            });
    });

    $('body').on('click', '.js__btn-delete', function(e) {
        if (confirm("Удалить запись?")) {
            var formData = new FormData();
            formData.append('id', $(this).data('id'));
            main.ajaxRequest('/api/informer/admin/delete/json', formData)
                .done(function( json ) {
                    list(active_page);
                });
        }
    });

    $('body').on('click', '.js__btn-display', function(e) {
        var formData = new FormData();
        formData.append('id', $(this).data('id'));
        main.ajaxRequest('/api/informer/admin/setDisplay/json', formData)
            .done(function( json ) {
                list(active_page);
            });
    });

    $('body').on('click', '.js__btn-top', function(e) {
        var formData = new FormData();
        formData.append('id', $(this).data('id'));
        main.ajaxRequest('/api/informer/admin/setTop/json', formData)
            .done(function( json ) {
                list(active_page);
            });
    });

    $('body').on('click', '.js__btn-sort', function(e) {
        var formData = new FormData();
        formData.append('id', $(this).data('id'));
        main.ajaxRequest('/api/informer/admin/setSort'+$(this).data('route')+'/json', formData)
            .done(function( json ) {
                list(active_page);
            });
    });

    $('body').on('click', '.js-modal__move_select', function(e) {
        var formData = new FormData();
        formData.append('id', $(this).data('id'));
        main.ajaxRequest('/api/informer/admin/move_select/json', formData)
            .done(function( json ) {
                list(active_page);
            });
    });

    $('body').on('click', '.js-modal__card', function(e) {
        card($(this).data('id'));
    });
    $('body').on('click', '.js-modal__card-save', function(e) {
        var formData = main.fetchformData('#js-modal__card-form');
        main.ajaxRequest('/api/informer/admin/save/json', formData)
            .done(function(json) {
                if ((json.data)&&(json.data.success)) {
                    list(active_page);
                }
            });
    });
    $('body').on('change', '.js-modal__card-save-item', function(e) {
        var formData = new FormData();
        formData.append('fk_item',     $(this).data('item'));
        formData.append('fk_informer', $(this).data('informer'));
        formData.append('checked',  $(this).is(':checked'));
        main.ajaxRequest('/api/informer/admin/saveItem/json', formData)
            .done(function( json ) {

            });
    });

    $('body').on('click', '.js-modal__history', function(e) {
        var template_history = Handlebars.compile($("#js-template__history").html());

        modal_doc_max("История изменений",'<div class="js-container__history"></div>','lg','');

        var formData = new FormData();
        formData.append('id', $(this).data('id'));
        main.ajaxRequest('/api/informer/admin/history/json', formData)
            .done(function( json ) {
                $('.js-container__history').html('');
                if ((json!=null)&&(json.data!=null)) {
                    var html = template_history(json.data);
                    $('.js-container__history:last').append(html);
                }
            });
    });

    $('body').on('click', '.js__btn-yaTurbo-send', function(e) {
        var formData = new FormData();
        formData.append('id',   $(this).data('id'));
        formData.append('turbo',   $(this).data('turbo'));
        main.ajaxRequest('/api/informer/yaTurbo/send/json', formData)
            .done(function( json ) {
                list(active_page);
                modal_doc_max("Изменения отправлены в Яндекс", 'Необходимо время на модерацию.', "min", "");
            });
    });

    $('body').on('click', '.js-informer__template', function(e) {
        var formData = new FormData();
        formData.append('fk_tpl', $(this).data('id'));
        main.ajaxRequest('/api/informer/tpl/update/json', formData)
            .fail(function(jqXHR) {
                if (jqXHR.status == 401) { // email and password is not valid
                    text_code = "Не верный логин или пароль";
                } else if (jqXHR.status == 403) { // limit exceeded
                    text_code = "Превышен лимит";
                }
            })
            .done(function(json) {
                menu();
            });
    });
});

$(function() {
    if($("div").is("#map")) {
        DG.then(function() {
            markers = DG.featureGroup();
            var map = DG.map('map', {
                center: [57.152007, 65.531581],
                zoom: 11
            });

            i = 0;
            $.each($('.js-block__maps'), function( index, value ) {
                if ($(this).data('marks') != '') {
                    var popup = DG.popup({sprawling: true, minWidth: 400})
                                .setLatLng($(this).data('marks'))
                                .setContent("<div class='mb0_5'><b>"+$(this).find('.js-title').html()+"</b></div>"+
                                            "<div class='mb0_5'>"+$(this).find('.js-trener').html()+"</div>"+
                                            "<a href='"+$(this).find('a').attr('href')+"'>Подробно</a>");


                    DG.marker($(this).data('marks'))
                        .addTo(markers)
                        .bindPopup(popup);
                    i = i + 1;
                }
            });
            if (i == 0) {
                if ($('#map').data('mark') != '') {
                    var popup = DG.popup({sprawling: true, minWidth: 400})
                                .setLatLng($('#map').data('mark'))
                                .setContent("<div class='mb0_5'><b>"+$(this).find('.js-title').html()+"</b></div>"+
                                            "<div class='mb0_5'>"+$(this).find('.js-trener').html()+"</div>"+
                                            "<a href='"+$(this).find('a').attr('href')+"'>Подробно</a>");


                    DG.marker($('#map').data('mark'))
                        .addTo(markers)
                        .bindPopup(popup);
                    map.panTo($('#map').data('mark'));
                }
            }
            markers.addTo(map);
            map.fitBounds(markers.getBounds());
        });
    }
});
$('body').on('click', '.js-informer__google', function(e) {
    tpl_informer__google = Handlebars.compile($("#js__informer-goople").html());
    var html = tpl_informer__google({title: $(this).data('title'), url: $(this).data('url')});
    $('.js-modal__block').html(html);
    $('#myModal_informer__google').modal().css({'heigth': '800px'});
});

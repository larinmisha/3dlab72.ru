<?php
    base::test();
    include_once "modules/informer/collections/main.php";
    if ($_GET['page']=='') $_GET['page'] = 1;
    $arrayData = $informerMain->select(['mid' => $this->modules['id'], 'pageNum' => $_GET['page'], 'pageLimit' => 9]);
    $this->meta['robots'] = "all, nofollow, noarchive";
?>
<?php if (count($arrayData)>0) { ?>
    <div class='tpl__files mb1'>
        <div class="container">
            <?php foreach ($arrayData['row'] as $row) { ?>
                <?php if (count($row['fields']['file'])>0) { ?>
                    <?php foreach ($row['fields']['file'] as $file) { ?>
                        <div class="files pt0_75 pr0_75 pb0_75 pl0_75 mb0_75">
                            <?php
                                if ($file['ext'] != 'pdf') {
                                    $url = 'https://docs.google.com/viewer?url='.$file['url'].'&embedded=true';
                                } else {
                                    $url = $file['url'];
                                }
                            ?>
                            <div class="logo">
                                <img src="<?php echo $file['icon_ext']; ?>">
                            </div>
                            <div class="info">
                                <div class="title">
                                    <a href="<?php echo $url; ?>" target="_blank">
                                        <?php if ($row['fields']['publication_date'] != '') { ?>
                                            <?php echo $row['fields']['publication_date']; ?>&nbsp;/&nbsp;
                                        <?php } ?>
                                        <?php echo $row['fields']['title']; ?>
                                    </a>
                                </div>
                                <div class="line">
                                    <span>[<?php echo $file['ext']; ?>, <?php echo $file['size']; ?>]</span>
                                    <div class="line_btn">
                                        <a class="link" href="<?php echo $file['url']; ?>" target="_blank">Посмотреть</a>
                                        <a class="link" href="<?php echo $file['url']; ?>" download>Скачать</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                <?php } ?>
            <?php } ?>
            <?php if (count($arrayData['page']) > 0) { ?>
                <div class="pagination">
                    <?php foreach ($arrayData['page'] as $page) { ?>
                        <a  href="<?php echo $arrayData['url']."?page=".$page['page']; ?>"
                            class=" <?php if ($page['active']) { ?>active<?php } else { ?><?php if ($page['page'] == '') { ?>disabled<?php } ?><?php } ?>" >
                            <?php if ($page['char']) { ?>
                                <?php echo $page['char']; ?>
                            <?php } else { ?>
                                <?php echo $page['page']; ?>
                            <?php } ?>
                        </a>
                    <?php } ?>
                </div>
            <?php } ?>
        </div>
    </div>
<?php } ?>

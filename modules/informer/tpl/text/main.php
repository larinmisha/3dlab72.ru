<?php
    base::test();
    include_once "modules/informer/collections/main.php";
    $arrayData = $informerMain->select(['mid' => $this->modules['id'], 'limit' => 100]);
    $this->meta['robots'] = "all, nofollow, noarchive";
?>
<?php if (count($arrayData)>0) { ?>
    <?php foreach ($arrayData['row'] as $row) { ?>
        <div class='tpl__text'>
            <div class="container">
                <h3><?php echo $row['fields']['title']; ?></h3>
                <?php if ($row['fields']['text'] != '') { ?>
                    <div class="text pb1">
                        <?php echo $row['fields']['text']; ?>
                    </div>
                <?php } ?>
                <?php if (count($row['fields']['file'])>0) { ?>
                    <div class="mb1">
                        <?php foreach ($row['fields']['file'] as $file) { ?>
                            <div class="files pt0_75 pr0_75 pb0_75 pl0_75 mb0_75">
                                <?php
                                    if ($file['ext'] != 'pdf') {
                                        $url = 'https://docs.google.com/viewer?url='.$file['url'].'&embedded=true';
                                    } else {
                                        $url = $file['url'];
                                    }
                                ?>
                                <div class="logo">
                                    <img src="<?php echo $file['icon_ext']; ?>">
                                </div>
                                <div class="info">
                                    <div class="title">
                                        <a href="<?php echo $url; ?>" target="_blank">
                                            <?php if ($row['fields']['publication_date'] != '') { ?>
                                                <?php echo $row['fields']['publication_date']; ?>&nbsp;/&nbsp;
                                            <?php } ?>
                                            <?php echo $row['fields']['title']; ?>
                                        </a>
                                    </div>
                                    <div class="line">
                                        <span>[<?php echo $file['ext']; ?>, <?php echo $file['size']; ?>]</span>
                                        <div class="line_btn">
                                            <a class="link" href="<?php echo $file['url']; ?>" target="_blank">Посмотреть</a>
                                            <a class="link" href="<?php echo $file['url']; ?>" download>Скачать</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                <?php } ?>
                <?php if (count($row['fields']['node'])>0) { ?>
                    <?php $node = $row['fields']['node']; ?>
                    <?php include_once dirname(__FILE__).'/../../../admin_cms/tree_node/tpl/node.php'; ?>
                <?php } ?>
            </div>
        </div>
    <?php } ?>
<?php } ?>

<script type="text/x-handlebars-template" id="js__informer-goople">
<div class="modal fade" id="myModal_informer__google" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class='modal-body'>
                <button type='button' class='close' data-dismiss='modal'>
                    <span aria-hidden='true'>&times;</span>
                </button>
                <div class="embed-responsive embed-responsive-4by3">
                    <iframe src=https://docs.google.com/viewer?url={{url}}&amp;embedded=true width='100%' height='100%' style='border: none;'></iframe>
                </div>
            </div>
        </div>
    </div>
</div>
</script>

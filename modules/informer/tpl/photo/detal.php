<?php
    base::test();
    include_once "modules/informer/collections/main.php";
    $row =  $informerMain->get_record_id(['id' => $_GET['view']]); // 'modules' => $this->modules,
    $this->meta['robots'] = "all, nofollow, noarchive";
    $this->meta['title'] = "Фотогалерея".' '.$row['fields']['publication_date'].' г.';
?>
<?php if (count($row)>0) { ?>
    <div class='tpl__photo__detal'>
        <div class="container">
            <div class="grid">
                <div class="left hidden-xs">&nbsp;</div>
                <div class="center">
                    <h2 class="pb1_75"><?php echo $row['fields']['title']; ?></h2>
                    <?php if (count($row['fields']['img']) > 0) { ?>
                        <div class="gallery js-gallery">
                            <?php foreach ($row['fields']['img'] as $img) { ?>
                                <a href='<?php echo $img['url']; ?>' class="js-item">
                                    <img src="<?php echo $img['url']; ?>" class='hidden' alt="<?php echo $img['title']; ?>">
                                    <div class="img js-sq" data-sq="0.66" style="background: url(<?php echo $img['url']; ?>) no-repeat center center / cover #eee"></div>
                                </a>
                            <?php } ?>
                        </div>
                    <?php } ?>
                    <?php if ($row['fields']['text'] != '') { ?>
                        <div class="text pb1">
                            <?php echo $row['fields']['text']; ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
<?php } else { ?>
    <?php http_response_code(404); ?>
    <div class="container">
        <div class="b-browser__wrapper pt4 pr4 pb4 pl4">
            <h1 class="pb1">Ошибка 404</h1>
            <p>Извините! Страница, которую Вы ищете, не может быть найдена</p>
            <p>Возможно, запрашиваемая Вами страница была перенесена или удалена. Также возможно, Вы допустили небольшую опечатку при вводе адреса – такое случается даже с нами, поэтому еще раз внимательно проверьте</p>
            <p>Переходите на <a href='/'>главную страницу</a> – там Вы также сможете найти много полезной информации</p>
        </div>
    </div>
<?php } ?>

<?php
    base::test();
    include_once "modules/informer/collections/main.php";
    $mas_node = $informerMain->select(['mid' => $this->modules['id'], 'limit' => 100]);
    foreach ($mas_node['row'] as $row) {
        $node[] = [ 'url' => $row['fields']['url'],
                    'title' => $row['fields']['title']
                ];
    }
?>
<div class="tpl__node pb2 pt2">
    <?php if (count($node)>0) { ?>
        <div class="container">
            <h2 class="pb1_75">Полезные ссылки</h2>
            <?php include_once dirname(__FILE__).'/../../../admin_cms/tree_node/tpl/node.php'; ?>
        </div>
    <?php } ?>
</div>

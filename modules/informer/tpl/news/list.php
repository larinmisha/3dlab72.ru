<?php
    base::test();
    include_once "modules/informer/collections/main.php";
    if ($_GET['page']=='') $_GET['page'] = 1;
    $arrayData = $informerMain->select(['mid' => $this->modules['id'], 'pageNum' => $_GET['page'], 'pageLimit' => 9]);
    base::minify_import("modules/informer/js/tpl_news.js");
    $this->meta['robots'] = "noindex";
?>
<?php if (count($arrayData)>0) { ?>
    <div class="tpl__news">
        <div class="container">
            <div class="rotate">
                <div class="left">
                    <div class="js-container__reestrs">
                        <div class="grid mb1_5">
                            <?php foreach ($arrayData['row'] as $row) { ?>
                                <div class="block">
                                    <a href="<?php echo $row['url']; ?>" >
                                        <?php if (count($row['fields']['img']) == 0) { ?>
                                            <?php if ($row['fields']['youtube'] != '') { ?>
                                                <div    class="img js-sq"
                                                        data-sq="0.66"
                                                        style="background-image: url(https://img.youtube.com/vi/<?php echo $row['fields']['youtube'][0]['media']; ?>/0.jpg);">
                                                <?php echo file_get_contents(__DIR__."/../../img/youtube.svg"); ?>
                                                </div>
                                            <?php } else { ?>
                                                <div class="img_none js-sq" data-sq="0.66">
                                                    <?php echo file_get_contents(__DIR__."/../../img/img_none.svg"); ?>
                                                </div>
                                            <?php }  ?>
                                        <?php } else { ?>
                                            <div class="img js-sq" data-sq="0.66" style="background-image: url(<?php echo $row['fields']['img'][0]['url']; ?>);">
                                            </div>
                                        <?php }  ?>
                                        <div class="info mt0_5 mr0_5 mb0_5 ml0_5">
                                            <div class="time mb0_25"><?php echo $row['fields']['publication_date']; ?></div>
                                            <div class="title mb0_5">
                                                <?php if ($row['top'] == 1) { ?>
                                                    <?php //echo file_get_contents(__DIR__."/../../img/pin.svg"); ?>
                                                <?php } ?>
                                                <?php echo $row['fields']['title']; ?>
                                            </div>
                                            <div class="category">
                                                <?php echo $row['fields']['category']; ?>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            <?php } ?>
                        </div>
                        <?php if (count($arrayData['page']) > 0) { ?>
                            <div class="pagination">
                                <?php foreach ($arrayData['page'] as $page) { ?>
                                    <a  href="<?php echo $arrayData['url']."?page=".$page['page']; ?>"
                                        class=" <?php if ($page['active']) { ?>active<?php } else { ?><?php if ($page['page'] == '') { ?>disabled<?php } ?><?php } ?>" >
                                        <?php if ($page['char']) { ?>
                                            <?php echo $page['char']; ?>
                                        <?php } else { ?>
                                            <?php echo $page['page']; ?>
                                        <?php } ?>
                                    </a>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="right visible-lg">
                    <div class="filter">
                        <?php if (count($arrayData['filter']) > 0) { ?>
                            <?php foreach ($arrayData['filter'] as $filter) { ?>
                                <div class="title pb0_5">
                                    <?php echo $filter['title'] ?>
                                </div>
                                <?php if (count($filter['item']) > 0) { ?>
                                    <?php foreach ($filter['item'] as $item) { ?>
                                        <div class="link pb0_5 js__filter" data-filter="<?php echo $filter['id'] ?>" data-item="<?php echo $item['id'] ?>">
                                            <?php echo $item['item']; ?>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                                <div class="link pb0_5 js__filter active">
                                    По всем категориям
                                </div>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/x-handlebars-template" id="js-template__reestrs">
        <div class="grid mb1_5">
            {{#each row}}
                <div class="block">
                    <a href="{{url}}">
                        {{#if fields.img}}
                            <div class="img js-sq" data-sq="0.66" style="background-image: url({{fields.img.[0].url}});"></div>
                        {{else}}
                            {{#if fields.youtube}}
                                <div    class="img js-sq"
                                        data-sq="0.66"
                                        style="background-image: url(https://img.youtube.com/vi/{{fields.youtube.0.media}}/0.jpg);">
                                <?php echo file_get_contents(__DIR__."/../../img/youtube.svg"); ?>
                                </div>
                            {{else}}
                                <div class="img_none js-sq" data-sq="0.66">
                                    <?php echo file_get_contents(__DIR__."/../../img/img_none.svg"); ?>
                                </div>
                            {{/if}}
                        {{/if}}
                        <div class="info mt0_5 mr0_5 mb0_5 ml0_5">
                            <div class="time mb0_25">{{fields.publication_date}}</div>
                            <div class="title mb0_5">
                                {{#if top}}
                                    <?php // echo file_get_contents(__DIR__."/../../img/pin.svg"); ?>
                                {{/if}}
                                {{{fields.title}}}
                            </div>
                            <div class="category">
                                {{fields.category}}
                            </div>
                        </div>
                    </a>
                </div>
            {{/each}}
        </div>
        {{#if page}}
            <div class="pagination">
                {{#each page}}
                    <a  href="JavaScript:" data-page="{{page}}"
                        class="{{#if active}}active{{else}} {{#compare page undefined operator="=="}}disabled{{else}}js__table-refresh{{/compare}} {{/if}}" >
                        {{#if char}}{{{char}}}{{else}}{{page}}{{/if}}
                    </a>
                {{/each}}
            </div>
        {{/if}}
    </script>
<?php } ?>

<?php
    base::test();
    include_once "modules/informer/collections/main.php";
    $row =  $informerMain->get_record_id(['id' => $_GET['view']]); // 'modules' => $this->modules,
    $list = $informerMain->select(['mid' => $this->modules['id'], 'pageNum' => 1, 'pageLimit' => 3]);
    $this->meta['robots'] = "all, nofollow, noarchive";
    $this->meta['title'] = $row['fields']['category'].' '.$row['fields']['publication_date'].' г.';
?>
<?php if (count($row)>0) { ?>
    <div class='tpl__news__detal'>
        <div class="container">
            <div class="grid">
                <div class="left hidden-xs">&nbsp;</div>
                <div class="center">
                    <h2 class="pb1_75"><?php echo $row['fields']['title']; ?></h2>
                    <?php if (count($row['fields']['img']) > 0) { ?>
                        <div class="js-gallery js-sq pb1" data-sq="0.65">
                            <div class="swiper-container swiper-container__news">
                                <div class="swiper-wrapper">
                                    <?php foreach ($row['fields']['img'] as $img) { ?>
                                        <div class="swiper-slide">
                                            <a href='<?php echo $img['url']; ?>' class="js-item">
                                                <img src="<?php echo $img['url']; ?>" class='hidden' alt="<?php echo $img['title']; ?>">
                                                <div style="background: url(<?php echo $img['url']; ?>) no-repeat center center / cover #eee"></div>
                                            </a>
                                        </div>
                                    <?php } ?>
                                </div>
                                <?php if (count($row['fields']['img']) > 1) { ?>
                                    <div class="swiper-button-next swiper-button-next__news"></div>
                                    <div class="swiper-button-prev swiper-button-prev__news"></div>
                                <?php } ?>
                            </div>
                            <script>
                                var swiper = new Swiper('.swiper-container__news', {
                                    slidesPerView: 1,
                                    spaceBetween: 0,
                                    navigation: {
                                        nextEl: '.swiper-button-next__news',
                                        prevEl: '.swiper-button-prev__news',
                                    }
                                });
                            </script>
                        </div>
                    <?php } ?>
                    <?php if ($row['fields']['text'] != '') { ?>
                        <div class="text pb1">
                            <?php echo $row['fields']['text']; ?>
                        </div>
                    <?php } ?>
                    <?php if (count($row['fields']['youtube'])>0) { ?>
                        <div class="text pb1">
                            <?php foreach ($row['fields']['youtube'] as $youtube) { ?>
                                <div class="block pb1_25">
                                    <div class="embed-responsive embed-responsive-16by9">
                                        <iframe class="embed-responsive-item" src="https://www.youtube-nocookie.com/embed/<?php echo $youtube['media']; ?>?rel=0&showinfo=0"></iframe>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    <?php } ?>
                    <?php if (count($row['fields']['file'])>0) { ?>
                        <?php foreach ($row['fields']['file'] as $file) { ?>
                            <div class="files pt0_75 pr0_75 pb0_75 pl0_75 mb0_75">
                                <?php
                                    if ($file['ext'] != 'pdf') {
                                        $url = 'https://docs.google.com/viewer?url='.$file['url'].'&embedded=true';
                                    } else {
                                        $url = $file['url'];
                                    }
                                ?>
                                <div class="logo">
                                    <img src="<?php echo $file['icon_ext']; ?>">
                                </div>
                                <div class="info">
                                    <div class="title">
                                        <a href="<?php echo $url; ?>" target="_blank">
                                            <?php if ($row['fields']['publication_date'] != '') { ?>
                                                <?php echo $row['fields']['publication_date']; ?>&nbsp;/&nbsp;
                                            <?php } ?>
                                            <?php echo $row['fields']['title']; ?>
                                        </a>
                                    </div>
                                    <div class="line">
                                        <span>[<?php echo $file['ext']; ?>, <?php echo $file['size']; ?>]</span>
                                        <div class="line_btn">
                                            <a class="link" href="<?php echo $file['url']; ?>" target="_blank">Посмотреть</a>
                                            <a class="link" href="<?php echo $file['url']; ?>" download>Скачать</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>
                <div class="right hidden-xs">
                    <?php if (count($list['row']) > 0) { ?>
                        <?php foreach ($list['row'] as $row) { ?>
                            <div class="list pb1">
                                <a href="/">
                                    <div class="data"><?php echo $row['fields']['publication_date']; ?></div>
                                    <div class="title"><?php echo $row['fields']['title']; ?></div>
                                </a>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
<?php } else { ?>
    <?php http_response_code(404); ?>
    <div class="container">
        <div class="b-browser__wrapper pt4 pr4 pb4 pl4">
            <h1 class="pb1">Ошибка 404</h1>
            <p>Извините! Страница, которую Вы ищете, не может быть найдена</p>
            <p>Возможно, запрашиваемая Вами страница была перенесена или удалена. Также возможно, Вы допустили небольшую опечатку при вводе адреса – такое случается даже с нами, поэтому еще раз внимательно проверьте</p>
            <p>Переходите на <a href='/'>главную страницу</a> – там Вы также сможете найти много полезной информации</p>
        </div>
    </div>
<?php } ?>

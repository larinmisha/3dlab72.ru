<?php base::test(); ?>
<script type="text/x-handlebars-template" id="js-template__menu">
    {{#if action}}
        <div class="btn-group btn-group-justified" role="group" aria-label="...">
            <div class="btn-group" role="group">
                <button type="button" class="btn btn-xs btn-default js__table-refresh js__table-main" data-id="0">
                    <span class="glyphicon glyphicon-refresh" aria-hidden="true"></span> Реестр
                </button>
            </div>
            <div class="btn-group" role="group">
                <button type="button" class="btn btn-xs btn-default js-modal__card" data-id="0">
                    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Добавить новую запись
                </button>
            </div>
            <div class="btn-group" role="group">
                <button type="button" class="btn btn-xs btn-default">
                    Шаблон: {{action.title}}
                </button>
            </div>
        </div>
        <hr>
    {{else}}
        <h4>Шаг1: Выберите тип шаблона:</h4>
        <div class="mb1">
            {{#each menu}}
                {{#if disable}}
                    <button class="mr0_25 js-informer__template" data-id="{{id}}">
                        {{title}}
                    </button>
                {{/if}}
            {{/each}}
        </div>
    {{/if}}
</script>

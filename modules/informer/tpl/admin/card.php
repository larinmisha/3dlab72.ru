<?php base::test(); ?>
<script type="text/x-handlebars-template" id="js-template__card">
    <form method="POST" id="js-modal__card-form">
        <input type="hidden" class="" name="id" value="{{id}}">
        {{#each fields}}
            <div class="form-group">
                <label for="item_{{id}}">{{title}}</label>

                {{#compare tpl_html "decimal" operator="=="}}
                    <input type="number" class="form-control" name="item_{{id}}" value="{{data}}">
                {{/compare}}
                {{#compare tpl_html "int" operator="=="}}
                    <input type="number" class="form-control" name="item_{{id}}" value="{{data}}">
                {{/compare}}
                {{#compare tpl_html "varchar" operator="=="}}
                    <input type="text" class="form-control" name="item_{{id}}" value="{{data}}">
                {{/compare}}
                {{#compare tpl_html "date" operator="=="}}
                    <input type="date" class="form-control" name="item_{{id}}" value="{{data}}">
                {{/compare}}
                {{#compare tpl_html "datetime" operator="=="}}
                    <input  type="text"
                            class="form-control js-informer__datepicker"
                            name="item_{{id}}"
                            value="{{data}}"
                            autocomplete="off"
                            data-empty="true"
                            data-timepicker="true">
                {{/compare}}
                {{#compare tpl_html "text" operator="=="}}
                    <textarea class="form-control" name="item_{{id}}" rows="10">{{{data}}}</textarea>
                {{/compare}}
                {{#compare tpl_html "text-tinymce" operator="=="}}
                    <textarea class="form-control js-tinymce" name="item_{{id}}" rows="10">{{{data}}}</textarea>
                {{/compare}}
                {{#compare tpl_html "2gis" operator="=="}}
                    <input type="hidden" name="item_{{id}}" id="item_{{id}}" value="{{data}}" data-buf="{{data}}">
                    <div class='js-maps js-sq' data-sq="0.3" id='map_item_{{id}}' data-field='item_{{id}}'></div>
                {{/compare}}
                {{#compare tpl_html "media" operator="=="}}
                    <div class="js-upload" data-id="{{../id}}" data-type="{{type}}"></div>
                {{/compare}}
                {{#compare tpl_html "checkbox" operator="=="}}
                    <ul>
                        {{#each data}}
                            <oi>
                                <input  type="checkbox"
                                        class="js-modal__card-save-item"
                                        data-item="{{id}}"
                                        data-informer="{{../../id}}"
                                        {{#compare checked 1 operator="=="}}checked{{/compare}}
                                >
                                {{item}}<br>
                            </oi>
                        {{/each}}
                    </ul>
                {{/compare}}
                {{#compare tpl_html "radio" operator="=="}}
                    <ul>
                        {{#each data}}
                            <oi>
                                <input  type="radio"
                                        name='item_{{../id}}'
                                        value="{{id}}"
                                        {{#compare checked 1 operator="=="}}checked{{/compare}}
                                >
                                {{item}}<br>
                            </oi>
                        {{/each}}
                    </ul>
                {{/compare}}
            </div>
        {{/each}}
        <div name="btn_work_update" class="btn btn-default btn-xs js-modal__card-save">
            <span class="glyphicon glyphicon-floppy-save" aria-hidden="true"></span>
            Сохранить
        </div>
    </form>
</script>

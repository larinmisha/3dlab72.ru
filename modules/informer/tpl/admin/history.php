<?php base::test(); ?>
<script type="text/x-handlebars-template" id="js-template__history">
    <p>
        <b>Заголовок публикации: </b> {{row.title}}
    </p>
    <table class="table table-hover">
        <thead>
            <tr>
                <th>Пользователь</th>
                <th>Действие</th>
                <th>Время</th>
            </tr>
        </thead>
        <tbody>
            {{#each row}}
                <tr>
                    <td>{{{fio}}}</td>
                    <td>
                        {{#if detal}}
                            <a href="JavaScript:" OnClick="$('.js-history__detal-{{id}}').toggle();">{{info}}</a>
                            {{#if file}}
                                <nobr><a href="{{file.url}}" target="_blank">(id: {{file.id}})</a></nobr>
                            {{/if}}
                            <div class="js-history__detal-{{id}}" style="display: none;">
                                {{#each detal}}
                                      <p>{{text}}</p>
                                      <footer>
                                        <ul>
                                            {{#each old}}
                                                <li>
                                                    </b>Убрал: </b>{{change}}
                                                </li>
                                            {{/each}}
                                            {{#each new}}
                                                <li>
                                                    </b>Добавил: </b>{{change}}
                                                </li>
                                            {{/each}}
                                        </ul>
                                      </footer>
                                {{/each}}
                            </div>
                        {{else}}
                            {{info}}
                            {{#if file}}
                                <nobr><a href="{{file.url}}" target="_blank">(id: {{file.id}})</a></nobr>
                            {{/if}}
                        {{/if}}
                    </td>
                    <td>{{timestamp}}</td>
                </tr>
            {{/each}}
        </tbody>
    </table>
</script>

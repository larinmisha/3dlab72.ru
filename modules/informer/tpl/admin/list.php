<?php base::test(); ?>
<script type="text/x-handlebars-template" id="js-template__reestr">
    <table class="table table-hover">
        <thead>
            <tr>
                <th width="115px">№  п.п.</th>
                {{#each fields}}
                    <td>
                        <nobr>{{this}}</nobr>
                    </td>
                {{/each}}
                <th width="105px">Действие</th>
            </tr>
        </thead>
        <tbody>
        {{#if move}}
            {{#compare move.status 1 operator="=="}}
                <tr class="info">
                    <th>
                        id: {{move.id}}
                    </th>
                    {{#each move.fields}}
                        <td>
                            {{data}}
                        </td>
                    {{/each}}
                    <td>
                        <span class="input-group-btn btn-group-xs">
                            <button type    = 'button'
                                    class   = 'btn btn-xs btn-default js-informer__api'
                                    data-url= '/api/informer/admin/move_save/json'>
                                <i class="glyphicon glyphicon-ok"></i> Перенос&nbsp;
                            </button>
                            <button type    = 'button'
                                    class   = 'btn btn-xs btn-error js-informer__api'
                                    data-url= '/api/informer/admin/move_select/json'>
                                <i class="glyphicon glyphicon-remove"></i>
                            </button>
                        </span>
                    </td>
                </tr>
                <tr class="info">
                    <td colspan='50'>
                        Текуший раздел размещения публикации: {{move.structure}}
                    </td>
                </tr>
            {{else}}
                <tr class="danger">
                    <td colspan='50'>
                        Выбранную публикацию невозможно переместить в данный раздел т.к. типы шаблонов не совпадают<br>
                        Текуший раздел размещения публикации: {{move.structure}}<br>
                        <button type    = 'button'
                                class   = 'btn btn-xs btn-error js-informer__api'
                                data-url= '/api/informer/admin/move_select/json'>
                            Отменить перенос публикации
                        </button>
                    </td>
                </tr>
            {{/compare}}
        {{/if}}
        {{#each row}}
            <tr>
                <th>
                    <span class="input-group ">
                        <span class="input-group-btn btn-group-xs">
                            <button type="button" class="btn btn-default js-modal__card" data-id="{{id}}">
                                <span >{{sort}}</span>
                                <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                            </button>
                            <button type="button" class="btn btn-default dropdown-toggle {{ya_turbo_color}}" data-toggle="dropdown" id="t1" aria-haspopup="true" aria-expanded="false">
                                <svg width="12" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                                    <path d="M7.95 12c.64-.8 1.4-1.67 2.3-2.55 3.43-3.46 7.25-3.67 7.6-3.3.38.36.15 4.15-3.3 7.62-.88.9-1.74 1.66-2.55 2.3v2.43c0 1 .03 1.03-.6 1.5-.34.25-1.8 1.44-3.36 2.4-.38.23-.7.04-.45-.3A12.4 12.4 0 0 0 9 19.6c.28-.66.28-1.26 0-1.78-1 .32-1.82.22-2.43-.37-.62-.6-.7-1.44-.38-2.45a2.03 2.03 0 0 0-1.8 0c-.76.28-1.58.76-2.5 1.4-.36.26-.55-.06-.3-.44.94-1.56 2.13-3 2.4-3.35.45-.6.5-.6 1.5-.6h2.43zm-3.07 5.4c.2.22-2.6 1.44-2.6 4.3 2.88 0 4.1-2.78 4.32-2.57.2.2-.47 1.83-1.62 2.97A6.63 6.63 0 0 1 0 24c0-2.3.75-3.84 1.9-4.98 1.15-1.15 2.77-1.82 2.98-1.6zM1.05 12c0 .48-.6.52-.6 0C.52 5.37 4.95 0 12 0c7.05 0 12 4.95 12 12s-5.37 11.5-12 11.55c-.23 0-.33-.15-.33-.3s.1-.3.33-.3c5.55 0 10.2-4.95 10.2-10.95C22.2 6 18 1.8 12 1.8 6 1.8 1.13 5.85 1.05 12z"></path>
                                </svg>
                            </button>

                            <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="t1">
                                <li class="dropdown-header">
                                    Турбо-страница<br>
                                    Статус: {{ya_turbo_title}}
                                </li>
                                <li role="separator" class="divider"></li>
                                {{#if ya_turbo_link}}
                                    <li>
                                        <a href="{{ya_turbo_link}}" target="_blank">
                                            <span class="glyphicon glyphicon-link" aria-hidden="true"></span>
                                            Посмотреть созданную страницу
                                        </a>
                                    </li>
                                {{/if}}
                                {{#if ya_turbo_url_status}}
                                    <li>
                                        <a href="{{ya_turbo_url_status}}" target="_blank">
                                            <span class="glyphicon glyphicon-link" aria-hidden="true"></span>
                                            Проверка статуса
                                        </a>
                                    </li>
                                {{/if}}
                                <li>
                                    <a href="JavaScript:" class="js__btn-yaTurbo-send" data-id="{{id}}" data-turbo="true">
                                        <svg width="14" height ="16" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                                            <path d="M7.95 12c.64-.8 1.4-1.67 2.3-2.55 3.43-3.46 7.25-3.67 7.6-3.3.38.36.15 4.15-3.3 7.62-.88.9-1.74 1.66-2.55 2.3v2.43c0 1 .03 1.03-.6 1.5-.34.25-1.8 1.44-3.36 2.4-.38.23-.7.04-.45-.3A12.4 12.4 0 0 0 9 19.6c.28-.66.28-1.26 0-1.78-1 .32-1.82.22-2.43-.37-.62-.6-.7-1.44-.38-2.45a2.03 2.03 0 0 0-1.8 0c-.76.28-1.58.76-2.5 1.4-.36.26-.55-.06-.3-.44.94-1.56 2.13-3 2.4-3.35.45-.6.5-.6 1.5-.6h2.43zm-3.07 5.4c.2.22-2.6 1.44-2.6 4.3 2.88 0 4.1-2.78 4.32-2.57.2.2-.47 1.83-1.62 2.97A6.63 6.63 0 0 1 0 24c0-2.3.75-3.84 1.9-4.98 1.15-1.15 2.77-1.82 2.98-1.6zM1.05 12c0 .48-.6.52-.6 0C.52 5.37 4.95 0 12 0c7.05 0 12 4.95 12 12s-5.37 11.5-12 11.55c-.23 0-.33-.15-.33-.3s.1-.3.33-.3c5.55 0 10.2-4.95 10.2-10.95C22.2 6 18 1.8 12 1.8 6 1.8 1.13 5.85 1.05 12z"></path>
                                        </svg>
                                        Публиковать / Изменить
                                    </a>
                                </li>
                                {{#if ya_turbo_color}}
                                    <li role="separator" class="divider"></li>
                                    <li>
                                        <a href="JavaScript:" class="js__btn-yaTurbo-send" data-id="{{id}}" data-turbo="false">
                                            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                            Удалить Турбо-страницу
                                        </a>
                                    </li>
                                {{/if}}
                            </ul>
                        </span>
                        {{#if send_email}}
                            <nobr>&nbsp;<span class="glyphicon glyphicon-envelope" aria-hidden="true"></span></nobr>
                        {{/if}}
                    </span>
                </th>
                {{#each fields}}
                    <td>
                        {{data}}
                    </td>
                {{/each}}
                <td>
                    <span class="input-group-btn btn-group-xs">
                        <button type="button" class="btn btn-default js__btn-top" data-id="{{id}}">
                            <i class="glyphicon glyphicon glyphicon-star{{#compare top null operator="=="}}-empty{{/compare}}"></i>
                        </button>
                        <button type="button" class="btn btn-{{#compare display null operator="=="}}default{{else}}error{{/compare}} js__btn-display" data-id="{{id}}">
                            <i class="glyphicon glyphicon-eye-{{#compare display null operator="=="}}open{{else}}close{{/compare}}"></i>
                        </button>
                        <button type="button" class="btn btn-default js__btn-sort" data-route="up" data-id="{{id}}">
                            <span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span>
                        </button>
                        <button type="button" class="btn btn-default js__btn-sort" data-route="down" data-id="{{id}}">
                            <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span>
                        </button>

                        <button type="button" class="btn btn-default dropdown-toggle" id="t2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <!-- <span class="caret"></span> -->
                            <span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="t2">

                            <li class="dropdown-header">
                                id# {{id}}
                                {{#if send_email}}
                                    &nbsp; &nbsp;
                                    <span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>
                                    {{send_email}}
                                {{/if}}
                            </li>
                            {{#if send_email}}
                                <li>
                                    <a href="JavaScript:" class="js__btn-email-status" data-id="{{id}}">
                                        <span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> &nbsp;
                                        Статус email рассылки
                                    </a>
                                </li>
                            {{else}}
                                <li>
                                    <a href="JavaScript:" class="js__btn-send-email" data-id="{{id}}">
                                        <span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> &nbsp;
                                        email рассылка
                                    </a>
                                </li>
                            {{/if}}
                            <li>
                                <a href="JavaScript:" class="js-modal__move_select" data-route="up" data-id="{{id}}">
                                    <span class="glyphicon glyphicon-retweet" aria-hidden="true"></span> &nbsp;
                                    Переместить
                                </a>
                            </li>
                            <li>
                                <a href="JavaScript:" class="js-modal__history" data-id="{{id}}">
                                    <span class="glyphicon glyphicon-hourglass" aria-hidden="true"></span> &nbsp;
                                    История изменений
                                </a>
                            </li>
                            <li role="separator" class="divider"></li>
                            <li>
                                <a href="JavaScript:" class="js__btn-delete" data-id="{{id}}">
                                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span> &nbsp;
                                    Подтвердить удаление
                                </a>
                            </li>
                        </ul>
                    </span>
                </td>
            </tr>
        {{/each}}
        </tbody>
    </table>
    {{#if page}}
        <div class="pagination">
            {{#each page}}
                <a  href="JavaScript:" data-page="{{page}}"
                    class="{{#if active}}active{{else}} {{#compare page undefined operator="=="}}disabled{{else}}js__table-refresh{{/compare}} {{/if}}" >
                    {{#if char}}{{{char}}}{{else}}{{page}}{{/if}}
                </a>
            {{/each}}
        </div>
    {{/if}}
</script>

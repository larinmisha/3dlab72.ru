<?php base::test();

    class Uploader extends Api {
        /**
         *  Check if input string is a valid YouTube URL
         *  and try to extract the YouTube Video ID from it.
         *  @author  Stephan Schmitz <eyecatchup@gmail.com>
         *  @param   $url   string   The string that shall be checked.
         *  @return  mixed           Returns YouTube Video ID, or (boolean) false.
         */
        public function parse_yturl($url='') {
            $pattern = '#^(?:https?://)?';    # Optional URL scheme. Either http or https.
            $pattern .= '(?:www\.)?';         #  Optional www subdomain.
            $pattern .= '(?:';                #  Group host alternatives:
            $pattern .=   'youtu\.be/';       #    Either youtu.be,
            $pattern .=   '|youtube\.com';    #    or youtube.com
            $pattern .=   '(?:';              #    Group path alternatives:
            $pattern .=     '/embed/';        #      Either /embed/,
            $pattern .=     '|/v/';           #      or /v/,
            $pattern .=     '|/watch\?v=';    #      or /watch?v=,
            $pattern .=     '|/watch\?.+&v='; #      or /watch?other_param&v=
            $pattern .=   ')';                #    End path alternatives.
            $pattern .= ')';                  #  End host alternatives.
            $pattern .= '([\w-]{11})';        # 11 characters (Length of Youtube video ids).
            $pattern .= '(?:.+)?$#x';         # Optional other ending URL parameters.
            preg_match($pattern, $url, $matches);
            return (isset($matches[1])) ? $matches[1] : false;
        }

        /**
         * Получение записи
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function ext_list($params = null) {
            global $cms;
            $mas = [];
            $mas['defult'] = "/modules/informer/uploader/img/defult.svg";
            $row_ = $cms->pdo->query("  SELECT  *
                                        FROM    bk_informer_ext
                                        Order by name");
            if ($row_->rowCount() > 0) {
                while($row = $row_->fetch()) {
                    $mas['ext'][$row['name']] = "/modules/informer/uploader/img/".$row['svg'];
                    $mas['list'][] = $row['name'];
                }
                $mas['list'] = implode(', ',$mas['list']);
            }
            return $mas;
        }
        /**
         * Получение записи
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function select($params = null) {
            global $cms;
            $ext_list = self::ext_list();
            $mas = [];
            $mas['id']          = $params['id'];
            $mas['type']        = $params['type'];
            $mas['ext_list']    = $ext_list['list'];
            if ($params['display'] != 'true') {
                $wh = " and f.display is NULL ";
            }
            if (is_numeric($_SESSION['informer']['media_move'])) {
                $move_ = $cms->pdo->prepare("   SELECT  i.id, m.fk_structure, i.fk_modules, f.title, f.type, i.timestamp, f.media
                                                FROM    bk_informer as i,
                                                        bk_informer_media as f,
                                                        bk_modules as m
                                                WHERE   1=1
                                                        and m.id            = i.fk_modules
                                                        and f.fk_informer   = i.id
                                                        and f.id            = :id
                                                        and f.type          = :type
                                                        and f.del           is NULL
                                                ");
                $move_->execute([   ':id'   => $_SESSION['informer']['media_move'],
                                    ':type' => $params['type']]);
                if ($move_->rowCount()>0) {
                    $move = $move_->fetch();

                    $fields_ = $cms->pdo->prepare(" SELECT  f.id, f.tpl_html, f.type, f.title,
                                                            d.item_varchar, d.item_text, d.item_datetime, d.item_decimal, d.item_int, d.item_date
                                                    FROM    bk_informer_tpl_modules as m
                                                            left join bk_informer_tpl_fields as f   on  m.fk_tpl = f.fk_tpl
                                                                                                        and f.basic = 1
                                                            left join bk_informer_data as d         on  d.fk_tpl_fields = f.id
                                                                                                        and d.fk_informer = :informer
                                                                                                        and d.fk_users_delete is NULL
                                                    WHERE   1=1
                                                            and m.fk_modules = :mod
                                                    ORDER BY f.sort
                                                ");
                    $fields_->execute([ ':mod'      => $move['fk_modules'],
                                        ':informer' => $move['id']
                                        ]);
                    if ($fields_->rowCount()>0) {
                        while ($fields = $fields_->fetch()) {
                            /*$item_ = $cms->pdo->prepare("   SELECT  fi.*
                                                            FROM    bk_informer_tpl_fields_item as fi
                                                                    left join bk_informer_data_item as i on fi.id = i.fk_fields_item
                                                                                                            and i.fk_informer = :id
                                                            WHERE   1=1
                                                                    and fi.fk_tpl_fields = :fields
                                                                    and i.id > 0
                                                            ORDER BY fi.item ASC");
                            $item_->execute([   ':id'       => $row['id'],
                                                ':fields'   => $fields['id']]);
                            if ($item_->rowCount()>0) {
                                while ($item = $item_->fetch()) {
                                    $fields['item_'.$fields['type']][] = $item['item'];
                                }
                            }*/
                            $move['fields'][] = ['title' => $fields['title'],
                                                'data'  => $fields['item_'.$fields['type']],
                                                ];
                        }
                    }
                    if ($move['title'] =='') {
                        $move['title'] = '-- У файла нет названия --';
                    }
                    $mas['move'] = ['id'        => $params['id'],
                                    'structure' => $cms->structure_page($move['fk_structure'], 'title'),
                                    'fields'    => $move['fields'],
                                    'media'     => $move['title'],
                                    'url'       => "/files/informer/".$move['type']."/".date('Y/m/', strtotime($move['timestamp'])).$move['media']
                                    ];
                }
            }

            $row_ = $cms->pdo->prepare("SELECT  f.*, s.timestamp as ts
                                        FROM    bk_informer as s,
                                                bk_informer_media as f
                                        WHERE   1=1
                                                and f.fk_informer   = s.id
                                                and s.id            = :id
                                                and f.type          = :type
                                                and f.del           is NULL
                                                {$wh}
                                        Order by f.sort, f.id");
            $row_->execute([':id'   => $params['id'],
                            ':type' => $params['type']
                            ]);
            if ($row_->rowCount() > 0) {
                while($row = $row_->fetch()) {
                    $url = null;
                    $ext = null;
                    if($params['type'] == 'youtube') {
                        $url = 'https://www.youtube.com/watch?v='.$row['media'];
                    } else if (in_array($params['type'], ['img', 'file'])) {
                        $url = "/files/informer/".$row['type']."/".date('Y/m/', strtotime($row['ts'])).$row['media'];

                        $r = strtolower(pathinfo($row['media'], PATHINFO_EXTENSION));
                        $ext = (array_key_exists($r, $ext_list['ext'])?$ext_list['ext'][$r]:$ext_list['defult']);
                    }

                    $mas['data'][] = [  'id'            => $row['id'],
                                        'title'         => $row['title'],
                                        'sort'          => $row['sort'],
                                        'display'       => $row['display'],
                                        'media'         => $row['media'],
                                        'ext'           => $ext,
                                        'url'           => $url,
                                        'timestamp'     => $row['timestamp']];
                }
            }
            return $mas;
        }


        /**
         * Смена Title для файла
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function setTitle($params = null) {
            global $cms;
            $mas = [];
            $row_ = $cms->pdo->prepare("UPDATE  bk_informer_media
                                        SET     title   = :title
                                        WHERE   id      = :id");
            $row_->execute([':id'    => $params['id'],
                            ':title' => $params['title']
                            ]);
            return $mas;
        }
        /**
         * Удаление фото
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function delete($params = null) {
            global $cms;
            $mas = [];
            $row_ = $cms->pdo->prepare("UPDATE  bk_informer_media
                                        SET     del = 1
                                        WHERE   id  = :id");
            $row_->execute([':id' => $params['id']]);
            return $mas;
        }

        /**
         * Скрыть/показать фото
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function see($params = null) {
            global $cms;
            $mas = [];
            // можно в один запрос и на if написать в select
            $row_ = $cms->pdo->prepare("SELECT  *
                                        FROM    bk_informer_media
                                        WHERE   id = :id");
            $row_->execute([':id' => $params['id']]);
            $row = $row_->fetch();
            if ($row['display'] == 1) {
                $display = NULL;
            } else {
                $display = 1;
            }
            $row_ = $cms->pdo->prepare("UPDATE  bk_informer_media
                                        SET     display = :display
                                        WHERE   id      = :id");
            $row_->execute([':id'       => $params['id'],
                            ':display'  =>$display]);

            return $mas;
        }

        /**
         * Добавление файла
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function insert($params = null) {
            global $cms;
            $mas = [];

            // Нет файлова
            if (isset($_FILES)) {
                $_FILES = $_FILES[key($_FILES)];
            } else {
                $mas['err'][] = "Не передан файл";
                echo json_encode($mas);
                die();
            }

            // Проверка размера файла
            if(!$_FILES['tmp_name'] != "") {
                $mas['err'][] = "Файл слишком большой: <b>".$_FILES['name']."</b> ";
                echo json_encode($mas);
                die();
            }

            // Нет такого объекта в БД
            $row_=$cms->pdo->prepare("SELECT * FROM bk_informer where id = :id LIMIT 1");
            $row_->execute([':id' => $params['id']]);
            if ($row_->rowCount() > 0) {
                $row = $row_->fetch();
            } else {
                $mas['err'][] = "Неверный идентификатор (".$params['id'].")";
                echo json_encode($mas);
                die();
            }

            // Формат файла (по типу)
            $r = strtolower(pathinfo($_FILES['name'], PATHINFO_EXTENSION));
            // if (!in_array($r, array("jpg", "png", "gif", "bmp", "jpeg", "doc", "docx", "tif", "pdf", "xls", "xlsx", "rtf", "odt", "odp", "ppt", "pptx", "pps", "ppsx", "zip", "rar"))) {
            if (!in_array($r, array("jpg", "png", "jpeg", "pdf", "doc", "docx", "rtf", "xls", "xlsx", "odt", "odp", "ppt", "pptx", "pps", "ppsx"))) {
                    $mas['err'][] = "Не верный формат файла";
                    echo json_encode($mas);
                    die();
                }
            // советую переменную dir не переименовывать, пусть будет такой формат, он универсальный.
            // если понадобиться новый тип файлов в папке то просто передашь новый параметр type и тут его тоже зарегистрируешь условием выше
            $media_name = uniqid('').".".$r;
            $path = base::file_dir(['informer',
                                    $params['type'],
                                    date("Y", strtotime($row['timestamp'])),
                                    date("m", strtotime($row['timestamp'])),
                                    ]);

            $file = $path.$media_name;
            if (copy($_FILES['tmp_name'], $file)) {
                $row_ = $cms->pdo->prepare("INSERT INTO bk_informer_media (`fk_informer`, `media`, `title`, `size`, `type`) VALUES (:id, :media, :title, :size, :type);");
                $row_->bindValue(':id', $params['id'], PDO::PARAM_INT);
                $row_->bindValue(':type', $params['type'], PDO::PARAM_STR);
                if ($params['save_title'] == 'on') {
                    $row_->bindValue(':title', basename($_FILES['name'], ".".$r), PDO::PARAM_STR);
                } else {
                    $row_->bindValue(':title', NULL, PDO::PARAM_NULL);
                }

                if (in_array($params['type'], ['img'])) {
                    include_once 'vendor/verot/class.upload.php/src/class.upload.php';
                    $handle = new \verot\Upload\Upload($file);
                    if ($params['size'] != 'original') {
                        $handle->file_overwrite     = true;
                        $handle->image_no_enlarging = true;
                        $handle->image_ratio        = true;
                        $handle->image_resize       = true;
                        $handle->image_y            = 1080;
                        $handle->image_x            = 1620;
                    }
                    $handle->process($path);
                    $mas['inf'][] = "/".$path.$handle->file_dst_name;
                    $row_->bindValue(':media', $handle->file_dst_name, PDO::PARAM_STR);
                    $row_->bindValue(':size', filesize($handle->file_dst_pathname), PDO::PARAM_INT);
                } else {
                    $img = 'file_defult.png';
                    if (in_array($r, array("xls", "xlsx"))) $img = 'file_excel.png';
                    if (in_array($r, array("doc", "docx"))) $img = 'file_word.png';
                    if (in_array($r, array("jpg", "png", "gif", "bmp", "jpeg"))) $img = 'file_image.png';
                    if (in_array($r, array("pdf"))) $img = 'file_pdf.png';

                    $mas['inf'][] = "/themes/admin/img/format_file/".$img;
                    $row_->bindValue(':media', $media_name, PDO::PARAM_STR);
                    $row_->bindValue(':size', $_FILES['size'], PDO::PARAM_INT);
                }
                $row_->execute();
                $id = $cms->pdo->lastInsertId();
                self::indexSortTable(['id'=>$id]);
            } else {
                $mas['err'][] = "не удалось записать файл: <b>".$_FILES['name']."</b>";
            }
            return $mas;
        }

        /**
         * Добавление видео
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function insert_media($params = null) {
            global $cms;
            $mas = [];

            $row_=$cms->pdo->prepare("SELECT * FROM bk_informer where id = :id LIMIT 1");
            $row_->execute([':id' => $params['id']]);
            if ($row_->rowCount() > 0) {
                $row = $row_->fetch();
            } else {
                $mas['err'][] = "Неверный идентификатор (".$params['id'].")";
                echo json_encode($mas);
                die();
            }

            if ($params['type'] == 'youtube') {
                $params['media'] = self::parse_yturl($params['media']);
            }
            if ($params['media'] == '') {
                $mas['err'][] = "Указаны неверные данные";
            }
            if (count($mas['err'])==0) {
                $row_ = $cms->pdo->prepare("INSERT INTO bk_informer_media (`fk_informer`, `title`, `media`, `type`)
                                            VALUES (:id, :title, :media, :type);");
                $row_->execute([':id'       => $params['id'],
                                ':title'    => $params['title'],
                                ':media'    => $params['media'],
                                ':type'     => $params['type']]);
                self::indexSortTable(['id'=>$cms->pdo->lastInsertId()]);
            }

            return $mas;
        }

        /**
         * Сортировка (Обновить индексы поля sort)
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function indexSortTable($params = null) {
            global $cms;
            $row_ = $cms->pdo->prepare("SELECT  *
                                        FROM    bk_informer_media
                                        WHERE   1=1
                                                and fk_informer in (SELECT  fk_informer
                                                                    FROM    bk_informer_media
                                                                    WHERE   1=1
                                                                            and id = :id)
                                                and del IS NULL
                                        ORDER BY    CASE
                                                        WHEN sort is NULL
                                                        THEN id
                                                        WHEN sort is not NULL
                                                        THEN sort
                                                    END ASC");
            $row_->execute([':id'=>$params['id']]);
            if ($row_->rowCount() > 0) {
                $i = [];
                while ($row = $row_->fetch()) {
                    if ($i[$row['type']] == '') $i[$row['type']] = 1;
                    if ($row['sort'] != $i[$row['type']]) { // если индекс не изменился, то и не обновляем
                        $sort_ = $cms->pdo->prepare("   UPDATE  bk_informer_media
                                                        SET     sort = :sort
                                                        WHERE   id = :id");
                        $sort_->execute([   ':id'   => $row['id'],
                                            ':sort' => $i[$row['type']]
                                            ]);
                    }
                    $i[$row['type']]++;
                }
            }
            return true;
        }

        /**
         * Сортировка (вверх)
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function setSortUp($params = null) {
            global $cms;
            self::indexSortTable($params);
            $row_ = $cms->pdo->prepare("SELECT  *
                                        FROM    bk_informer_media
                                        WHERE   1=1
                                                and del IS NULL
                                                and id  = :id
                                        order by sort, id
                                        ");
            $row_->execute([':id' => $_POST['id']]);
            if ($row_->rowCount() > 0) {
                $row_first = $row_->fetch();
                if ($row_first['sort'] > 1) {
                    $sort_ = $cms->pdo->prepare("   UPDATE  bk_informer_media
                                                    SET     sort = :sort
                                                    WHERE   1=1
                                                            and sort        = :sid
                                                            and fk_informer = :idi");
                    $sort_->execute([   ':sid'  => $row_first['sort']-1,
                                        ':sort' => $row_first['sort'],
                                        ':idi'  => $row_first['fk_informer']
                                        ]);

                    $sort_ = $cms->pdo->prepare("   UPDATE  bk_informer_media
                                                    SET     sort = :sort
                                                    WHERE   1=1
                                                            and id          = :id
                                                            and fk_informer = :idi");
                    $sort_->execute([   ':id'   => $row_first['id'],
                                        ':sort' => $row_first['sort']-1,
                                        ':idi'  => $row_first['fk_informer']
                                        ]);
                }
            }
            // Informer::log(['id'=>$row_first['fk_informer'], 'idf'=>$row_first['id'], 'info'=>'Сортировка файлов (вверх)']);

            return $mas;
        }
        /**
         * Сортировка (вниз)
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function setSortDown($params = null) {
            global $cms;
            self::indexSortTable($params);
            $max_ = $cms->pdo->prepare("SELECT  max(sort) as max
                                        FROM    bk_informer_media
                                        WHERE   1=1
                                                and del IS NULL
                                                and fk_informer in (SELECT  fk_informer
                                                                    FROM    bk_informer_media
                                                                    WHERE   1=1
                                                                            and id = :id)
                                        ");
            $max_->execute([':id'=>$params['id']]);
            $max = $max_->fetch();
            $row_ = $cms->pdo->prepare("SELECT  *
                                        FROM    bk_informer_media
                                        WHERE   1=1
                                                and del IS NULL
                                                and id  = :id
                                        order by sort, id
                                        ");
            $row_->execute([':id' => $params['id']]);
            if ($row_->rowCount() > 0) {
                $row_first = $row_->fetch();
                if ($row_first['sort'] < $max['max']) {
                    $sort_ = $cms->pdo->prepare("UPDATE     bk_informer_media
                                                    SET     sort = :sort
                                                    WHERE   1=1
                                                            and sort        = :sid
                                                            and fk_informer = :idi");
                    $sort_->execute([   ':sid'  => $row_first['sort']+1,
                                        ':sort' => $row_first['sort'],
                                        ':idi'  => $row_first['fk_informer']
                                        ]);

                    $sort_ = $cms->pdo->prepare("UPDATE     bk_informer_media
                                                    SET     sort = :sort
                                                    WHERE   1=1
                                                            and id          = :id
                                                            and fk_informer = :idi");
                    $sort_->execute([   ':id'   => $row_first['id'],
                                        ':sort' => $row_first['sort']+1,
                                        ':idi'  => $row_first['fk_informer']
                                        ]);
                }
            }
            // Informer::log(['id'=>$row_first['fk_informer'], 'idf'=>$row_first['id'], 'info'=>'Сортировка файлов (вниз)']);
            return true;
        }

        /**
         * Переместить файл (старт)
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function move_select($params = null) {
            global $cms;
            $mas = [];
            $row_ = $cms->pdo->prepare("SELECT  *
                                        FROM    bk_informer_media
                                        WHERE   id = :id");
            $row_->execute([':id' => $params['id']]);
            if ($row_->rowCount() > 0) {
                $_SESSION['informer']['media_move'] = $params['id'];
            } else {
                unset($_SESSION['informer']['media_move']);
            }
            return true;
        }

        /**
         * Переместить файл (куда переместить)
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function move_save($params = null) {
            global $cms;
            $move_ = $cms->pdo->prepare("   SELECT  f.type, i.timestamp, f.media
                                            FROM    bk_informer as i,
                                                    bk_informer_media as f
                                            WHERE   1=1
                                                    and f.fk_informer = i.id
                                                    and f.del is NULL
                                                    and f.id = :id
                                            ");
            $move_->execute([':id' => $_SESSION['informer']['media_move']]);
            if ($move_->rowCount() > 0) {
                $move = $move_->fetch();

                $new_ = $cms->pdo->prepare("SELECT  id, timestamp
                                            FROM    bk_informer
                                            WHERE   1=1
                                                    and id = :id
                                            ");
                $new_->execute([':id' => $params['id']]);
                if ($new_->rowCount() > 0) {
                    $new = $new_->fetch();
                    if ((date("Y/m", strtotime($move['timestamp'])) != date("Y/m", strtotime($new['timestamp']))) &&(in_array($move['type'], ['img', 'file']))) {
                        $file_in = base::file_dir(['informer',
                                                    $move['type'],
                                                    date("Y", strtotime($move['timestamp'])),
                                                    date("m", strtotime($move['timestamp'])),
                                                    ]).$move['media'];
                        $file_out= base::file_dir(['informer',
                                                    $move['type'],
                                                    date("Y", strtotime($new['timestamp'])),
                                                    date("m", strtotime($new['timestamp'])),
                                                    ]).$move['media'];
                        rename($file_in, $file_out);
                    }
                    $row_ = $cms->pdo->prepare("UPDATE  bk_informer_media
                                                SET     fk_informer = :fk_informer,
                                                        sort        = 0
                                                WHERE   id          = :id");
                    $row_->execute([':id'           => $_SESSION['informer']['media_move'],
                                    ':fk_informer'  => $new['id']
                                    ]);
                    self::indexSortTable(['id' => $_SESSION['informer']['media_move']]);

                    unset($_SESSION['informer']['media_move']);
                }
            }
            return true;
        }
    }

    $uploader = new Uploader();

    if ($uploader->getAction()!='') { // вызов через api, иначе вызов напрямую через php
        include_once dirname(__FILE__)."/../collections/admin.php";
        $informer = new InformerAdmin();

        echo $uploader->query()->fetch();
    }
?>

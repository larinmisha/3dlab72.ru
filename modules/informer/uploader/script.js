var template_row;
var template_spin;
function designerClass() {}

designerClass.prototype = Object.create(mainClass.prototype);
designerClass.prototype.constructor = designerClass;

designerClass.prototype.init = function(element) {

    $.ajax({url: '/modules/informer/uploader/tpl/spin.template'}) // важно GET
        .done(function( tpl ) {
            template_spin = Handlebars.compile(tpl);
        });
    $.ajax({url: '/modules/informer/uploader/tpl/row.template'}) // важно GET
        .done(function( tpl ) {
            template_row = Handlebars.compile(tpl);
            designer.refresh(element);
            designer.active_events(element);
        });
};
designerClass.prototype.active_events = function(element) {
    $(element).on('click', '.js-upload__api', function(e) {
        this_ = this;
        url = $(this).data('url');
        if (url != '') {
            var formData = new FormData();
            for (var i in $(this).data()) {
                if (i != 'url') {
                    formData.append(i, $(this).data(i));
                }
            }

            main.ajaxRequest(url, formData)
                .done(function( json ) {
                    if (url == '/api/informer/uploader/delete/json') {
                        $(this_).parents('.js-block').hide('slow', function(){
                            $(this).remove();
                        });
                    } else {
                        designer.refresh(element);
                    }

                });
        } else {
            console.log('отсутствует URL');
        }
    });

    $(element).on('click', '.js-block .js-success', function(e) {
        this_ = this;
        $.ajax({
            type: "POST",
            url: '/api/informer/uploader/setTitle/json',
            data: { 'id':    $(this).data('id'),
                    'title': $(this).parents('.js-block').find('textarea').val()
                  }
        }).done(function( json ) {
            $(this_).parents('.js-block').find('.js-success').hide();
        });
    });

    $(element).on('change keyup paste', ' textarea',  function() {
        $(this).parents('.js-block').find('.js-success').show();
    });

    $(element).on('click', '.js-media__save', function() {
        var formData = new FormData();
        formData.append('id',       $(element).data('id'));
        formData.append('type',     $(element).data('type'));
        formData.append('media',    $(element).find('.js-media__media').val());
        main.ajaxRequest('/api/informer/uploader/insert_media/json', formData)
            .done(function( json ) {
                if (json.err != undefined) {
                    jQuery.each( json.err, function( i, val ) {
                        alert(val);
                    });
                }
                designer.refresh(element);
            });
    });
    $(element).on('click', '.js-file__btn', function() {
        f = $(element).find('.js-file__input').get(0);
        if(f.files.length < 1) {
            modal_doc_max('Ошибка','Необходимо выбрать файл для загрузки','md','');
        } else {
            for(var i = 0; i < f.files.length; i++) {
                id = 'js-upload__spin-'+$.now(); // уникальное число, беру unix
                $(element).find('.js-file__status').prepend(template_spin({'id': id, 'name': f.files[i].name}));
                up = new uploader(f.files[i], {
                    url: '/api/informer/uploader/insert/json',
                    // prefix: 'my_name',
                    id: id,
                    post: { 'type'      : $(this).data('type'),
                            'id'        : $(this).data('id'),
                            'save_title': $('.js-file__ch:checked').val(),
                            'size'      : $('.js-file__size').val()
                            },
                });
                up.send();
            }
        }
    });
    $(element).on('change', '.js-file__input', function() {
        var input = $(this),
            numFiles = input.get(0).files ? input.get(0).files.length : 1,
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');

        input.trigger('fileselect', [numFiles, label]);
        $('.js-file__btn').prop('disabled', false);
    });

    function uploader(input, options) {
        var $this = this;
        this.settings = {
            prefix:'file',
            id:'file',
            post: '',
            url:window.location.href,
            onprogress:function(ev){
                pr = ((ev.loaded/ev.total)*100)+'%';
                $('#'+$this.settings.id+'_progress').html(pr);
                $('#'+$this.settings.id+'_progress').css('width',pr);
            },
            error:function(msg){ console.log(msg); },
            success:function(data){
                json = JSON.parse(data);
                if (json !== undefined) {
                    if (json.inf !== undefined) {
                        jQuery.each( json.inf, function( i, val ) {
                            $('#'+$this.settings.id+'_img').attr('src', val);
                            $('#'+$this.settings.id+'_span').html('Файл загружен');
                            $('#'+$this.settings.id+'_progress').removeClass("active");
                            designer.refresh(element);
                            $('#'+$this.settings.id+'_alert').hide('slow', function(){
                                $(this).remove();
                            });
                        });
                    }
                    if (json.err !== undefined) {
                        jQuery.each( json.err, function( i, val ) {
                            $('#'+$this.settings.id+'_span').html(val);
                            $('#'+$this.settings.id+'_alert').removeClass('alert-success');
                            $('#'+$this.settings.id+'_alert').addClass('alert-danger');
                        });
                    }
                }
            }
        };
        $.extend(this.settings, options);

        this.input = input;
        this.xhr = new XMLHttpRequest();

        this.send = function(){
            var data = new FormData();
            data.append(String($this.settings.prefix),$this.input);
            $this.upload(data);
        };

        this.upload = function(data){
            jQuery.each($this.settings.post, function( i, val ) {
                data.append(i, val);
            });
            $this.xhr.open('POST',$this.settings.url, true);
            $this.xhr.send(data);
        };

        if(this.settings.onprogress) this.xhr.upload.addEventListener('progress',this.settings.onprogress,false);
        this.xhr.onreadystatechange = function(ev){
            if($this.xhr.readyState == 4) {
                if($this.xhr.status == 200) {
                    if($this.settings.success) $this.settings.success($this.xhr.responseText,ev);
                    $this.input.value = '';
                } else {
                    if($this.settings.error) $this.settings.error(ev);
                }
            }
        };
    }
}

designerClass.prototype.refresh = function(element) {
    var formData = new FormData();
    formData.append('id',       $(element).data('id'));
    formData.append('type',     $(element).data('type'));
    formData.append('display',  true);
    main.ajaxRequest('/api/informer/uploader/select/json', formData)
        .done(function( json ) {
            // template = Handlebars.compile($("#js-upload__card").html());
            $(element).html(template_row(json));
        });
};

var designer = new designerClass();
$('.js-upload').each(function(i,elem) {
    designer.init(this);
});


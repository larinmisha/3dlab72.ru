<?php base::test();

    class InformerAdmin extends Api {
        /**
         * История из лога
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function history($params = null) {
            global $cms;
            /* для сравнения текста в будущем!
            $old['text']    = str_replace(array("\r\n", "\r", "\n", "\t"), '', $old['text']); // удаляем переносы строк
            $params['text'] = str_replace(array("\r\n", "\r", "\n", "\t"), '', $params['text']); // удаляем переносы строк
            preg_match_all('/>([^<\/]+?)</is', $old['text']   , $search['buf']['old']);
            preg_match_all('/>([^<\/]+?)</is', $params['text'], $search['buf']['new']);
            $oldt = array_diff($search['buf']['old'][1], $search['buf']['new'][1]);
            $newt = array_diff($search['buf']['new'][1], $search['buf']['old'][1]);

            if ((count($oldt)>0)||(count($newt)>0)) {
                $detal[] = ['text' => 'Содержание', 'old' => $oldt, 'new' => $newt];
            }*/
            if ($cms->is_access(1, '', 'informer')) {
                $row_ = $cms->pdo->prepare("SELECT  u.f, u.i, u.o,
                                                    l.id, l.info, l.detal, l.timestamp as l_ts, l.fk_informer_file,
                                                    i.time_public, i.timestamp as i_ts,
                                                    f.type, f.media, f.title
                                            FROM    bk_informer_log as l
                                                    left join bk_users as u on l.fk_users = u.id
                                                    left join bk_informer as i on l.fk_informer = i.id
                                                    left join bk_informer_media as f on l.fk_informer_file = f.id
                                            WHERE   1=1
                                                    and l.fk_informer = :id
                                            ORDER BY l.timestamp ASC
                                            ");
                $row_->execute([':id'=>$params['id']]);
                if ($row_->rowCount()>0) {
                    while ($row = $row_->fetch()) {
                        $fio = trim($row['f'].' '.$row['i'].' '.$row['o']);
                        if ($fio =='') $fio = 'пользователь удалён';
                        if ($row['fk_informer_file']>0) {
                            if($params['type'] != 'youtube') {
                                $file['id'] = $row['fk_informer_file'];
                                $file['url'] = "/files/informer/".$row['type']."/".date('Y/m/', strtotime($row['i_ts'])).$row['media'];
                                if ($row['title']!='') {
                                    $file['title'] = $row['title'];
                                } else {
                                    $file['title'] = '-- без названия --';
                                }
                            }
                        } else {
                            $file = NULL;
                        }
                        $mas['data']['row'][] = [   'id'        => $row['id'],
                                                    'info'      => $row['info'],
                                                    'timestamp' => $row['l_ts'],
                                                    'detal'     => json_decode($row['detal']),
                                                    'fio'       => $fio,
                                                    'file'      => $file
                                                ];
                    }
                }
            } else {
                $mas['message']['refresh'] = [];
            }
            return $mas;
        }


        /**
         * Логирование
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function log($params = null) {
            global $cms;

            /*if ($params['idf'] == '')   $params['idf']   = NULL;
            if ($params['detal'] == '') $params['detal'] = NULL;
            if (count($params['detal'])>0) {
                foreach ($params['detal'] as $key => $detal) {
                    if (is_array($detal['old'])) {
                        $buf = [];
                        foreach ($detal['old'] as $row) {
                            $buf[] = ['change' => $row];
                        }
                        $params['detal'][$key]['old'] = $buf;
                    } else if ($detal['old']!='') {
                        unset($params['detal'][$key]['old']);
                        $params['detal'][$key]['old'][] = ['change' => $detal['old']];
                    }
                    if (is_array($detal['new'])>0) {
                        $buf = [];
                        foreach ($detal['new'] as $row) {
                            $buf[] = ['change' => $row];
                        }
                        $params['detal'][$key]['new'] = $buf;
                    } else if ($detal['new']!='') {
                        unset($params['detal'][$key]['new']);
                        $params['detal'][$key]['new'][] = ['change' => $detal['new']];
                    }
                }
            }

            $row_ = $cms->pdo->prepare("INSERT INTO bk_informer_log(fk_users, fk_informer, fk_informer_file, info, detal)
                                        VALUES (:fk_users, :fk_informer, :fk_file, :info, :detal)");
            $row_->execute([':fk_users'         => $_SESSION['user']['id'],
                            ':fk_informer'      => $params['id'],
                            ':fk_file'          => $params['idf'],
                            ':info'             => $params['info'],
                            ':detal'            => json_encode($params['detal']),
                            ]);

            $row_ = $cms->pdo->prepare("UPDATE bk_informer SET ya_turbo_status = NULL WHERE id = :id");
            $row_->execute([':id' => $params['id']]);*/

            return 1;
        }

        /**
         * Переместить файл (старт)
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function move_select($params = null) {
            global $cms;
            $mas = [];
            $row_ = $cms->pdo->prepare("SELECT * FROM bk_informer WHERE id = :id");
            $row_->execute([':id' => $params['id']]);
            if ($row_->rowCount()>0) {
                $_SESSION['informer']['public_move'] = $params['id'];
            } else {
                unset($_SESSION['informer']['public_move']);
            }
            return true;
        }


        /**
         * Перемещение записи
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function move_save($params = null) {
            global $cms;
            $row_ = $cms->pdo->prepare("SELECT  i.fk_modules, m.fk_structure
                                        FROM    bk_informer as i,
                                                bk_modules as m
                                        WHERE   1=1
                                                and m.id = i.fk_modules
                                                and i.id = :id");
            $row_->execute([':id'=>$_SESSION['informer']['public_move']]);
            $row = $row_->fetch();
            if ($cms->is_access($row['fk_structure'])) {
                $row_ = $cms->pdo->prepare("UPDATE bk_informer SET `fk_modules` = :modules, sort=NULL WHERE id = :id");
                $row_->execute([':id'=>$_SESSION['informer']['public_move'], ':modules'=>$params['mod']]);

                self::indexSortTable(['id'=>$row['fk_modules']]);
                self::indexSortTable(['id'=>$params['mod']]);
                // log
                /*$buf = [];
                $breadcrumb  = $cms->breadcrumb($old['fk_structure']);
                foreach ($breadcrumb as $item) {
                    $buf[] = $item['title'];
                }
                self::log(['id'=>$_SESSION['informer']['public_move'], 'info'=>'Запись перемещена из раздела: '.implode('/', $buf)]);*/
                // log

                unset($_SESSION['informer']['public_move']);

                return false;
            } else {
                $mas['message']['refresh'] = [];
                return $mas;
            }
        }

        /**
         * Получение записи
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function select($params = null) {
            global $cms;
            if ($cms->is_access(1, '', 'informer')) {

                if (is_numeric($_SESSION['informer']['public_move'])) {
                    $move_ = $cms->pdo->prepare("   SELECT  m.id, m.fk_structure
                                                    FROM    bk_informer as i,
                                                            bk_modules as m
                                                    WHERE   1=1
                                                            and m.id = i.fk_modules
                                                            and i.id = :id
                                                    ");
                    $move_->execute([':id' => $_SESSION['informer']['public_move']]);
                    if ($move_->rowCount()>0) {
                        $move = $move_->fetch();

                        $fields_ = $cms->pdo->prepare(" SELECT  f.id, f.tpl_html, f.type, f.title,
                                                                d.item_varchar, d.item_text, d.item_datetime, d.item_decimal, d.item_int, d.item_date
                                                        FROM    bk_informer_tpl_modules as m
                                                                left join bk_informer_tpl_fields as f   on  m.fk_tpl = f.fk_tpl
                                                                                                            and f.basic = 1
                                                                left join bk_informer_data as d         on  d.fk_tpl_fields = f.id
                                                                                                            and d.fk_informer = :informer
                                                                                                            and d.fk_users_delete is NULL
                                                        WHERE   1=1
                                                                and m.fk_modules = :mod
                                                        ORDER BY f.sort
                                                    ");
                        $fields_->execute([ ':mod'      => $params['mod'],
                                            ':informer' => $_SESSION['informer']['public_move']
                                            ]);
                        if ($fields_->rowCount()>0) {
                            while ($fields = $fields_->fetch()) {
                                /*$item_ = $cms->pdo->prepare("   SELECT  fi.*
                                                                FROM    bk_informer_tpl_fields_item as fi
                                                                        left join bk_informer_data_item as i on fi.id = i.fk_fields_item
                                                                                                                and i.fk_informer = :id
                                                                WHERE   1=1
                                                                        and fi.fk_tpl_fields = :fields
                                                                        and i.id > 0
                                                                ORDER BY fi.item ASC");
                                $item_->execute([   ':id'       => $row['id'],
                                                    ':fields'   => $fields['id']]);
                                if ($item_->rowCount()>0) {
                                    while ($item = $item_->fetch()) {
                                        $fields['item_'.$fields['type']][] = $item['item'];
                                    }
                                }*/
                                $buf['fields'][] = ['title' => $fields['title'],
                                                    'data'  => $fields['item_'.$fields['type']],
                                                    ];
                            }
                        }

                        $row_ = $cms->pdo->prepare("SELECT  fk_tpl
                                                    FROM    bk_informer_tpl_modules
                                                    WHERE   1=1
                                                            and fk_modules in(:mod, :mod2)
                                                    Group by fk_tpl
                                                    -- HAVING count(fk_tpl) = 2
                                                    ");
                        $row_->execute([':mod' => $params['mod'],
                                        ':mod2' => $move['id']
                                        ]);

                        $mas['move'] = ['id'        => $_SESSION['informer']['public_move'],
                                        'structure' => $cms->structure_page($move['fk_structure'],'title'),
                                        'fields'    => $buf['fields'],
                                        'status'    => $row_->rowCount()
                                        ];
                    }
                }

                $fields_ = $cms->pdo->prepare(" SELECT  f.id, f.tpl_html, f.type, f.title
                                                FROM    bk_informer_tpl_modules as m
                                                        left join bk_informer_tpl_fields as f   on  m.fk_tpl = f.fk_tpl
                                                                                                    and f.basic = 1
                                                WHERE   1=1
                                                        and m.fk_modules = :mod
                                                ORDER BY f.sort
                                            ");
                $fields_->execute([':mod' => $params['mod']]);
                if ($fields_->rowCount()>0) {
                    while ($fields = $fields_->fetch()) {
                        $mas['fields'][] = $fields['title'];
                    }
                }

                $col_page = 10;

                if ($params['page']=='') $params['page']=1;

                $limit = " LIMIT ".(($params['page']-1)*$col_page).", ".$col_page;
                $row_ = $cms->pdo->prepare("SELECT  SQL_CALC_FOUND_ROWS i.*
                                            FROM    bk_informer as i,
                                                    bk_modules as m
                                            WHERE   1=1
                                                    and i.fk_modules = m.id
                                                    and m.id = :id
                                                    and i.del IS NULL
                                                    and m.del=0
                                            ORDER BY i.sort DESC
                                            ".$limit);
                $row_->execute([':id'=>$params['mod']]);
                if ($row_->rowCount()>0) {
                    $page['page_max'] = $cms->pdo->query("SELECT FOUND_ROWS()")->fetchColumn(); // сколько записей в БД
                    while ($row = $row_->fetch()) {
                        $fields_ = $cms->pdo->prepare(" SELECT  f.id, f.tpl_html, f.type, f.title,
                                                                d.item_varchar, d.item_text, d.item_datetime, d.item_decimal, d.item_int, d.item_date
                                                        FROM    bk_informer_tpl_modules as m
                                                                left join bk_informer_tpl_fields as f   on  m.fk_tpl = f.fk_tpl
                                                                                                            and f.basic = 1
                                                                left join bk_informer_data as d         on  d.fk_tpl_fields = f.id
                                                                                                            and d.fk_informer = :informer
                                                                                                            and d.fk_users_delete is NULL
                                                        WHERE   1=1
                                                                and m.fk_modules = :mod
                                                        ORDER BY f.sort
                                                    ");
                        $fields_->execute([ ':mod'      => $params['mod'],
                                            ':informer' => $row['id']
                                            ]);
                        if ($fields_->rowCount()>0) {
                            while ($fields = $fields_->fetch()) {
                                $item_ = $cms->pdo->prepare("   SELECT  fi.*
                                                                FROM    bk_informer_tpl_fields_item as fi
                                                                        left join bk_informer_data_item as i on fi.id = i.fk_fields_item
                                                                                                                and i.fk_informer = :id
                                                                WHERE   1=1
                                                                        and fi.fk_tpl_fields = :fields
                                                                        and i.id > 0
                                                                ORDER BY fi.item ASC");
                                $item_->execute([   ':id'       => $row['id'],
                                                    ':fields'   => $fields['id']]);
                                if ($item_->rowCount()>0) {
                                    while ($item = $item_->fetch()) {
                                        $fields['item_'.$fields['type']][] = $item['item'];
                                    }
                                }
                                $row['fields'][] = ['title' => $fields['title'],
                                                    'data'  => $fields['item_'.$fields['type']],
                                                    ];
                            }
                        }

                        if ($row['ya_turbo_status'] == '')          {
                            $row['ya_turbo_title']  = 'Ожидает отправки';
                            $row['ya_turbo_color']  = '';
                            $row['ya_turbo_link']    = $row['ya_turbo_url'];
                        }
                        if ($row['ya_turbo_status'] == 'ok')        {
                            $row['ya_turbo_title']  = 'Активна';
                            $row['ya_turbo_color']  = 'btn-success';
                            $row['ya_turbo_link']    = $row['ya_turbo_url'];
                        }
                        if ($row['ya_turbo_status'] == 'error')     {
                            $row['ya_turbo_title']  = 'Ошибка при создании';
                            $row['ya_turbo_color']  = 'btn-warning';
                            $row['ya_turbo_url_status']    = '/api/informer/yaTurbo/status/row?token='.$row['ya_turbo_url'];
                        }
                        if ($row['ya_turbo_status'] == 'process')   {
                            $row['ya_turbo_title']  = 'Создаётся';
                            $row['ya_turbo_color']  = 'btn-warning';
                            $row['ya_turbo_url_status']    = '/api/informer/yaTurbo/status/row?token='.$row['ya_turbo_url'];
                        }
                        if ($row['ya_turbo_status'] == 'delete')    {
                            $row['ya_turbo_title']  = 'Удаление';
                            $row['ya_turbo_color']  = 'btn-warning';
                            $row['ya_turbo_url_status']    = '/api/informer/yaTurbo/status/row?token='.$row['ya_turbo_url'];
                        }

                        $mas['row'][] = $row;
                    }


                    if (is_numeric($col_page)) {
                        $mas['url'] = $cms->url($row['fk_structure'], 1);
                        $page['page_max'] = intval(($page['page_max'] - 1) / $col_page) + 1;  // максимальное количество страниц
                        if ($params['page'] > $page['page_max']) {
                            $params['page']=$page['page_max'];
                        }
                        if (($params['page']-1) < 1) {
                            $page['page_s'] = 1;
                        } else {
                            $page['page_s'] = $params['page']-1;
                        }
                        if (($params['page']+1) > $page['page_max']) {
                            $page['page_f'] = $page['page_max'];
                        } else {
                            $page['page_f'] = $params['page']+1;
                        }
                        if ($page['page_f'] > $page['page_s']) {
                            if ($params['page'] > 1) {
                                $mas['page'][] = ['page' => $params['page']-1, 'char' => '&laquo;'];
                            } else {
                                $mas['page'][] = ['char' => '&laquo;'];
                            }
                            if ($page['page_s'] > 1) {
                                $mas['page'][] = ['page' => 1];
                                $mas['page'][] = ['char' => '...'];
                            }
                            for ($i = $page['page_s']; $i <= $page['page_f']; $i++) {
                                if ($params['page'] == $i) {
                                    $mas['page'][] = ['char' => $i, 'active'=> 1];
                                } else {
                                    $mas['page'][] = ['page' => $i];
                                }
                            }
                            if ($page['page_f'] < $page['page_max']) {
                                $mas['page'][] = ['char' => '...'];
                                $mas['page'][] = ['page' => $page['page_max']];
                            }
                            if ($params['page'] != $page['page_max']) {
                                $mas['page'][] = ['page' => $params['page']+1, 'char' => '&raquo;'];
                            } else {
                                $mas['page'][] = ['char' => '&raquo;'];
                            }
                        }
                    }


                }
            } else {
                $mas['message']['refresh'] = [];
            }
            return $mas;
        }

        /**
         * Получение записи
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function record($params = null) {
            global $cms;
            if ($cms->is_access(1, '', 'informer')) {
                if ($params['id'] == 0) {
                    $row_ = $cms->pdo->prepare("INSERT INTO bk_informer(fk_modules, fk_users_add) VALUES (:mod, :user)");
                    $row_->execute([':mod'  => $params['mod'],
                                    ':user' => $_SESSION['user']['id']
                                    ]);
                    $params['id'] = $cms->pdo->lastInsertId();
                    self::indexSortTable(['id' => $params['mod']]);
                }

                $info_ = $cms->pdo->prepare("   SELECT  id
                                                FROM    bk_informer
                                                WHERE   1=1
                                                        and id = :id
                                                        and del is NULL
                                            ");
                $info_->execute([':id'=>$params['id']]);
                if ($info_->rowCount()>0) {
                    $info = $info_->fetch();
                    $mas['id'] = $info['id'];

                    $fields_ = $cms->pdo->prepare(" SELECT  f.id, f.tpl_html, f.type, f.title,
                                                            d.item_varchar, d.item_text, d.item_datetime, d.item_decimal, d.item_int, d.item_date
                                                    FROM    bk_informer_tpl_modules as m
                                                            left join bk_informer_tpl_fields as f   on  m.fk_tpl = f.fk_tpl
                                                            left join bk_informer_data as d         on  d.fk_tpl_fields = f.id
                                                                                                        and d.fk_informer = :informer
                                                                                                        and d.fk_users_delete is NULL
                                                    WHERE   1=1
                                                            and m.fk_modules = :mod
                                                    ORDER BY f.sort
                                                ");
                    $fields_->execute([ ':mod'      => $params['mod'],
                                        ':informer' => $info['id']
                                        ]);
                    if ($fields_->rowCount()>0) {
                        while ($fields = $fields_->fetch()) {
                            if (in_array($fields['tpl_html'], ['checkbox'])) {
                                $item_ = $cms->pdo->prepare("   SELECT  fi.*, if(i.id, 1, 0) as checked
                                                                FROM    bk_informer_tpl_fields_item as fi
                                                                        left join bk_informer_data_item as i on fi.id = i.fk_fields_item
                                                                                                                and i.fk_informer = :id
                                                                WHERE   1=1
                                                                        and fi.fk_tpl_fields = :fields
                                                                ORDER BY fi.item ASC");
                                $item_->execute([   ':id'       => $info['id'],
                                                    ':fields'   => $fields['id']]);
                                if ($item_->rowCount()>0) {
                                    while ($item = $item_->fetch()) {
                                        $fields['item_'.$fields['type']][] = [  'id'        => $item['id'],
                                                                                'item'      => $item['item'],
                                                                                'checked'   => $item['checked']
                                                                                ];
                                    }
                                }
                            } else if (in_array($fields['tpl_html'], ['radio'])) {
                                $item_ = $cms->pdo->prepare("   SELECT  fi.*, if(d.fk_informer, 1, 0) as checked
                                                                FROM    bk_informer_tpl_fields_item as fi
                                                                        left join bk_informer_data as d on fi.id = d.item_radio
                                                                                                            and d.fk_users_delete is NULL
                                                                                                            and d.fk_informer = :id
                                                                WHERE   1=1
                                                                        and fi.fk_tpl_fields = :fields
                                                                ORDER BY fi.item ASC");
                                $item_->execute([   ':id'       => $info['id'],
                                                    ':fields'   => $fields['id']]);
                                if ($item_->rowCount()>0) {
                                    while ($item = $item_->fetch()) {
                                        $fields['item_'.$fields['type']][] = [  'id'        => $item['id'],
                                                                                'item'      => $item['item'],
                                                                                'checked'   => $item['checked']
                                                                                ];
                                    }
                                }
                            } else if (in_array($fields['tpl_html'], ['select'])) {
                                $item_ = $cms->pdo->prepare("   SELECT  fi.*, if(d.fk_informer, 1, 0) as checked
                                                                FROM    bk_informer_tpl_fields_item as fi
                                                                        left join bk_informer_data as d on fi.id = d.item_select
                                                                                                            and d.fk_users_delete is NULL
                                                                                                            and d.fk_informer = :id
                                                                WHERE   1=1
                                                                        and fi.fk_tpl_fields = :fields
                                                                ORDER BY fi.item ASC");
                                $item_->execute([   ':id'       => $info['id'],
                                                    ':fields'   => $fields['id']]);
                                if ($item_->rowCount()>0) {
                                    while ($item = $item_->fetch()) {
                                        $fields['item_'.$fields['type']][] = [  'id'        => $item['id'],
                                                                                'item'      => $item['item'],
                                                                                'checked'   => $item['checked']
                                                                                ];
                                    }
                                }
                            }

                            $mas['fields'][] = ['id'        => $fields['id'],
                                                'tpl_html'  => $fields['tpl_html'],
                                                'title'     => $fields['title'],
                                                'type'      => $fields['type'],
                                                'data'      => $fields['item_'.$fields['type']],
                                                ];
                        }
                    }
                }
            }
            return $mas;
        }

        /**
         * Добавление записи
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function save($params = null) {
            global $cms;
            if ($cms->is_access(1, '', 'informer')) {

                if ($params['id'] == 0) {
                    $row_ = $cms->pdo->prepare("INSERT INTO bk_informer(fk_modules) VALUES (:mod)");
                    $row_->execute([':mod' => $params['mod']]);
                    $params['id'] = $cms->pdo->lastInsertId();

                    $mas['message']['true'][] = 'Данные сохранены';
                    self::log(['id'=>$mas['data']['id'], 'info'=>'Создание карточки']);
                }
                $row_ = $cms->pdo->prepare("SELECT  f.id, f.type
                                            FROM    bk_informer_tpl_fields as f,
                                                    bk_informer_tpl_modules as m
                                            WHERE   1=1
                                                    and m.fk_tpl = f.fk_tpl
                                                    and m.fk_modules = :mod
                                            ORDER BY f.sort
                                            ");
                $row_->execute([':mod' => $params['mod']]);
                if ($row_->rowCount()>0) {
                    while ($row = $row_->fetch()) {
                        $fields[$row['id']] = $row['type'];
                    }
                }

                foreach ($params as $item => $value) {
                    if (preg_match("/item_([0-9]*)/", $item, $rez)) {
                        if ($fields[$rez[1]] != '') {
                            if ($fields[$rez[1]] == 'datetime') {
                                $value = date('Y-m-d H:i:s', strtotime($value));
                            }

                            $row_ = $cms->pdo->prepare("SELECT  *
                                                        FROM    bk_informer_data
                                                        WHERE   1=1
                                                                and fk_tpl_fields = :fk_tpl_fields
                                                                and fk_informer = :fk_informer
                                                                and (item_{$fields[$rez[1]]} = :item or item_{$fields[$rez[1]]} is NULL)
                                                                and fk_users_delete is NULL
                                                        ");
                            $row_->execute([':fk_tpl_fields'=> $rez[1],
                                            ':fk_informer'  => $params['id'],
                                            ':item'         => $value
                                            ]);
                            if ($row_->rowCount() == 0) {
                                $row_ = $cms->pdo->prepare("UPDATE  bk_informer_data
                                                            SET     fk_users_delete = :fk_users,
                                                                    timestamp_delete = NOW()
                                                            WHERE   1=1
                                                                    and fk_tpl_fields = :fk_tpl_fields
                                                                    and fk_informer = :fk_informer
                                                                    and fk_users_delete is NULL
                                                            ");
                                $row_->execute([':fk_tpl_fields'=> $rez[1],
                                                ':fk_informer'  => $params['id'],
                                                ':fk_users'     => $_SESSION['user']['id']
                                                ]);

                                $row_ = $cms->pdo->prepare("INSERT INTO bk_informer_data ( fk_tpl_fields, fk_informer, item_{$fields[$rez[1]]}, fk_users_add)
                                                                                  VALUES (:fk_tpl_fields,:fk_informer,:item, :fk_users)
                                                            ");
                                $row_->execute([':fk_tpl_fields'=> $rez[1],
                                                ':fk_informer'  => $params['id'],
                                                ':item'         => $value,
                                                ':fk_users'     => $_SESSION['user']['id']
                                                ]);
                            } else {
                                $row = $row_->fetch();
                                $id = $row['id'];
                                $row_ = $cms->pdo->prepare("UPDATE  bk_informer_data
                                                            SET     fk_tpl_fields           = :fk_tpl_fields,
                                                                    item_{$fields[$rez[1]]} = :item,
                                                                    fk_users_add            = :fk_users
                                                            WHERE   id = :id");
                                $row_->execute([':id'           => $id,
                                                ':fk_tpl_fields'=> $rez[1],
                                                ':item'         => $value,
                                                ':fk_users'     => $_SESSION['user']['id']
                                                ]);
                            }
                        }
                    }
                }
                $mas['data']['success'] = 1;
            }
            return $mas;
        }
        public function saveItem($params = null) {
            global $cms;
            if ($cms->is_access(1, '', 'informer')) {
                if (is_numeric($params['fk_item']) && is_numeric($params['fk_informer'])) {
                    if ($params['checked'] == 'true') {
                        $row_ = $cms->pdo->prepare("SELECT  id
                                                    FROM    bk_informer_data_item
                                                    WHERE   1=1
                                                            and fk_informer = :fk_informer
                                                            and fk_fields_item = :fk_fields_item");
                        $row_->execute([':fk_informer' => $params['fk_informer'], ':fk_fields_item' => $params['fk_item']]);
                        if ($row_->rowCount() == 0) {
                            $row_ = $cms->pdo->prepare("INSERT INTO bk_informer_data_item(fk_informer, fk_fields_item) VALUES (:fk_informer, :fk_fields_item)");
                            $row_->execute([':fk_informer' => $params['fk_informer'], ':fk_fields_item' => $params['fk_item']]);
                        }
                    } else {
                        $row_ = $cms->pdo->prepare("DELETE FROM bk_informer_data_item WHERE fk_informer = :fk_informer and fk_fields_item = :fk_fields_item");
                        $row_->execute([':fk_informer' => $params['fk_informer'], ':fk_fields_item' => $params['fk_item']]);
                    }
                }
                return true;
            }
        }

        /**
         * Удаление записи
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function delete($params = null) {
            global $cms;
            if ($cms->is_access(1, '', 'informer')) {
                $row_ = $cms->pdo->prepare("UPDATE bk_informer SET `del` = '1' WHERE id = :id");
                $row_->execute([':id'=>$params['id']]);
                self::log(['id'=>$params['id'], 'info'=>'Карточка удалена']);

                $row_ = $cms->pdo->prepare("SELECT fk_modules FROM bk_informer WHERE id = :id");
                $row_->execute([':id'=>$params['id']]);
                $row = $row_->fetch();

                self::indexSortTable(['id' => $row['fk_modules']]);
                $mas['message']['true'][] = 'Запись удалена';
            } else {
                $mas['message']['refresh'] = [];
            }
            return $mas;
        }

        /**
         * Отправка рассылки
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function send_email($params = null) {
            global $cms, $sendMail;
            $row_ = $cms->pdo->prepare("SELECT  *
                                        FROM    bk_informer
                                        WHERE   1=1
                                                and id = :id
                                                and send_email IS NULL
                                                and del IS NULL");
            $row_->execute([':id'=>$params['id']]);
            if ($row_->rowCount()>0) {
                $row = $row_->fetch();
                $count = $sendMail->get(['unic'      => 'informer_'.$row['id'],
                                         'user'      => 'all',
                                         'subject'   => $row['title'],
                                         'message'   => $row['text']
                                        ]);
                $row_ = $cms->pdo->prepare("UPDATE bk_informer SET send_email = :send_email WHERE id = :id");
                $row_->execute([':id'=>$params['id'], ':send_email' => $count]);

                return true;
            } else {
                $mas['message']['refresh'] = [];
                return $mas;
            }
        }

        /**
         * Статус рассылки
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function email_status($params = null) {
            global $cms, $sendMail;
            if ($cms->is_access(1, '', 'informer')) {
                return $sendMail->status(['unic' => 'informer_'.$params['id']]);;
            } else {
                $mas['message']['refresh'] = [];
                return $mas;
            }
        }

        /**
         * Видимость записи
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function setDisplay($params = null) {
            global $cms;
            if ($cms->is_access(1, '', 'informer')) {
                $row_ = $cms->pdo->prepare("SELECT  *
                                            FROM    bk_informer
                                            WHERE   1=1
                                                    and del IS NULL
                                                    and id  = :id
                                            ");
                $row_->execute([':id'=>$params['id']]);
                if ($row_->rowCount()>0) {
                    $row = $row_->fetch();
                    if ($row['display']==1) $display = NULL; else $display = 1;
                    $row_ = $cms->pdo->prepare("UPDATE bk_informer SET `display` = :display WHERE id = :id");
                    $row_->execute([':display'=>$display,
                                    ':id'=>$row['id']
                                    ]);
                    $mas['data'][] = ['getDisplay' => $display];
                    if ($display == 1) {
                        self::log(['id'=>$params['id'], 'info'=>'Запись скрыта']);
                        $mas['message']['true'][] = 'Запись скрыта';
                    } else {
                        self::log(['id'=>$params['id'], 'info'=>'Запись опубликована']);
                        $mas['message']['true'][] = 'Запись опубликована';
                    }
                } else {
                    $mas['message']['true'][] = 'Данные отсутствуют';
                }
            } else {
                $mas['message']['refresh'] = [];
            }
            return $mas;
        }

        /**
         * Видимость записи
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function setTop($params = null) {
            global $cms;
            if ($cms->is_access(1, '', 'informer')) {
                $row_ = $cms->pdo->prepare("SELECT  *
                                            FROM    bk_informer
                                            WHERE   1=1
                                                    and del IS NULL
                                                    and id  = :id
                                            ");
                $row_->execute([':id'=>$params['id']]);
                if ($row_->rowCount()>0) {
                    $row = $row_->fetch();
                    if ($row['top']==1) $top = NULL; else $top = 1;
                    $row_ = $cms->pdo->prepare("UPDATE bk_informer SET `top` = :top WHERE id = :id");
                    $row_->execute([':top'=>$top,
                                    ':id'=>$row['id']
                                    ]);
                    $mas['data'][] = ['gettop' => $top];
                    if ($top == 1) {
                        self::log(['id'=>$params['id'], 'info'=>'Запись скрыта']);
                        $mas['message']['true'][] = 'Запись скрыта';
                    } else {
                        self::log(['id'=>$params['id'], 'info'=>'Запись опубликована']);
                        $mas['message']['true'][] = 'Запись опубликована';
                    }
                } else {
                    $mas['message']['true'][] = 'Данные отсутствуют';
                }
            } else {
                $mas['message']['refresh'] = [];
            }
            return $mas;
        }

        /**
         * Сортировка (Обновить индексы поля sort по дате)
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        /*public function indexSortUpdate($params = null) { // переделать на id напрямую fk_modules
            global $cms;
            if ($cms->is_access(1, '', 'informer')) {
                $row_ = $cms->pdo->prepare("SELECT  i.*
                                            FROM    bk_informer as i,
                                                    bk_modules as m
                                            WHERE   1=1
                                                    and i.fk_modules = m.id
                                                    and m.fk_structure = :node
                                                    and i.del IS NULL
                                            ORDER BY time_public ASC");
                $row_->execute([':node'=>$params['node']]);
                if ($row_->rowCount()>0) {
                    $i = 1;
                    while ($row = $row_->fetch()) {
                        if ($row['sort']!=$i) { // если индекс не изменился, то и не обновляем
                            $sort_ = $cms->pdo->prepare("UPDATE bk_informer SET `sort` = :sort WHERE id = :id");
                            $sort_->execute([':id'=>$row['id'], ':sort'=>$i]);
                        }
                        $i++;
                    }
                }
                return true;
            } else {
                return false;
            }
        }*/

        /**
         * Сортировка (Обновить индексы поля sort)
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function indexSortTable($params = null) { // переделать на id напрямую fk_modules
            global $cms;
            if ($cms->is_access(1, '', 'informer')) {
                $row_ = $cms->pdo->prepare("SELECT  *
                                            FROM    bk_informer
                                            WHERE   1=1
                                                    and fk_modules = :id
                                                    and del IS NULL
                                            ORDER BY    CASE
                                                            WHEN sort is NULL
                                                            THEN id
                                                            WHEN sort is not NULL
                                                            THEN sort
                                                        END ASC");
                $row_->execute([':id'=>$params['id']]);
                if ($row_->rowCount()>0) {
                    $i = 1;
                    while ($row = $row_->fetch()) {
                        if ($row['sort']!=$i) { // если индекс не изменился, то и не обновляем
                            $sort_ = $cms->pdo->prepare("UPDATE bk_informer SET `sort` = :sort WHERE id = :id");
                            $sort_->execute([':id'=>$row['id'], ':sort'=>$i]);
                        }
                        $i++;
                    }
                }
                return true;
            } else {
                return false;
            }
        }

        /**
         * Сортировка (вверх)
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function setSortUp($params = null) {
            global $cms;
            if ($cms->is_access(1, '', 'informer')) {
                $max_ = $cms->pdo->prepare("SELECT  max(sort) as max
                                            FROM    bk_informer
                                            WHERE   1=1
                                                    and del IS NULL
                                                    and fk_modules = (  SELECT  fk_modules
                                                                        FROM    bk_informer
                                                                        WHERE   1=1
                                                                                and id = :id)
                                            ");
                $max_->execute([':id'=>$params['id']]);
                $max = $max_->fetch();
                $row_ = $cms->pdo->prepare("SELECT  *
                                            FROM    bk_informer
                                            WHERE   1=1
                                                    and del IS NULL
                                                    and id  = :id
                                            ");
                $row_->execute([':id'=>$params['id']]);
                if ($row_->rowCount()>0) {
                    $row = $row_->fetch();
                    self::indexSortTable(['id' => $row['fk_modules']]);
                    $row_->execute([':id'=>$params['id']]);
                    $row = $row_->fetch();

                    if ($row['sort']<$max['max']) {
                        $sort_ = $cms->pdo->prepare("UPDATE bk_informer SET `sort` = :sort WHERE sort = :sid and fk_modules = :idm");
                        $sort_->execute([':sid'=>$row['sort']+1, ':sort'=>$row['sort'], ':idm'=>$row['fk_modules']]);

                        $sort_ = $cms->pdo->prepare("UPDATE bk_informer SET `sort` = :sort WHERE id = :id and fk_modules = :idm");
                        $sort_->execute([':id'=>$row['id'], ':sort'=>$row['sort']+1, ':idm'=>$row['fk_modules']]);
                    }
                    self::log(['id'=>$params['id'], 'info'=>'Сортировка (вниз)']);
                }
                return true;
            } else {
                $mas['message']['refresh'] = [];
                return $mas;
            }
        }
        /**
         * Сортировка (вниз)
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function setSortDown($params = null) {
            global $cms;
            if ($cms->is_access(1, '', 'informer')) {
                $row_ = $cms->pdo->prepare("SELECT  *
                                            FROM    bk_informer
                                            WHERE   1=1
                                                    and del IS NULL
                                                    and id  = :id
                                            ");
                $row_->execute([':id'=>$params['id']]);
                if ($row_->rowCount()>0) {
                    $row = $row_->fetch();
                    self::indexSortTable(['id' => $row['fk_modules']]);

                    $row_->execute([':id'=>$params['id']]);
                    $row = $row_->fetch();
                    if ($row['sort']>1) {
                        $sort_ = $cms->pdo->prepare("UPDATE bk_informer SET `sort` = :sort WHERE sort = :sid and fk_modules = :idm");
                        $sort_->execute([':sid'=>$row['sort']-1, ':sort'=>$row['sort'], ':idm'=>$row['fk_modules']]);

                        $sort_ = $cms->pdo->prepare("UPDATE bk_informer SET `sort` = :sort WHERE id = :id and fk_modules = :idm");
                        $sort_->execute([':id'=>$row['id'], ':sort'=>$row['sort']-1, ':idm'=>$row['fk_modules']]);
                    }
                    self::log(['id'=>$params['id'], 'info'=>'Сортировка (вверх)']);
                }
                return true;
            } else {
                $mas['message']['refresh'] = [];
                return $mas;
            }
        }
    }

    $informerAdmin = new InformerAdmin();

    if ($informerAdmin->getAction()!='') { // вызов через api, иначе вызов напрямую через php
        echo $informerAdmin->query()->fetch();
    }

?>

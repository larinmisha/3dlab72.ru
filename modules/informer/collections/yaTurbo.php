<?php base::test();
    /*
        Статусы:
        null    + нет/url
        ok      + url
        error   + token
        process + token
        delete  + token
    */
    class YaTurbo extends Api {
        /**
         * Отправка запроса на статус
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function status($params = null) {
            global $cms;
            if ($params['token']!='') {
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, 'https://api.webmaster.yandex.net/v4/user');
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_HTTPHEADER, ['Authorization: OAuth '.conf::$yaTurboKey]);
                $user = json_decode(curl_exec($ch), true);
                curl_close($ch);

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, 'https://api.webmaster.yandex.net/v4/user/'.$user['user_id'].'/hosts');
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_HTTPHEADER, ['Authorization: OAuth '.conf::$yaTurboKey]);
                $hosts = json_decode(curl_exec($ch), true);
                curl_close($ch);

                foreach ($hosts['hosts'] as $host) {
                    if ($host['unicode_host_url'] == conf::$http) {
                        $host_id = $host['host_id'];
                    }
                }
                if ($host_id != '') {
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, 'https://api.webmaster.yandex.net/v4/user/'.$user['user_id'].'/hosts/'.$host_id.'/turbo/tasks/'.$params['token']);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, ['Authorization: OAuth '.conf::$yaTurboKey]);
                    $link = json_decode(curl_exec($ch), true);
                    if ($link['load_status'] == 'OK') {
                        $upd_ = $cms->pdo->prepare("UPDATE bk_informer SET ya_turbo_status = :status, ya_turbo_url = :url WHERE ya_turbo_url = :token Limit 1");
                        if ($link['turbo_pages'][0]['title'] != '') { // опубликовали
                            $upd_->execute([':token'    => $params['token'],
                                            ':status'   => 'ok',
                                            ':url'      => $link['turbo_pages'][0]['preview']]);
                        } else { // удалили
                            $upd_->execute([':token'    => $params['token'],
                                            ':status'   => '',
                                            ':url'      => $link['turbo_pages'][0]['preview']]);
                        }
                    } else if ($link['load_status'] == 'ERROR') {
                        $upd_ = $cms->pdo->prepare("UPDATE bk_informer SET ya_turbo_status = :status WHERE ya_turbo_url = :token Limit 1");
                        $upd_->execute([':token'    => $params['token'],
                                        ':status'   => 'error']);
                    }
                    echo "<pre>";print_r($link);echo "</pre>";
                } else {
                    echo "Ошибка проверки параметра «host-id»: это поле обязательно для заполнения.<br>Текущий домен: ".conf::$http."<br>Список доступных:";
                    echo "<pre>";print_r($hosts['hosts']);echo "</pre>";
                }
            } else {
                $mas['developer']['message'][] = "token is not valid";
                http_response_code(401);
            }
            return true;
        }

        /**
         * Отправка запроса на изменение
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function send($params = null) {
            global $cms;
            $upd_ = $cms->pdo->prepare("UPDATE bk_informer SET ya_turbo_status = :status, ya_turbo_url = :url WHERE id = :id");
            if (conf::$yaTurboKey!='') {
                $xml = YaTurbo::items(['id' => $params['id'], 'turbo' => $params['turbo']]);
                if ($xml!==false) {
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, 'https://api.webmaster.yandex.net/v4/user');
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, ['Authorization: OAuth '.conf::$yaTurboKey]);
                    $user = curl_exec($ch);
                    $user = json_decode(curl_exec($ch), true);
                    curl_close($ch);

                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, 'https://api.webmaster.yandex.net/v4/user/'.$user['user_id'].'/hosts');
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, ['Authorization: OAuth '.conf::$yaTurboKey]);
                    $hosts = json_decode(curl_exec($ch), true);
                    curl_close($ch);
                    foreach ($hosts['hosts'] as $host) {
                        if ($host['unicode_host_url'] == conf::$http) {
                            $host_id = $host['host_id'];
                        }
                    }

                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, 'https://api.webmaster.yandex.net/v4/user/'.$user['user_id'].'/hosts/'.$host_id.'/turbo/uploadAddress');
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, ['Authorization: OAuth '.conf::$yaTurboKey]);
                    $link = json_decode(curl_exec($ch), true);

                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $link['upload_address']);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, ["Accept: application/rss+xml", "Content-type: application/rss+xml",
                                                            'Authorization: OAuth '.conf::$yaTurboKey]);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
                    $task = json_decode(curl_exec($ch), true);
                    curl_close($ch);
                    $upd_->execute([':id'       => $params['id'],
                                    ':status'   => 'process',
                                    ':url'      => $task['task_id']]);
                    return true;
                } else {
                    $upd_->execute([':id'       => $params['id'],
                                    ':status'   => 'error',
                                    ':url'      => null]);
                    return false;
                }
            } else {
                $upd_->execute([':id'       => $params['id'],
                                ':status'   => null,
                                ':url'      => null]);
                return false;
            }
        }

        /**
         * Получение xml
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function items($params = null) {
            global $cms;

            $arrayData = InformerMain::informerMain(['id' => $params['id']]);

            if (count($arrayData) > 0) {
                $dom = new DomDocument('1.0', 'utf-8');
                $rss = $dom->appendChild($dom->createElement('rss'));

                $domAttribute = $dom->createAttribute('xmlns:yandex');
                $domAttribute->value = 'http://news.yandex.ru';
                $rss->appendChild($domAttribute);

                $domAttribute = $dom->createAttribute('xmlns:media');
                $domAttribute->value = 'http://search.yahoo.com/mrss/';
                $rss->appendChild($domAttribute);

                $domAttribute = $dom->createAttribute('xmlns:turbo');
                $domAttribute->value = 'http://turbo.yandex.ru';
                $rss->appendChild($domAttribute);

                $domAttribute = $dom->createAttribute('version');
                $domAttribute->value = '2.0';
                $rss->appendChild($domAttribute);

                $channel = $rss->appendChild($dom->createElement('channel'));

                $title = $channel->appendChild($dom->createElement('title'));
                $title->appendChild($dom->createTextNode($cms->structure_page(1,'title'))); // Название канала

                $link = $channel->appendChild($dom->createElement('link'));
                $link->appendChild($dom->createTextNode(conf::$http)); // url

                $description = $channel->appendChild($dom->createElement('description'));
                $description->appendChild($dom->createTextNode("Новостная лента: ".$cms->structure_page(1,'title'))); // Краткое описание канала

                $language = $channel->appendChild($dom->createElement('language'));
                $language->appendChild($dom->createTextNode('ru'));

                foreach ($arrayData as $row) {

                    $item = $channel->appendChild($dom->createElement('item'));
                        $domAttribute = $dom->createAttribute('turbo');
                        $domAttribute->value = $params['turbo']; // true - по умолчанию, false - для удаления
                        $item->appendChild($domAttribute);

                        // $item->appendChild($dom->createTextNode('Great American Novel'));

                        $link = $item->appendChild($dom->createElement('link'));
                        $link->appendChild($dom->createTextNode(substr(conf::$http, 0, -1).$row['url'])); // url

                        $pubDate = $item->appendChild($dom->createElement('pubDate'));
                        $pubDate->appendChild($dom->createTextNode(date("r", strtotime($row['time_public']))));

                        // $author = $item->appendChild($dom->createElement('author'));
                        // $author->appendChild($dom->createTextNode(''));  // автор

                        /*$metrics = $item->appendChild($dom->createElement('metrics'));

                            $yandex = $metrics->appendChild($dom->createElement('yandex'));
                                // $domAttribute = $dom->createAttribute('schema_identifier');
                                // $domAttribute->value = '123'; // id
                                // $yandex->appendChild($domAttribute);

                                $breadcrumblist = $yandex->appendChild($dom->createElement('breadcrumblist'));



                                        $breadcrumb = $breadcrumblist->appendChild($dom->createElement('breadcrumb'));
                                            $domAttribute = $dom->createAttribute('url');
                                            $domAttribute->value = conf::$http;
                                            $breadcrumb->appendChild($domAttribute);

                                            $domAttribute = $dom->createAttribute('text');
                                            $domAttribute->value = 'Главная';
                                            $breadcrumb->appendChild($domAttribute);

                                    foreach ($menu as $mitem) {
                                        $breadcrumb = $breadcrumblist->appendChild($dom->createElement('breadcrumb'));
                                            $domAttribute = $dom->createAttribute('url');
                                            $domAttribute->value = substr(conf::$http, 0, -1).$mitem['url'];
                                            $breadcrumb->appendChild($domAttribute);

                                            $domAttribute = $dom->createAttribute('text');
                                            $domAttribute->value = $mitem['title'];
                                            $breadcrumb->appendChild($domAttribute);
                                    }*/
                        $text  = '<header>';
                        $text .= '<h1>'.$row['title'].'</h1>';
                        if (count($row['material']['img']) > 0) {
                            $text .= '<figure>';
                            $text .=     '<img src="'.$row['material']['img'][0]['path'].'"/>';
                            $text .= '</figure>';
                            unset($row['material']['img'][0]);
                        }
                        $text .= '<menu>';
                            include_once dirname(__FILE__)."/../../admin_cms/tree_node/collections/defult.php";
                            $menu = $defultNode->menu(['nones' => '550, 551, 552, 553, 549']);
                            $text .= '<a href="'.conf::$http.'">Открыть сайт</a>';
                            foreach ($menu as $mitem) {
                                $text .= '<a href="'.substr(conf::$http, 0, -1).$mitem['url'].'">'.$mitem['title'].'</a>';
                            }
                        $text .= '</menu>';
                        $text .= '</header>';
                        $text .=  $row['text'];
                        $text .= '<div data-block="share" data-network="facebook, odnoklassniki, telegram, twitter, vkontakte"></div>';

                        $text .= '<div data-block="widget-feedback" data-stick="right">';
                        $text .=     '<div data-block="chat" data-type="vkontakte" data-url="https://vk.com/"></div>';
                        $text .= '</div>';
                        $text .= '<div data-block="gallery">';
                        if (count($row['material']['img']) > 0) {
                            foreach ($row['material']['img'] as $img) {
                                $text .= '<img src="'.$img['path'].'"/>';
                            }
                        }
                        $text .= '</div>';
                        $text .= '<form action="https://yandex.ru/search/?text={text}&site=tesseract.ru&lr=55" method="GET">';
                        $text .=     '<input type="search" name="text" placeholder="Поиск по сайту" />';
                        $text .= '</form>';

                        $turbo_content = $item->appendChild($dom->createElement('turbo:content'));
                        $turbo_content->appendChild($dom->createCDATASection($text));
                }
                // $dom->formatOutput = true; // красивый вывод
                return $dom->saveXML();
            } else {
                return false;
            }
        }
    }
    if ($this->is_access(1)) { // доступно только авторизированным
        $yaTurbo = new YaTurbo();
        if ($yaTurbo->getAction()!='') { // вызов через api, иначе вызов напрямую через php
            include_once dirname(__FILE__)."/../collections/main.php";
            echo $yaTurbo->query()->fetch();
        }
    } else {
        http_response_code(401);
    }
?>

<?php base::test();

    class InformerMain extends Api {
        public $col_page = 9;
        /**
         * Получение записи
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function icon_ext($name = null) {
            global $cms;
            $row_ = $cms->pdo->prepare("SELECT  svg
                                        FROM    bk_informer_ext
                                        WHERE   1=1
                                                and name = :name
                                        Order by name");
            $row_->execute([':name' => $name]);
            if ($row_->rowCount() > 0) {
                $row = $row_->fetch();
                return "/modules/informer/uploader/img/".$row['svg'];
            } else {
                return "/modules/informer/uploader/img/defult.svg";
            }
        }
        /**
         * Конвертор размеров
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        function formatSize($bytes) {
            if ($bytes >= 1073741824) {
                $bytes = number_format($bytes / 1073741824, 2) . ' GB';
            } elseif ($bytes >= 1048576) {
                $bytes = number_format($bytes / 1048576, 2) . ' MB';
            } elseif ($bytes >= 1024) {
                $bytes = number_format($bytes / 1024, 2) . ' KB';
            } elseif ($bytes > 1) {
                $bytes = $bytes . ' bytes';
            } elseif ($bytes == 1) {
                $bytes = $bytes . ' byte';
            } else {
                $bytes = '0 bytes';
            }
            return $bytes;
        }

        /**
         * сбор данных с id информеров!
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function data_collection($mas_id = []) {
            global $cms;
            foreach ($mas_id as $row) {
                if (is_numeric($row) && $row > 0) {
                    $id[] = $row;
                }
            }
            $mas = [];
            if (count($id) > 0) {
                $buf = ['fields' => [], 'item' => [], 'media' => []];

                $i_id = "i.id=".implode(' or i.id=', $id);

                $row_ = $cms->pdo->query("  SELECT  f.id, f.tpl_html, f.type, f.title, f.name_json, m.fk_modules
                                            FROM    bk_informer_tpl_modules as m,
                                                    bk_informer_tpl_fields as f,
                                                    bk_informer as i
                                            WHERE   1=1
                                                    and m.fk_tpl = f.fk_tpl
                                                    and i.fk_modules = m.fk_modules
                                                    and ($i_id)
                                            ORDER BY f.sort");
                if ($row_->rowCount()>0) {
                    while ($row = $row_->fetch()) {
                        $buf['fields'][$row['fk_modules']][] = $row;
                    }
                }

                $fk_informer = "i.fk_informer=".implode(' or i.fk_informer=', $id);
                $row_ = $cms->pdo->query("  SELECT  fi.*, i.fk_informer
                                            FROM    bk_informer_tpl_fields_item as fi
                                                    left join bk_informer_data_item as i on fi.id = i.fk_fields_item
                                                                                            and ($fk_informer)
                                            WHERE   1=1
                                                    and i.id > 0
                                            ORDER BY fi.item ASC");
                if ($row_->rowCount()>0) {
                    while ($row = $row_->fetch()) {
                        if ($row['tpl_html'] == 'checkbox') {
                            $buf['item'][$row['fk_informer']][$row['fk_tpl_fields']][] = $row['item'];
                        } else {
                            $buf['item'][$row['fk_informer']][$row['fk_tpl_fields']] = $row['item'];
                        }
                    }
                }

                $row_ = $cms->pdo->query("  SELECT  m.*, i.timestamp
                                            FROM    bk_informer_media as m,
                                                    bk_informer as i
                                            WHERE   1=1
                                                    and i.id = m.fk_informer
                                                    and m.del is NULL
                                                    and m.display is NULL
                                                    and ($i_id)
                                            ORDER BY sort");
                if ($row_->rowCount()>0) {
                    while ($row = $row_->fetch()) {
                        if ($row['type'] == 'youtube') {
                            $buf['media'][$row['fk_informer']][$row['type']][] = [  'media' => $row['media'],
                                                                                    'title' => $row['title']
                                                                                    ];

                        } else if ($row['type'] == 'node') {
                            $buf['media'][$row['fk_informer']][$row['type']][] = [  'media' => $row['media'],
                                                                                    'title' => $cms->structure_page($row['media'], 'title'),
                                                                                    'url'   => $cms->url($row['media']),
                                                                                    ];
                        } else if ($row['type'] != '') {
                            $url = "/files/informer/".$row['type']."/".date('Y/m/',strtotime($row['timestamp'])).$row['media'];
                            if (is_file(__DIR__."/../../..".$url)) {
                                $buf['media'][$row['fk_informer']][$row['type']][] = [  'url'       => $url,
                                                                                        'title'     => $row['title'],
                                                                                        'size'      => InformerMain::formatSize(filesize(__DIR__."/../../..".$url)),
                                                                                        'ext'       => strtolower(pathinfo($row['media'], PATHINFO_EXTENSION)),
                                                                                        'icon_ext'  => $this->icon_ext(strtolower(pathinfo($row['media'], PATHINFO_EXTENSION)))
                                                                                        ];
                            } else {
                                $buf['media'][$row['fk_informer']][$row['type']][] = [  'url'       => $url,
                                                                                        'title'     => $row['title'],
                                                                                        'size'      => 0,
                                                                                        'ext'       => strtolower(pathinfo($row['media'], PATHINFO_EXTENSION)),
                                                                                        'icon_ext'  => $this->icon_ext(strtolower(pathinfo($row['media'], PATHINFO_EXTENSION)))
                                                                                        ];
                                // рассылка на мыло
                            }
                        }
                    }
                }

                // все кроме radio  и select
                $fk_informer = "d.fk_informer=".implode(' or d.fk_informer=', $id);
                $data_ = $cms->pdo->query(" SELECT  d.fk_informer, d.fk_tpl_fields, d.item_varchar, d.item_text, d.item_datetime, d.item_decimal, d.item_int, d.item_date
                                            FROM    bk_informer_data as d,
                                                    bk_informer_tpl_fields as f
                                            WHERE   1=1
                                                    and f.id = d.fk_tpl_fields
                                                    and f.type <> 'radio'
                                                    and f.type <> 'select'
                                                    and ($fk_informer)
                                                    and d.fk_users_delete is NULL
                                            ");
                if ($data_->rowCount()>0) {
                    while ($data = $data_->fetch()) {
                        $data_row[$data['fk_informer']][$data['fk_tpl_fields']] = $data;
                    }
                }

                // radio
                $fk_informer = "d.fk_informer=".implode(' or d.fk_informer=', $id);
                $data_ = $cms->pdo->query(" SELECT  d.fk_informer, d.fk_tpl_fields, fi.item as item_radio
                                            FROM    bk_informer_data as d,
                                                    bk_informer_tpl_fields as f,
                                                    bk_informer_tpl_fields_item as fi
                                            WHERE   1=1
                                                    and f.id = d.fk_tpl_fields
                                                    and fi.id = d.item_radio
                                                    and f.type = 'radio'
                                                    and ($fk_informer)
                                                    and d.fk_users_delete is NULL
                                            ");
                if ($data_->rowCount()>0) {
                    while ($data = $data_->fetch()) {
                        $data_row[$data['fk_informer']][$data['fk_tpl_fields']] = $data;
                    }
                }

                $row_ = $cms->pdo->query("  SELECT  i.id, i.top, i.timestamp, i.fk_modules, m.fk_structure
                                            FROM    bk_informer as i,
                                                    bk_modules as m
                                            where   1=1
                                                    and m.id = i.fk_modules
                                                    and i.display IS NULL
                                                    and i.del IS NULL
                                                    and m.del = 0
                                                    and ($i_id)
                                            GROUP BY i.id
                                            ORDER BY i.top DESC, i.sort DESC");
                while ($row = $row_->fetch()) {
                    $mas_fields = null;

                    foreach ($buf['fields'][$row['fk_modules']] as $fields) {
                        if (in_array($fields['tpl_html'], ['checkbox'])) { // , 'radio', 'select'
                            $data = $buf['item'][$row['id']][$fields['id']];
                        } else if (in_array($fields['tpl_html'], ['media'])) {
                            $data = $buf['media'][$row['id']][$fields['type']];
                        } else {
                            $data = $data_row[$row['id']][$fields['id']]['item_'.$fields['type']];
                            if ((in_array($fields['type'], ['datetime','date']))&&($data != '')) {
                                $data = date("d.m.Y", strtotime($data));
                            }
                        }
                        $mas_fields[$fields['name_json']] = $data;
                    }

                    $mas[] = [  'id'        => $row['id'],
                                'top'       => $row['top'],
                                'url'       => $cms->url($row['fk_structure']).'?view='.$row['id'],
                                'fields'    => $mas_fields,
                            ];
                }
            }
            return $mas;
        }

        /**
         * Получение списка для List
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function get_record_id($params = null) {
            global $cms;
            $mas = [];
            if (is_numeric($params['id'])) {
                $row_ = $cms->pdo->prepare("SELECT   i.id, i.timestamp, i.fk_modules, m.fk_structure
                                            FROM    bk_informer as i,
                                                    bk_modules as m
                                            WHERE   1=1
                                                    and m.id = i.fk_modules
                                                    and i.display IS NULL
                                                    and i.del IS NULL
                                                    and i.id = :id");
                $row_->execute([':id' => $params['id']]);
                if ($row_->rowCount()>0) {
                    while ($row = $row_->fetch()) {
                        $id[] = $row['id'];
                    }
                    $mas = $this->data_collection($id)[0];
                }
            }
            return $mas;
        }
        /**
         * Получение списка для List
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function select($params = null) {
            global $cms;
            if (is_numeric($params['pageNum'])&&($params['pageNum'] > 0)) {
                if (is_numeric($params['pageLimit'])&&(in_array($params['pageLimit'], range(0, 100)))) {
                    $col_page = $params['pageLimit'];
                } else {
                    $col_page = $this->col_page;
                }
                $limit = " LIMIT ".(($params['pageNum']-1)*$col_page).", ".$col_page;
            } else if (is_numeric($params['limit'])) {
                $limit = " LIMIT ".$params['limit'];
            } else if ($params['pageNum'] != -1) {
                $limit = " LIMIT ".$this->col_page;
            }
            if ((is_numeric($params['node']))&&($params['node'] > 0)) {
                $bindSQL[] ='m.fk_structure = :node';
                $bindValue[] =[ 'prefix' => ':node',
                                'value'  => $params['node'],
                                'param'  => PDO::PARAM_INT];
            }
            if ((is_numeric($params['mid']))&&($params['mid'] > 0)) {
                $bindSQL[] ='i.fk_modules = :mod';
                $bindValue[] =[ 'prefix' => ':mod',
                                'value'  => $params['mid'],
                                'param'  => PDO::PARAM_INT];
            }
            if ($params['top'] == 1) {
                $bindSQL[] ='i.top = :top';
                $bindValue[] =[ 'prefix' => ':top',
                                'value'  => '1',
                                'param'  => PDO::PARAM_INT];
            }
            if ($params['top'] == -1) {
                $bindSQL[] ='i.top is NULL';
            }
            if (count($bindSQL) > 0) {
                $bindSQL = " and ".implode(' and ', $bindSQL);
            }
            foreach ($params as $key => $value) {
                $buf = explode('_', $key);
                if (($buf[0] == 'category')&&(is_numeric($buf[1]))) {
                    $join = " right join bk_informer_data as d on d.fk_informer = i.id  and fk_tpl_fields = :fk_tpl_fields and item_radio = :item_radio ";
                    // $join = " right join bk_informer_data as d on d.fk_informer = i.id  and fk_tpl_fields = :fk_tpl_fields and YEAR(item_datetime) = :item_datetime ";
                    $bindValue[] =[ 'prefix' => ':fk_tpl_fields',
                                    'value'  => $buf[1],
                                    'param'  => PDO::PARAM_INT];
                    $bindValue[] =[ 'prefix' => ':item_radio',
                                    'value'  => $value,
                                    'param'  => PDO::PARAM_INT];
                }
            }

            $row_ = $cms->pdo->prepare("SELECT  SQL_CALC_FOUND_ROWS i.id, i.top, i.timestamp, i.fk_modules, m.fk_structure
                                        FROM    bk_informer as i
                                                left join bk_modules as m on m.id = i.fk_modules
                                                {$join}
                                        WHERE   1=1
                                                and i.display IS NULL
                                                and i.del IS NULL
                                                and m.del = 0
                                                {$bindSQL}
                                        GROUP BY i.id
                                        ORDER BY i.sort DESC
                                        ".$limit);

            if (count($bindValue) > 0) {
                foreach ($bindValue as $row) {
                    $row_->bindValue($row['prefix'], $row['value']);
                }
            }
            $row_->execute([]);
            if ($row_->rowCount()>0) {
                $page['page_max'] = $cms->pdo->query("SELECT FOUND_ROWS()")->fetchColumn();
                while ($row = $row_->fetch()) {
                    $id[] = $row['id'];
                }
                $return['row'] = $this->data_collection($id);

                if ((is_numeric($params['mid']))&&($params['mid'] > 0)) {
                    $fields_ = $cms->pdo->prepare(" SELECT  f.id, f.type, f.title, f.name_json
                                                    FROM    bk_informer_tpl_modules as m,
                                                            bk_informer_tpl_fields as f
                                                    WHERE   1=1
                                                            and m.fk_tpl = f.fk_tpl
                                                            and f.use_filter = 1
                                                            and m.fk_modules = :id
                                                    ORDER BY f.sort");
                    $fields_->execute([':id' => $params['mid']]);
                    if ($fields_->rowCount()>0) {
                        while ($fields = $fields_->fetch()) {
                            if ($fields['type'] == 'datetime') {
                                $fk_informer = "d.fk_informer=".implode(' or d.fk_informer=', $id);
                                $data_ = $cms->pdo->prepare("   SELECT  max(YEAR(d.item_datetime)) as max, min(YEAR(d.item_datetime)) as min
                                                                FROM    bk_informer_data as d
                                                                WHERE   1=1
                                                                        and d.fk_users_delete is NULL
                                                                        and d.fk_tpl_fields = :fid
                                                                        and ($fk_informer)
                                                            ");
                                $data_->execute([':fid' => $fields['id']]);
                                if ($data_->rowCount()>0) {
                                    $data = $data_->fetch();
                                    for ($i = $data['max']; $i >= $data['min']; $i--) {
                                        $year[] = $i;
                                    }
                                    $return['filter'][] = ['id'         => $fields['id'],
                                                           'type'       => $fields['type'],
                                                           'title'      => $fields['title'],
                                                           'name_json'  => $fields['name_json'],
                                                           'item'       => $year
                                                            ];
                                }
                            }
                        }
                    }
                }

                // доработать в верхний блок
                $tpl_ = $cms->pdo->query("  SELECT  f.id, f.title
                                            FROM    bk_informer_tpl as t,
                                                    bk_informer_tpl_fields as f
                                            WHERE   1=1
                                                    and t.id = f.fk_tpl
                                                    and t.name = 'news'
                                                    and f.type = 'radio'
                                            ORDER BY f.sort");
                if ($tpl_->rowCount() > 0) {
                    while ($tpl = $tpl_->fetch()) {
                        $item_ = $cms->pdo->prepare("   SELECT  id, item
                                                        FROM    bk_informer_tpl_fields_item
                                                        WHERE   1=1
                                                                and fk_tpl_fields = :id
                                                        ORDER BY sort");
                        $item_->execute([':id' => $tpl['id']]);
                        if ($item_->rowCount() > 0) {
                            while ($item = $item_->fetch()) {
                                $tpl['item'][] = $item;
                            }
                        }
                        $return['filter'][] = $tpl;
                    }
                }



                if (is_numeric($col_page)) {
                    $return['url'] = $cms->url($row['fk_structure']);
                    $page['page_max'] = intval(($page['page_max'] - 1) / $col_page) + 1;  // максимальное количество страниц
                    if ($params['pageNum'] > $page['page_max']) {
                        $params['pageNum']=$page['page_max'];
                    }
                    if (($params['pageNum']-1) < 1) {
                        $page['page_s'] = 1;
                    } else {
                        $page['page_s'] = $params['pageNum']-1;
                    }
                    if (($params['pageNum']+1) > $page['page_max']) {
                        $page['page_f'] = $page['page_max'];
                    } else {
                        $page['page_f'] = $params['pageNum']+1;
                    }
                    if ($page['page_f'] > $page['page_s']) {
                        if ($params['pageNum'] > 1) {
                            $return['page'][] = ['page' => $params['pageNum']-1, 'char' => '&laquo;'];
                        }
                        if ($page['page_s'] > 1) {
                            $return['page'][] = ['page' => 1];
                        }
                        if ($page['page_s'] > 2) {
                            $return['page'][] = ['char' => '...'];
                        }
                        for ($i = $page['page_s']; $i <= $page['page_f']; $i++) {
                            if ($params['pageNum'] == $i) {
                                $return['page'][] = ['char' => $i, 'active'=> 1];
                            } else {
                                $return['page'][] = ['page' => $i];
                            }
                        }
                        if ($page['page_f'] < $page['page_max']-1) {
                            $return['page'][] = ['char' => '...'];
                        }
                        if ($page['page_f'] < $page['page_max']) {
                            $return['page'][] = ['page' => $page['page_max']];
                        }
                        if ($params['pageNum'] != $page['page_max']) {
                            $return['page'][] = ['page' => $params['pageNum']+1, 'char' => '&raquo;'];
                        }
                    }
                }
            }
            return $return;
        }

    }

    $informerMain = new InformerMain();

    if ($informerMain->getAction()!='') { // вызов через api, иначе вызов напрямую через php
        echo $informerMain->query()->fetch();
    }
?>

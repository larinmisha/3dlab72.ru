<?php base::test();

    class InformerTpl extends Api {

        /**
         * Получение записи
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function select($params = null) {
            global $cms;
            if ($cms->is_access(1, '', 'informer')) {
                $row_ = $cms->pdo->prepare("SELECT  tpl.id, tpl.title, tpl.name, if(tm.id, 1, NULL) as `check`
                                            FROM    bk_informer_tpl_modules as tm
                                                    RIGHT JOIN bk_informer_tpl as tpl on tpl.id = tm.fk_tpl and tm.fk_modules = :mod ");
                $row_->execute([':mod' => $params['mod']]);
                if ($row_->rowCount() > 0) {
                    while ($row = $row_->fetch()) {
                        $check_ = $cms->pdo->prepare("  SELECT fk_structure
                                                        FROM    bk_informer_tpl_modules as tm,
                                                                bk_modules as m
                                                        WHERE   1=1
                                                                and m.id = tm.fk_modules
                                                                and m.del = 0
                                                                and m.fk_structure = :node
                                                                and tm.fk_tpl = :id
                                                        ");
                        $check_->execute([  ':node'  => $params['node'],
                                            ':id'   => $row['id']
                                            ]);
                        $row['disable'] = !$check_->rowCount();


                        $mas['menu'][] = $row;
                        if ($row['check'] == 1) {
                            $mas['action'] = ['id' => $row['id'], 'title' => $row['title']];
                        }
                    }
                }
                return $mas;
            }
        }

        /**
         * Обновление записи
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function update($params = null) {
            global $cms;
            if ($cms->is_access(1)) {
                $row_ = $cms->pdo->prepare("SELECT  ms.id
                                            FROM    bk_modules as ms,
                                                    bk_modul as m,
                                                    bk_informer_tpl_modules as tplm
                                            WHERE   1=1
                                                    and m.id = ms.fk_modul
                                                    and m.id = tplm.fk_modules
                                                    and ms.open=1
                                                    and ms.del=0
                                                    and m.dirrect = 'informer'
                                                    and ms.id = :id
                                            ORDER BY ms.id ASC
                                            ");
                $row_->execute([':id'=>$params['mod']]);
                if ($row_->rowCount()>0) {
                    $row = $row_->fetch();
                    $row_ = $cms->pdo->prepare("INSERT INTO bk_informer_tpl_modules
                                                SET fk_modules = :fk_modules,
                                                    fk_tpl = :fk_tpl");
                    $row_->execute([':fk_tpl'       => $params['fk_tpl'],
                                    ':fk_modules'   => $row['id']]);
                    $mas['message']['true'][] = 'Шаблон установлен';
                } else {
                    $mas['message']['true'][] = 'Данные отсутствуют';
                    http_response_code(400);
                }
            } else {
                http_response_code(401);
            }
            return $mas;
        }
    }

    $informerTpl = new InformerTpl();

    if ($informerTpl->getAction()!='') { // вызов через api, иначе вызов напрямую через php
        echo $informerTpl->query()->fetch();
    }
?>

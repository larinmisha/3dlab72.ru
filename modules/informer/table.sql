/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Дамп таблицы bk_informer
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bk_informer`;

CREATE TABLE `bk_informer` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `fk_modules` int(6) DEFAULT '0',
  `top` tinyint(1) DEFAULT NULL,
  `sort` int(6) DEFAULT NULL,
  `send_email` int(6) DEFAULT NULL COMMENT 'Количество пользователей получивших рассылку',
  `ya_turbo_status` varchar(255) DEFAULT NULL COMMENT 'Статус турбо страниц',
  `ya_turbo_url` varchar(255) DEFAULT NULL COMMENT 'URL (turbo/processing)',
  `display` tinyint(1) DEFAULT '1' COMMENT '1 - по умолчанию не публикуем',
  `del` tinyint(1) DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `fk_users_add` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_bk_informer_bk_modules` (`fk_modules`),
  CONSTRAINT `fk_bk_informer_bk_modules` FOREIGN KEY (`fk_modules`) REFERENCES `bk_modules` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы bk_informer_data
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bk_informer_data`;

CREATE TABLE `bk_informer_data` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `fk_tpl_fields` int(6) DEFAULT NULL,
  `fk_informer` int(6) DEFAULT NULL,
  `item_varchar` varchar(255) DEFAULT NULL COMMENT 'название на html',
  `item_text` text COMMENT 'название поля',
  `item_datetime` datetime DEFAULT NULL,
  `item_decimal` decimal(10,2) DEFAULT NULL,
  `item_int` int(6) DEFAULT NULL,
  `item_date` date DEFAULT NULL,
  `item_radio` int(6) DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fk_users_add` int(6) DEFAULT NULL COMMENT 'Кто добавил',
  `fk_users_delete` int(6) DEFAULT NULL COMMENT 'Кто удалил',
  `timestamp_delete` datetime DEFAULT NULL COMMENT 'Когда удалил',
  PRIMARY KEY (`id`),
  KEY `fk_bk_informer_tpl_data_bk_informer_tpl_fields` (`fk_tpl_fields`),
  KEY `fk_bk_informer_tpl_data_bk_informer` (`fk_informer`),
  CONSTRAINT `fk_bk_informer_tpl_data_bk_informer` FOREIGN KEY (`fk_informer`) REFERENCES `bk_informer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_bk_informer_tpl_data_bk_informer_tpl_fields` FOREIGN KEY (`fk_tpl_fields`) REFERENCES `bk_informer_tpl_fields` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы bk_informer_data_item
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bk_informer_data_item`;

CREATE TABLE `bk_informer_data_item` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `fk_informer` int(6) DEFAULT NULL,
  `fk_fields_item` int(6) DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_bk_informer_tpl_data_item_bk_informer` (`fk_informer`),
  KEY `fk_bk_informer_tpl_data_item_bk_informer_tpl_fields_item` (`fk_fields_item`),
  CONSTRAINT `fk_bk_informer_tpl_data_item_bk_informer` FOREIGN KEY (`fk_informer`) REFERENCES `bk_informer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_bk_informer_tpl_data_item_bk_informer_tpl_fields_item` FOREIGN KEY (`fk_fields_item`) REFERENCES `bk_informer_tpl_fields_item` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы bk_informer_ext
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bk_informer_ext`;

CREATE TABLE `bk_informer_ext` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL COMMENT 'расширения через запятую',
  `svg` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `bk_informer_ext` WRITE;
/*!40000 ALTER TABLE `bk_informer_ext` DISABLE KEYS */;

INSERT INTO `bk_informer_ext` (`id`, `name`, `svg`)
VALUES
  (1,'jpg','jpg.svg'),
  (2,'jpeg','jpg.svg'),
  (3,'svg','svg.svg'),
  (5,'bmp','bmp.svg'),
  (6,'gif','gif.svg'),
  (7,'png','png.svg'),
  (8,'pdf','pdf.svg'),
  (9,'ppt','ppt.svg'),
  (10,'txt','txt.svg'),
  (11,'doc','doc.svg'),
  (12,'docx','doc.svg'),
  (13,'xls','xls.svg'),
  (14,'xlsx','xls.svg'),
  (15,'tif','tif.svg'),
  (16,'zip','zip.svg'),
  (17,'mp3','mp3.svg');

/*!40000 ALTER TABLE `bk_informer_ext` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы bk_informer_log
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bk_informer_log`;

CREATE TABLE `bk_informer_log` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `fk_users` int(6) DEFAULT NULL,
  `fk_informer` int(6) DEFAULT NULL,
  `fk_informer_file` int(6) DEFAULT NULL,
  `info` varchar(255) DEFAULT NULL,
  `detal` text,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_bk_informer_log_bk_users` (`fk_users`),
  KEY `fk_bk_informer_log_bk_informer` (`fk_informer`),
  KEY `fk_bk_informer_log_bk_informer_file` (`fk_informer_file`),
  CONSTRAINT `fk_bk_informer_log_bk_informer` FOREIGN KEY (`fk_informer`) REFERENCES `bk_informer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_bk_informer_log_bk_informer_file` FOREIGN KEY (`fk_informer_file`) REFERENCES `bk_informer_media` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_bk_informer_log_bk_users` FOREIGN KEY (`fk_users`) REFERENCES `bk_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы bk_informer_media
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bk_informer_media`;

CREATE TABLE `bk_informer_media` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `fk_informer` int(6) DEFAULT NULL,
  `title` text,
  `media` varchar(255) DEFAULT NULL,
  `type` varchar(10) DEFAULT NULL,
  `sort` int(6) DEFAULT NULL,
  `size` int(6) DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `display` int(6) DEFAULT NULL,
  `del` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_bk_informer_file_bk_informer` (`fk_informer`),
  CONSTRAINT `fk_bk_informer_file_bk_informer` FOREIGN KEY (`fk_informer`) REFERENCES `bk_informer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы bk_informer_tpl
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bk_informer_tpl`;

CREATE TABLE `bk_informer_tpl` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT 'text' COMMENT 'название на html',
  `title` varchar(255) DEFAULT NULL COMMENT 'название поля',
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `bk_informer_tpl` WRITE;
/*!40000 ALTER TABLE `bk_informer_tpl` DISABLE KEYS */;

INSERT INTO `bk_informer_tpl` (`id`, `name`, `title`, `timestamp`)
VALUES
  (1,'news','Новости','2020-08-18 00:20:54'),
  (2,'text','Текстовый формат','2020-08-18 00:21:02'),
  (3,'node','Полезные ссылки','2020-08-20 04:34:12'),
  (4,'video','Видеогалерея','2020-09-17 07:50:43'),
  (5,'photo','Фотогалерея','2020-09-26 12:57:37'),
  (6,'files','Файловый архив','2020-09-26 13:49:01'),
  (7,'','node','2020-09-26 19:30:42'),
  (8,'contacts','Контакты','2020-09-28 19:07:35');

/*!40000 ALTER TABLE `bk_informer_tpl` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы bk_informer_tpl_fields
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bk_informer_tpl_fields`;

CREATE TABLE `bk_informer_tpl_fields` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `fk_tpl` int(6) DEFAULT NULL,
  `name_json` varchar(255) DEFAULT NULL,
  `tpl_html` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL COMMENT 'int',
  `title` varchar(255) DEFAULT NULL COMMENT 'название поля',
  `basic` tinyint(1) DEFAULT NULL COMMENT 'Основные поля для таблицы в админке',
  `use_filter` tinyint(1) DEFAULT NULL,
  `sort` tinyint(1) DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_bk_informer_tpl_fields_bk_informer_tpl` (`fk_tpl`),
  CONSTRAINT `fk_bk_informer_tpl_fields_bk_informer_tpl` FOREIGN KEY (`fk_tpl`) REFERENCES `bk_informer_tpl` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `bk_informer_tpl_fields` WRITE;
/*!40000 ALTER TABLE `bk_informer_tpl_fields` DISABLE KEYS */;

INSERT INTO `bk_informer_tpl_fields` (`id`, `fk_tpl`, `name_json`, `tpl_html`, `type`, `title`, `basic`, `use_filter`, `sort`, `timestamp`)
VALUES
  (1,1,'title','varchar','varchar','Заголовок',1,NULL,1,'2020-08-18 03:20:17'),
  (2,1,'publication_date','datetime','datetime','Дата публикации',1,NULL,2,'2020-08-18 03:23:46'),
  (3,1,'text','text-tinymce','text','Содержание',NULL,NULL,3,'2020-08-18 03:24:18'),
  (4,1,'img','media','img','Фото-материалы',NULL,NULL,4,'2020-08-18 04:18:58'),
  (5,1,'file','media','file','Прикрепленные файлы',NULL,NULL,5,'2020-08-18 04:19:22'),
  (6,1,'youtube','media','youtube','Ютуб',NULL,NULL,6,'2020-08-18 04:19:22'),
  (7,2,'title','varchar','varchar','Заголовок',1,NULL,1,'2020-08-18 03:20:17'),
  (8,2,'text','text-tinymce','text','Содержание',NULL,NULL,2,'2020-08-18 03:24:18'),
  (10,2,'file','media','file','Прикрепленные файлы',NULL,NULL,4,'2020-08-18 04:19:22'),
  (17,7,'node','media','node','URL ссылка',NULL,NULL,2,'2020-08-23 08:55:52'),
  (18,3,'title','varchar','varchar','Название',1,NULL,1,'2020-08-17 22:20:17'),
  (20,3,'url','varchar','varchar','URL ссылка',NULL,NULL,1,'2020-09-25 22:58:50'),
  (21,4,'title','varchar','varchar','Заголовок',1,NULL,1,'2020-09-26 12:59:34'),
  (22,4,'publication_date','datetime','datetime','Дата публикации',1,NULL,2,'2020-09-26 13:00:40'),
  (23,4,'text','text-tinymce','text','Содержание',NULL,NULL,3,'2020-09-26 13:01:26'),
  (24,4,'youtube','media','youtube','Ютуб',NULL,NULL,6,'2020-09-26 13:03:08'),
  (25,5,'img','media','img','Фото-материалы',NULL,NULL,4,'2020-09-26 13:04:19'),
  (26,5,'title','varchar','varchar','Заголовок',1,NULL,1,'2020-09-26 13:51:19'),
  (27,5,'publication_date','datetime','datetime','Дата публикации',1,NULL,2,'2020-09-26 13:51:55'),
  (28,5,'text','text-tinymce','text','Содержание',NULL,NULL,3,'2020-09-26 13:53:05'),
  (29,6,'title','varchar','text','Заголовок',1,NULL,1,'2020-09-26 14:26:25'),
  (30,6,'file','media','file','Файл (один!)',NULL,NULL,3,'2020-09-26 14:29:24'),
  (37,7,'publication_date','date','date','Дата публикации',1,NULL,2,'2020-09-27 19:40:50'),
  (44,8,'text','text-tinymce','text','Содержание',NULL,NULL,1,'2020-09-28 19:09:01'),
  (45,8,'maps','varchar','varchar','Координата на карту',1,NULL,1,'2020-09-28 19:15:05');

/*!40000 ALTER TABLE `bk_informer_tpl_fields` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы bk_informer_tpl_fields_item
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bk_informer_tpl_fields_item`;

CREATE TABLE `bk_informer_tpl_fields_item` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `fk_tpl_fields` int(6) DEFAULT NULL,
  `item` varchar(255) DEFAULT NULL,
  `sort` tinyint(1) DEFAULT NULL,
  `timestamp_add` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_bk_informer_tpl_fields_item_bk_informer_tpl_fields` (`fk_tpl_fields`),
  CONSTRAINT `fk_bk_informer_tpl_fields_item_bk_informer_tpl_fields` FOREIGN KEY (`fk_tpl_fields`) REFERENCES `bk_informer_tpl_fields` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы bk_informer_tpl_modules
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bk_informer_tpl_modules`;

CREATE TABLE `bk_informer_tpl_modules` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `fk_modules` int(6) DEFAULT NULL,
  `fk_tpl` int(6) DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_bk_informer_tpl_modules_bk_modules` (`fk_modules`),
  KEY `fk_bk_informer_tpl_modules_bk_informer_tpl` (`fk_tpl`),
  CONSTRAINT `fk_bk_informer_tpl_modules_bk_informer_tpl` FOREIGN KEY (`fk_tpl`) REFERENCES `bk_informer_tpl` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_bk_informer_tpl_modules_bk_modules` FOREIGN KEY (`fk_modules`) REFERENCES `bk_modules` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

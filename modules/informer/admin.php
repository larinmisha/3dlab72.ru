<script src="https://maps.api.2gis.ru/2.0/loader.js"></script>
<div class="js-container__menu"></div>
<div class="js-container__reestr"></div>
<?php
    base::test();

    base::minify_import("node_modules/handlebars/dist/handlebars.min.js");
    base::minify_import("library/comparehandlebars.js");

    base::minify_import("modules/informer/uploader/script.js"); // для uploader
    base::minify_import("modules/informer/js/admin.js");

    include_once "tpl/admin/menu.php";
    include_once "tpl/admin/list.php";
    include_once "tpl/admin/card.php";
    include_once "tpl/admin/history.php";
?>

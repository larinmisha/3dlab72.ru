<?php
    header('Cache-Control: private, no-cache="set-cookie"');
    header('Expires: 0');
    header('Pragma: no-cache');

    # Кол-во символов в строке
    $symbols_num = 4;

    # Генерация массива символов контрольной строки
    for($i = 0; $i < $symbols_num; $i++) {
        $captcha_key_array[] = rand(0, 9);
    }
    $captcha_key = implode('', $captcha_key_array);
    $_SESSION['captcha'] = strtolower($captcha_key);
    # Берём щрифты
    $fonts_dir = opendir($modules['dir'].'fonts');

    while(false !== ($module = readdir($fonts_dir))) {
        if($module != '.' and $module != '..') {
            $fonts_array[] = $module;
        }
    }
    $fonts_num = count($fonts_array);
    # Ширина-высота изображения
    $width = $symbols_num * 25;
    $height = 50;
    # Создание изображения
    $image = imagecreatetruecolor($width, $height);
    # Рисуем фон
    $background = imagecolorallocate($image, rand(230, 255), rand(230, 255), rand(230, 255));
    imagefill($image, 0, 0, $background);
    # Рисуем "шахматную" доску
    $cube_side = rand(8, 12); # размер грани квадратика
    $q = 0;
    while($q <= $height / $cube_side) {
        $i = 0;
        while($i <= $width) {
            if(fmod($q, 2)) {
                $cube_side = rand(8, 12); # Размер стороны квадратика
                $color = imagecolorallocate($image, rand(150, 255), rand(150, 255), rand(150, 255));
                imagefilledrectangle ($image, $i*2+$cube_side, $q*$cube_side, $i*2+$cube_side*2, $q*$cube_side+$cube_side, $color );
            } else {
                $cube_side = rand(8, 12); # Размер стороны квадратика
                $color = imagecolorallocate($image, rand(150, 255), rand(150, 255), rand(150, 255));
                imagefilledrectangle ($image, $i*2, $q*$cube_side, $i*2+$cube_side, $q*$cube_side+$cube_side, $color );
            }
            $i = $i + $cube_side;
        }
        $q++;
    }
    # Рисуем строку
    $i = 10;
    foreach($captcha_key_array as $index => $value) {
        $x_position = rand(6, $height - 16);
        $str_color = imagecolorallocate($image, rand(50, 150), rand(50, 150), rand(50, 150));
        $captcha_key_array = imagettftext($image, 25, rand(-30, 30), $i, rand(25, 45), $str_color, $modules['dir'].'fonts/'.$fonts_array[rand(0, $fonts_num-1)], $value);
        $i = $i + rand(15, 25) ;
    }
    # Рисуем рамку
    $black = imagecolorallocate($image, 0, 0, 0);
    imageline($image, 0, 0, 0, $height, $black); # left
    imageline($image, $width - 1, 0, $width - 1, $height, $black); # right
    imageline($image, $width, 0, 0, 0, $black); # up
    imageline($image, $width, $height - 1, 0, $height - 1, $black); # down
    # Выводим изображение в браузер
    header('content-type: image/png');
    imagepng($image);
    # Высвобождаем ресурсы
    imagedestroy($image);
?>

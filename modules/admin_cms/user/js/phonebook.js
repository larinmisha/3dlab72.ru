$(function() {
    var active_page;
    function list_user(page, search) {
        active_page = page;
        $('.js__block-loader').show();
        $.ajax({
            type: "POST",
            url: '/api/user/phonebook/select/json',
            data: {node: NODE, page: page, search: search}
        }).done(function( json ) {
            var html = template(json);
            $('.js-container__reestr').html(html);
            $('.js__block-loader').fadeOut(400);
        });
    }
    function list_access(id) {
        $('.js__block-loader').show();
        $.ajax({
            type: "POST",
            url: '/api/user/phonebook/accessForm/json',
            data: {id: id}
        }).done(function( json ) {
            var html = template_form_access(json);
            $('.js-container__reestr').html(html);
            $('.js__block-loader').fadeOut(400);
            TinyMCE();
        });
    }
    if ($("#js-template__form_user").html()!=undefined) {
        var template             = Handlebars.compile($("#js-template__form_user").html());
        var template_form_access = Handlebars.compile($("#js-template__form_access").html());
        Handlebars.registerHelper("inc", function(value, options) {
            return parseInt(value) + 1;
        });
        list_user(1);
    }
    $('body').on('click', '.js__table-refresh', function(e) {
        search = '';
        if ($(this).data('saearch') == 1) {
            search = $('.js__table-refresh-input').val();
        }
        list_user($(this).data('page'), search);
    });

    $('body').on('click', '.js-modal__user_access', function(e) {
        list_access($(this).data('id'));
    });

    $('body').on('click', '.js__btn-phonebookHide', function(e) {
        user = $(this).data('user');
        $('.js__block-loader').show();
        var formData = new FormData();
        formData.append('id',               $(this).data('id'));

        main.ajaxRequest('/api/user/phonebook/phonebookHide/json', formData)
            .done(function(json, textStatus, jqXHR) {
                list_access(user);
            });
    });

    $('body').on('click', '.js__btn-accessHide', function(e) {
        user = $(this).data('user');
        $('.js__block-loader').show();
        var formData = new FormData();
        formData.append('id',               $(this).data('id'));

        main.ajaxRequest('/api/user/phonebook/accessHide/json', formData)
            .done(function(json, textStatus, jqXHR) {
                list_access(user);
            });
    });

    $('body').on('click', '.js__btn-userDelete', function(e) {
        user = $(this).data('user');
        $('.js__block-loader').show();
        var formData = new FormData();
        formData.append('id',               $(this).data('id'));

        main.ajaxRequest('/api/user/phonebook/userDelete/json', formData)
            .done(function(json, textStatus, jqXHR) {
                list_user(1);
            });
    });

    $('body').on('click', '.js__btn-accessDelete', function(e) {
        user = $(this).data('user');
        $('.js__block-loader').show();
        var formData = new FormData();
        formData.append('id',               $(this).data('id'));

        main.ajaxRequest('/api/user/phonebook/accessDelete/json', formData)
            .done(function(json, textStatus, jqXHR) {
                list_access(user);
            });
    });

    $('body').on('click', '.js__btn-phonebookDelete', function(e) {
        user = $(this).data('user');
        $('.js__block-loader').show();
        var formData = new FormData();
        formData.append('id',               $(this).data('id'));

        main.ajaxRequest('/api/user/phonebook/phonebookDelete/json', formData)
            .done(function(json, textStatus, jqXHR) {
                list_access(user);
            });
    });

    $('body').on('click', '.js-modal__phonebookSave', function(e) {
        user = $(this).data('user');
        var formData = new FormData();
        formData.append('id',               $(this).data('id'));
        formData.append('fk_users_access',  $(this).data('access'));
        formData.append('type',             $('#type_'+$(this).data('id')).val());
        formData.append('name',             $('#name_'+$(this).data('id')).val());
        formData.append('description',      $('#description_'+$(this).data('id')).val());

        main.ajaxRequest('/api/user/phonebook/phonebookSave/json', formData)
            .done(function(json) {
                list_access(user);
            });
    });

    $('body').on('click', '.js-modal__accessSave', function(e) {
        user = $(this).data('user');
        var formData = new FormData();
        formData.append('id',               $(this).data('id'));
        formData.append('fk_users',         $(this).data('user'));
        formData.append('fk_access_group',  $('#group_'+$(this).data('id')).val());
        formData.append('department',       $('#department_'+$(this).data('id')).val());

        main.ajaxRequest('/api/user/phonebook/accessSave/json', formData)
            .done(function(json) {
                list_access(user);
            });
    });

    $('body').on('click', '.js-modal__userSave', function(e) {
        user = $(this).data('id');
        var formData = new FormData();
        formData.append('id',               $(this).data('id'));
        formData.append('f',                $('#user_f').val());
        formData.append('i',                $('#user_i').val());
        formData.append('o',                $('#user_o').val());
        formData.append('sort',             $('#user_sort').val());
        formData.append('email',            $('#user_email').val());
        formData.append('description',      $('#user_description').val());
        if ($('#user_password').val() != '') {
            formData.append('password',         $('#user_password').val());
        }

        main.ajaxRequest('/api/user/phonebook/userSave/json', formData)
            .done(function(json) {
                if (user == '') {
                    user = json.id;
                }
                list_access(user);
            });
    });

    $('body').on('click', '.js__btn-materials', function(e) {
        modal_doc_max($(this).data('title'),'<div class="js-upload" data-id="'+$(this).data('id')+'" data-type="'+$(this).data('type')+'"></div>','lg','');
        $('.js-upload').each(function(i,elem) {
            designer.init(this);
        });
    });
});

var tpl_login;
var tpl_registratio;
var tpl_profile;
var tpl_message;
$(function() {
    tpl_login        = Handlebars.compile($("#js__user-login").html());
    tpl_registration = Handlebars.compile($("#js__user-registration").html());
    tpl_profile      = Handlebars.compile($("#js__user-profile").html());
    tpl_message      = Handlebars.compile($("#js__user-message").html());
});
function authclass() {

    $(document).ready(function () {
        auth.init();
    });

    this.init = function (e) {

        // Вход
        $('body').on('click', '.js-user__login', function(e) {
            auth.form_login();
        });
        $('body').on('click', '.js-user__login_save', function(e) {
            auth.login_save();
        });

        // Регистрация
        $('body').on('click', '.js-user__registration', function(e) {
            var html = tpl_registration({domen: document.location.origin});
            $('.js-modal__block').html(html);
            $('#myModal_user__registration').modal();
        });
        $('body').on('click', '.js-user__registration_save', function(e) {
            auth.registration_save();
        });

        $('body').on('click', '.js-user__profile', function(e) {
            var html = tpl_profile({domen: document.location.origin});
            $('.js-modal__block').html(html);
            $('#myModal_user__profile').modal();
        });
        $('body').on('click', '.js-user__update_password_save', function(e) {
            auth.update_password();
        });
        $('body').on('click', '.js-user__update_profile_save', function(e) {
        });
        $('body').on('click', '.js-user__update_subscribe', function(e) {
            $('.js-user__update_ch').val($(this).data('subscribe'));
            auth.update_profile();
        });

        $('body').on('click', '.js-user__logout', function(e) {
            location.href = "/?logout";
        });
        $('body').on('click', '.js-user__update', function(e) {
            $('#form_login').hide();
            $('#form_UpdatePassword').show();
        });
        $('body').on('click', '.js-user__update_save', function(e) {
            $('#myModal_user__login').modal('hide');
            setTimeout(() => auth.update_login(), 1000);
        });
        if ($('.js-open_form_login').val() == 1) {
            auth.form_login();
        }

        // keypress
        // $('body').on('keypress', '.js-user__login_keypress', function(e) {
        //  if (e.which === 13) {
        //      auth.login_save();
        //  }
        // });
        // $('body').on('keypress', '.js-user__reg_keypress', function(e) {
        //  if (e.which === 13) {
        //      auth.reg_save();
        //  }
        // });
        // $('body').on('keypress', '.js-user__update_keypress', function(e) {
        //  if (e.which === 13) {
        //      auth.update_save();
        //  }
        // });
    };
}

authclass.prototype = {
    form_login: function(email) {
        var html = tpl_login({domen: document.location.origin});
        $('.js-modal__block').html(html);
        $('#myModal_user__login').modal();
    },
    subscribe_visit: function(email) {
        var data = new FormData();
        data.append('email', email);
        main.ajaxRequest('/api/user/profile/subscribe_visit/json', data)
            .fail(function(jqXHR) {
                json = {'text': 'Не верный формат Email', 'status': false};
                var html = tpl_message(json);
                $('.js-modal__block').html(html);
                $('#myModal_user__message').modal();
            }).done(function(json) {
                json = {'text': 'Подписка продлена', 'status': true};
                var html = tpl_message(json);
                $('.js-modal__block').html(html);
                $('#myModal_user__message').modal();
            });
    },
    setRegistrationCode: function(code, email) {
        var data = new FormData();
        data.append('code', code);
        data.append('email', email);
        main.ajaxRequest('/api/user/email/setRegistrationCode/json', data)
            .fail(function(jqXHR) {
                json = {'text': 'Код не найден, или уже использовался', 'status': false};
                var html = tpl_message(json);
                $('.js-modal__block').html(html);
                $('#myModal_user__message').modal();
            }).done(function(json) {
                json = {'text': 'Ваша учетная запись активна!', 'status': true};
                var html = tpl_message(json);
                $('.js-modal__block').html(html);
                $('#myModal_user__message').modal();
            });
    },
    login_save: function() {
        $('.js-form__login').animate({height: 'hide'}, 1);
        $('.js-form-loader__login').delay(100).animate({height: 'show'}, 500);
        $('.js-user__info').animate({height: 'hide'}, 1);
        var formData = main.fetchformData('#form_login');
        main.ajaxRequest('/api/user/email/login/json', formData)
            .fail(function(jqXHR) {
                if (jqXHR.status == 401) { // email and password is not valid
                    text_code = "Не верный логин или пароль";
                } else if (jqXHR.status == 403) { // limit exceeded
                    text_code = "Превышен лимит";
                }
                $('.js-user__info .alert').addClass('alert-danger'); // можно прописать в форму
                $('.js-form__login').delay(1900).animate({height: 'show'}, 500);
                $('.js-form-loader__login').delay(400).animate({height: 'hide'}, 500);
                $('.js-user__login_info .text').html(text_code);
                $('.js-user__login_info').delay(1500).animate({height: 'show'}, 500);
            })
            .done(function(json) {
                if ($.urlParam('mobile')) {
                    $(location).attr('href', "/?token="+json.token);
                } else {
                    $(location).attr('href',location.href.replace(/logout$/g, ''));
                }
            });
    },
    registration_save: function() {
        $('.js-form__registration').animate({height: 'hide'}, 1);
        $('.js-form-loader__registration').delay(100).animate({height: 'show'}, 500);
        $('.js-user__registration_info').animate({height: 'hide'}, 1);
        $('.js-user__registration_info .alert').removeClass('alert-info alert-danger');
        var formData = main.fetchformData('#form_registration');
        formData.append('whoSent', 'web');
        main.ajaxRequest('/api/user/email/getRegistrationCode/json', formData)
            .fail(function(jqXHR) {
                if (jqXHR.status == 401) { // email and password is not valid
                    text_code = "Есть незаполненные поля, либо не верный формат одного из значений";
                } else if (jqXHR.status == 400) { // limit exceeded
                    text_code = "Такой email уже зарегистрирован";
                } else if (jqXHR.status == 423) { // captcha
                    text_code = "Не верно указан код с картинки";
                }
                $('.js-user__registration_info .alert').addClass('alert-danger'); // можно прописать в форму
                $('.js-form__registration').delay(1900).animate({height: 'show'}, 500);
                $('.js-form-loader__registration').delay(400).animate({height: 'hide'}, 500);
                $('.js-user__registration_info .text').html(text_code);
                $('.js-user__registration_info').delay(1500).animate({height: 'show'}, 500);
            })
            .done(function(json) {
                $('.js-user__registration_info .alert').addClass('alert-info'); // можно прописать в форму
                $('.js-form-loader__registration').delay(400).animate({height: 'hide'}, 500);
                $('.js-user__registration_info .text').html("Вам отправленно письмо с подтверждением регистрации.");
                $('.js-user__registration_info').delay(1500).animate({height: 'show'}, 500);
            });
    },
    update_password: function() {
        $('.js-form__update_password').animate({height: 'hide'}, 1);
        $('.js-form-loader__update_password').delay(100).animate({height: 'show'}, 500);
        $('.js-user__info').animate({height: 'hide'}, 1);
        var formData = main.fetchformData('#form_update_password');
        main.ajaxRequest('/api/user/json?action=update_password', formData)
            .done(function(json) {
                $('.js-user__info .alert').removeClass('alert-info alert-danger');
                if (json.status == 1) {
                    $('.js-user__info .alert').addClass('alert-info');
                } else {
                    $('.js-user__info .alert').addClass('alert-danger');
                }
                buf = '';
                if ((json.message != null)&&(json.message.length>0)) {
                    $.each(json.message, function(i,v) {
                        buf = buf + v + "<br>";
                    });
                }
                $('.js-form__update_password').delay(1900).animate({height: 'show'}, 500);
                $('.js-form-loader__update_password').delay(400).animate({height: 'hide'}, 500);
                $('.js-user__update_password_info .text').html(buf);
                $('.js-user__update_password_info').delay(1500).animate({height: 'show'}, 500);
            });
    },
    update_profile: function() {
        $('.js-form__update_profile').animate({height: 'hide'}, 1);
        $('.js-form-loader__update_profile').delay(100).animate({height: 'show'}, 500);
        $('.js-user__info').animate({height: 'hide'}, 1);
        var formData = main.fetchformData('#form_update_profile');
        main.ajaxRequest('/api/user/json?action=update_profile', formData)
            .done(function(json) {
                $('.js-user__info .alert').removeClass('alert-info alert-danger');
                if (json.status == 1) {
                    $('.js-user__info .alert').addClass('alert-info');
                } else {
                    $('.js-user__info .alert').addClass('alert-danger');
                }
                buf = '';
                if ((json.message != null)&&(json.message.length>0)) {
                    $.each(json.message, function(i,v) {
                        buf = buf + v + "<br>";
                    });
                }
                $('.js-form__update_profile').delay(1900).animate({height: 'show'}, 500);
                $('.js-form-loader__update_profile').delay(400).animate({height: 'hide'}, 500);
                $('.js-user__update_profile_info .text').html(buf);
                $('.js-user__update_profile_info').delay(1500).animate({height: 'show'}, 500);
            });
    },
    update_login: function() {
        var formData = main.fetchformData('#form_UpdatePassword');
        formData.append('whoSent', 'web');
        main.ajaxRequest('/api/user/email/getUpdatePasswordCode/json', formData)
            .fail(function(jqXHR) {
                if (jqXHR.status == 401) { // email and password is not valid
                    json = {'text': 'Есть незаполненные поля, либо не верный формат одного из значений', 'status': false};
                }
                var html = tpl_message(json);
                $('.js-modal__block').html(html);
                $('#myModal_user__message').modal();
            })
            .done(function(json) {
                json = {'text': 'Вам отправленно письмо с инструкцией.', 'status': true};
                var html = tpl_message(json);
                $('.js-modal__block').html(html);
                $('#myModal_user__message').modal();
            });
    }
};

var auth = new authclass();

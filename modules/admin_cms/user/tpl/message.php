<script type="text/x-handlebars-template" id="js__user-message">
<div class="modal fade" id="myModal_user__message" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class='modal-header'>
                <button type='button' class='close' data-dismiss='modal'>
                    <span aria-hidden='true'>&times;</span>
                </button>
                <h4 class='modal-title' id='myModalLabel'>Верификация</h4>
            </div>
            <div class='modal-body'>
                <div class='form-user'>
                    <div class='info'>
                        <div class="alert {{#if status}}alert-info{{else}}alert-danger{{/if}}" role="alert">
                            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                            <span class='text'>
                                {{text}}
                                {{#each message}}
                                    {{this}}<br>
                                {{/each}}
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class='modal-footer'>
            </div>
        </div>
    </div>
</div>
</script>

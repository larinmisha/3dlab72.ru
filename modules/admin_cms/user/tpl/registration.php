<script type="text/x-handlebars-template" id="js__user-registration">
<div class="modal fade" id="myModal_user__registration" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class='modal-header'>
                <button type='button' class='close' data-dismiss='modal'><span aria-hidden='true'>&times;</span></button>
                <h4 class='modal-title' id='myModalLabel'>Регистрация</h4>
            </div>
            <div class='modal-body modal_body'>
                <form method='post' action='' id='form_registration'>
                    <div class='form-user'>
                        <div class='info js-user__registration_info' style='display:none;'>
                            <div class="alert alert-info" role="alert">
                                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                <span class='text'></span>
                            </div>
                        </div>
                        <div class="js-form__registration">
                            <div class="row">
                                <div class="col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label for="email">Фамилия:</label>
                                        <input type="text" name='f' class='form-control' value="">
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label for="email">Имя:</label>
                                        <input type="text" name='i' class='form-control' value="">
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label for="email">Отчество:</label>
                                        <input type="text" name='o' class='form-control' value="">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <label for="email">Телефон:</label>
                                        <input type="text" name='phone' class='form-control' value="" placeholder="9221234567">
                                    </div>
                                </div>
                                <div class="col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <label for="email">E-mail:</label>
                                        <input type="text" name='email' class='form-control' value="" placeholder="info@site.ru">
                                    </div>
                                </div>
                                <div class="col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <label for="email">Пароль:</label>
                                        <input type="password" name='password' class='form-control' value="">
                                    </div>
                                </div>
                            </div>
                            <label for="captcha">Введите код с картинки:</label>
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <img src="/api/captcha/" class="b-captcha js-captcha">
                                    <button type="button" name="recaptcha" class="btn btn-default b-captcha js-recaptcha"><i class="glyphicon glyphicon-refresh"></i></button>
                                </span>
                                <input type="text" name="captcha" class="form-control b-captcha" value="">
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name='subscribe'> <span class="map_help">Согласен получать рассылку новостей</span>
                                </label>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-block btn-success js-user__registration_save" type="button">Зарегистрироваться</button>
                            </div>
                        </div>
                        <div class="form-loader js-form-loader__registration">
                            <div class="form-group">
                                <center><label for="password">Обработка запроса</label></center>
                                <div  class="form-control loader"></div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class='modal-footer'>
                <div class="js-form__registration">
                    <center>
                        <table class="hidden">
                            <tr>
                                <td>
                                    <a target='_blank' href="/?login=vk">
                                        <?php echo file_get_contents(__DIR__."/../img/vk.svg"); ?>
                                    </a>
                                    &nbsp; &nbsp;
                                </td>
                                <td>
                                    <a target='_blank' href="/?login=facebook">
                                        <?php echo file_get_contents(__DIR__."/../img/facebook.svg"); ?>
                                    </a>
                                    &nbsp; &nbsp;
                                </td>
                                <td>
                                    <a target='_blank' href="/?esia">
                                        <?php echo file_get_contents(__DIR__."/../img/esia.svg"); ?>
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </center>
                </div>
            </div>
        </div>
    </div>
</div>
</script>

<?php
    base::test();
    include "modules/admin_cms/user/collections/phonebook.php";
    $arrayData = $user_phonebook->book(['tree' => $_GET['tree'], 'user' => $_GET['user']]);
    $this->meta['title'] = $arrayData['group']['name'];
?>
<div class="phonebook">
    <div class="container">
        <?php if (count($arrayData)>0) { ?>
            <?php if (count($arrayData['row'])>0) { ?>
                <?php foreach ($arrayData['row'] as $row) { ?>
                    <div class="row">
                        <div class="col-xs-12 col-sm-8 col-xm-8 col-md-8">
                            <dl class="dl-horizontal mb2">
                                <dt>Ф.И.О.:</dt>
                                <dd><?php echo $row['f'].' '.$row['i'].' '.$row['o']; ?></dd>
                                <?php foreach ($row['position'] as $position) { ?>
                                    <?php if (count($position['group']) > 0) { ?>
                                        <dt>Место работы:</dt>
                                        <?php foreach ($position['group'] as $group) { ?>
                                            <dd>
                                                <?php if ($group['link'] == 1) { ?>
                                                    <a href="?tree=<?php echo $group['id']; ?>"><?php echo $group['name']; ?></a>
                                                <?php } else { ?>
                                                    <?php echo $group['name']; ?>
                                                <?php } ?>
                                            </dd>
                                        <?php } ?>
                                    <?php } ?>
                                    <?php if (count($position['position']) > 0) { ?>
                                        <dt>Должность:</dt>
                                        <dd><?php echo $position['position']; ?></dd>
                                    <?php } ?>
                                    <?php if (count($position['info']['address']) > 0) { ?>
                                        <dt>Адрес:</dt>
                                        <?php foreach ($position['info']['address'] as $info) { ?>
                                            <dd>
                                                <?php echo $info['name']; ?>
                                                <?php if ($info['description'] != '') { ?>
                                                    (<?php echo $info['description']; ?>)
                                                <?php } ?>
                                            </dd>
                                        <?php } ?>
                                    <?php } ?>

                                    <?php if (count($position['info']['email']) > 0) { ?>
                                        <dt>E-mail:</dt>
                                        <?php foreach ($position['info']['email'] as $info) { ?>
                                            <dd>
                                                <a href="mailto:<?php echo $info['name']; ?>"><?php echo $info['name']; ?></a>
                                                <?php if ($info['description'] != '') { ?>
                                                    (<?php echo $info['description']; ?>)
                                                <?php } ?>
                                            </dd>
                                        <?php } ?>
                                    <?php } ?>

                                    <?php if (count($position['info']['phone']) > 0) { ?>
                                        <dt>Телефон:</dt>
                                        <?php foreach ($position['info']['phone'] as $info) { ?>
                                            <dd>
                                                <a href="tel:<?php echo $info['name']; ?>"><?php echo $info['name']; ?></a>
                                                <?php if ($info['description'] != '') { ?>
                                                    (<?php echo $info['description']; ?>)
                                                <?php } ?>
                                            </dd>
                                        <?php } ?>
                                    <?php } ?>

                                    <?php if (count($position['info']['inside']) > 0) { ?>
                                        <dt>Внутр. телефон:</dt>
                                        <?php foreach ($position['info']['inside'] as $info) { ?>
                                            <dd>
                                                <?php echo $info['name']; ?>
                                                <?php if ($info['description'] != '') { ?>
                                                    (<?php echo $info['description']; ?>)
                                                <?php } ?>
                                            </dd>
                                        <?php } ?>
                                    <?php } ?>
                                <?php } ?>
                            </dl>
                        </div>
                        <div class="hidden-xs col-sm-2 col-xm-2 col-md-2">
                            <?php if ($row['photo'] != '') { ?>
                                <div class="mx1_75">
                                    <img src="<?php echo $row['photo']; ?>" class="img-responsive" alt="<?php echo $row['f'].' '.$row['i'].' '.$row['o']; ?>">
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>
                <?php if (count($arrayData['group']['tree']) > 0) { ?>
                    <ul>
                        <?php foreach ($arrayData['group']['tree'] as $group) { ?>
                            <li>
                                <a href="?tree=<?php echo $group['id']; ?>"><?php echo $group['name']; ?></a>
                            </li>
                        <?php } ?>
                    </ul>
                <?php } ?>
            <?php } else { ?>
                <h3>По данному запросу пользователь не найден</h3>
            <?php } ?>
        <?php } ?>
    </div>
</div>

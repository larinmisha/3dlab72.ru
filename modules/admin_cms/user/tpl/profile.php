<script type="text/x-handlebars-template" id="js__user-profile">
<div class="modal fade" id="myModal_user__profile" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class='modal-header'>
                <button type='button' class='close' data-dismiss='modal'><span aria-hidden='true'>&times;</span></button>
                <h4 class='modal-title' id='myModalLabel'>Профиль</h4>
            </div>
            <div class='modal-body modal_body'>
                <div class="form-user">
                    <div class='info js-user__info js-user__update_profile_info' style='display:none;'>
                        <div class="alert" role="alert">
                            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                            <span class='text'></span>
                        </div>
                    </div>

                    <div class='info js-user__info js-user__update_password_info' style='display:none;'>
                        <div class="alert" role="alert">
                            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                            <span class='text'></span>
                        </div>
                    </div>

                    <div class='row'>
                        <div class="col-xs-6 col-xlg-6">
                            <form method='post' action='' id='form_update_profile'>
                                <div class='js-form__update_profile'>
                                    <div class="form-group">
                                        <label for="f">Фамилия:</label>
                                        <input type="text" name='f' class='form-control' value="<?php echo $_SESSION['user']['f']; ?>">
                                    </div>

                                    <div class="form-group">
                                        <label for="i">Имя:</label>
                                        <input type="text" name='i' class='form-control' value="<?php echo $_SESSION['user']['i']; ?>">
                                    </div>

                                    <div class="form-group">
                                        <label for="o">Отчество:</label>
                                        <input type="text" name='o' class='form-control' value="<?php echo $_SESSION['user']['o']; ?>">
                                    </div>

                                    <div class="form-group mt1">
                                        <button class="btn btn-block btn-default js-user__update_profile_save" type="button">Сохранить ФИО</button>
                                    </div>

                                    <div class="form-group mt1">
                                        <input type="hidden" name='subscribe' class='form-control js-user__update_ch' value="<?php echo $_SESSION['user']['subscribe']; ?>">
                                        <?php if ($_SESSION['user']['subscribe']==1) { ?>
                                            <button class="btn btn-block btn-default js-user__update_subscribe" type="button" data-subscribe="">Отписаться от новостей</button>
                                        <?php } else { ?>
                                            <button class="btn btn-block btn-default js-user__update_subscribe" type="button" data-subscribe="1">Подписаться на новости</button>
                                        <?php } ?>
                                    </div>

                                    <!-- <div class="form-group">
                                        <label for="email">E-mail:</label>
                                        <input type="text" name='email' class='form-control' value="<?php echo $_SESSION['user']['email']; ?>" <?php if ($_SESSION['user']['email']!='') echo 'disabled'; ?>>
                                    </div>

                                    <div class="form-group">
                                        <label for="phone">Телефон:</label>
                                        <input type="text" name='phone' class='form-control' value="<?php echo $_SESSION['user']['phone']; ?>" data-mask="(999) 999-99-99" <?php if ($_SESSION['user']['email']!='') echo 'disabled'; ?>>
                                    </div> -->
                                </div>

                                <div class="form-loader js-form-loader__update_profile">
                                    <div class="form-group">
                                        <center><label for="password">Обработка запроса</label></center>
                                        <div  class="form-control loader"></div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-xs-6 col-xlg-6">
                            <form method='post' action='' id='form_update_password'>
                                <div class='js-form__update_password'>
                                    <div class="form-group">
                                        <label for="old_password">Старый пароль:</label>
                                        <input type="password" name='old_password' class='form-control' value="" autocomplete="off">
                                    </div>

                                    <div class="form-group">
                                        <label for="new_password">Новый пароль:</label>
                                        <input type="password" name='new_password' class='form-control' value="" autocomplete="off">
                                    </div>

                                    <div class="form-group">
                                        <label for="new_password_repeat">Повторите пароль:</label>
                                        <input type="password" name='new_password_repeat' class='form-control' value="" autocomplete="off">
                                    </div>

                                    <div class="form-group mt1">
                                        <button class="btn btn-block btn-default js-user__update_password_save" type="button">Сменить пароль</button>
                                    </div>
                                </div>

                                <div class="form-loader js-form-loader__update_password">
                                    <div class="form-group">
                                        <center><label for="password">Обработка запроса</label></center>
                                        <div  class="form-control loader"></div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class='modal-footer'>
            </div>
        </div>
    </div>
</div>
</script>


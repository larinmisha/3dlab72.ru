<script type="text/x-handlebars-template" id="js__user-login">
<div class="modal fade" id="myModal_user__login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class='modal-header'>
                <button type='button' class='close' data-dismiss='modal'><span aria-hidden='true'>&times;</span></button>
                <h3 class='modal-title' id='myModalLabel'>Авторизация</h3>
            </div>
            <div class='modal-body modal_body'>
                <div class="login_esia">
                    <div class="txt mb1">Для получения доступа к закрытым разделам, авторизуйтесь через Госуслуги. Если вы не зарегистрированы в системе, оставьте заявку по телефону указанному внизу сайта.</div>
                    <div class="esia">
                        <a target='_blank' href="/?esia">
                            <?php echo file_get_contents(__DIR__."/../img/esia.svg"); ?>
                        </a>
                        <span class="ml1">Вход через ЕСИА</span>
                    </div>
                </div>
                <form method='post' action='' id='form_login' class="hidden">
                    <div class='form-user js-form__login'>
                        <div class='info js-user__info js-user__login_info' style='display:none;'>
                            <div class="alert alert-info" role="alert">
                                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                <span class='text'></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="login">Логин:</label>
                            <input type="text" name='email' id='js-input_login_tip' class='form-control' placeholder="Пример: user@mail.ru" value="">
                        </div>
                        <div class="form-group">
                            <label for="password">Пароль:</label>
                            <input type="password" name='password' class='form-control' value="">
                        </div>
                        <div class="form-group">
                            <input id="ch" name="ch" type="checkbox" class="ch_save_log" checked="checked">
                            <span class="ch_save_log_t">Запомнить</span>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-block btn-success js-user__login_save" type="button">Войти</button>
                        </div>
                        <div class="form-group hidden">
                            <button class="btn btn-block btn-default js-user__update new_pas" type="button">
                                Забыли пароль?
                            </button>
                        </div>
                    </div>
                    <div class="form-loader js-form-loader__login">
                        <div class="form-group">
                            <center><label for="password">Обработка запроса</label></center>
                            <div  class="form-control loader"></div>
                        </div>
                    </div>
                </form>

                <form method='post' action='' id='form_UpdatePassword' style='display:none;'>
                    <div class='form-user js-form__update_login'>
                        <div class='info js-user__info js-user__update_login_info' style='display:none;'>
                            <div class="alert alert-info" role="alert">
                                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                <span class='text'></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="email">E-mail:</label>
                            <input type="text" name='email' class='form-control' value="">
                        </div>

                        <div class="form-group">
                            <label for="password">Новый пароль:</label><br>
                            <input type="password" name='password' class='form-control' value="">
                            <small>(вступит в силу после подтверждения)</small>
                        </div>

                        <div class="form-group">
                            <button class="btn btn-block btn-default js-user__update_save" type="button">Восстановить пароль</button>
                        </div>

                    </div>

                    <div class="form-loader js-form-loader__update_login">
                        <div class="form-group">
                            <center><label for="password">Обработка запроса</label></center>
                            <div  class="form-control loader"></div>
                        </div>
                    </div>
                </form>
            </div>
            <div class='modal-footer'>
                <center>
                    <table>
                        <tr>
                            <td class="hidden">
                                <a target='_blank' href="/?login=vk">
                                    <?php echo file_get_contents(__DIR__."/../img/vk.svg"); ?>
                                </a>
                                &nbsp; &nbsp;
                            </td>
                            <td class="hidden">
                                <a target='_blank' href="/?login=facebook">
                                    <?php echo file_get_contents(__DIR__."/../img/facebook.svg"); ?>
                                </a>
                                &nbsp; &nbsp;
                            </td>
                            <td>

                            </td>
                        </tr>
                    </table>
                </center>
            </div>
        </div>
    </div>
</div>
</script>

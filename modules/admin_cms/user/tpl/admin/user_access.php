<?php base::test(); ?>
<script type="text/x-handlebars-template" id="js-template__form_access">
    <table class="table">
        <thead>
            <tr>
                <th>id</th>
                <th>Фамилия</th>
                <th>Имя</th>
                <th>Отчество</th>
                <th>Email/login</th>
                <th>Сортировка</th>
                <th>Пароль</th>
                <th>&nbsp;</th>
            </tr>
            <tr>
                <th>
                    id
                </th>
                <th>
                    <input type="text" class="form-control" id="user_f" placeholder="Фамилия" value="{{user.f}}">
                </th>
                <th>
                    <input type="text" class="form-control" id="user_i" placeholder="Имя" value="{{user.i}}">
                </th>
                <th>
                    <input type="text" class="form-control" id="user_o" placeholder="Отчество" value="{{user.o}}">
                </th>
                <th>
                    <input type="text" class="form-control" id="user_email" placeholder="Email" value="{{user.email}}">
                </th>
                <th>
                    <input type="text" class="form-control" id="user_sort" placeholder="[0-99]" value="{{user.sort}}">
                </th>
                <th>
                    <input type="text" class="form-control" id="user_password" placeholder="{{user.password}}" value="">
                </th>
                <th>
                    <span class="input-group-btn btn-group-sm">
                        <div class="btn btn-default js-modal__userSave" data-id="{{user.id}}">
                            <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                            Сохранить
                        </div>
                    </span>
                </th>
            </tr>
            <tr>
                <td colspan="8">
                    <textarea class="form-control js-tinymce" id="user_description" rows="3">{{{user.description}}}</textarea>
                </td>
            </tr>
        </thead>
    </table>

    {{#if user.id}}
        <table class="table">
            <thead>
                <tr>
                    <th>Группа доступа</th>
                    <th>Должность</th>
                    <th>Действие</th>
                </tr>
            </thead>
            {{#each access}}
                <tr>
                    <td>
                        <select class="form-control input-sm" id="group_{{id}}">
                            {{#each group}}
                                <option value="{{id}}" {{#if selected}}selected{{/if}}>{{name}}</option>
                            {{/each}}
                        </select>
                    </td>
                    <td>
                        <input type="text" class="form-control input-sm" placeholder="Должность" id="department_{{id}}" value="{{department}}">
                    </td>
                    <td>
                        <span class="input-group-btn btn-group-sm">
                            <div class="btn btn-default js-modal__accessSave" data-id="{{id}}" data-user="{{../user.id}}">
                                <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                            </div>
                            <button type="button" class="btn btn-{{#if hide}}error{{else}}default{{/if}} js__btn-accessHide" data-id="{{id}}" data-user="{{../user.id}}">
                                <i class="glyphicon glyphicon-eye-{{#if hide}}close{{else}}open{{/if}}"></i>
                            </button>
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li class="dropdown-header">
                                    access id# {{id}}
                                </li>
                                <li role="separator" class="divider"></li>
                                <li>
                                    <a href="JavaScript:" class="js__btn-accessDelete" data-id="{{id}}" data-user="{{../user.id}}">
                                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span> &nbsp;
                                        Подтвердить удаление
                                    </a>
                                </li>
                            </ul>
                        </span>

                    </td>
                </tr>
                {{#each phonebook}}
                    <tr>
                        <td colspan="2">
                            <div class="row">
                                <div class="col-xs-3">
                                    <select class="form-control input-sm" id="type_{{id}}">
                                        <option value="">--Тип информации--</option>
                                        {{#each type_info}}
                                            <option value="{{id}}" {{#if selected}}selected{{/if}}>{{name}}</option>
                                        {{/each}}
                                    </select>
                                </div>
                                <div class="col-xs-4">
                                    <input type="text" class="form-control" id="name_{{id}}" value="{{name}}" placeholder="Информация">
                                </div>
                                <div class="col-xs-5">
                                    <input type="text" class="form-control" id="description_{{id}}" value="{{description}}" placeholder="Дополнительно">
                                </div>
                            </div>
                        </td>
                        <td>
                            <span class="input-group-btn btn-group-sm">
                                <div class="btn btn-default js-modal__phonebookSave" data-id="{{id}}" data-access="{{../id}}" data-user="{{../../user.id}}">
                                    <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                                </div>
                                <button type="button" class="btn btn-{{#if hide}}error{{else}}default{{/if}} js__btn-phonebookHide" data-id="{{id}}" data-user="{{../../user.id}}">
                                    <i class="glyphicon glyphicon-eye-{{#if hide}}close{{else}}open{{/if}}"></i>
                                </button>
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li class="dropdown-header">
                                        info id# {{id}}
                                    </li>
                                    <li role="separator" class="divider"></li>
                                    <li>
                                        <a href="JavaScript:" class="js__btn-phonebookDelete" data-id="{{id}}" data-user="{{../../user.id}}">
                                            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span> &nbsp;
                                            Подтвердить удаление
                                        </a>
                                    </li>
                                </ul>
                            </span>

                        </td>
                    </tr>
                {{/each}}
                <tr>
                    <td colspan="2">
                        <div class="row">
                            <div class="col-xs-3">
                                <select class="form-control input-sm" id="type_new">
                                    <option value="">--Тип информации--</option>
                                    {{#each ../type_info}}
                                        <option value="{{id}}" {{#if selected}}selected{{/if}}>{{name}}</option>
                                    {{/each}}
                                </select>
                            </div>
                            <div class="col-xs-4">
                                <input type="text" class="form-control" id="name_new" placeholder="Информация">
                            </div>
                            <div class="col-xs-5">
                                <input type="text" class="form-control" id="description_new" placeholder="Дополнительно">
                            </div>
                        </div>
                    </td>
                    <td>
                        <span class="input-group-btn btn-group-sm">
                            <div class="btn btn-default js-modal__phonebookSave" data-id="new" data-access="{{id}}" data-user="{{../user.id}}">
                                <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                                Добавить
                            </div>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <hr style="border-top: 1px solid #000;">
                    </td>
                </tr>
            {{/each}}
            <tr>
                <td>
                    <select class="form-control input-sm" id="group_new">
                        <option value="">--Группа доступа--</option>
                        {{#each group}}
                            <option value="{{id}}">{{name}}</option>
                        {{/each}}
                    </select>
                </td>
                <td>
                    <input type="text" class="form-control input-sm" placeholder="Должность" id="department_new" value="">
                </td>
                <td>
                    <span class="input-group-btn btn-group-sm">
                        <div class="btn btn-default js-modal__accessSave" data-id="new" data-user="{{user.id}}">
                            <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                            Добавить
                        </div>
                    </span>
                </td>
            </tr>
        </table>
    {{/if}}

    <table class="table">
        <thead>
            <tr>
                <th>Тип</th>
                <th>Последний вход</th>
                <th>Регистрация</th>
            </tr>
            {{#each verification}}
                <tr>
                    <th>
                        {{#if svg}}
                            <img src='{{svg}}' style="width: 18px;"> {{title}}
                        {{/if}}
                    </th>
                    <th>
                        {{last_visit}}
                    </th>
                    <th>
                        {{timestamp}}
                    </th>
                </tr>
            {{/each}}
        </thead>
    </table>
</script>

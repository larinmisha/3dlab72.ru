<?php base::test(); ?>
<script type="text/x-handlebars-template" id="js-template__form_user">
    <div class="input-group input-group-sm mb1">
        <span class="input-group-addon" id="sizing-addon1">
            Фамилия(без ИО) или Email
        </span>
        <input class="form-control js__table-refresh-input" placeholder="Можно искать по части строки" value="">
        <span class="input-group-btn">
            <button type="submit" class="btn btn-default js__table-refresh" data-saearch="1" data-order="ASC">
                Поиск
            </button>
        </span>
    </div>
    <table class="table table-hover">
        <thead>
            <tr>
                <th>Пользователь</th>
                <th width="50%">Доступ</th>
                <th width="100px">Действие</th>
            </tr>
        </thead>
        <tbody>
        {{#if row}}
            {{#each row}}
                <tr>
                    <td>
                        <nobr>
                            <b>{{f}} {{i}} {{o}}</b>
                        </nobr>
                        <br>
                        {{#each verification}}
                            {{#if svg}}
                                <img src='{{svg}}' style="width: 18px;">
                            {{/if}}
                        {{/each}}
                        {{email}}
                        <br>
                        {{#if last_visit}}
                            <small {{#if activity}}style="color: red;"{{/if}}>Визит: {{last_visit}}</small><br>
                        {{/if}}

                    </td>
                    <td>
                        {{#each access}}
                            <span class="input-group ">
                                <b>{{inc @index}}. {{name}}</b>
                                {{#if admin}}
                                    &nbsp;<span class="glyphicon glyphicon-{{admin}}" aria-hidden="true"></span>
                                {{/if}}
                            </span>
                            {{#if department}}
                                <div><small>Должность: {{department}}</small></div>
                            {{/if}}
                        {{/each}}
                    </td>
                    <td>
                        <span class="input-group-btn btn-group-xs">
                            <button type="button" class="btn btn-{{#if photo}}info{{else}}default{{/if}} js__btn-materials" data-id="{{id}}" data-type="photo" data-title="" style="width: 55px;">
                                <i class="glyphicon glyphicon-picture"></i>
                            </button>
                            <button type="button" class="btn btn-default js-modal__user_access" data-id="{{id}}" data-type="clubs" data-title="Доступ">
                                <i class="glyphicon glyphicon-pencil"></i>
                            </button>
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li class="dropdown-header">
                                    user id# {{id}}
                                </li>
                                <li role="separator" class="divider"></li>
                                <li>
                                    <a href="JavaScript:" class="js__btn-userDelete" data-id="{{id}}">
                                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span> &nbsp;
                                        Подтвердить удаление
                                    </a>
                                </li>
                            </ul>
                        </span>
                    </td>
                </tr>
            {{/each}}
        {{else}}
            <tr>
                <td colspan=3>По выбраным параметрам пользователей не найдено</td>
            </tr>
        {{/if}}
        </tbody>
    </table>
    {{#if page}}
        <nav>
            <ul class="pagination">
                {{#if page.block1}}
                    <li class='disabled'>
                        <a href='JavaScript:'>&laquo;</a>
                    </li>
                {{else}}
                    <li>
                        <a class="js__table-refresh" href="JavaScript:" data-page="{{page.block1.page}}">&laquo;</a>
                    </li>
                {{/if}}
                {{#if page.block2}}
                    <li>
                        <a class="js__table-refresh" href="JavaScript:" data-page="{{page.block2.page}}">1</a>
                        <li class='disabled'><a href='JavaScript:'>...</a></li>
                    </li>
                {{/if}}
                {{#each page.block3}}
                    {{#if active}}
                        <li class="active">
                            <a href='JavaScript:'>{{page}}</a>
                        </li>
                    {{else}}
                        <li class='hidden-xs'>
                            <a class="js__table-refresh" href="JavaScript:" data-page="{{page}}">{{page}}</a>
                        </li>
                    {{/if}}
                {{/each}}
                {{#if page.block4}}
                    <li>
                        <a class="js__table-refresh" href="JavaScript:" data-page="{{page.block4.page}}">{{page.block4.page}}</a>
                        <li class='disabled'><a href='JavaScript:'>...</a></li>
                    </li>
                {{/if}}
                {{#if page.block5}}
                    <li>
                        <a class="js__table-refresh" href="JavaScript:" data-page="{{page.block5.page}}">&raquo;</a>
                    </li>
                {{else}}
                    <li class='disabled'>
                        <a href='JavaScript:'>&raquo;</a>
                    </li>
                {{/if}}
            </ul>
        </nav>
    {{/if}}
</script>

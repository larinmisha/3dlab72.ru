<?php
    base::test();

    base::minify_import("node_modules/handlebars/dist/handlebars.min.js");
    base::minify_import("modules/admin_cms/user/js/phonebook.js");
    base::minify_import("modules/admin_cms/user/uploader/script.js");
    ?>
        <div class="btn-group btn-group-justified" role="group" aria-label="...">
            <div class="btn-group" role="group">
                <button type="button" class="btn btn-xs btn-default js__table-refresh" data-id="0">
                    <span class="glyphicon glyphicon-refresh" aria-hidden="true"></span> Реестр
                </button>
            </div>
            <div class="btn-group" role="group">
                <button type="button" class="btn btn-xs btn-default js-modal__user_access">
                    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Добавить нового пользователя
                </button>
            </div>
        </div>
        <hr>
        <div class="js-container__menu"></div>
        <div class="js-container__reestr"></div>
    <?php
    include "tpl/admin/reestr_user.php";
    include "tpl/admin/user_access.php";
?>

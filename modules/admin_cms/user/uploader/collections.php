<?php base::test();

    class Uploader extends Api {
        /**
         * Получение записи
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function select($params = null) {
            global $cms;
            $mas = [];
            $mas['id']   = $params['id'];
            $mas['type'] = $params['type'];
            if ($params['display']!='true') {
                $wh = " and f.hide is NULL ";
            }
            $row_ = $cms->pdo->prepare("SELECT photo
                                        FROM    bk_users
                                        WHERE   1=1
                                                and photo is not NULL
                                                and id = :id");
            $row_->execute([':id' => $params['id']]);
            if ($row_->rowCount()>0) {
                while($row = $row_->fetch()) {
                    $img = "/files/user/".$row['photo'];
                    $mas['img'] = $img;
                }
            } else {
                $mas['message']['true'][] = 'Данные отсутствуют';
            }
            return $mas;
        }

        /**
         * подгрузка tpl
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function template($params = null) {
            global $cms;
            include "modules/admin_cms/user/uploader/tpl/".$_REQUEST['type']."/".$_REQUEST['tpl'].".php";
        }

        /**
         * Добавление файла
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function insert($params = null) {
            global $cms;
            $mas = [];

            // Нет файлова
            if (isset($_FILES)) {
                $_FILES = $_FILES[key($_FILES)];
            } else {
                $mas['err'][] = "Не передан файл";
                echo json_encode($mas);
                die();
            }

            // Проверка размера файла
            if(!$_FILES['tmp_name'] != "") {
                $mas['err'][] = "Файл слишком большой: <b>".$_FILES['name']."</b> ";
                echo json_encode($mas);
                die();
            }

            // Нет такого объекта в БД
            $row_=$cms->pdo->prepare("SELECT * FROM bk_users where id = :id LIMIT 1");
            $row_->execute([':id' => $params['id']]);
            if ($row_->rowCount()>0) {
                $row = $row_->fetch();
            } else {
                $mas['err'][] = "Неверный идентификатор (".$params['id'].")";
                echo json_encode($mas);
                die();
            }

            // Формат файла (по типу)
            $r = strtolower(pathinfo($_FILES['name'], PATHINFO_EXTENSION));
            if (!in_array($r, array("jpg", "png", "gif", "bmp", "jpeg", "doc", "docx", "tif", "pdf", "xls", "xlsx", "rtf", "odt", "odp", "ppt", "pptx", "pps", "ppsx", "zip", "rar"))) {
                    $mas['err'][] = "Не верный формат файла";
                    echo json_encode($mas);
                    die();
                }
            // советую переменную dir не переименовывать, пусть будет такой формат, он универсальный.
            // если понадобиться новый тип файлов в папке то просто передашь новый параметр type и тут его тоже зарегистрируешь условием выше
            $file_name = uniqid('').".".$r;
            $path = base::file_dir(['user']);

            $file = $path.$file_name;
            if (copy($_FILES['tmp_name'], $file)) {
                $row_ = $cms->pdo->prepare("UPDATE  bk_users
                                            SET     photo = :photo
                                            WHERE   id = :id");
                include_once 'vendor/verot/class.upload.php/src/class.upload.php';
                $handle = new \verot\Upload\Upload($file);
                if ($params['size'] != 'original') {
                    $handle->file_overwrite     = true;
                    $handle->image_no_enlarging = true;
                    $handle->image_ratio        = true;
                    $handle->image_resize       = true;
                    $handle->image_y            = 1080;
                    $handle->image_x            = 1620;
                }
                $handle->process($path);
                unlink(__DIR__.'/../../../../files/user/'.$row['photo']);
                $row_->execute([':id' => $params['id'], ':photo' => $handle->file_dst_name]);
            } else {
                $mas['err'][] = "не удалось записать файл: <b>".$_FILES['name']."</b>";
            }
            return $mas;
        }

    }

    $uploader = new Uploader();

    if ($uploader->getAction()!='') { // вызов через api, иначе вызов напрямую через php
        echo $uploader->query()->fetch();
    }
?>

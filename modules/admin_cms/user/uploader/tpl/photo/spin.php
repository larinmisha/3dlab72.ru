<style>
    .progress {
        margin-bottom: 0;
    }
    .progress .progress-bar {
        min-width: 2em;
    }
</style>
<div class="alert alert-success" role="alert" id='{{id}}_alert'>
    <label>Загрузка файла: {{name}}</label>
    <div class='progress'>
        <div id='{{id}}_progress' class='progress-bar progress-bar-success progress-bar-striped active' role='progressbar' aria-valuenow='0' aria-valuemin='0' aria-valuemax='100'>
        0%
        </div>
    </div>
    <strong id='{{id}}_strong'>Статус:</strong> <span id='{{id}}_span'>Идёт загрузка и обработка файла</span>
</div>

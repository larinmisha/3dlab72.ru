<div class='form-group'>
    <div class="input-group">
        <input type='file' id='file' multiple class='form-control js-file__input'>
        <span class="input-group-btn">
            <button type='button' class='btn btn-success js-file__btn' data-type='{{type}}' data-id='{{id}}' disabled>Загрузить</button>
        </span>
    </div>
    <div class="row js-file__status"></div>
</div>
<div class="js-upload__list_file"></div>

<div class="row">
    <center>
        <img src="{{img}}" style="width: 50%;">
    </center>
</div>


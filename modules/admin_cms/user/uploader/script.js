function designerClass() {}

designerClass.prototype = Object.create(mainClass.prototype);
designerClass.prototype.constructor = designerClass;

designerClass.prototype.init = function(element) {
    designer.refresh(element);
    designer.active_events(element);
};
designerClass.prototype.active_events = function(element) {
    $(element).on('click', '.js-file__btn', function() {
        f = $(element).find('.js-file__input').get(0);
        if(f.files.length < 1) {
            modal_doc_max('Ошибка','Необходимо выбрать файл для загрузки','md','');
        } else {
            this_ = this;
            $.ajax({
                url: '/api/user/uploader/template/row',
                data: {'tpl': "spin", 'type': $(element).data('type')}
            }).done(function( tpl ) {
                for(var i = 0; i < f.files.length; i++) {
                    id = 'js-upload__spin-'+$.now(); // уникальное число, беру unix
                    var template = Handlebars.compile(tpl);
                    var html = template({'id': id, 'name': f.files[i].name});

                    $(element).find('.js-file__status').prepend(html);
                    up = new uploader(f.files[i], {
                        url: '/api/user/uploader/insert/json',
                        // prefix: 'my_name',
                        id: id,
                        post: { 'type'      : $(this_).data('type'),
                                'id'        : $(this_).data('id'),
                                'save_title': $('.js-file__ch:checked').val(),
                                'size'      : $('.js-file__size').val()
                                },
                    });
                    up.send();
                }
            });
        }
    });
    $(element).on('change', '.js-file__input', function() {
        var input = $(this),
            numFiles = input.get(0).files ? input.get(0).files.length : 1,
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');

        input.trigger('fileselect', [numFiles, label]);
        $('.js-file__btn').prop('disabled', false);
    });

    function uploader(input, options) {
        var $this = this;
        this.settings = {
            prefix:'file',
            id:'file',
            post: '',
            url:window.location.href,
            onprogress:function(ev){
                pr = ((ev.loaded/ev.total)*100)+'%';
                $('#'+$this.settings.id+'_progress').html(pr);
                $('#'+$this.settings.id+'_progress').css('width',pr);
            },
            error:function(msg){ console.log(msg); },
            success:function(data){
                designer.refresh(element);
            }
        };
        $.extend(this.settings, options);

        this.input = input;
        this.xhr = new XMLHttpRequest();

        this.send = function(){
            var data = new FormData();
            data.append(String($this.settings.prefix),$this.input);
            $this.upload(data);
        };

        this.upload = function(data){
            jQuery.each($this.settings.post, function( i, val ) {
                data.append(i, val);
            });
            $this.xhr.open('POST',$this.settings.url, true);
            $this.xhr.send(data);
        };

        if(this.settings.onprogress) this.xhr.upload.addEventListener('progress',this.settings.onprogress,false);
        this.xhr.onreadystatechange = function(ev){
            if($this.xhr.readyState == 4) {
                if($this.xhr.status == 200) {
                    if($this.settings.success) $this.settings.success($this.xhr.responseText,ev);
                    $this.input.value = '';
                } else {
                    if($this.settings.error) $this.settings.error(ev);
                }
            }
        };
    }
}

designerClass.prototype.refresh = function(element) {
    $.ajax({
        type: "POST",
        url: '/api/user/uploader/select/json',
        data: {'id': $(element).data('id'), 'type': $(element).data('type'), 'display': true}
    }).done(function( json ) {
        $(element).empty();
        $.ajax({
            type: "POST",
            url: '/api/user/uploader/template/row',
            data: {'tpl': "row", 'type': $(element).data('type')}
        }).done(function( tpl ) {
            var template = Handlebars.compile(tpl);
            var html = template(json);
            $(element).html(html);
            TinyMCE();
        });
    });
};

var designer = new designerClass();
$('.js-upload').each(function(i,elem) {
    designer.init(this);
});


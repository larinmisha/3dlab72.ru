<?php
    if ($_GET['login'] == 'vk') {
        $_SESSION['login_HTTP_REFERER'] = $_SERVER['HTTP_REFERER'];
        $_SESSION['login_HTTP_REFERER'] = str_replace("logout", "", $_SESSION['login_HTTP_REFERER']);
        $get_url = ['client_id'     => conf::$auth['vk']['id'],
                    'redirect_uri'  => conf::$http."?mobile=".$_GET['mobile'],
                    'code'          => $_GET['code'],
                    'display'       => 'popup',
                    'scope'         => 'email',
                    'state'         => 'vk',
                    'revoke'        => 1];
        header('HTTP/1.1 200 OK');
        header('Location: https://oauth.vk.com/authorize?'.http_build_query($get_url));
        die();
    }
    // https://tesseract24.ru/?login=vk&mobile=true
    if (($_GET['state'] == 'vk')&&($_GET['code'] != '')) {
        $get_url = ['client_id'     => conf::$auth['vk']['id'],
                    'client_secret' => conf::$auth['vk']['secret'],
                    'redirect_uri'  => conf::$http."?mobile=".$_GET['mobile'],
                    'code'          => $_GET['code']];
        $buf = file_get_contents("https://oauth.vk.com/access_token?".http_build_query($get_url));
        if ($buf != '') {
            $json = json_decode($buf,1);
            $get_url = ['user_id'       => $json['user_id'],
                        'fields'        => 'contacts',
                        'v'             => '5.52',
                        'access_token'  => $json['access_token']];
            $buf = file_get_contents('https://api.vk.com/method/users.get?'.http_build_query($get_url));

            $json_options = json_decode($buf,1);


            include_once __DIR__."/collections/profile.php";
            include_once __DIR__."/collections/provider.php";
            $api = $provider->login(['provider'     => 'vk',
                                    'email'        => $json['email'],
                                    'id_provider'  => $json_options['response'][0]['id'],
                                    'f'            => $json_options['response'][0]['last_name'],
                                    'i'            => $json_options['response'][0]['first_name'],
                                    'o'            => '',
                                    'ch'           => 1]);
            if (count($api['message']['true']) > 0) {
                if ($_GET['mobile'] == 'true') {
                    header('Location: '.conf::$http.'?token='.$api['token']);
                } else if ($_SESSION['login_HTTP_REFERER']!='') {
                    header('Location: '.$_SESSION['login_HTTP_REFERER']);
                } else {
                    header('Location: '.conf::$http);
                }
            } else {
                foreach ($api['message']['false'] as $value) {
                    echo "<pre>";print_r($value);echo "</pre>";
                }
                $get_url = ['client_id'     => conf::$auth['vk']['id'],
                            'redirect_uri'  => conf::$http."?mobile=".$_GET['mobile'],
                            'code'          => $_GET['code'],
                            'display'       => 'popup',
                            'scope'         => 'email',
                            'state'         => 'vk',
                            'revoke'        => 1];
                ?>
                <a href="<?php echo 'https://oauth.vk.com/authorize?'.http_build_query($get_url); ?>">Повторить авторизацию</a>
                <?php
            }
        }

        die();
    }

    if ($_GET['login'] == 'facebook') {
        $_SESSION['login_HTTP_REFERER'] = $_SERVER['HTTP_REFERER'];
        $_SESSION['login_HTTP_REFERER'] = str_replace("logout", "", $_SESSION['login_HTTP_REFERER']);
        $mobile = '';
        if ($_GET['mobile'] != '') $mobile = "?mobile=".$_GET['mobile'];
        $get_url = ['client_id'     => conf::$auth['facebook']['id'],
                    'redirect_uri'  => conf::$http.$mobile,
                    // 'display'       => 'popup',
                    'scope'         => 'email',
                    'state'         => 'facebook',
                    ];
        header('HTTP/1.1 200 OK');
        header('Location: https://www.facebook.com/v3.3/dialog/oauth?'.http_build_query($get_url));
        die();
    }
    // https://tesseract24.ru/?login=facebook&mobile=true
    if (($_GET['state'] == 'facebook')&&($_GET['code'] != '')) {
        $mobile = '';
        if ($_GET['mobile'] != '') $mobile = "?mobile=".$_GET['mobile'];

        $get_url = ['client_id'     => conf::$auth['facebook']['id'],
                    'redirect_uri'  => conf::$http.$mobile,
                    'client_secret' => conf::$auth['facebook']['secret'],
                    'code'          => $_GET['code']];
        $buf = file_get_contents("https://graph.facebook.com/v3.3/oauth/access_token?".http_build_query($get_url));
        if ($buf != '') {
            $json = json_decode($buf,1);
            $get_url = ['client_id'     => conf::$auth['facebook']['id'],
                        'redirect_uri'  => conf::$http.$mobile,
                        'client_secret' => conf::$auth['facebook']['secret'],
                        'code'          => $_GET['code'],
                        'access_token'  => $json['access_token'],
                        'fields'        => 'id,name,email,gender,location'
                        ];
            $buf = file_get_contents('https://graph.facebook.com/v3.3/me?'.http_build_query($get_url));

            $json_options = json_decode($buf,1);


            include_once __DIR__."/collections/profile.php";
            include_once __DIR__."/collections/provider.php";
            $api = $provider->login(['provider'     => 'facebook',
                                    'email'        => $json_options['email'],
                                    'id_provider'  => $json_options['id'],
                                    'f'            => $json_options['name'],
                                    'i'            => '',
                                    'o'            => '',
                                    'ch'           => 1]);
            if (count($api['message']['true']) > 0) {
                if ($_GET['mobile'] == 'true') {
                    header('HTTP/1.1 200 OK');
                    header('Location: '.conf::$http.'?token='.$api['token']);
                } else if ($_SESSION['login_HTTP_REFERER']!='') {
                    header('Location: '.$_SESSION['login_HTTP_REFERER']);
                } else {
                    header('Location: '.conf::$http);
                }
            } else {
                foreach ($api['message']['false'] as $value) {
                    echo "<pre>";print_r($value);echo "</pre>";
                }
                $mobile = '';
                if ($_GET['mobile'] != '') $mobile = "?mobile=".$_GET['mobile'];
                $get_url = ['client_id'     => conf::$auth['facebook']['id'],
                            'redirect_uri'  => conf::$http.$mobile,
                            // 'display'       => 'popup',
                            'scope'         => 'email',
                            'state'         => 'facebook',
                            ];
                header('HTTP/1.1 200 OK');
                header('Location: https://www.facebook.com/v3.3/dialog/oauth?'.http_build_query($get_url));
            }
        }

        die();
    }
    if ((array_key_exists('esia', $_GET))&&(isset($_GET['code']))) { // state - нельзя использовать, оно есть в ответе ЕСИА
            include_once __DIR__."/collections/profile.php";
            include_once __DIR__."/collections/provider.php";
            $config = [
               'clientId'           => 'TYUM720004',
               'redirectUrl'        => conf::$http."",
               'portalUrl'          => 'https://esia.gosuslugi.ru/',
               'privateKeyPath'     => '/etc/esia/privkey.pem',
               'certPath'           => '/etc/esia/certificate.pem',
               'scope'              => 'fullname email mobile usr_org inn id_doc',
               'tmpPath'            => 'cache',
            ];

            $esia = new \esia\OpenId($config);
            $esia->getToken($_GET['code']);

            $personInfo  = $esia->getPersonInfo();
            $contactInfo = $esia->getContactInfo();
            // $subjectInfo = $esia->buildRequest()->call('rs/sbjs/' . $esia->oid);

            foreach ($contactInfo as $k => $v) {
                if ($v->vrfStu=='VERIFIED') {
                    if ($v->type=='MBT') {
                        $v->value = substr(preg_replace('~[^0-9]+~','',$v->value), -10);;
                        if (strlen($v->value)==10) {
                            $mas['phone']=$v->value;
                        }
                    } else if ($v->type=='EML') {
                        $mas['email']=$v->value;
                    }
                }
            }
            $api = $provider->login(['provider'     => 'esia',
                                    'email'        => $mas['email'],
                                    'phone'        => $mas['phone'],
                                    'id_provider'  => $esia->oid,
                                    'f'            => $personInfo->lastName,
                                    'i'            => $personInfo->firstName,
                                    'o'            => $personInfo->middleName,
                                    'ch'           => 0]);
            if (count($api['message']['true']) > 0) {
                if (array_key_exists('mobile', $_GET)) {
                    header('HTTP/1.1 200 OK');
                    header('Location: '.conf::$http.'?token='.$api['token']);
                } else {
                    header('Location: '.conf::$http);
                }
            }
        die();
    } else if (array_key_exists('esia', $_GET)) {
        if ($_GET['error_description'] != '') {
            echo "<pre>";print_r($_GET);echo "</pre>";
        } else {
            $config = [
               'clientId'           => 'TYUM720004',
               'redirectUrl'        => conf::$http."?esia".((array_key_exists('mobile', $_GET))?'&mobile':''),
               'portalUrl'          => 'https://esia.gosuslugi.ru/',
               'privateKeyPath'     => '/etc/esia/privkey.pem',
               'certPath'           => '/etc/esia/certificate.pem',
               'scope'              => 'fullname email mobile usr_org inn id_doc',
               'tmpPath'            => 'cache',
            ];

            $esia = new \esia\OpenId($config);
            ?>
            <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
            <HTML><HEAD>
            <META http-equiv="Content-Type" content="text/html; charset=utf-8"></HEAD>
            <meta http-equiv='refresh' content='0; url="<?php echo $esia->getUrl(); ?>"' />
            <BODY></BODY></HTML>
            <?php
        }
        die();
    }
?>

<?php
    base::test();
    if (isset($this->api['collection'])) {
        if ($this->api['collection']=='uploader') {
            include_once(dirname(__FILE__)."/uploader/collections.php");
        } else {
            include_once(dirname(__FILE__)."/collections/{$this->api['collection']}.php");
        }
    }
?>

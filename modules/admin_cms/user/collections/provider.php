<?php base::test();

    class Provider extends Api {
        /**
         * Авторизация
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function login($params = null) {
            global $cms;

            $security_ = $cms->pdo->prepare("   SELECT  *
                                                FROM    bk_security
                                                WHERE   1=1
                                                        AND ip= :ip
                                                        AND DATE(timestamp) = DATE(NOW())
                                                LIMIT 1");
            $security_->execute([':ip' => conf::$ip]);
            if ($security_->rowCount() <= 100) {

                $row_ = $cms->pdo->prepare("SELECT  id, fk_users_external
                                            FROM    bk_users_verification
                                            WHERE   1=1
                                                    and id_provider = :id_provider
                                                    and provider    = :provider
                                            LIMIT   1");
                $row_->execute([':provider'     => $params['provider'],
                                ':id_provider'  => $params['id_provider']]);
                if ($row_->rowCount() == 0) {

                    $res_ = $cms->pdo->prepare("   SELECT  *
                                                    FROM    bk_users
                                                    WHERE   1=1
                                                            AND email= :email
                                                    LIMIT 1");
                    $res_->execute([':email' => $params['email']]);
                    if ($res_->rowCount() == 0) {
                        $ins_ = $cms->pdo->prepare("INSERT INTO  bk_users
                                                    SET email      = :email,
                                                        f          = :f,
                                                        i          = :i,
                                                        o          = :o");
                        $ins_->execute([':email'    => $params['email'],
                                        ':f'        => $params['f'],
                                        ':i'        => $params['i'],
                                        ':o'        => $params['o']]);
                        $id = $cms->pdo->lastInsertId();
                    } else {
                        $res = $res_->fetch();
                        $id = $res['id'];
                    }

                    $ins_ = $cms->pdo->prepare("INSERT INTO  bk_users_verification
                                                SET fk_users_external   = :id,
                                                    id_provider         = :id_provider,
                                                    provider            = :provider");
                    $ins_->execute([':id'           => $id,
                                    ':id_provider'  => $params['id_provider'],
                                    ':provider'     => $params['provider']]);

                    $row_->execute([':provider'     => $params['provider'],
                                    ':id_provider'  => $params['id_provider']]);

                }
                if ($row_->rowCount() > 0) {
                    $row = $row_->fetch();

                    $upd_ = $cms->pdo->prepare("UPDATE bk_users SET last_visit = NOW() WHERE id = :id");
                    $upd_->execute([':id' => $row['fk_users_external']]);

                    $upd_ = $cms->pdo->prepare("UPDATE bk_users_verification SET last_visit = NOW() WHERE id = :id");
                    $upd_->execute([':id' => $row['id']]);

                    user_profile::tokenGeneration($row['fk_users_external']);
                    $mas['message']['true'][] = 'Вход выполнен';
                    $mas['token'] = $_SESSION['user']['token'];
                }

                if (ctype_alnum($mas['token'])) {
                    if ($params['ch'] != '') {
                        setcookie("token", $_SESSION['user']['token'], time()+60*60*24*30, "/");
                    }
                }
            } else {
                $mas['message']['false'][] = 'Количество попыток превысило лимит';
            }
            $row_ = $cms->pdo->query("  DELETE
                                        FROM    bk_security
                                        WHERE   timestamp < NOW() - INTERVAL 7 DAY");
            return $mas;
        }
    }

    $provider = new Provider();

    if ($provider->getAction()!='') { // вызов через api, иначе вызов напрямую через php
        echo $provider->query()->fetch();
    }
?>

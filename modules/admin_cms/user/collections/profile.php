<?php base::test();

    class User_profile extends Api {
        /**
         * Получение пользователя по id (используется только я вдре)
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function tokenGeneration($user = null) {
            global $cms;
            if (is_numeric($user)) {
                $user_ = $cms->pdo->prepare("   SELECT  *
                                                FROM    bk_users
                                                WHERE   id = :id
                                                LIMIT   1");
                $user_->execute([':id' => $user]);
                if ($user_->rowCount() > 0) {
                    $_SESSION['user'] = $user_->fetch();
                    $token_ = $cms->pdo->prepare("  SELECT  *
                                                    FROM    bk_users_token
                                                    WHERE   fk_users = :id
                                                    LIMIT   1");
                    $token_->execute([':id' => $_SESSION['user']['id']]);
                    if ($token_->rowCount() == 0) {
                        $ins_ = $cms->pdo->prepare("INSERT INTO  bk_users_token
                                                    SET fk_users     = :fk_users,
                                                        token        = :token,
                                                        last_visit   = NOW(),
                                                        finish_visit = NOW() + INTERVAL 1 MONTH
                                                    ");
                        $ins_->execute([':fk_users' =>  $_SESSION['user']['id'],
                                        ':token'    =>  hash('sha256', time().$_SESSION['user']['id'])
                                        ]);
                        $token_->execute([':id' => $_SESSION['user']['id']]);
                    }

                    $token = $token_->fetch();
                    $_SESSION['user']['token'] = $token['token'];
                    if ($token['last_visit'] != date("Y-m-d")) {
                        $upd_ = $cms->pdo->prepare("UPDATE  bk_users_token
                                                    SET     last_visit   = NOW(),
                                                            finish_visit = NOW()+INTERVAL 30 DAY
                                                    WHERE   id = :id
                                                    LIMIT   1");
                        $upd_->execute([':id' => $token['id']]);
                    }
                }
            } else {
                unset($_SESSION['user']);
            }
        }

        /**
         * Продление подписки
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function subscribe_visit($params = null) {
            global $cms;

            if (base::is_email($params['email'])) {
                $upd_ = $cms->pdo->prepare("UPDATE  bk_users
                                            SET     last_visit = NOW(),
                                                    subscribe = 1
                                            WHERE   email = :email
                                            LIMIT   1");
                $upd_->execute([':email' => $params['email']]);
                $mas['developer']['message'][] = "Подписка продлена";
            } else {
                $mas['developer']['message'][] = "email is not valid";
                http_response_code(401);
            }
            return $mas;
        }

        /**
         * Обновление пароля (через токен)
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function updatePassword($params = null) {
            global $cms;
            if ($cms->is_user()) { // всегда, token можно передать через get
                $row_ = $cms->pdo->prepare("SELECT  *
                                            FROM    bk_users
                                            WHERE   1=1
                                                    and id = :id
                                                    and password = :oldPassword
                                            ");
                $row_->execute([':id'           => $_SESSION['user']['id'],
                                ':oldPassword' => hash('sha256', $params['oldPassword'])
                                ]);
                if ($row_->rowCount() == 0) {
                    $mas['developer']['message'][] = "oldPassword is not valid";
                    http_response_code(400);
                } else {
                    $row_ = $cms->pdo->prepare("UPDATE  bk_users
                                                SET     password = :newPassword,
                                                        `new` = 0
                                                WHERE   1=1
                                                        and id = :id
                                                LIMIT   1");
                    $row_->execute([':id'           => $_SESSION['user']['id'],
                                    ':newPassword'  => hash('sha256', $params['newPassword'])
                                    ]);

                }
            } else {
                $mas['developer']['message'][] = "token is not valid";
                http_response_code(401);
            }

            return $mas;
        }

        /**
         * Обновить редактируемые данные пользователя (используя токен)
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function updateProfile($params = null) {
            global $cms;
            if ($cms->is_user()) {
                $table_ = $cms->pdo->prepare("  SELECT data_type, column_name,column_comment
                                                FROM information_schema.columns
                                                WHERE   1=1
                                                        and table_schema = :table_schema
                                                        and table_name = :table_name
                                                        and column_name = :column_name
                                                Order by ordinal_position ASC");
                $table_->execute([  ':table_schema' => conf::$db['name'],
                                    ':table_name'   => 'bk_users',
                                    ':column_name'   => $params['field']]);
                if ($table_->rowCount() > 0) {
                    $upd_ = $cms->pdo->prepare("UPDATE  bk_users
                                                SET     {$params['field']} = :value
                                                WHERE   id      = :id
                                                LIMIT   1");
                    $upd_->execute([':value'        => $params['value'],
                                    ':id'       => $_SESSION['user']['id']]);
                    $mas['developer']['message'][] = "data updated";
                } else {
                    $mas['developer']['message'][] = "field is not valid";
                    http_response_code(401);
                }
            } else  {
                $mas['developer']['message'][] = "token is not valid";
                http_response_code(401);
            }
            return $mas;
        }
        /*
        public function updateProfile($params = null) {
            global $cms;
            if ($cms->is_user()) {
                $upd_ = $cms->pdo->prepare("UPDATE  bk_users
                                            SET     f       = :f,
                                                    i       = :i,
                                                    o       = :o,
                                                    phone   = :phone
                                            WHERE   id      = :id
                                            LIMIT   1");
                $upd_->execute([':f'        => $params['f'],
                                ':i'        => $params['i'],
                                ':o'        => $params['o'],
                                ':phone'    => $params['phone'],
                                ':id'       => $_SESSION['user']['id']]);
                $mas['developer']['message'][] = "data updated";
            } else  {
                $mas['developer']['message'][] = "token is not valid";
                http_response_code(401);
            }
            return $mas;
        }*/

        /**
         * Получить данные пользователя (используя токен)
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function getProfile($params = null) {
            global $cms;
            if ($cms->is_user()) {
                $row_ = $cms->pdo->prepare("SELECT  f, i, o, email, phone, last_visit, timestamp
                                            FROM    bk_users
                                            WHERE   id      = :id
                                            LIMIT   1");
                $row_->execute([':id' => $_SESSION['user']['id']]);
                $row = $row_->fetch();
                $mas['row'] = $row;
            } else  {
                $mas['developer']['message'][] = "token is not valid";
                http_response_code(401);
            }
            return $mas;
        }
    }

    $user_profile = new User_profile();

    if ($user_profile->getAction()!='') { // вызов через api, иначе вызов напрямую через php
        echo $user_profile->query()->fetch();
    }
?>

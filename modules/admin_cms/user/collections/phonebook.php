<?php base::test();

    class User_phonebook extends Api {

        public function tree_group($params = null) {
            global $cms;
            $group_ = $cms->pdo->prepare("  SELECT  *
                                            FROM    bk_access_group
                                            WHERE   1=1
                                                    and hide is NULL
                                                    and id = :id");
            $group_->execute([':id' => $params['id']]);
            if ($group_->rowCount()>0) {
                while ($group = $group_->fetch()) {
                    if (is_numeric($group['fk_group'])) {
                        $mas = $this->tree_group(['id' => $group['fk_group'], 'link' => 1]);
                    }
                    $mas[] = ['id'    => $group['id'],
                            'name'  => $group['name'],
                            'link'  => $params['link']
                            ];
                }
            }
            return $mas;
        }
        /**
         * Получение записи
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function book($params = null) {
            global $cms;
                if (!is_numeric($params['tree'])) $params['tree'] = 2;
                if (is_numeric($params['user'])) {
                    $wh = " and u.id = ".$params['user']." ";
                }
                if (is_numeric($params['limit'])) {
                    $limit = " LIMIT ".$params['limit'];

                }
                $user_ = $cms->pdo->prepare("   SELECT  u.id, u.f, u.i, u.o, u.photo, u.description
                                                FROM    bk_access_group_user as gu,
                                                        bk_users as u
                                                WHERE   1=1
                                                        and hide is NULL
                                                        and u.id = gu.fk_users
                                                        and gu.fk_access_group = :tree
                                                        {$wh}
                                                ORDER BY -u.sort DESC, u.f ASC
                                                {$limit}"); // gu.fk_access_group ASC,
                $user_->execute([':tree' => $params['tree']]);
                if ($user_->rowCount()>0) {
                    while ($user = $user_->fetch()) {
                        $position = [];
                        $group_ = $cms->pdo->prepare("  SELECT  id, department, fk_access_group
                                                        FROM    bk_access_group_user
                                                        WHERE   1=1
                                                                and hide is NULL
                                                                and fk_users = :user");
                        $group_->execute([':user' => $user['id']]);
                        if ($group_->rowCount()>0) {
                            while ($group = $group_->fetch()) {
                                $info = [];
                                $phonebook_ = $cms->pdo->prepare("  SELECT  type, name, description
                                                                    FROM    bk_access_group_user_phonebook
                                                                    WHERE   1=1
                                                                            and hide is NULL
                                                                            and fk_users_access = :group
                                                                    ORDER BY type ASC");
                                $phonebook_->execute([':group' => $group['id']]);
                                if ($phonebook_->rowCount()>0) {
                                    while ($phonebook = $phonebook_->fetch()) {
                                        if ($phonebook['type'] == 'phone') {
                                            $tt = preg_match('/^89/', $phonebook['name'], $matches);
                                            if (count($matches) > 0) {
                                                $phonebook['name'] = preg_replace(  "/(\\d{1})(\\d{3})(\\d{3})(\\d{2})(\\d{2})$/i",
                                                                                    "$1 ($2) $3-$4-$5",
                                                                                    $phonebook['name']);
                                            } else {
                                                $phonebook['name'] = preg_replace(  "/(\\d{1})(\\d{4})(\\d{2})(\\d{2})(\\d{2})$/i",
                                                                                    "$1 ($2) $3-$4-$5",
                                                                                    $phonebook['name']);
                                            }

                                        }
                                        // if ($phonebook['type'] == 'inside') {
                                        //         $phonebook['name'] = preg_replace(  "/(\\d{2})(\\d{2})$/i",
                                        //                                             "$1-$2",
                                        //                                             $phonebook['name']);
                                        // }
                                        $info[$phonebook['type']][] = $phonebook;
                                    }
                                }
                                $tree_group = $this->tree_group(['id' => $group['fk_access_group']]);
                                unset($tree_group[0]);
                                $tree_group = array_values($tree_group);
                                if ($group['department'] != '') {
                                    $position[] = [ 'position'  => $group['department'],
                                                    'group'     => $tree_group,
                                                    'info'      => $info];
                                }
                            }
                        }
                        if ($user['photo'] != '') {
                            $user['photo'] = "/files/user/".$user['photo'];
                        }

                        // if (count($position) > 0) {
                            $mas['row'][] = [   'id'            => $user['id'],
                                                'f'             => $user['f'],
                                                'i'             => $user['i'],
                                                'o'             => $user['o'],
                                                'description'   => $user['description'],
                                                'photo'         => $user['photo'],
                                                'position'      => $position
                                            ];
                        // }
                    }
                }

                $group_ = $cms->pdo->prepare("   SELECT  id, name
                                                FROM    bk_access_group
                                                WHERE   1=1
                                                        and hide is NULL
                                                        and id = :tree");
                $group_->execute([':tree' => $params['tree']]);
                if ($group_->rowCount()>0) {
                    $group = $group_->fetch();
                    $mas['group'] = $group;

                    $group_ = $cms->pdo->prepare("   SELECT  id, name
                                                    FROM    bk_access_group
                                                    WHERE   1=1
                                                            and hide is NULL
                                                            and fk_group = :tree");
                    $group_->execute([':tree' => $params['tree']]);
                    if ($group_->rowCount()>0) {
                        while ($group = $group_->fetch()) {
                            $mas['group']['tree'][] = $group;
                        }
                    }

                }

            return $mas;
        }

        /**
         * Получение записи
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function select($params = null) {
            global $cms;
            if ($cms->is_access(1)) {
                $col_page = 100;
                if ($params['search'] != '') {
                    $wh = " and (u.f like '%".$params['search']."%' or email  like '%".$params['search']."%')";
                }

                if ($params['page']=='') $params['page']=1;

                $limit = " LIMIT ".(($params['page']-1)*$col_page).", ".$col_page;
                $row_ = $cms->pdo->query("  SELECT SQL_CALC_FOUND_ROWS u.id, u.f, u.i, u.o, u.email, u.timestamp, u.last_visit, u.photo, u.password, min(gu.id) as col
                                            FROM    bk_users as u
                                                    left join bk_access_group_user as gu on gu.fk_users = u.id
                                            WHERE   1=1 {$wh}
                                            GROUP BY u.id, u.f, u.i, u.o
                                            ORDER BY col DESC, u.f, u.i, u.o ASC
                                            ".$limit);
                if ($row_->rowCount()>0) {
                    $page['page_max'] = $cms->pdo->query("SELECT FOUND_ROWS()")->fetchColumn(); // сколько записей в БД
                    while ($row = $row_->fetch()) {
                        $users = [  'id'         => $row['id'],
                                    'f'          => $row['f'],
                                    'i'          => $row['i'],
                                    'o'          => $row['o'],
                                    'photo'      => $row['photo'],
                                    'email'      => $row['email'],
                                    'last_visit' => $row['last_visit'],
                                    'activity'   => ((strtotime($row['last_visit']) < time()-(60*60*24*31))?1:0),
                                ];

                        if ($row['password'] != '') {
                            $users['verification'][] = ['svg' => '/modules/admin_cms/user/img/email.svg'];
                        }

                        $verifi_ = $cms->pdo->prepare(" SELECT  provider, last_visit, timestamp
                                                        FROM    bk_users_verification
                                                        WHERE   1=1
                                                                and fk_users_external = :id");
                        $verifi_->execute([':id' => $row['id']]);
                        if ($verifi_->rowCount()>0) {
                            while ($verifi = $verifi_->fetch()) {
                                $users['verification'][] = ['svg' => '/modules/admin_cms/user/img/'.$verifi['provider'].'.svg'];
                            }
                        }
                        $access_ = $cms->pdo->prepare(" SELECT  gu.id, gu.department, gu.hide, g.name, g.full
                                                        FROM    bk_access_group as g,
                                                                bk_access_group_user as gu
                                                        WHERE   1=1
                                                                and g.id = gu.fk_access_group
                                                                and gu.fk_users = :id
                                                                -- and gu.hide is NULL -- тут не надо так как в админке!
                                                        ORDER BY    gu.sort ASC, g.sort ASC");
                        $access_->execute([':id'=>$row['id']]);
                        if ($access_->rowCount()>0) {
                            while ($access = $access_->fetch()) {
                                if ($access['full'] == 1) {
                                    $access['admin'] = 'fire';
                                } else {
                                    $test_ = $cms->pdo->prepare("   SELECT  *
                                                                    FROM    bk_access_group as g,
                                                                            bk_access_group_structure as gs
                                                                    WHERE   1=1
                                                                            and g.id = gs.fk_access_group
                                                                            and g.id = :id");
                                    $test_->execute([':id'=>$access['id']]);
                                    if ($test_->rowCount() > 0) {
                                        $access['admin'] = 'leaf';
                                    }
                                }
                                $users['access'][] = $access;
                            }
                        }

                        $mas['row'][] = $users;
                    }

                    $page['page_max']=intval(($page['page_max'] - 1) / $col_page) + 1;  // максимальное количество страниц
                    if ($params['page']>$page['page_max']) $params['page']=$page['page_max']; // если номер старницы больше чем максимальное количество страниц
                    if (($params['page']-3)<1) {
                        $page['page_s']=1;
                    } else {
                        $page['page_s']=$params['page']-3;
                    }
                    if (($params['page']+3)>$page['page_max']) {
                        $page['page_f']=$page['page_max'];
                    } else {
                        $page['page_f']=$params['page']+3;
                    }
                    $page['url'] = $cms->url($params['node'], 1);

                    if ($page['page_f'] > $page['page_s']) {
                        if ($params['page']==1) {
                            $mas['page']['block1'] = ['page' => $params['page']-1];
                        }
                        if ($page['page_s'] > 1) {
                            $mas['page']['block2'] = ['page' => 1];
                        }
                        for($i=$page['page_s'];$i<=$page['page_f'];$i++){

                            $mas['page']['block3'][] = ['page' => $i, 'active'=>(($params['page']==$i)?1:NULL)];
                        }
                        if ($page['page_f'] < $page['page_max']) {
                            $mas['page']['block4'] = ['page' => $page['page_max']];
                        }
                        if ($params['page']!=$page['page_max']) {
                            $mas['page']['block5'] = ['page' => $params['page']+1];

                        }
                    }
                } else {
                    $mas['message']['true'][] = 'Данные отсутствуют';
                }
            } else {
                http_response_code(426);
            }
            return $mas;
        }

        /**
         * Добавление записи
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function phonebookSave($params = null) {
            global $cms;
            $params['table']            = "bk_access_group_user_phonebook";
            return $this->save($params);
        }

        /**
         * Добавление записи
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function accessSave($params = null) {
            global $cms;
            $params['table']            = "bk_access_group_user";
            return $this->save($params);
        }

        /**
         * Добавление записи
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function userSave($params = null) {
            global $cms;
            $params['table']            = "bk_users";
            return $this->save($params);
        }

        /**
         * Добавление записи
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function save($params = null) {
            global $cms;
            if ($cms->is_access(1)) {
                $tb = $params['table'];
                $row_ = $cms->pdo->prepare("SHOW TABLES like :table");
                $row_->execute([':table' => $tb]);
                if ($row_->rowCount() == 1) {
                    $cms->pdo->beginTransaction();
                    if (!is_numeric($params['id'])) { // новая запись
                        $row_ = $cms->pdo->query("INSERT INTO ".$tb."(id) VALUES (NULL)");
                        $params['id'] = $cms->pdo->lastInsertId();
                        $mas['id'] = $params['id'];
                    }

                    $columns_ = $cms->pdo->prepare("SELECT column_name
                                                    FROM information_schema.columns
                                                    WHERE   1=1
                                                            and table_schema = :table_schema
                                                            and table_name = :table_name
                                                            and extra <> 'auto_increment'
                                                            and column_type <> 'timestamp'
                                                    Order by ordinal_position ASC");
                    $columns_->execute([':table_schema' => conf::$db['name'],
                                        ':table_name'   => $tb]);
                    while ($columns = $columns_->fetch()) {
                        if ((array_key_exists($columns['column_name'], $params)) and (is_numeric($params['id']))) {
                            if ($params[$columns['column_name']] == '') {
                                $params[$columns['column_name']] = NULL;
                            }
                            if ($columns['column_name'] == 'password') {
                                $params[$columns['column_name']] = hash('sha256', $params[$columns['column_name']]);
                            }
                            $upd_ = $cms->pdo->prepare("UPDATE  ".$tb."
                                                        SET     ".$columns['column_name']." = :params
                                                        WHERE   id = :id
                                                        Limit 1");
                            $upd_->execute([':id' => $params['id'], ':params' => $params[$columns['column_name']]]);
                        }
                    }
                    $cms->pdo->commit();
                } else {
                    http_response_code(426);
                }
            } else {
                http_response_code(426);
            }
            return $mas;
        }

        /**
         * закрыть доступ
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function userGroup($params = null) {
            global $cms;
            $row_ = $cms->pdo->prepare("SELECT  *
                                        FROM    bk_access_group
                                        ORDER BY sort, id ASC");
            $row_->execute([':id'=>$params['id']]);
            if ($row_->rowCount() > 0) {
                while ($row = $row_->fetch()) {
                    if (is_numeric($row['fk_group'])) {
                        $row['name'] = $mas[$row['fk_group']]['name'].' / '.$row['name'];
                    }
                    $mas[$row['id']] = ['id'    => $row['id'],
                                        'name'  => $row['name'],
                                        'full'  => $row['full']
                                        ];
                }
            }
            return $mas;
        }
        /**
         * закрыть доступ
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function accessForm($params = null) {
            global $cms;
            if ($cms->is_access(1)) {
                $mas['group'] = $this->userGroup();
                $mas['type_info']['address'] = ['id' => 'address', 'name' => 'Адресс:'];
                $mas['type_info']['phone']   = ['id' => 'phone',   'name' => 'Телефон:'];
                $mas['type_info']['inside']  = ['id' => 'inside',  'name' => 'Внутр. телефон:'];
                $mas['type_info']['email']   = ['id' => 'email',   'name' => 'E-mail:'];
                $row_ = $cms->pdo->prepare("SELECT  id, f, i, o, email, password, description, timestamp, sort, last_visit
                                            FROM    bk_users
                                            WHERE   1=1
                                                    and id = :id");
                $row_->execute([':id'=>$params['id']]);
                if ($row_->rowCount() > 0) {
                    $row = $row_->fetch();
                    if ($row['password'] != '') {
                        $row['password'] = 'Обновить пароль';
                    } else {
                        $row['password'] = 'Нет пароля';
                    }
                    $mas['user'] = $row;

                    if ($row['password'] == '') {
                        $mas['verification'][] = ['svg'        => '',
                                                    'last_visit' => $row['last_visit'],
                                                    'timestamp'  => $row['timestamp']
                                                    ];
                    } else {
                        $mas['verification'][] = ['svg'        => '/modules/admin_cms/user/img/email.svg',
                                                    'title'      => $row['email'],
                                                    'last_visit' => $row['last_visit'],
                                                    'timestamp'  => $row['timestamp']
                                                    ];
                    }

                    $verifi_ = $cms->pdo->prepare(" SELECT  provider, last_visit, timestamp
                                                    FROM    bk_users_verification
                                                    WHERE   1=1
                                                            and fk_users_external = :id");
                    $verifi_->execute([':id' => $row['id']]);
                    if ($verifi_->rowCount()>0) {
                        while ($verifi = $verifi_->fetch()) {
                            $mas['verification'][] = ['svg'        => '/modules/admin_cms/user/img/'.$verifi['provider'].'.svg',
                                                        'title'      => $verifi['provider'],
                                                        'last_visit' => $verifi['last_visit'],
                                                        'timestamp'  => $verifi['timestamp']
                                                        ];
                        }
                    }


                }
                $row_ = $cms->pdo->prepare("SELECT  id, fk_access_group, department, hide
                                            FROM    bk_access_group_user
                                            WHERE   1=1
                                                    and fk_users = :fk_users
                                            ORDER BY sort ASC");
                $row_->execute([':fk_users'=>$params['id']]);
                if ($row_->rowCount() > 0) {
                    while ($row = $row_->fetch()) {
                        $row['group'] = $mas['group'];
                        $row['group'][$row['fk_access_group']]['selected'] = 1;

                        $phonebook_ = $cms->pdo->prepare("  SELECT  id, type, name, description, hide
                                                            FROM    bk_access_group_user_phonebook
                                                            WHERE   1=1
                                                                    and fk_users_access = :id
                                                            ORDER BY type ASC");
                        $phonebook_->execute([':id'=>$row['id']]);
                        if ($phonebook_->rowCount()>0) {
                            while ($phonebook = $phonebook_->fetch()) {
                                $phonebook['type_info'] = $mas['type_info'];
                                $phonebook['type_info'][$phonebook['type']]['selected'] = 1;
                                $row['phonebook'][] = $phonebook;
                            }
                        }

                        $mas['access'][] = $row;
                    }
                }
            } else {
                http_response_code(426);
            }
            return $mas;
        }

        /**
         * закрыть доступ
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function accessHide($params = null) {
            global $cms;
            $params['table']            = "bk_access_group_user";

            if ($cms->is_access(1)) {
                $row_ = $cms->pdo->prepare("SELECT  *
                                            FROM    bk_access_group_user
                                            WHERE   id = :id");
                $row_->execute([':id'=>$params['id']]);
                if ($row_->rowCount() > 0) {
                    $row = $row_->fetch();
                    if ($row['hide'] != 1) {
                        $params['hide'] = 1;
                        $mas['message']['true'][] = 'Доступ закрыт';
                    } else {
                        $params['hide'] = 0;
                        $mas['message']['true'][] = 'Доступ открыт';
                    }
                    return $this->save($params);
                }
            } else {
                http_response_code(426);
            }
            return $mas;
        }

        /**
         * закрыть доступ
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function phonebookHide($params = null) {
            global $cms;
            $params['table']            = "bk_access_group_user_phonebook";

            if ($cms->is_access(1)) {
                $row_ = $cms->pdo->prepare("SELECT  *
                                            FROM    bk_access_group_user_phonebook
                                            WHERE   id = :id");
                $row_->execute([':id'=>$params['id']]);
                if ($row_->rowCount() > 0) {
                    $row = $row_->fetch();
                    if ($row['hide'] != 1) {
                        $params['hide'] = 1;
                        $mas['message']['true'][] = 'Доступ закрыт';
                    } else {
                        $params['hide'] = 0;
                        $mas['message']['true'][] = 'Доступ открыт';
                    }
                    return $this->save($params);
                }
            } else {
                http_response_code(426);
            }
            return $mas;
        }

        /**
         * закрыть доступ
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function userDelete($params = null) {
            global $cms;
            if ($cms->is_access(1)) {
                $udp_ = $cms->pdo->prepare("DELETE
                                            FROM    bk_users
                                            WHERE   id = :id");
                $udp_->execute([':id' => $params['id']]);
                $mas['developer']['message'][] = 'Пользоватеь удалён';
            } else {
                http_response_code(426);
            }
            return $mas;
        }

        /**
         * закрыть доступ
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function accessDelete($params = null) {
            global $cms;
            if ($cms->is_access(1)) {
                $udp_ = $cms->pdo->prepare("DELETE
                                            FROM    bk_access_group_user
                                            WHERE   id = :id");
                $udp_->execute([':id' => $params['id']]);
                $mas['developer']['message'][] = 'Доступ удалён';
            } else {
                http_response_code(426);
            }
            return $mas;
        }

        /**
         * закрыть доступ
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function phonebookDelete($params = null) {
            global $cms;
            if ($cms->is_access(1)) {
                $udp_ = $cms->pdo->prepare("DELETE
                                            FROM    bk_access_group_user_phonebook
                                            WHERE   id = :id");
                $udp_->execute([':id' => $params['id']]);
                $mas['developer']['message'][] = 'Информация удалена';
            } else {
                http_response_code(426);
            }
            return $mas;
        }

    }

    $user_phonebook = new User_phonebook();

    if ($user_phonebook->getAction()!='') { // вызов через api, иначе вызов напрямую через php
        echo $user_phonebook->query()->fetch();
    }

?>

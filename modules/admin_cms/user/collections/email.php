<?php base::test();
    class User_email extends Api {
        /**
         * Авторизация через логин и пароль
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function login($params = null) {
            global $cms;
            $security_ = $cms->pdo->prepare("   SELECT  *
                                                FROM    bk_security
                                                WHERE   1=1
                                                        AND ip= :ip
                                                        AND DATE(timestamp) = DATE(NOW())
                                                LIMIT 1");
            $security_->execute([':ip' => conf::ip()]);
            if ($security_->rowCount() <= 100) {
                $user_= $cms->pdo->prepare("SELECT  *
                                            FROM    bk_users
                                            WHERE   1=1
                                                    and email=:email
                                                    -- and password=:password
                                                    and (password=:password or password=:md5)
                                                    and password is NOT NULL
                                            LIMIT   1");
                $user_->execute([':email'    => $params['email'],
                                ':password' => hash('sha256', $params['password']),
                                ':md5'      => md5($params['password']) // чисты пароль для быстрой генерации, либо md5() для ТНД
                               ]);
                if ($user_->rowCount() > 0) {
                    $user = $user_->fetch();
                    if ($user['password'] == md5($params['password'])) { // для старых сайтов, где md5 было
                        $upd_= $cms->pdo->prepare("UPDATE  bk_users
                                                    SET     password=:password
                                                    WHERE   id=:id
                                                    LIMIT   1");
                        $upd_->execute([':id'       => $user['id'],
                                        ':password' => hash('sha256', $params['password'])
                                        ]);
                    }
                    user_profile::tokenGeneration($user['id']);
                    $mas['token'] = $_SESSION['user']['token'];
                } else {
                    $row_ = $cms->pdo->prepare("INSERT INTO bk_security(ip) VALUES (:ip)");
                    $row_->execute([':ip' => conf::$ip]);
                    $mas['developer']['message'][] = "email and password is not valid";
                    http_response_code(401);
                }
                if (ctype_alnum($mas['token'])) {
                    if ($params['ch'] != '') {
                        setcookie("token", $_SESSION['user']['token'], time()+60*60*24*30, "/");
                    }
                }
                $mas['developer']['message'][] = 'limit exceeded';
            } else {
                $mas['developer']['message'][] = 'limit exceeded';
                http_response_code(403);
            }
            $row_ = $cms->pdo->query("  DELETE
                                        FROM    bk_security
                                        WHERE   timestamp < NOW() - INTERVAL 7 DAY");
            return $mas;
        }

        /**
         * Проверяем существует ли данный email в БД
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function isUserRegistered($params = null) {
            global $cms;
            if (($params['email']!='')&&(base::is_email($params['email'])==false)) {
                http_response_code(400);
                $mas['developer']['message'][] = "email is not valid";
            } else {
                $row_ = $cms->pdo->prepare("SELECT  *
                                            FROM    bk_users
                                            WHERE   1=1
                                                    and email = :email
                                                    and password is NOT NULL
                                            LIMIT   1");
                $row_->execute([':email' => $params['email']]);
                if ($row_->rowCount() > 0) {
                    $mas['isRegistered'] = true;
                } else {
                    $mas['isRegistered'] = false;
                }
            }
            return $mas;
        }

        /**
         * Регистрация нового пользователя (через code отправленного на email)
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function setUpdatePasswordCode($params = null) {
            global $cms;
            $row_ = $cms->pdo->prepare("SELECT  *
                                        FROM    bk_users_code
                                        WHERE   1=1
                                                and code = :code
                                                and login = :login
                                                and timestamp > (NOW() - INTERVAL 30 DAY)
                                                and del is NULL
                                        LIMIT 1");
            $row_->execute([':code' => $params['code'], ':login' => $params['login']]);
            if ($row_->rowCount() == 0) {
                $mas['developer']['message'][] = "code is not valid";
                $_SESSION['user']['message'][] = [  'type'     => 'warning',
                                                    'title'     => 'Код восстановления пароля устарел.',
                                                    'message'   => 'Либо вы уже воспользоватилсь им. '
                                                    ];
                http_response_code(400);
            } else {
                $row = $row_->fetch();
                $buf = json_decode($row['json'],1);

                $upd_ = $cms->pdo->prepare("UPDATE  bk_users
                                            SET     password = :password,
                                                    `new` = 0
                                            WHERE   1=1
                                                    and id = :id
                                                    and email = :email
                                            LIMIT   1");
                $upd_->execute([':id'       => $buf['id'],
                                ':email'    => $buf['email'],
                                ':password' => $buf['password']
                                ]);

                user_profile::tokenGeneration($buf['id']);
                $mas['token'] = $_SESSION['user']['token'];

                $upd_ = $cms->pdo->prepare("UPDATE bk_users_code SET del = NOW() WHERE id = :id LIMIT 1");
                $upd_->execute([':id' => $row['id']]);
                $_SESSION['user']['message'][] = [  'type'     => 'info',
                                                    'title'     => 'Пароль изменён.',
                                                    'message'   => 'Пароль изменён.'
                                                    ];
            }
            return $mas;
        }

        /**
         * Регистрация нового пользователя (через code отправленного на email)
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function setRegistrationCode($params = null) {
            global $cms;
            $code_ = $cms->pdo->prepare("SELECT  *
                                        FROM    bk_users_code
                                        WHERE   1=1
                                                and code = :code
                                                and login = :email
                                                and function_name = 'getRegistrationCode'
                                                and timestamp > (NOW() - INTERVAL 30 DAY)
                                                and del is NULL
                                        LIMIT 1");
            $code_->execute([':code' => $params['code'], ':email' => $params['email']]);
            if ($code_->rowCount() == 0) {
                $mas['developer']['message'][] = "code is not valid";
                http_response_code(400);
            } else {
                $code = $code_->fetch();

                $upd_ = $cms->pdo->prepare("UPDATE bk_users_code SET del = NOW() WHERE id = :id LIMIT 1");
                $upd_->execute([':id' => $code['id']]);

                $updEmail_ = $cms->pdo->prepare("UPDATE bk_users_code SET del = NULL WHERE email = :email LIMIT 1");

                $buf = json_decode($code['json'],1);
                // echo "<pre>";print_r(base::is_email($params['email'])==false);echo "</pre>";
                // if (($params['email']!='')&&(base::is_email($params['email'])==false)) {
                if (($params['email']!='')) {
                    $row_ = $cms->pdo->prepare("SELECT  id
                                                FROM    bk_users
                                                WHERE   1=1
                                                        and email = :email
                                                LIMIT 1");
                    $row_->execute([':email' => $buf['email']]);
                    if ($row_->rowCount() == 0) {
                        $ins_ = $cms->pdo->prepare("INSERT INTO bk_users (email, phone, password, f, i, o, subscribe)
                                                    VALUES (:email, :phone, :password, :f, :i, :o, :subscribe)");
                        $ins_->execute([':email'        => $buf['email'],
                                        ':phone'        => $buf['phone'],
                                        ':password'     => $buf['password'], // уже закодированый лежит
                                        ':f'            => $buf['f'],
                                        ':i'            => $buf['i'],
                                        ':o'            => $buf['o'],
                                        ':subscribe'    => (($buf['subscribe'] != '')?1:null)
                                    ]);
                        user_profile::tokenGeneration($cms->pdo->lastInsertId());
                        $updEmail_->execute([':email' => $buf['email']]); // ниже профиля, иначе lastInsertId не срабатывает
                        $mas['token'] = $_SESSION['user']['token'];
                    } else {
                        $row = $row_->fetch();
                        if ($row['password'] == '') { // пустой пароль только если сначала была регистрация через соц.сети
                            $upd_ = $cms->pdo->prepare("UPDATE  bk_users
                                                        SET     password = :password,
                                                                phone = :phone,
                                                                f = :f,
                                                                i = :i,
                                                                o = :o
                                                        WHERE   id = :id
                                                        LIMIT   1");
                            $upd_->execute([':id'       => $row['id'],
                                            ':password' => $buf['password'],
                                            ':phone'    => $buf['phone'],
                                            ':f'        => $buf['f'],
                                            ':i'        => $buf['i'],
                                            ':o'        => $buf['o']
                                            ]);
                            user_profile::tokenGeneration($row['id']);
                            $updEmail_->execute([':email' => $row['email']]);
                            $mas['token'] = $_SESSION['user']['token'];
                        }
                    }
                    if ($_SESSION['user']['token'] != '') {
                        $mas['token'] = $_SESSION['user']['token'];
                    }
                } else {
                    http_response_code(400);
                    $mas['developer']['message'][] = "email is not valid";
                }
            }
            return $mas;
        }

        /**
         * Регистрация нового пользователя (через code отправленного на email)
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function getRegistrationCode($params = null) {
            global $cms;
            global $sendMail;

            // чистим от лишнего (если есть шаблон ввода), остаются только цифры
            // $params['phone']= preg_replace('/([^0-9])/', "", $params['phone']);
            // if (strlen($params['phone']) != 10) {
            //     http_response_code(401);
            //     $mas['developer']['message'][] = "phone is not valid";
            // }
            if (!in_array($params['whoSent'],['ios', 'android', 'web'])) {
                http_response_code(401);
                $mas['developer']['message'][] = "whoSent is not valid";
            }
            if ($params['whoSent'] == 'web') {
                if (
                    // ($params['phone']   == '')||
                    ($params['f']       == '')||
                    ($params['i']       == '')||
                    ($params['o']       == '')) {
                    $mas['developer']['message'][] = "required fields";
                    http_response_code(401);
                }
                if ($params['captcha'] != $_SESSION['captcha']) {
                    $mas['developer']['message'][] = "captcha is not valid";
                    http_response_code(423);
                }
            }
            if (!base::is_email($params['email'])) {
                $mas['developer']['message'][] = "email is not valid";
                http_response_code(401);
            } else {
                $row_ = $cms->pdo->prepare("SELECT  id
                                            FROM    bk_users
                                            WHERE   1=1
                                                    and email = :email
                                                    and password is NOT NULL
                                            LIMIT 1");
                $row_->execute([':email' => $params['email']]);
                if ($row_->rowCount()>0) {
                    $mas['developer']['message'][] = 'this e-mail is already registered';
                    http_response_code(400);
                } else {
                    $code = rand(100000, 999999);
                    $row_upd_pas_ = $cms->pdo->prepare("INSERT INTO bk_users_code (login, code, as_send, function_name, who_sent, json)
                                                        VALUES (:email, :code, 'email', :function_name, :who_sent, :json)");
                    // $url = conf::$http."api/user/email/setUpdatePasswordCode/html?login=".$params['email']."&code=".$code;
                    $url = conf::$http."?email=".$params['email']."&setRegistrationCode=".$code;
                    $sendMail->get(['email'     => [$params['email']],
                                    'subject'   => 'Подтверждение регистрации',
                                    'message'   => "Здравствуйте! <br>
                                                    <br>
                                                    <p>Сегодня, ".date("d.m.Y").", неизвестным пользователем была осуществлена регистрация на сайте «<a href='".conf::$http."'>".$cms->structure_page(1, 'title')."</a>» с использованием Вашего Email. Если это были Вы, то, пожалуйста, подтвердите адрес Вашей электронной почты, перейдя по этой ссылке и Вы сразу будете авторизованы на сайте: <a href='".$url."'>".$url."</a>
                                                    </p>
                                                    <p>
                                                        Если Вы регистрируетесь через мобильное приложение, то введите код «".$code."».
                                                    </p>
                                                    <br>
                                                    <br>
                                                    <b>Внимание!</b> Ссылка действительна 24 часа. После чего Ваша заявка о регистрации будет удаленна из базы данных.
                                                    <hr>
                                                    Спасибо за участие в проекте!<br>
                                                    Пожалуйста, не отвечайте на это сообщение, так как оно сгенерировано автоматически."
                                    ]);
                    $row_upd_pas_->execute([':email'        => $params['email'],
                                            ':code'         => $code,
                                            ':function_name'=> 'getRegistrationCode',
                                            ':who_sent'     => $params['whoSent'],
                                            ':json'         => json_encode(['email'     => $params['email'],
                                                                            'phone'     => $params['phone'],
                                                                            'password'  => hash('sha256', $params['password']),
                                                                            'f'         => $params['f'],
                                                                            'i'         => $params['i'],
                                                                            'o'         => $params['o'],
                                                                            'subscribe' => (($params['subscribe'] != '')?1:null)
                                                                            ])
                                            ]);
                }
            }
            return $mas;
        }

        /**
         * Обновление пароля (через code отправленного на email)
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function getUpdatePasswordCode($params = null) {
            global $cms;
            global $sendMail;

            if (!in_array($params['whoSent'],['ios', 'android', 'web'])) {
                http_response_code(401);
                $mas['developer']['message'][] = "whoSent is not valid";
            } else {
                $row_ = $cms->pdo->prepare("SELECT  id
                                            FROM    bk_users
                                            WHERE   1=1
                                                    and email = :email
                                            LIMIT 1");
                $row_->execute([':email' => $params['email']]);
                if ($row_->rowCount()==0) {
                    $mas['developer']['message'][] = 'email is not valid (не выводим, зачем палить не существующие email)';
                } else {
                    $row = $row_->fetch();
                    $code = rand(100000, 999999);
                    $row_upd_pas_ = $cms->pdo->prepare("INSERT INTO bk_users_code (login, code, as_send, function_name, who_sent, json)
                                                        VALUES (:email, :code, 'email', :function_name, :who_sent, :json)");
                    // $url = conf::$http."?email=".$params['email']."&setUpdatePasswordCode=".$code;
                    $url = conf::$http."api/user/email/setUpdatePasswordCode/html?login=".$params['email']."&code=".$code;
                    $sendMail->get(['email'     => [$params['email']],
                                    'subject'   => 'Востановление доступа',
                                    'message'   => "Здравствуйте!<br>
                                                    <br>
                                                    <p>Сегодня, ".date("d.m.Y").", неизвестным пользователем была осуществлена попытка востановления доступа на сайте «<a href='".conf::$http."'>".$cms->structure_page(1, 'title')."</a>» используя Ваш Email. Если это были Вы, то, пожалуйста, подтвердите адрес Вашей электронной почты, перейдя по этой ссылке и Вы сразу будете авторизованы на сайте: <a href='".$url."'>".$url."</a>
                                                    </p>
                                                    <p>
                                                        Код для мобильное приложения: ".$code."
                                                    </p>
                                                    <br>
                                                    <br>
                                                    В противном случае, если это были не Вы, то просто игнорируйте это письмо.
                                                    <br>
                                                    <br>
                                                    <b>Внимание!</b> Ссылка действительна 24 часа. После чего Ваша заявка о регистрации будет удаленна из базы данных.
                                                    <hr>
                                                    Спасибо за участие в проекте!<br>
                                                    Пожалуйста, не отвечайте на это сообщение, так как оно сгенерировано автоматически."
                                    ]);
                    $row_upd_pas_->execute([':email'        => $params['email'],
                                            ':code'         => $code,
                                            ':function_name'=> 'getUpdatePasswordCode',
                                            ':who_sent'     => $params['whoSent'],
                                            ':json'         => json_encode(['id'        => $row['id'],
                                                                            'email'     => $params['email'],
                                                                            'password'  => hash('sha256', $params['password'])
                                                                            ])
                                            ]);
                }
            }
            return $mas;
        }
    }

    $user_email = new User_email();

    if ($user_email->getAction()!='') { // вызов через api, иначе вызов напрямую через php
        include_once __DIR__."/../collections/profile.php";
        echo $user_email->query()->fetch();
    }
?>

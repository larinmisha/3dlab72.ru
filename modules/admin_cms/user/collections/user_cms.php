<?php
    base::test();

    class User_cms {
        /**
         * Получение пользователя по токену
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function get_user_token($params = null) {
            global $cms;

            if (array_key_exists('logout', $_GET)) {
                setcookie("token", '', 1, "/");
                unset($_SESSION['user']);
            } else {
                if ($_SERVER['HTTP_TOKEN']!='') {
                    $token = $_SERVER['HTTP_TOKEN'];
                } else if (ctype_alnum($_SESSION['user']['token'])) {
                    $token = $_SESSION['user']['token'];
                } else if (ctype_alnum($_COOKIE['token'])) {
                    $token = $_COOKIE['token'];
                }
            }
            if (ctype_alnum($token)) {
                $token_ = $cms->pdo->prepare("  SELECT  u.*, t.token, t.last_visit as tlv
                                                FROM    bk_users as u,
                                                        bk_users_token as t
                                                WHERE   1=1
                                                        and u.id = t.fk_users
                                                        and t.token  = :token
                                                LIMIT   1");
                $token_->execute([':token' => $token]);
                if ($token_->rowCount() > 0) {
                    $_SESSION['user'] = $token_->fetch();
                    if ($_SESSION['user']['tlv'] != date("Y-m-d")) {
                        $upd_ = $cms->pdo->prepare("UPDATE  bk_users_token
                                                    SET     last_visit   = NOW(),
                                                            finish_visit = NOW()+INTERVAL 30 DAY
                                                    WHERE   token = :token
                                                    LIMIT   1");
                        $upd_->execute([':token' => $token]);

                        $upd_ = $cms->pdo->prepare("   UPDATE  bk_users
                                                        SET     last_visit = NOW()
                                                        WHERE   id = :id
                                                        LIMIT   1");
                        $upd_->execute([':id' => $_SESSION['user']['id']]);
                    }
                    unset($_SESSION['user']['tlv']);
                } else {
                    http_response_code(401);
                    setcookie("token", '', 1, "/");
                    unset($_SESSION['user']);
                }
            }
        }

    }
    $user_cms = new User_cms();
?>

<?php base::test();

    class Modul extends Api {

        /**
         * Список модулей на странице
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function modulPage($params = null) {
            global $cms;
            if ($params['node'] > 0) {
                if (!$cms->is_access($params['node'], 'mod_list')) {
                    $mas['message']['false'][] = 'Отсутсвуют права доступа (mod_list)';
                }
            } else {
                $mas['message']['false'][] = 'Не выбран раздел';
            }
            if (count($mas['message']['false']) == 0) {
                $mas['data']['chpu']        = $cms->url($params['node']);
                $mas['data']['chpu_admin']  = $cms->url($params['node'], 1);
                if ($cms->group_cms['full'] == 1) {
                    $mas['data']['breadcrumb']  = "[".$params['node']."] ".$cms->structure_page($params['node'], 'title');
                } else {
                    $mas['data']['breadcrumb']  = $cms->structure_page($params['node'], 'title');
                }

                if ($_SESSION['last_open_modul'] > 0) {
                    // если переданного модуля нет в данном разделе
                    $row_ = $cms->pdo->prepare("SELECT  ms.id, ms.open, ms.info, m.dirrect, m.title, m.dirrect, m.type_script
                                                FROM    bk_modules as ms,
                                                        bk_modul as m
                                                WHERE   1=1
                                                        and ms.fk_modul=m.id
                                                        and ms.fk_structure= :node
                                                        and ms.id= :modules
                                                        and ms.del=0
                                                ORDER BY sort ASC");
                    $row_->execute([':node'     => $params['node'],
                                    ':modules'  => $_SESSION['last_open_modul']]);
                    if ($row_->rowCount() == 0) {
                        $_SESSION['last_open_modul'] = '';
                    }
                }

                $row_ = $cms->pdo->prepare("SELECT  ms.id, ms.open, ms.info, m.dirrect, m.title, m.dirrect, m.type_script
                                            FROM    bk_modules as ms,
                                                    bk_modul as m
                                            WHERE   ms.fk_modul=m.id and
                                                    ms.fk_structure= :node and
                                                    ms.del=0
                                            ORDER BY sort ASC");
                $row_->execute([':node' => $params['node']]);
                if ($row_->rowCount() > 0) {
                    while ($row = $row_->fetch()) {
                        if ($row['open']==1) {
                            $row['glyphicon'] = 'open';
                            $row['event']['see'] = 1;
                        } else {
                            $row['glyphicon'] = 'close';
                            $row['event']['see'] = 0;
                        }
                        if(is_file("modules/".(($row['type_script']==2)?"admin_cms/":"").$row['dirrect']."/admin.php")){
                            $row['event']['open'] = 0;
                        } else {
                            $row['event']['open'] = 1;
                        }
                        if (
                            ((!is_numeric($params['modules']))&&($row['event']['open']==0)&&($_SESSION['last_open_modul']   == $row['id'])) ||
                            ((!is_numeric($params['modules']))&&($row['event']['open']==0)&&($_SESSION['last_open_modul']   == ''))         ||
                            (( is_numeric($params['modules']))&&($row['event']['open']==0)&&($params['modules']             == $row['id']))
                        ) {
                            $_SESSION['last_open_modul'] = $row['id'];
                            $row['event']['activ'] = 1;
                        } else {
                            $row['event']['activ'] = 0;
                        }

                        $mas['data']['row'][] = $row;
                    }
                }
            }
            return $mas;
        }

        /**
         * Добавить модуль в node
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function insert($params = null) {
            global $cms;
            if (!$cms->is_access($params['id_tree'], 'mod_add')) {
                $mas['message']['false'][] = 'Отсутсвуют права доступа (mod_add)';
            }
            if (!$params['id_modul']>0) {
                $mas['message']['false'][] = 'Не выбран модуль';
            }
            if (count($mas['message']['false']) == 0) {
                $row_ = $cms->pdo->prepare("SELECT  *
                                            FROM    bk_modul
                                            WHERE   id = :id");
                $row_->execute([':id' => $params['id_modul']]);
                if ($row_->rowCount() > 0) {
                    $row = $row_->fetch();
                    if ($row['install_bd'] == 0) {
                        $dir = "modules".(($row['type_script']==2)?"/admin_cms":"")."/".$row['dirrect']."/tables.sql";
                        if (file_exists($dir)) {
                            $sql = file_get_contents($dir);
                            $cms->pdo->query($sql);
                            $row_ = $cms->pdo->prepare("UPDATE  bk_modul
                                                        SET     install_bd = '1'
                                                        WHERE   id = :id;");
                            $row_->execute([':id' => $row['id']]);
                            $mas['message']['false'] = 'Таблицы распакованы';
                        } else {
                            $mas['message']['false'][] = 'Файл '.$dir.' не существует';
                        }
                    }

                    // можно сделать через сортировку
                    $row_ = $cms->pdo->prepare("SELECT  count(sort)+1 as s
                                                FROM    bk_modules
                                                WHERE   fk_structure = :fk_s");
                    $row_->execute([':fk_s' => $params['id_tree']]);
                    $sort = $row_->fetchColumn();
                    $row_ = $cms->pdo->prepare("INSERT INTO bk_modules (fk_structure, fk_modul, info, `sort`)
                                                VALUES (:fk_s, :fk_m, :info, :sort)");
                    $row_->execute([ ':fk_s' => $params['id_tree'],
                                     ':fk_m' => $params['id_modul'],
                                     ':info' => $params['info'],
                                     ':sort' => $sort]);
                    $_SESSION['last_open_modul'] = $cms->pdo->lastInsertId();
                    $mas['message']['true'][] = 'Модуль добавлен';
                    $mas['data']['node'] = $params['id_tree'];
                } else {
                    $mas['message']['false'][] = 'Модуль не найден';
                }
            }
            return $mas;
        }

        /**
         * Получение параметров
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function setings($params = null) {
            global $cms;
            if ($cms->is_access(1)) {
                $buf[1]['tip'] = 'Модули';
                $buf[0]['tip'] = 'Скрипты';
                if ($cms->is_access(1)) { // доработать на тип доступа
                    $buf[2]['tip'] = 'Системные';
                }

                $row_ = $cms->pdo->query("SELECT id, title, type_script FROM bk_modul where type_script in (0,1,2)");
                while ($row = $row_->fetch()) {
                    $buf[$row['type_script']]['row'][] = $row;
                }
                $mas['data']['modul'][] = $buf[1];
                $mas['data']['modul'][] = $buf[0];
                $mas['data']['modul'][] = $buf[2];
                $mas['data']['node'] = $params['node'];
                if ($cms->group_cms['full'] == 1) {
                    $mas['data']['breadcrumb']  = "[".$params['node']."] ".$cms->structure_page($params['node'], 'title');
                } else {
                    $mas['data']['breadcrumb']  = $cms->structure_page($params['node'], 'title');
                }
            } else {
                $mas['message']['false'][] = 'Нет доступа, или раздел не существует';
            }
            return $mas;
        }

        /**
         * Удаление модуля со страницы
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function delete($params = null) {
            global $cms;
            $row_ = $cms->pdo->prepare("SELECT  s.id as sid
                                        FROM    bk_structure as s,
                                                bk_modules as m
                                        WHERE   1=1
                                                and s.id = m.fk_structure
                                                and s.del = 0
                                                and m.del = 0
                                                and m.id = :id");
            $row_->execute([':id' => $params['id']]);
            if ($row_->rowCount() > 0){
                $row = $row_->fetch();
                if (!$cms->is_access($row['sid'], 'mod_delete')) {
                    $mas['message']['false'][] = 'Отсутсвуют права доступа (mod_delete)';
                }
            } else {
                $mas['message']['false'][] = 'Модуль не найден';
            }
            if (count($mas['message']['false']) == 0) {
                $row_ = $cms->pdo->prepare("UPDATE  bk_modules
                                            SET     del = 1
                                            WHERE   id = :id ");
                $row_->execute([':id' => $params['id']]);
                $mas['data'][] = ['detail' => ''];
                $mas['message']['true'][] = 'Модуль удален';
            }
            return $mas;
        }

        /**
         * Скрыть / показать
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function see($params = null) {
            global $cms;
            $row_ = $cms->pdo->prepare("SELECT  s.id as sid, m.*
                                        FROM    bk_structure as s,
                                                bk_modules as m
                                        WHERE   1=1
                                                and s.id = m.fk_structure
                                                and s.del = 0
                                                and m.del = 0
                                                and m.id = :id");
            $row_->execute([':id' => $params['id']]);
            if ($row_->rowCount() > 0){
                $row = $row_->fetch();
                if (!$cms->is_access($row['sid'], 'mod_display')) {
                    $mas['message']['false'][] = 'Отсутсвуют права доступа (mod_display)';
                }
            } else {
                $mas['message']['false'][] = 'Модуль не найден';
            }
            if (count($mas['message']['false']) == 0) {
                $upd_ = $cms->pdo->prepare("UPDATE bk_modules SET `open`= :open WHERE id = :id ");
                if ($row['open']==1) {
                    $upd_->execute([':open' => 0, ':id' => $params['id']]);
                    $mas['data'][] = ['detail' => 'Модуль скрыт'];
                } else {
                    $upd_->execute([':open' => 1, ':id' => $params['id']]);
                    $mas['data'][] = ['detail' => 'Модуль отображен'];
                }
            }
            return $mas;
        }

        /**
         * Сортировка модулей
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function sort($params = null) {
            global $cms;
            $row_s_ = $cms->pdo->prepare("SELECT  s.id as sid, m.*
                                        FROM    bk_structure as s,
                                                bk_modules as m
                                        WHERE   1=1
                                                and s.id = m.fk_structure
                                                and s.del = 0
                                                and m.del = 0
                                                and m.id = :id");
            $row_s_->execute([':id' => $params['id']]);
            if ($row_s_->rowCount() > 0){
                $row_s = $row_s_->fetch();
                if (!$cms->is_access($row_s['sid'], 'mod_sort')) {
                    $mas['message']['false'][] = 'Отсутсвуют права доступа (mod_sort)';
                }
                if (!in_array($params['order'], ['ASC','DESC'])) { // кнопка вниз и вниз
                    $mas['message']['false'][] = 'Не указано направление сортировки';
                }
            } else {
                $mas['message']['false'][] = 'Модуль не найден';
            }
            if (count($mas['message']['false']) == 0) {
                $row_ = $cms->pdo->prepare("SELECT  id, sort
                                            FROM    bk_modules
                                            WHERE   1=1
                                                    and fk_structure = :fk_s
                                                    and del=0
                                            ORDER BY sort ".$params['order']);
                $row_->execute([':fk_s' => $row_s['sid']]);
                while ($row=$row_->fetch()) {
                    if ((isset($sort_['1']))&&(!isset($sort_['2']))) { // Записали следующий модуль по списку
                        $sort_['2']=$row;
                    }
                    if ($row['id']==$params['id']) { // Нашли модуль который нужно переместить
                        $sort_['1']=$row;
                    }
                }
                if (isset($sort_['2']['id'])) { // если в середине списка (меняем местами
                    $upd_ = $cms->pdo->prepare("UPDATE  bk_modules
                                                SET    `sort` = :sort
                                                WHERE   id = :id ");
                    $upd_->execute(['sort' => $sort_['2']['sort'], 'id' => $sort_['1']['id']]);
                    $upd_->execute(['sort' => $sort_['1']['sort'], 'id' => $sort_['2']['id']]);
                } else if (isset($sort_['1']['id'])) { // если последний элемент списка то меняем с первым
                    $row_ = $cms->pdo->prepare("SELECT  id, sort
                                                FROM    bk_modules
                                                WHERE   1=1
                                                        and fk_structure = :fks
                                                        and id != :id
                                                ORDER BY `sort` ASC ");
                    $row_->execute(['fks' => $row_s['sid'], 'id' => $sort_['1']['id']],1);
                    while ($row=$row_->fetch()) {
                        $mas_[]=$row;
                    }
                    $upd_ = $cms->pdo->prepare("UPDATE  bk_modules
                                                SET    `sort` = :sort
                                                WHERE   id = :id ");
                    if ($params['order']=='ASC') { // кнопка вниз
                        for ($i=0;$i<=count($mas_)-1;$i++) {
                            $upd_->execute(['sort' => $i+2, 'id' => $mas_[$i]['id']]);
                        }
                        $upd_->execute(['sort' => 1, 'id' => $sort_['1']['id']]);
                    }
                    if ($params['order']=='DESC') {    // кнопка вверх
                        for ($i=0;$i<=count($mas_)-1;$i++) {
                            $upd_->execute(['sort' => $i+1, 'id' => $mas_[$i]['id']]);
                        }
                        $upd_->execute(['sort' => count($mas_)+1, 'id' => $sort_['1']['id']]);
                    }
                }
                $mas['data'][] = ['detail' => 'Модуль перемещен'];
            }
            return $mas;
        }
    }

    $modul = new Modul();

    if ($modul->getAction()!='') { // вызов через api, иначе вызов напрямую через php
        echo $modul->query()->fetch();
    }

?>


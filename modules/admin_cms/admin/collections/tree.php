<?php base::test();

    class Tree extends Api {

        /**
         * Добавить node
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function insert($params = null) {
            global $cms;
            if ($params['tree_node']=='NULL') $params['tree_node'] = NULL;
            if (!$cms->is_access($params['id_tree'], 'structure_add')) {
                $mas['message']['false'][] = 'Отсутсвуют права доступа (structure_add)';
            }
            if (!($params['id_tree'] > 0)) {
                $mas['message']['false'][] = 'Не выбран узел дерева';
            }
            if (($params['url'] == '')&&($params['id_tree']>1)) {
                $mas['message']['false'][] = 'Не заполенно поле "URL"';
            }
            if ($params['title'] == '') {
                $mas['message']['false'][] = 'Не заполенно поле "Название"';
            }
            if ($params['open'] == '') {
                $mas['message']['false'][] = 'Не заполенно поле "Раздел открыт"';
            }
            if (!in_array($params['tree_node'], [NULL,1,-1])) {
                $mas['message']['false'][] = 'Не заполенно поле "На карту сайта?"';
            }

            if (count($mas['message']['false']) == 0) {
                $row_=$cms->pdo->prepare("SELECT count(sort)+1 as s FROM `bk_structure` WHERE fk_structure= :sid limit 1");
                $row_->execute([':sid' => $params['id_tree']]);

                $row2_ = $cms->pdo->prepare("INSERT INTO bk_structure (fk_structure, url, title, description, keywords, open, tree_node, sort)
                                             VALUES (:sid, :url, :title, :description, :keywords, :open, :tree_node, :sort)");
                $row2_->execute([   ':sid'         => $params['id_tree'],
                                    ':url'         => $params['url'],
                                    ':title'       => $params['title'],
                                    ':description' => $params['description'],
                                    ':keywords'    => $params['keywords'],
                                    ':open'        => $params['open'],
                                    ':tree_node'   => $params['tree_node'],
                                    ':sort'        => $row_->fetch()['s']
                                ]);
                $mas['message']['true'][] = 'Новый узел добавлен';

                $mas['data']['node'] = $cms->pdo->lastInsertId();
                $mas['data']['url']  = $cms->url($cms->pdo->lastInsertId(), 1);
            }
            return $mas;
        }

        /**
         * Изменить node
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function update($params = null) {
            global $cms;
            if (!$cms->is_access($params['id_tree'], 'structure_edit')) {
                echo form::info("Ошибка: ","Отсутсвуют права доступа (structure_edit)","danger");
            }
            if ($params['tree_node']=='NULL') $params['tree_node'] = NULL;
            if (!($params['id_tree'] > 0)) {
                $mas['message']['false'][] = 'Не выбран узел дерева';
            }
            if (($params['url'] == '')&&($params['id_tree']>1)) {
                $mas['message']['false'][] = 'Не заполенно поле "URL"';
            }
            if ($params['title'] == '') {
                $mas['message']['false'][] = 'Не заполенно поле "Название"';
            }
            if ($params['open'] == '') {
                $mas['message']['false'][] = 'Не заполенно поле "Раздел открыт"';
            }
            if (!in_array($params['tree_node'], [NULL,1,-1])) {
                $mas['message']['false'][] = 'Не заполенно поле "Отображение узлов"';
            }
            if (count($mas['message']['false']) == 0) {
                $row_ = $cms->pdo->prepare("   UPDATE bk_structure
                                                SET     title=:title,
                                                        url=:url,
                                                        description=:description,
                                                        keywords=:keywords,
                                                        open=:open,
                                                        tree_node=:tree_node
                                                WHERE id=:id
                                                LIMIT 1");
                $row_->execute([':url'         => $params['url'],
                                ':title'       => $params['title'],
                                ':description' => $params['description'],
                                ':keywords'    => $params['keywords'],
                                ':open'        => $params['open'],
                                ':tree_node'   => $params['tree_node'],
                                ':id'          => $params['id_tree']
                        ]);
                $mas['message']['true'][] = 'Узел успешно обновлён';

                $mas['data']['node'] = $params['id_tree'];
            }

            return $mas;
        }

        /**
         * Удалить node
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function delete($params = null) {
            global $cms;
            if (!$cms->is_access($params['id_tree'], 'structure_delete')) {
                $mas['message']['false'][] = 'Отсутсвуют права доступа (structure_delete)';
            }
            if (!($params['id_tree'] > 0)) {
                $mas['message']['false'][] = 'Не выбран узел дерева';
            }
            if (count($mas['message']['false']) == 0) {
                $row_ = $cms->pdo->prepare("UPDATE  bk_structure
                                            SET     del='1'
                                            WHERE   id =:id
                                            LIMIT   1");
                $row_->execute([':id' => $params['id_tree']]);

                $row_ = $cms->pdo->prepare("SELECT fk_structure
                                            FROM  bk_structure
                                            WHERE   id =:id
                                            LIMIT   1");
                $row_->execute([':id' => $params['id_tree']]);
                $row = $row_->fetch();
                $mas['data']['node'] = $row['fk_structure'];

                $mas['message']['true'][] = 'Узел успешно Удалён';
            }
            return $mas;
        }

        /**
         * Получение параметров
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function setings($params = null) {
            global $cms;
            if ($cms->is_access(1)) {
                if ($params['new'] != 'true') {
                    $row_ = $cms->pdo->prepare("SELECT  *
                                                FROM    bk_structure
                                                WHERE   1=1
                                                        and id = :id
                                                        and del=0
                                                ORDER BY sort ASC
                                                LIMIT 1");
                    $row_->execute([':id' => $params['id']]);
                    $row = $row_->fetch();
                } else {
                    $row['open'] = 1;
                    $row['tree_node'] = 1;
                    $row['id']   = $params['id'];
                    $row['new']  = true;
                }

                $row['open_row'] = [['id' => '1', 'text' => 'да' , 'checked' => (($row['open']==1)?true:false)],
                                    ['id' => '0', 'text' => 'нет', 'checked' => (($row['open']==0)?true:false)]];
                $row['tree_node'] = [
                                    ['id' => '1',    'text' => 'Список категорий' ,        'checked' => (($row['tree_node']== 1)?true:false)],
                                    ['id' => 'NULL', 'text' => 'Не отображать подразделы', 'checked' => (($row['tree_node']=='')?true:false)],
                                    ['id' => '-1',   'text' => 'Карта сайта',              'checked' => (($row['tree_node']==-1)?true:false)]
                                ];
                $mas['data'] = $row;
            } else {
                $mas['message']['false'][] = 'Нет доступа, или раздел не существует';
            }
            return $mas;
        }

        /**
         * Получение Дерева
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function node($params = null) {
            global $cms;
            $tree = $params['node'];
            while (true) {
                $row_=$cms->pdo->prepare("  SELECT  *
                                            FROM    bk_structure
                                            WHERE   1=1
                                                    and fk_structure=:fks
                                                    and del=0
                                            ORDER BY sort ASC");
                $row_->execute([':fks'=>$tree]);
                $buf_level = $level;
                unset($level);
                while($row=$row_->fetch()) {
                    if ($cms->is_access($row['id'])) { // Проверка прав(Просмотр списка модулей)
                        $row2_=$cms->pdo->prepare(" SELECT  *
                                                    FROM    bk_structure
                                                    WHERE   1=1
                                                            and fk_structure=:fks
                                                            and del=0
                                                    ORDER BY sort ASC");
                        $row2_->execute([':fks'=>$row['id']]);
                        if ($row2_->rowCount() == 0) {
                            $node = false;
                        } else {
                            $node = true;
                            if ($row['id'] == $buf_id) { // нашли корень дерева прошлого прохода дерева
                                $node = $buf_level;
                            }
                        }

                        if ($row['open'] == 0) $row['open'] = ''; // поменять в БД

                        $level[] = ['id'    => $row['id'],
                                    'title' => $row['title'],
                                    'open'  => $row['open'],
                                    'node'  => $node,
                                    'url'   => $cms->url($row['id'], 1),
                                    'access'=> $cms->group_cms['full']
                                    ];
                    }
                }
                if (($tree==0)||($params['lavel']==1)) break;
                $buf_id = $tree;
                $tree   = $cms->structure_page($tree,'fk_structure');
            }
            $mas['data']['node'] = $level;
            return $mas;
        }

        /**
         * Перемещение узла дерева
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function move_nod($params = null) {
            global $cms;
            // 1) если prev_id = 0 то добавляем элемент в начало и всем sort+1
            $row_ = $cms->pdo->prepare("UPDATE  bk_structure
                                        SET     sort = (SELECT @a:= @a + 2 FROM (SELECT @a:= 0) as tbl)
                                        WHERE   fk_structure = :fks
                                        ORDER BY sort");
            $row_->execute([':fks' => $params['new_parent']]);

            $row_ = $cms->pdo->prepare("SELECT  fk_structure
                                        FROM    bk_structure
                                        WHERE   id=:id
                                        LIMIT 1");
            $row_->execute([':id'=>$params['my_id']]);
            $row = $row_->fetch();
            $params['old_parent'] = $row['fk_structure'];

            if($params['new_parent'] != $params['old_parent']){
                $row_ = $cms->pdo->prepare("UPDATE  bk_structure
                                            SET     fk_structure = :fks
                                            WHERE   id = :id");
                $row_->execute([':fks' => $params['new_parent'],
                                ':id'  => $params['my_id']
                               ]);

                $row_ = $cms->pdo->prepare("UPDATE  bk_structure
                                            SET     sort = (SELECT @a:= @a + 1 FROM (SELECT @a:= 0) as tbl)
                                            WHERE   fk_structure = :fks
                                            ORDER BY sort");
                $row_->execute([':fks' => $params['old_parent']]);
            }

            $row_ = $cms->pdo->prepare("SELECT  sort
                                        FROM    bk_structure
                                        WHERE   id=:id
                                        LIMIT 1");
            $row_->execute([':id'=>$params['prev_id']]);
            $row = $row_->fetch();

            $row_ = $cms->pdo->prepare("UPDATE  bk_structure
                                        SET     sort=:sort
                                        WHERE   id=:id ");
            $row_->execute([':sort' => $row['sort']+1,
                            ':id'   => $params['my_id']
                          ]);

            $row_ = $cms->pdo->prepare("UPDATE  bk_structure
                                        SET     sort = (SELECT @a:= @a + 1 FROM (SELECT @a:= 0) as tbl)
                                        where   fk_structure = :fks
                                        ORDER BY sort");
            $row_->execute([':fks' => $params['new_parent']]);
            return ['data' => ['url' => $cms->url($params['my_id'], 1)]];
        }
    }

    $tree = new Tree();

    if ($tree->getAction()!='') { // вызов через api, иначе вызов напрямую через php
        echo $tree->query()->fetch();
    }

?>


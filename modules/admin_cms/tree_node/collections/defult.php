<?php base::test();

    class DefultNode extends Api {

        /**
         * Одно вложение
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function node($params = null) {
            global $cms;
            $node = self::select(['id' => $cms->open_page]);
            include dirname(__FILE__).'/../tpl/node.php';
        }

        /**
         * Дерево сайта
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function tree($params = null) {
            global $cms;

            $node = self::select(['id' => 0, 'tree' => true]);
            self::tpl_tree($node);
        }

        /**
         * рекурсивный вызов шаблона
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function tpl_tree($node) {
            global $cms;
            include dirname(__FILE__).'/../tpl/tree.php';
        }

        /**
         * Собираем массив по дереву
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function select($params) {
            global $cms;

            if ($params['id'] >= 0) {
                $row_=$cms->pdo->prepare("  SELECT  id, title, fk_structure
                                            FROM    bk_structure
                                            WHERE   1=1
                                                    and open = 1
                                                    and del  = 0
                                                    and fk_structure = :id
                                            ORDER BY sort ASC");
                $row_->execute([':id' => $params['id']]);
                while ($row = $row_->fetch()) {
                    if (isset($params['tree'])) {
                        if ($row['id']>0) {
                            $node = self::select([ 'id'    => $row['id'],
                                                   'tree'  => true
                                                 ]);
                        }
                    }
                    $data[] = [   'id'    => $row['id'],
                                            'title' => $row['title'],
                                            'url'   => $cms->url($row['id']),
                                            'node'  => $node
                                        ];
                }
            }
            return $data;
        }


        /**
         * рекурсивный вызов шаблона
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function menu($params) {
            global $cms;
            $row_ = $cms->pdo->query("  SELECT  *
                                        FROM    bk_structure
                                        WHERE   1=1
                                                and open = 1
                                                and del  = 0
                                                -- and fk_structure = 1
                                                and id in (".$params['nones'].")
                                        ORDER BY sort ASC");
            if ($row_->rowCount() > 0) {
                while ($row = $row_->fetch()) {
                    $nodes = [];
                    $node_ = $cms->pdo->prepare(" SELECT  *
                                                    FROM    bk_structure
                                                    WHERE   1=1
                                                            and open = 1
                                                            and del  = 0
                                                            and fk_structure = :node
                                                    ORDER BY sort ASC
                                                    limit 10");
                    $node_->execute([':node' => $row['id']]);
                    if ($node_->rowCount() > 0) {
                        while ($node = $node_->fetch()) {
                            $nodes[] = ['url'    => $cms->url($node['id']),
                                        'title'  => $cms->structure_page($node['id'],'title'),
                                        'active' => (($cms->open_page == $node['id'])?1:0),
                                        ];
                        }
                    }

                    $menu[] = [ 'url'    => $cms->url($row['id']),
                                'title'  => $cms->structure_page($row['id'],'title'),
                                'active' => (($cms->open_page == $row['id'] || $cms->structure_page($cms->open_page, 'fk_structure') == $row['id'])?1:0),
                                'node'   => $nodes
                                ];
                }
            }
            return $menu;
        }

    }

    $defultNode = new DefultNode();

    if ($defultNode->getAction()!='') { // вызов через api, иначе вызов напрямую через php
        echo $defultNode->query()->fetch();
    }

?>

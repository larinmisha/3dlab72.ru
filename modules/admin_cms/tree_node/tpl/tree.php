<?php base::test(); ?>
<?php if (isset($node)) { ?>
    <ul class='tree'>
        <?php foreach ($node as $row) { ?>
            <li>
                <a href='<?php echo $row['url']; ?>' target='_blank'>
                    <?php if (count($row['node'])>0) { ?>
                        <b><?php echo $row['title']; ?></b>
                    <?php } else { ?>
                        <?php echo $row['title']; ?>
                    <?php } ?>
                </a>
                <?php
                    if (count($row['node'])>0) {
                        echo self::tpl_tree($row['node']);
                    }
                ?>
            </li>
        <?php } ?>
    </ul>
<?php } ?>

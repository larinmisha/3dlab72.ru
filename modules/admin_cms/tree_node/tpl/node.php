<?php base::test(); ?>
<?php if (isset($node)) { ?>
    <div class="tpl__menu-node">
        <!-- <div class="container"> -->
            <div class="node">
                <?php foreach ($node as $row) { ?>
                    <a  href="<?php echo $row['url']; ?>"
                    <?php if (preg_match('#^http#', $row['url']) === 1) { ?> target="_blank" <?php } ?>>
                        <div class="btns">
                            <div>
                                <?php echo $row['title']; ?>
                            </div>
                            <svg width="26" height="26" viewBox="0 0 26 26" fill="#757575" xmlns="http://www.w3.org/2000/svg">
                                <path d="M23.7071 13.7071C24.0976 13.3166 24.0976 12.6834 23.7071 12.2929L17.3431 5.92893C16.9526 5.53841 16.3195 5.53841 15.9289 5.92893C15.5384 6.31946 15.5384 6.95262 15.9289 7.34315L21.5858 13L15.9289 18.6569C15.5384 19.0474 15.5384 19.6805 15.9289 20.0711C16.3195 20.4616 16.9526 20.4616 17.3431 20.0711L23.7071 13.7071ZM3 14L23 14L23 12L3 12L3 14Z"/>
                            </svg>
                        </div>
                    </a>
                <?php } ?>
            </div>
        <!-- </div> -->
    </div>
<?php } ?>


<?php base::test(); ?>
<?php http_response_code(404); ?>

<div class="b-browser__wrapper">

    <h1 class="b-browser__header">Ошибка 404</h1>

    <br>

    <p>Извините! Страница, которую Вы ищете, не может быть найдена</p>
    <p>Возможно, запрашиваемая Вами страница была перенесена или удалена. Также возможно, Вы допустили небольшую опечатку при вводе адреса – такое случается даже с нами, поэтому еще раз внимательно проверьте</p>
    <p>Переходите на <a href='/'>главную страницу</a> – там Вы также сможете найти много полезной информации</p>

</div>

<!-- todo: переместить стили -->
<style type="text/css">
    body {
        background-color: #FDFDFD;
    }
        .b-browser__wrapper {
            font-family: 'PT Sans',sans-serif;
            background-color: #fff;
            border-radius: 4px;
            -webkit-border-radius: 4px;
            -webkit-box-sizing: content-box;
            border: 1px solid #e1e1e1;
            box-sizing: content-box;
            font-family: 'PT Sans',sans-serif;
            height: 485px;
            left: 50%;
            margin-left: -417px;
            margin-top: -270px;
            padding: 55px 96px 0;
            position: absolute;
            text-align: center;
            top: 50%;
            width: 640px
        }
            .b-browser__header {
                color: #00bcf2
            }
            .b-browser__wrapper h1,
            .b-browser__wrapper h3,
            .b-browser__wrapper h4 {
                margin: 75px 0 0 0;
                font-size: 76px;
            }
</style>

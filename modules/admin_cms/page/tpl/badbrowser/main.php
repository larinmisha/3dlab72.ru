<?php base::test(); ?>

<div class="b-browser__wrapper">

    <h1 class="b-browser__header">Ваш браузер безнадежно устарел!</h1>

    <br>
    <br>

    <h4>Рекомендуем обновиться до актуальной версии, в противном случае вы никогда не сможете написать нам сообщениее, ямы на дорогах останутся там где были, надписи на фасадах будут продолжать раздражать вас всякий раз, а вместо детской площадки в вашем дворе будет унылая деревянная горка!</h4>

    <br>
    <br>

    <table class="b-browser">
        <tbody>
            <tr>
                <td>
                    <a href="http://www.google.com/chrome/" target="_blank" class="b-browser__link">
                        <img src="<?php echo $modules['http']; ?>img/chrome.png" class="b-browser__image" title="">
                        <span class="b-browser__title">установить</span>
                    </a>
                </td>
                <td>
                    <a href="http://browser.yandex.ru" target="_blank" class="b-browser__link">
                        <img src="<?php echo $modules['http']; ?>img/yandex.png" class="b-browser__image" title="">
                        <span class="b-browser__title">установить</span>
                    </a>
                </td>
                <td>
                    <a href="https://www.mozilla.org/ru/firefox/products/" target="_blank" class="b-browser__link">
                        <img src="<?php echo $modules['http']; ?>img/firefox.png" class="b-browser__image" title="">
                        <span class="b-browser__title">установить</span>
                    </a>
                </td>
                <td>
                    <a href="http://www.opera.com" target="_blank" class="b-browser__link">
                        <img src="<?php echo $modules['http']; ?>img/opera.png" class="b-browser__image" title="">
                        <span class="b-browser__title">установить</span>
                    </a>
                </td>
            </tr>
        </tbody>
    </table>

    <br>

    <h3>Просто выберите картинку которая вам нравится, нажмите и старайтесь следовать инструкции, иначе читайте абзац выше!</h3>

</div>

<!-- todo: переместить стили -->
<style type="text/css">
    body {
        background-color: #FDFDFD;
    }
        .b-browser__wrapper {
            background-color: #fff;
            -webkit-border-radius: 4px;
            border-radius: 4px;
            border: 1px solid #e1e1e1;
            -webkit-box-sizing: content-box;
            box-sizing: content-box;
            height: 485px;
            left: 50%;
            margin-left: -485px;
            margin-top: -270px;
            padding: 55px 96px 0;
            position: absolute;
            text-align: center;
            top: 50%;
            width: 780px
        }
        .b-browser {
            margin: 0 auto;
            text-align: center
        }
            .b-browser__header {
                color: #00bcf2
            }
            .b-browser__wrapper h1,
            .b-browser__wrapper h3,
            .b-browser__wrapper h4 {
                margin: 0
            }
            .b-browser__link {
                -webkit-border-radius: 10px;
                border-radius: 10px;
                -webkit-box-sizing: content-box;
                box-sizing: content-box;
                color: #fff;
                display: block;
                font-size: 14px;
                font-weight: 700;
                height: 130px;
                padding: 15px 15px 0;
                width: 100px
            }
                .b-browser__link:hover {
                    background: #e5f8fe;
                    color: #000;
                    text-decoration: none
                }
                .b-browser__image {
                    margin-bottom: 5px
                }
</style>

<?php
	base::test();
    if ($modules['fk_structure']>0) $mas['rzd'] = ['title'=>$this->structure_page($modules['fk_structure'],'title')];

    $row_ = $this->pdo->prepare("SELECT * FROM bk_poligon_settings where fk_modules=:id");
    $row_->execute(array(   ':id'=>$modules['id']));
    $mas['map'] = $row_->fetch();

    $row_ = $this->pdo->prepare("SELECT     svg.id, svg.svg
                                    FROM    bk_poligon as p,
                                            bk_poligon_settings_svg as svg
                                    where   1=1
                                            and p.fk_svg=svg.id
                                            and p.del=0
                                            and p.fk_modules=:id
                                    GROUP BY svg.id
                                    ORDER BY  p.sort, p.name ASC ");
    $row_->execute(array(':id'=>$modules['id']));
    while($row=$row_->fetch()) {
        $row['svg'] = $modules['http'].'img/placemark/'.$row['svg'];
        $mas['layout'][] = $row;
    }
    $mas['layout'][] = ['id'=>'search','svg'=>$modules['http'].'img/placemark/search.png'];
    $mas['layout'][] = ['id'=>'0','svg'=>$modules['http'].'img/placemark/search.png']; // заглушка для 0


    $row_ = $this->pdo->prepare("SELECT     g.id, g.name_group, g.defult, g.archive
                                    FROM    bk_poligon as p,
                                            bk_poligon_settings_group as g
                                    where   1=1
                                            and p.fk_group=g.id
                                            and p.del=0
                                            and g.del=0
                                            and p.fk_modules=:id
                                    GROUP BY g.id
                                    ORDER BY  p.sort, p.name ASC ");
    $row_->execute(array(':id'=>$modules['id']));
    while($row=$row_->fetch()) {
        $mas['group'][] = $row;
    }

    $row_ = $this->pdo->prepare("SELECT     p.*, g.archive
                                    FROM    bk_poligon as p,
                                            bk_poligon_settings_group as g
                                    where   1=1
                                            and p.fk_group=g.id
                                            and p.del=0
                                            and g.del=0
                                            and p.fk_modules=:id
                                    ORDER BY  p.sort, p.name ASC
                                ");
    $row_->execute(array(':id'=>$modules['id']));
    while($row=$row_->fetch()) {
        $row['information'] = json_decode($row['information']);
        $mas['poligon'][] = $row;
    }

	if ($_GET['test']=='test') {
        echo "<pre>";print_r($mas);echo "</pre>";
	} else {
		echo json_encode($mas);
	}
?>


CREATE TABLE `bk_poligon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_modules` int(6) NOT NULL DEFAULT '0',
  `fk_svg` int(6) NOT NULL DEFAULT '0',
  `fk_group` int(6) NOT NULL DEFAULT '0' COMMENT 'Группа меток',
  `name` text NOT NULL,
  `year` int(6) NOT NULL DEFAULT '-1' COMMENT 'год (-1 это, без года)',
  `poligon` text NOT NULL,
  `visible_poligon` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1-отображать полигон',
  `placemark` varchar(50) NOT NULL DEFAULT '',
  `visible_placemark` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1-отображать метку',
  `center` varchar(50) NOT NULL DEFAULT '[57.147699999992305,65.54009999999998]',
  `zoom` varchar(5) NOT NULL DEFAULT '10',
  `strokeWidth` tinyint(4) NOT NULL DEFAULT '4',
  `strokeWidthOn` tinyint(4) NOT NULL DEFAULT '2',
  `strokeColor` varchar(10) NOT NULL DEFAULT 'aad5ea99',
  `strokeColorOn` varchar(10) NOT NULL DEFAULT 'aad5ea99',
  `fillColor` varchar(10) NOT NULL DEFAULT 'ffffff99',
  `fillColorOn` varchar(10) NOT NULL DEFAULT 'fbf7e899',
  `fk_raion` tinyint(1) NOT NULL DEFAULT '0',
  `sort` int(1) NOT NULL DEFAULT '0',
  `del` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1-удалить',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `bk_poligon_file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_poligon` int(6) NOT NULL,
  `files` varchar(255) NOT NULL COMMENT 'имя файла',
  `type_files` int(6) NOT NULL DEFAULT '0',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `del` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1-удалить',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `bk_poligon_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_modules` int(6) NOT NULL DEFAULT '0',
  `center` varchar(50) NOT NULL DEFAULT '[57.147699999992305,65.54009999999998]',
  `zoom` varchar(3) NOT NULL DEFAULT '10',
  `minzoom` varchar(3) NOT NULL DEFAULT '',
  `maxzoom` varchar(3) NOT NULL DEFAULT '',
  `layer` varchar(100) NOT NULL DEFAULT '',
  `restrictMapArea` varchar(100) NOT NULL DEFAULT '' COMMENT 'область просмотра карты так, чтобы пользователь не мог выйти за пределы этой области',
  `projection` text NOT NULL COMMENT 'Проекция карты.',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `bk_poligon_settings_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_modules` int(6) NOT NULL,
  `name_group` varchar(255) NOT NULL COMMENT 'имя фильтра',
  `defult` int(6) NOT NULL DEFAULT '0' COMMENT 'Основной фальтр который активируется на странице по умолчанию ',
  `archive` int(6) NOT NULL DEFAULT '0' COMMENT 'передавать в api, но не показывать(только по факту если есть сообщение по нему)',
  `del` int(6) NOT NULL DEFAULT '0' COMMENT '1-удалить',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `bk_poligon_settings_group` (`id`, `fk_modules`, `name_group`, `defult`, `archive`, `del`)
VALUES
  (1,0,'0',0,0,0);

CREATE TABLE `bk_poligon_settings_svg` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_structure` int(6) NOT NULL COMMENT 'Не обязательное поле, пишется но в модуле не должно быть использовано, это для сторонних модулей если нужно тут хранить картинку на метку ',
  `fk_modules` int(6) NOT NULL,
  `name_svg` varchar(255) NOT NULL COMMENT 'имя фильтра',
  `svg` varchar(255) NOT NULL,
  `defult` int(6) NOT NULL DEFAULT '0' COMMENT 'Основной фальтр который активируется на странице по умолчанию ',
  `display` int(6) NOT NULL DEFAULT '0',
  `del` int(6) NOT NULL DEFAULT '0' COMMENT '1-удалить',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

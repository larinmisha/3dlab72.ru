<?php
    base::test(); // Скрипт защиты от запуска напрямую
    # --------------------------------------------------- Удаление записи из ленты ---------------------------------------------------
    if ($_GET['deletelent']!='') {
        $this->pdo->query("UPDATE bk_poligon_file
                        SET del='1'
                        WHERE id='{$_GET['deletelent']}'
                        LIMIT 1");
        echo form::info("Увеломление!", "Фото удалено", "success");
        $_GET['action']="form-lent";
    }
    if ($_GET['typelent']!='') {
        if ($_GET['tf']==0) {
            $type_files = 1;
        } else {
            $type_files = 0;
        }
        $this->pdo->query("UPDATE bk_poligon_file
                        SET type_files='{$type_files}'
                        WHERE id='{$_GET['typelent']}'
                        LIMIT 1");
        echo form::info("Увеломление!", "Статус у фото изменён", "success");
        $_GET['action']="form-lent";
    }

    # --------------------------------------------------- Удаление новости ---------------------------------------------------

    include "classes/class.upload.php";
    if (isset($_POST['save-form-lent'])) {

        $path="files/poligon/file/".date("Y/m/");
        if (!file_exists($path)) {
            mkdir($path, 0775, true);
        }
        $f_name ='';
        if ($_POST['photo_h']==1) {    // Есть мы вышли в меню с выбором файла то может для того чтобы удалить,
            if ($_FILES['photo']['tmp_name']!="") {
                $r =strtolower(pathinfo($_FILES['photo']['name'], PATHINFO_EXTENSION));
                $f_name = uniqid('p_').".{$r}";
                if (in_array($r, array("jpg", "png", "gif", "bmp", "jpeg"))) {
                    $file = $path.$f_name;
                    if (copy($_FILES['photo']['tmp_name'], $file)) {
                        $handle = new Upload($file);
                        $handle->file_overwrite = true; // перезаписать файл
                        $handle->image_resize        = true;
                        $handle->image_y               = 1080;
                        $handle->image_x               = 1920;
                        $handle->image_ratio         = true;
                        $handle->process($path);
                        /* http://programilla.com/blog/siteconstruction/81.html - тут параметры */
                        // echo 'путь к записанному файлу '.$path.$f_name.'<br>';

                        $row_ = $this->pdo->prepare("INSERT INTO `bk_poligon_file`(`fk_poligon`, `files`, `type_files`) VALUES (:id, :file, :tf)");
                        $row_->execute(array(   ':id'=>$_POST['save-form-lent'],
                                                ':file'=>$f_name,
                                                ':tf'=>$_POST['tf']
                                            ));

                        echo form::info("Уведомление!","Фото успешно добавлено","success");

                    } else echo "не удалось записать файл";
                }
            }
        }

        $_GET['action']="form-lent";
    }

    if ($_GET['action']=="form-lent") {
        # --------------------------------------------------- Добавление/Редактирование новости ---------------------------------------------------
        $row_=$this->pdo->prepare("SELECT name FROM bk_poligon WHERE id=:id and del=0");
        $row_->execute(array(':id'=>$_GET['lenta']));
        $row = $row_->fetch();
        echo form::start($this->structure_page($modules['fk_structure'],'chpu_admin').'tpl-file_lenta-'.$_GET['lenta']);
        echo form::hidden("save-form-lent","","",$_GET['lenta']);
        echo form::header(array("but_0"=>   array   (   "value"     =>  "Добавить",
                                                                    "name"      =>  "btn_save",
                                                                    "type"      =>  "submit",
                                                                    "OnClick"   =>  "")
                                            )
                                    );
        echo form::info("Полигон: ",$row['name'],'success');
        echo form::file("Фото","photo",$row['photo']);
        echo form::radio('Тип фото', 'tf', ['0'=>'Фото до','1'=>'Фото после'], 0);
        echo form::footer();
        echo form::end();


        $row_=$this->pdo->prepare(" SELECT * FROM bk_poligon_file WHERE fk_poligon = :id and del=0");
        $row_->execute(array(':id'=>$_GET['lenta']));
        ?>
        <div class="row">
            <?php while($row = $row_->fetch()) { ?>
                <?php
                    $path="/files/poligon/file/".date("Y/m/",strtotime($row['timestamp'])).$row['files'];
                ?>
                <div class="col-xs-6 col-md-4">
                    <div OnClick="$('#b_del_<?php echo $row['id']; ?>').toggle();" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></div>
                    <span id='b_del_<?php echo $row['id']; ?>' style='display:none;'>
                        ->
                        <a href="<?php echo $this->structure_page($modules['fk_structure'],'chpu_admin').'tpl-file_lenta-'.$_GET['lenta'].'_deletelent-'.$row['id']; ?>" title="Уралить">
                            <button type="button" class="btn btn-default btn-xs">
                                <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>вы уверены?
                            </button>
                        </a>

                    </span>
                    <a href="<?php echo $this->structure_page($modules['fk_structure'],'chpu_admin').'tpl-file_lenta-'.$_GET['lenta'].'_typelent-'.$row['id'].'_tf-'.$row['type_files']; ?>" >
                        <button type="button" class="btn btn-default btn-xs">
                            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                            <?php if ($row['type_files']==0) echo "Фото до"; else echo "Фото после"; ?>
                        </button>
                    </a>
                    <img src="<?php echo $path; ?>" class="img-responsive">
                </div>
            <?php } ?>
        </div>
        <?php

        # --------------------------------------------------- Добавление/Редактирование новости ---------------------------------------------------
    }


    if ($_GET['action']=="") {
        # --------------------------------------------------- Вывод списка новостей --------------------------------------------------- #
        $wh = '';
        if ($_SESSION['mod_poligon'][$modules['id']]['s_year']>0) {
            $wh = " and p.year = ".$_SESSION['mod_poligon'][$modules['id']]['s_year'];
        }
        $row_=$this->pdo->query("SELECT p.id,
                                        p.name,
                                        p.year,
                                        (SELECT count(ff.id) FROM bk_poligon_file as ff WHERE ff.del=0 and ff.type_files=0 and ff.fk_poligon=p.id ) as tip0,
                                        (SELECT count(ff.id) FROM bk_poligon_file as ff WHERE ff.del=0 and ff.type_files=1 and ff.fk_poligon=p.id ) as tip1
                                FROM    bk_poligon as p left join bk_poligon_file as f on p.id=f.fk_poligon and f.del=0
                                WHERE   p.fk_modules = ".$modules['id']." and
                                        p.del=0
                                        {$wh}
                                group by p.id, p.year, p.name
                                ORDER BY p.name ASC");
        if($row_->rowCount()>0) {
        ?>
            <table class="table table-hover">
                <tr>
                    <th>Полигон</th>
                    <th>Файлов до</th>
                    <th>Файлов после</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                </tr>
            <?php
                while($row=$row_->fetch()) {

            ?>
                <tr >
                    <td>
                        <?php echo $row['name']; ?>
                        <br>
                        <b><?php if ($row['year']>0) { echo "(".$row['year']." год)"; } ?></b>
                    </td>
                    <td <?php if ($row['tip0']==0) echo "class='danger'"; ?>><?php echo $row['tip0']; ?></td>
                    <td <?php if ($row['tip1']==0) echo "class='danger'"; ?>><?php echo $row['tip1']; ?></td>
                    <td>
                        <a href='<?php echo "{$this->structure_page($modules['id'],'chpu_admin')}tpl-file_lenta-{$row['id']}_action-form-lent"; ?>' title='Редактировать'>
                            <button type='button' class='btn btn-default btn-xs'>
                                <span class='glyphicon glyphicon-pencil' aria-hidden='true'></span>
                            </button>
                        </a>
                    </td>
                </tr>
            <?php
                }
            ?>
            </table>
        <?php
        } else {
            echo form::info("Уведомление!","Лента пуста","success");
        }
        # --------------------------------------------------- Вывод списка новостей --------------------------------------------------- #
    }
?>


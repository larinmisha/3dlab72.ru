<?php
	base::test();

    if (!array_key_exists('tpl', $_GET)) {
        if (isset($_POST['btn_save'])) {
            if (($_POST['poligon']!='') and ($_POST['placemark']!='') and ($_POST['center']!='')) {
                if (!array_key_exists('visible_placemark', $_POST)) $_POST['visible_placemark']=0;
                if (!array_key_exists('fk_svg', $_POST)) $_POST['fk_svg']=0;
                if (!array_key_exists('visible_poligon', $_POST)) $_POST['visible_poligon']=0;
                if ($_POST['id']>0) {
                    $row_ = $this->pdo->prepare("   UPDATE `bk_poligon`
                                                    SET `name`=:name,
                                                        `year`=:year,
                                                        `fk_svg`=:fk_svg,
                                                        `poligon`=:poligon,
                                                        `visible_poligon`=:visible_poligon,
                                                        `placemark`=:placemark,
                                                        `visible_placemark`=:visible_placemark,
                                                        `center`=:center,
                                                        `zoom`=:zoom,
                                                        `strokeWidth`=:strokeWidth,
                                                        `strokeColor`=:strokeColor,
                                                        `fillColor`=:fillColor
                                                    WHERE `id`=:id ");
                    $row_->execute(array(   ':id'=>$_POST['id'],
                                            ':name'=>$_POST['name'],
                                            ':year'=>$_POST['year'],
                                            ':fk_svg'=>$_POST['fk_svg'],
                                            ':poligon'=>$_POST['poligon'],
                                            ':visible_poligon'=>$_POST['visible_poligon'],
                                            ':placemark'=>$_POST['placemark'],
                                            ':visible_placemark'=>$_POST['visible_placemark'],
                                            ':center'=>$_POST['center'],
                                            ':zoom'=>$_POST['zoom'],
                                            ':strokeWidth'=>$_POST['strokeWidth'],
                                            ':strokeColor'=>$_POST['strokeColor'],
                                            ':fillColor'=>$_POST['fillColor']
                                            ));
                } else {
                    $row_ = $this->pdo->prepare("INSERT INTO `bk_poligon` ( `fk_modules`,
                                                                            `name`,
                                                                            `year`,
                                                                            `fk_svg`,
                                                                            `poligon`,
                                                                            `visible_poligon`,
                                                                            `placemark`,
                                                                            `visible_placemark`,
                                                                            `center`,
                                                                            `zoom`,
                                                                            `strokeWidth`,
                                                                            `strokeColor`,
                                                                            `fillColor`)
                                                                    VALUES (:id,
                                                                            :name,
                                                                            :year,
                                                                            :fk_svg,
                                                                            :poligon,
                                                                            :visible_poligon,
                                                                            :placemark,
                                                                            :visible_placemark,
                                                                            :center,
                                                                            :zoom,
                                                                            :strokeWidth,
                                                                            :strokeColor,
                                                                            :fillColor )");
                    $row_->execute(array(   ':id'=>$modules['id'],
                                            ':name'=>$_POST['name'],
                                            ':year'=>$_POST['year'],
                                            ':fk_svg'=>$_POST['fk_svg'],
                                            ':poligon'=>$_POST['poligon'],
                                            ':visible_poligon'=>$_POST['visible_poligon'],
                                            ':placemark'=>$_POST['placemark'],
                                            ':visible_placemark'=>$_POST['visible_placemark'],
                                            ':center'=>$_POST['center'],
                                            ':zoom'=>$_POST['zoom'],
                                            ':strokeWidth'=>$_POST['strokeWidth'],
                                            ':strokeColor'=>$_POST['strokeColor'],
                                            ':fillColor'=>$_POST['fillColor']
                                            ));
                }
            } else {
                echo form::info("Ошибка","Пусой полигон, невозможно сохранить");
            }
        }


        if (isset($_POST['btn_save_set'])) {
            $row = $this->pdo->query("SELECT * FROM `bk_poligon_settings` WHERE `fk_modules` = {$modules['id']}")->fetch();
            if ($row['id']>0) {
                $row_ = $this->pdo->prepare("   UPDATE `bk_poligon_settings`
                                                SET `center`=:center,
                                                    `zoom`=:zoom,
                                                    `minzoom`=:minzoom,
                                                    `maxzoom`=:maxzoom,
                                                    `layer`=:layer,
                                                    `restrictMapArea`=:restrictMapArea,
                                                    `projection`=:projection
                                                WHERE `id`=:id ");
                $row_->execute(array( ':id'=>$row['id'],
                                      ':center'=>$_POST['map_center'],
                                      ':zoom'=>$_POST['map_zoom'],
                                      ':minzoom'=>$_POST['map_minzoom'],
                                      ':maxzoom'=>$_POST['map_maxzoom'],
                                      ':layer'=>$_POST['layer'],
                                      ':restrictMapArea'=>$_POST['restrictMapArea'],
                                      ':projection'=>$_POST['projection']
                                      ));
            } else {
                $row_ = $this->pdo->prepare("INSERT INTO `bk_poligon_settings` (`fk_modules`, `center`, `zoom`, `minzoom`, `maxzoom`, `layer`, `restrictMapArea`, `projection`)
                                        VALUES (:id, :center, :zoom, :minzoom, :maxzoom, :layer, :restrictMapArea, :projection )");
                $row_->execute(array( ':id'=>$row['id'],
                                      ':center'=>$_POST['map_center'],
                                      ':zoom'=>$_POST['map_zoom'],
                                      ':minzoom'=>$_POST['map_minzoom'],
                                      ':maxzoom'=>$_POST['map_maxzoom'],
                                      ':layer'=>$_POST['layer'],
                                      ':restrictMapArea'=>$_POST['restrictMapArea'],
                                      ':projection'=>$_POST['projection']
                                      ));

            }
        }
        if ((isset($_POST['btn_del']))&&($_POST['id']>0)) {
            $row_ = $this->pdo->prepare("UPDATE `bk_poligon` SET del='1' WHERE `id`=:id ");
            $row_->execute(array(   ':id'=>$_POST['id']));
        }
    }



    $svg_defult=0;
    $row_=$this->pdo->prepare("SELECT id, name_svg, defult FROM bk_poligon_settings_svg WHERE fk_modules = :id  ORDER BY name_svg ASC");
    $row_->execute(array(':id'=>$modules['id']));
    while($row=$row_->fetch()) {
        $svg[$row['id']] = $row['name_svg'];
        if ($row['defult']==1) {
            $svg_defult = $row['id'];
       }
    }
    if ($row_->rowCount()==0) {
        echo "<div class='alert alert-warning' role='alert'><strong>Ошибка:</strong> Перед добавлением, необходимо добавить типы меток на карте</div>";

    }




	if (isset($_POST['btn_search'])) {
		$_SESSION['mod_poligon'][$modules['id']]['s_year']  = $_POST['s_year'];
	}

 	$row_=$this->pdo->prepare("SELECT Distinct year FROM bk_poligon WHERE del=0 and fk_modules = :id  ORDER BY year ASC");
 	$row_->execute(array(':id'=>$modules['id']));
 	$s_year[''] = '-- За все года --';
	while($row=$row_->fetch()){
		$s_year[$row['year']] = $row['year'].' год';
	}
    if (array_key_exists('tpl', $_GET)) {
       echo form::start($this->structure_page($modules['fk_structure'],'chpu_admin').'tpl-'.$_GET['tpl']);
    } else {
	   echo form::start($this->structure_page($modules['fk_structure'],'chpu_admin'));
    }
	echo form::header(array("but_0"=>	array	(	"value"		=>	"Поиск",
																"type"		=>	"submit",
																"name"		=>	"btn_search",
																"OnClick"	=>	"")
							),'Форма поиска'
					);
	echo form::select("Год","s_year",$s_year,$_SESSION['mod_poligon'][$modules['id']]['s_year']);
	echo form::footer();
	echo form::end();
?>
	<ul class="nav nav-tabs">
		<li role="presentation" class="<?php if (!array_key_exists('tpl', $_GET)) echo 'active'; ?>"><a href="<?php echo $this->structure_page($modules['fk_structure'],'chpu_admin'); ?>">На карте</a></li>
		<li role="presentation" class="<?php if ($_GET['tpl']=='table') echo 'active'; ?>"><a href="<?php echo $this->structure_page($modules['fk_structure'],'chpu_admin'); ?>tpl-table">Дополнительная информация</a></li>
        <li role="presentation" class="<?php if ($_GET['tpl']=='file') echo 'active'; ?>"><a href="<?php echo $this->structure_page($modules['fk_structure'],'chpu_admin'); ?>tpl-file">Фото</a></li>
	</ul>
<?php
	if ($_POST['visible_poligon']=='') $_POST['visible_poligon']=0;

    if ($_GET['tpl']=='file') {
        include $modules['dir']."admin_file.php";
    } else if ($_GET['tpl']=='table') {
		if (file_exists($modules['dir']."admin_dop_info/".$modules['id'].".php")) {
			include $modules['dir']."admin_dop_info/".$modules['id'].".php";
		} else {
			echo form::info("Ошибка","обаботчик не предусмотрен");
		}
	} else {

		?>
		<div id="map" style="width: 100%; height: 550px;"></div>
		<div style='height: 40px;margin: 10px 0;'>
			Выберите объект: <select id="mySelect" style="width: 400px;" ><option value=''>-- Выберите объект --</option></select>
			<div class='btn btn-default form-control' id='poligon_new' style=' width: 200px;float: right;'>Создать новый полигон</div>
		</div>
		<div id='poligon_form' style='display:none;'>
		<?php
			echo form::start($this->structure_page($modules['fk_structure'],'chpu_admin'));
			echo form::header(array("but_1"=>	array	(	"value"		=>	"Сохранить",
																		"type"		=>	"submit",
																		"name"		=>	"btn_save",
																		"OnClick"	=>	""),
									"but_0"=>	array	(	"value"		=>	"Удалить",
																		"type"		=>	"submit",
																		"name"		=>	"btn_del",
																		"OnClick"	=>	"")
									)
							);
			echo form::text("id (Если его затереть, появится новый полигон)","id",'');
			echo form::text("Год(сортировка), если года нет то -1 ","year",'-1');
			echo "<div style='border: 1px solid #CCCCCC;padding: 10px;margin: 10px 0;'>";
                echo form::select('Тип метки на карте', 'fk_svg', $svg, $svg_defult, '');
				echo form::checkbox("","visible_placemark",array('1'=>'Показывать метку на карте'),1);
				echo form::text("name","name",'');
				echo form::text("Placemark","placemark",'');
			echo "</div>";
			echo "<div style='border: 1px solid #CCCCCC;padding: 10px;margin: 10px 0;'>";
				echo form::checkbox("","visible_poligon",array('1'=>'Показывать полигон на карте'),1);
				echo form::text("Толщина линии","strokeWidth",'5');
				echo form::text("Цвет линии","strokeColor",'1e90ff99');
				echo form::text("Цвет заливки","fillColor",'1e90ff70');
				echo form::text("Рoligon","poligon",'');
			echo "</div>";
			echo "<div style='border: 1px solid #CCCCCC;padding: 10px;margin: 10px 0;'>";
				echo form::checkbox("","ch",array('1'=>'Настройка центра карты'),0);
				echo form::text("Центр карты","center",'');
				echo form::text("zoom","zoom",'');
			echo "</div>";
			echo form::footer();
			echo form::end();
		?>
		</div>

		<div id='poligon_settings' style='display:;'>
		<?php
			echo form::start($this->structure_page($modules['fk_structure'],'chpu_admin'));
			echo form::header(array("but_0"=>	array	(	"value"		=>	"Сохранить",
																		"type"		=>	"submit",
																		"name"		=>	"btn_save_set",
																		"OnClick"	=>	"")
									)
							);
			echo form::hidden("id (Если его затереть, появится новый полигон)","id",'');
				echo "<div style='border: 1px solid #CCCCCC;padding: 10px;margin: 10px 0;'>";
					echo form::checkbox("","ch",array('1'=>'Настройка центра карты (пока вручную, не успел настроить)'),0);
					echo form::text("Центр карты","map_center",'');
					echo form::text("zoom","map_zoom",'');
					echo form::text("minzoom","map_minzoom",'');
					echo form::text("maxzoom","map_maxzoom",'');
				echo "</div>";
				echo form::text("Слои карты (лежат /files/poligon/...)","layer",'');
				echo form::text("Область просмотра карты","restrictMapArea",'');
				echo form::text("Проекция карты (координатная сетка)","projection",'');
			echo form::footer();
			echo form::end();
		?>
		</div>





		<script src="http://api-maps.yandex.ru/2.1/?load=package.full&lang=ru-RU" type="text/javascript"></script>
		<script type="text/javascript">
		$( document ).ready(function() {
			$('#name_id').attr('readonly','readonly');
			ymaps.ready(init);
			var myMap; //переменная карты
			var create_new=false;
			var myPlacemark_new;
			var myPolygon_new;
			var myPlacemark={};
			var myPolygon={};

			function init() {
				myMap = new ymaps.Map("map", {
					center: [57.1477,65.5401],
					zoom: 10,
					type: "yandex#map",
					behaviors: ['default', 'scrollZoom'] // Включим масштабирование карты колесом мыши
				},{
					avoidFractionalZoom: false,
				});

				// при изменении центра карты
				function move_map() {
					if ($("#name_ch").is(':checked')) {
						$('#name_center').val('['+myMap.getCenter()+']');
						$('#name_zoom').val(myMap.getZoom());
					}
				}
				myMap.events.add('boundschange', function (event) {move_map();});

				$('#name_center, #name_zoom').change(function () { //изменение центра карты и zoom
					if ($("#name_ch").is(':checked')) {
						if (($('#name_center').val()!='')&&($('#name_zoom').val()!='')) {
							myMap.setCenter(JSON.parse($('#name_center').val()), $('#name_zoom').val(), {});
						}
					}
				});
				// ^-^ при изменении центра карты
				// Стили полигона
				$('#name_fillColor, #name_strokeColor').change(function () {
					if ($('#mySelect').find(':selected').val()!='') {
						ms = $('#mySelect').find(':selected').val();
						if ($('#name_fillColor').val()!='') {  // цвет заливки
							myPolygon[ms].options.set({fillColor : $('#name_fillColor').val()});
						}
						if ($('#name_strokeColor').val()!='') { // цвет линии
							myPolygon[ms].options.set({strokeColor : $('#name_strokeColor').val()});
						}
						if ($('#name_strokeWidth').val()!='') { // толщина линии
							myPolygon[ms].options.set({strokeWidth : $('#name_strokeWidth').val()});
						}
					}
				});
				// ^-^ Стили полигона


				$('#name_poligon').change(function () { //действие при изменение координат объекта
					if ($('#name_poligon').val()=='') $('#name_poligon').val('[]');
					if ($('#mySelect').find(':selected').val()!='') {
						myPolygon[$('#mySelect').find(':selected').val()].geometry.setCoordinates(JSON.parse($('#name_poligon').val()));
					} else {
						myPolygon_new.geometry.setCoordinates(JSON.parse($('#name_poligon').val()));
					}
				});
				jQuery.getJSON('/api/poligon/?start=data&mod=<?php echo $modules['id']; ?>', function (json) {
					if (json!=null) {
						if (json.map!=false) {
							if (json.map.layer!='') {
								var layer = function(){
									return new ymaps.Layer(function(tile, zoom) {
										return "http://dom.tyumen-city.ru/files/poligon/"+json.map.layer+"/" + zoom + "/" + tile[0] + "-" + tile[1] + ".jpeg";
									});
								};
								var lineageMap = new ymaps.MapType('LineageMap', [layer]);
								ymaps.mapType.storage.add(json.map.layer+'#map', lineageMap);
								myMap.setType(json.map.layer+'#map');
							}
							if (json.map.restrictMapArea!='') myMap.options.set('restrictMapArea', JSON.parse(json.map.restrictMapArea));
							if (json.map.projection!='') {
								var projection = new ymaps.projection.Cartesian(JSON.parse(json.map.projection));
								myMap.options.set('projection', projection);
							}

							if (json.map.zoom!='') myMap.setZoom(JSON.parse(json.map.zoom));
							if (json.map.minzoom!='') myMap.options.set('minZoom', json.map.minzoom);
							if (json.map.maxzoom!='') myMap.options.set('maxZoom', json.map.maxzoom);
							if (json.map.center!='') myMap.setCenter(JSON.parse(json.map.center));


							$('#name_map_center').val(json.map.center);
							$('#name_map_zoom').val(json.map.zoom);
							$('#name_map_minzoom').val(json.map.minzoom);
							$('#name_map_maxzoom').val(json.map.maxzoom);
							$('#name_layer').val(json.map.layer);
							$('#name_restrictMapArea').val(json.map.restrictMapArea);
							$('#name_projection').val(json.map.projection);
						}
                        // console.log(json);
                        if (json.layout!=null) {
                            if (json.layout.length>0) {
                                for (i=0;i<=json.layout.length-1;i++) {
                                    ymaps.layout.storage.add('icon#svg_'+json.layout[i].id, ymaps.templateLayoutFactory.createClass('<img src="'+json.layout[i].svg+'" style="width:50px;"></span>'));
    									// console.log('icon#svg_'+json.layout[i].id);
                                }
                            }
                        }
                        if (json.rzd!=null) {
                            if (json.poligon.length>0) {
                                for (i=0;i<=json.poligon.length-1;i++) {
                                    if (($('#name_s_year').find(':selected').val()==json.poligon[i].year)||($('#name_s_year').find(':selected').val()=='')) {
                                        // console.log('icon#svg_'+json.poligon[i].fk_svg);
                                        myPlacemark[json.poligon[i].id]= new ymaps.Placemark(JSON.parse(json.poligon[i].placemark), {
    										iconContent: json.poligon[i].name+' ('+json.poligon[i].id+')'
    									}, {
                                            preset: 'islands#blueDotIcon',
    										// preset: 'islands#blueStretchyIcon',
    										iconLayout: 'icon#svg_'+json.poligon[i].fk_svg,
                                            iconOffset: [-25, -50],
                                            iconShape: {
                                                type: 'Rectangle',
                                                coordinates: [[0, 0], [50, 50]]
                                            },
    										id: json.poligon[i].id,
    										name: json.poligon[i].name,
    										center: json.poligon[i].center,
    										year: json.poligon[i].year,
    										zoom: json.poligon[i].zoom,
                                            v: json.poligon[i].visible_placemark,
    										fk_svg: json.poligon[i].fk_svg,
    									});
    									myPlacemark[json.poligon[i].id].events.add('mouseenter', function (e) {
                                            e.get('target').options.set({preset: 'islands#blueStretchyIcon'});
                                            e.get('target').options.unset('iconLayout');
                                            e.get('target').options.unset('iconOffset');
                                            // e.get('target').options.unset('iconShape');
                                        });
                                        myPlacemark[json.poligon[i].id].events.add('mouseleave', function (e) {
                                            e.get('target').options.set({preset: 'islands#blueDotIcon'});
                                            e.get('target').options.set({iconLayout: 'icon#svg_'+e.get('target').options.get('fk_svg')});
                                            e.get('target').options.set({iconOffset: [-25, -50]});
    										// e.get('target').options.set({iconShape: {type: 'Rectangle', coordinates: [[0, 0], [50, 50]]}});
    									});
    									myMap.geoObjects.add(myPlacemark[json.poligon[i].id]);
    									myPolygon[json.poligon[i].id] = new ymaps.Polygon(JSON.parse(json.poligon[i].poligon), {
    											balloonContent: ""
    										}, {
    											strokeColor: json.poligon[i].strokeColor,
    											strokeWidth: json.poligon[i].strokeWidth,
    											strokeOpacity: 1,
    											iconOffset: [0, 0],
    											fillOpacity: 1,
    											fillColor: json.poligon[i].fillColor,
    											draggable: false,      // объект можно перемещать, зажав левую кнопку мыши
    											v: json.poligon[i].visible_poligon,
    									});
    									myMap.geoObjects.add(myPolygon[json.poligon[i].id]);
                                        year='';
                                        if (json.poligon[i].year>0) year = '('+json.poligon[i].year+' год) ';
    									$('#mySelect').append($('<option>', { value : json.poligon[i].id }).text(year+json.poligon[i].name));
    								}
    							}
    						}
                        }
					}
				});





				$('#poligon_new').on('click', function() { // Кнопка - новый полигон
					$("#name_ch").prop( "checked", true );
					move_map(); // новый центр карты, иначе из выходе из редактора в новый полигон, остаются старые
					if (myPlacemark_new) {
						myMap.geoObjects.remove(myPlacemark_new);
						myPlacemark_new='';
					}
					if (myPolygon_new) {
						myMap.geoObjects.remove(myPolygon_new);
						myPolygon_new='';
					}
					create_new = true;
					$('#poligon_form').show();
					$('#name_id').val('');
					$('#name_year').val('-1');
					$('#name_name').val('new');
					$('#name_placemark').val('');
					$('#name_poligon').val('');
					if ($('#mySelect').find(':selected').val()!='') {
						myPolygon[$('#mySelect').find(':selected').val()].editor.stopEditing();
					}
					$('#mySelect').prop('selectedIndex', 0);
				});

				myMap.events.add('click', function (e) { // Клик по карте
					if (create_new) {
						if (!myPlacemark_new) {
							myPlacemark_new= new ymaps.Placemark(e.get('coords'), {
								iconContent: 'new'
							}, {
								preset: 'islands#blueStretchyIcon',
								draggable: true
							});
							$('#name_placemark').val('['+myPlacemark_new.geometry.getCoordinates()+']');
							myPlacemark_new.events.add("dragend", function () {   // событие перемещения
								$('#name_placemark').val('['+this.geometry.getCoordinates()+']');
							},myPlacemark_new);
							myMap.geoObjects.add(myPlacemark_new);
						} else if (!myPolygon_new) {
							myPolygon_new = new ymaps.Polygon([[e.get('coords')]], {
									balloonContent: "11"
								}, {
									strokeColor: '1e90ff99',
									strokeWidth: 5,
									strokeOpacity: 1,
									fillOpacity: 1,
									fillColor: '1e90ff70',
									draggable: false      // объект можно перемещать, зажав левую кнопку мыши
							});
							myPolygon_new.events.add('geometrychange', function (e) {
								$('#name_poligon').val(JSON.stringify(e.get('target').geometry.getCoordinates()));
							});
							myPolygon_new.events.fire('geometrychange'); // чтобы прописать первую точку а поле
							myMap.geoObjects.add(myPolygon_new);
							myPolygon_new.editor.startEditing();
							myPolygon_new.editor.startDrawing();
							create_new=false;
						}
					}
				});

				$('#mySelect').on('change', function() { // событие выбора полигона
					id = $(this).val();
					$("#name_ch").prop( "checked", false );
					if (id>0) {
						$('#poligon_form').show();
					} else {
						$('#poligon_form').hide();
						myMap.setCenter([57.1477,65.5401], 10, {});
					}
					$.each(myPlacemark, function(e) {
						if (e==id) {
							$('#name_visible_placemark').prop("checked", ((this.options.get('v')==1)?true:false));
                            // console.log("#name_fk_svg [value='"+this.options.get('fk_svg')+"']");
                            $("#name_fk_svg [value='"+this.options.get('fk_svg')+"']").prop('selected', true);
							this.options.set({draggable : true});
							$('#name_id').val(this.options.get('id'));
							$('#name_year').val(this.options.get('year'));
							$('#name_name').val(this.options.get('name'));
							$('#name_center').val(this.options.get('center'));
							$('#name_zoom').val(this.options.get('zoom'));
							$('#name_placemark').val('['+this.geometry.getCoordinates()+']');

							this.events.add("dragend", function () {   // событие перемещения
								$('#name_placemark').val('['+this.geometry.getCoordinates()+']');

							},this);
						} else {
							this.options.set({draggable : false});
							this.events.remove("dragend");
						}
					});
					$.each(myPolygon, function(e) {
						if (e==id) {
							$('#name_visible_poligon').prop("checked", ((this.options.get('v')==1)?true:false));
							$('#name_strokeWidth').val(this.options.get('strokeWidth'));
							$('#name_strokeColor').val(this.options.get('strokeColor'));
							$('#name_fillColor').val(this.options.get('fillColor'));
							this.editor.startEditing();
							$('#name_poligon').val(JSON.stringify(this.geometry.getCoordinates()));
							this.events.add('geometrychange', function (e) {
								$('#name_poligon').val(JSON.stringify(this.geometry.getCoordinates()));
							},this);
                            myMap.setBounds(this.geometry.getBounds(),{checkZoomRange:true}); // центрируем
						} else {
							this.editor.stopEditing();
						}
					});
				});
			}
		});
		</script>
<?php }?>


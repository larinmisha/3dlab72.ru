<style>
    .list_objects_blago .table .center {text-align: center;}
    .list_objects_blago .table .title {text-align: center;font-weight: 700;}
    .list_objects_blago .cat {color: #4590ce;font-weight: normal;font-size: 24px;text-align: center;margin: 20px 0;}
</style>
<div class="list_objects_blago">
    <div class="container">
        <div class="cat">Перечень объектов "<?php echo $this->structure_page($modules['fk_structure'],'title'); ?>"</div>
            <ul class="nav nav-tabs" role="tablist">
                <?php
                    $class="active";
                    // $this->pdo->sql(1);
                    $row_=$this->pdo->query("SELECT Distinct year FROM bk_poligon where fk_modules=".$modules['id']." and year>0 and del=0 ORDER BY year DESC");
                    while($row=$row_->fetch()) {
                    ?>
                        <li role="presentation" class="<?php echo $class; ?>">
                            <a href="#num<?php echo $row['year']; ?>" aria-controls="num<?php echo $row['year']; ?>" role="tab" data-toggle="tab">
                                <div>в <?php echo $row['year']; ?> году</div>
                            </a>
                        </li>
                    <?php
                    $class='';
                    }
                ?>
            </ul>
            <div class="tab-content">
                <?php
                    $num=1;
                    $class="active";
                    // $row_=$this->pdo->query("SELECT Distinct year FROM bk_poligon where fk_modules=".$modules['id']." and year>0 and del=0 ORDER BY year DESC");
                    // while($row=$row_->fetch()) {
                    ?>
                    <div role="tabpanel" class="tab-pane <?php echo $class; ?>" id="num<?php echo $row['year']; ?>">

                        <br>
                        <table class="table table-striped table-bordered table-condensed">
                            <tr>
                                <td class="title">&nbsp;</td>
                                <td class="title">Адресс</td>
                                <td class="title">Организация</td>
                                <td class="title">ФИО</td>
                                <td class="title">Телефон</td>
                                <td class="title">Площадь</td>
                                <td class="title">Фото</td>
                            </tr>
                            <?php
                            error_reporting(E_ERROR | E_WARNING | E_PARSE);
                            ini_set('display_errors', 1);
                            // $this->pdo->sql(1);
                                $row2_=$this->pdo->query("SELECT     p.name, p.year, ir.*
                                                            FROM    bk_poligon as p left join bk_poligon_info_dog_walking as ir on p.id=ir.fk_poligon and ir.del=0 and ir.display=0
                                                            WHERE   p.fk_modules = ".$modules['id']." and
                                                                    p.del=0 and
                                                                    ir.del=0 and display=0
                                                            group by p.id, p.year, p.name
                                                            ORDER BY p.name ASC");
                            ?>
                            <?php while($row2=$row2_->fetch()) {
                                ?>
                                <tr>
                                    <td class="center"><?php echo $num; ?></td>
                                    <td class="center"><?php echo $row2['name'] ?></td>
                                    <td class="center"><?php echo $row2['org'] ?></td>
                                    <td class="center"><?php echo $row2['fio']; ?></td>
                                    <td class="center"><?php echo $row2['phone']; ?></td>
                                    <td class="center"><?php echo $row2['area']; ?></td>
                                    <td class="center"><img src="http://dom.tyumen-city.ru/files/poligon/dog_walking/<?php echo $row2['photo']; ?>" style='width:150px'></td>
                                </tr>
                                <?php if (count($row2['vid'])>0) { ?>
                                    <tr>
                                        <td colspan=6>
                                            Вид работ:<br>
                                            <ul>
                                            <?php
                                                if ($row2['vid']!='') {
                                                    ?><li><?php echo $row2['vid']; ?></li><?php
                                                }
                                            ?>
                                            </ul>
                                        </td>
                                    </tr>
                                <?php
                                    }
                                    $num++;
                                }
                                ?>
                        </table>
                    </div>
                    <?php
                    $class="";
                    // }
                ?>
            </div>
    </div>
</div>
<script>
    $('#myTabs a').click(function (e) {
        e.preventDefault()
        $(this).tab('show')
    })
</script>

<?php
	base::test(); // Скрипт защиты от запуска напрямую
	# --------------------------------------------------- Покзывать/Скрыть запись в ленте ---------------------------------------------------
	if ($_GET['action']=="lamp") {
		$this->pdo->query("UPDATE bk_poligon_info_dog_walking SET display='{$_GET['d']}' WHERE id='{$_GET['lenta']}' LIMIT 1");
		$_GET['action']=""; // чистим событие, чтобы вывести ленту
	}
	# --------------------------------------------------- Покзывать/Скрыть запись в ленте ---------------------------------------------------

	# --------------------------------------------------- Удаление записи из ленты ---------------------------------------------------
	if ($_POST['delete-lent-true']!='') {
		$this->pdo->query("UPDATE bk_poligon_info_dog_walking
						SET del='1'
						WHERE id='{$_POST['delete-lent-true']}'
						LIMIT 1");
		echo form::info("Увеломление!", "Новость успешно удалена", "success");
		$_GET['action']="";
	}

	if ($_GET['action']=="delete-lent") {
		$row = $this->pdo->query("Select * From bk_poligon_info_dog_walking WHERE id='{$_GET['lenta']}'")->fetch();
		echo form::start($this->structure_page($modules['fk_structure'],'chpu_admin'));
		echo form::hidden("id_mod","","",$modules['id']);
		echo form::hidden("delete-lent-true","","",$_GET['lenta']);
		echo form::header(array("but_1"=>	array	(	"value"		=>	"Удалить",
																	"name"		=>	"btn_save",
																	"type"		=>	"submit",
																	"OnClick"	=>	"")
											)
									);
		echo form::info("Предупреждение!",
								"Вы пытаетесь удалить запись!
								<br>Организация: {$row['org']}
								",
								"danger");
		echo form::footer();
		echo form::end();
	}
	# --------------------------------------------------- Удаление новости ---------------------------------------------------
?>
<blockquote class="blockquote-reverse">
	<p>
		Добавить новую запись
		<a href='<?php echo "{$this->structure_page($modules['fk_structure'],'chpu_admin')}tpl-table_action-form-lent"; ?>' title='Добавить новую запись'>
			<button type='button' class='btn btn-default btn-xs'>
				<span class='glyphicon glyphicon-pencil' aria-hidden='true'></span>
			</button>
		</a>
	</p>
</blockquote>
<?php

	include "classes/class.upload.php";
	if (isset($_POST['save-form-lent'])) {

		$path="files/poligon/relax_zone/";
		$f_name ='';
		if ($_POST['photo_h']==1) {	// Есть мы вышли в меню с выбором файла то может для того чтобы удалить,
			if ($_FILES['photo']['tmp_name']!="") {
				$r =strtolower(pathinfo($_FILES['photo']['name'], PATHINFO_EXTENSION));
				$f_name = $_POST[okr].'_'.uniqid('').".{$r}";
				if (in_array($r, array("jpg", "png", "gif", "bmp", "jpeg"))) {
					$file = $path.$f_name;
					if (copy($_FILES['photo']['tmp_name'], $file)) {
						$handle = new Upload($file);
						$handle->file_overwrite = true; // перезаписать файл
						$handle->image_resize        = true;
						$handle->image_y               = 1080;
						$handle->image_x               = 1920;
						$handle->image_ratio         = true;
						$handle->process($path);
						/* http://programilla.com/blog/siteconstruction/81.html - тут параметры */
						echo 'путь к записанному файлу '.$path.$f_name.'<br>';
					} else echo "не удалось записать файл";
				}
			}
		}



		if (($_POST['save-form-lent'])>0) { // Редактирование
			$this->pdo->query("UPDATE `bk_poligon_info_dog_walking` SET 	`fk_poligon`='{$_POST['okr']}',
													`org`='{$_POST['org']}',
													`fio`='{$_POST['fio']}',
													`phone`='{$_POST['phone']}',
													`area`='{$_POST['area']}',
													`photo`='{$f_name}'
							WHERE id='{$_POST['save-form-lent']}'
							LIMIT 1");
			echo form::info("Уведомление!","Успешно изменена","success");
		} else {
			$this->pdo->query("INSERT INTO `bk_poligon_info_dog_walking`(`fk_poligon`, `org`, `fio`, `phone`, `area`, `photo`) VALUES ('{$_POST['okr']}','{$_POST['org']}','{$_POST['fio']}','{$_POST['phone']}','{$_POST['area']}','{$f_name}')");
			echo form::info("Уведомление!","Успешно добавлена","success");
		}

	}

	if ($_GET['action']=="form-lent") {
		# --------------------------------------------------- Добавление/Редактирование новости ---------------------------------------------------
		$row_=$this->pdo->query("SELECT *
							FROM 	bk_poligon as p
							where 	p.fk_modules=".$modules['id']." and
									p.del=0
							ORDER BY  sort, name ASC ");
		while($row=$row_->fetch()) {
			$mas[$row['id']] = $row['name'];
		}
		if ($_GET['lenta']>0) {
			$row = $this->pdo->query("Select * From bk_poligon_info_dog_walking WHERE id='{$_GET['lenta']}'")->fetch();
		}
		if ($row['fk_region']=='') $row['fk_region']=0;
		echo form::start($this->structure_page($modules['fk_structure'],'chpu_admin').'tpl-table');
		echo form::hidden("id_mod","","",$modules['id']);
		echo form::hidden("save-form-lent","","",$_GET['lenta']);
		echo form::header(array("but_1"=>	array	(	"value"		=>	"Сохранить",
																	"name"		=>	"btn_save",
																	"type"		=>	"submit",
																	"OnClick"	=>	"")
											)
									);

		echo form::select("Полигон","okr",$mas,$row['fk_poligon']);
		echo form::text("Организация","org",$row['org']);
		echo form::text("ФИО","fio",$row['fio']);
		echo form::text("Телефон","phone",$row['phone']);
		echo form::text("Площадь","area",$row['area']);
		echo form::file("Фото","photo",$row['photo']);

		echo form::footer();
		echo form::end();
		# --------------------------------------------------- Добавление/Редактирование новости ---------------------------------------------------
	}


	if ($_GET['action']=="") {
		# --------------------------------------------------- Вывод списка новостей --------------------------------------------------- #
		$col_page = 100;
		if ($_GET['page']!='') $page=$_GET['page']; else $page=1;
		$limit = " LIMIT ".(($page-1)*$col_page).", {$col_page}";

		$row_=$this->pdo->query("SELECT d.*,p.name as p_okr
							FROM 	bk_poligon_info_dog_walking as d, bk_poligon as p
							where 	p.id=d.fk_poligon and
									p.fk_modules=".$modules['id']." and
									d.del=0 and
									p.del=0
							ORDER BY  sort, name ASC
							{$limit}");
		if($row_->rowCount()>0) {
		?>
			<table class="table table-hover">
				<tr>
					<th>Полигон</th>
					<th>Организация</th>
					<th>&nbsp;</th>
					<th>&nbsp;</th>
					<th>&nbsp;</th>
				</tr>
			<?php
				while($row=$row_->fetch()) {
					if ($row['display']==1) $danger = "class='danger'"; else $danger = "";
			?>
				<tr <?php echo $danger; ?>>
					<td><?php echo $row['p_okr']; ?></td>
					<td><?php echo $row['org']; ?></td>
					<td>
						<a href='<?php echo "{$this->structure_page($modules['id'],'chpu_admin')}tpl-table_page-{$page}_lenta-{$row['id']}_action-form-lent"; ?>' title='Редактировать'>
							<button type='button' class='btn btn-default btn-xs'>
								<span class='glyphicon glyphicon-pencil' aria-hidden='true'></span>
							</button>
						</a>
					</td>
					<td>
						<a href='<?php echo "{$this->structure_page($modules['id'],'chpu_admin')}tpl-table_page-{$page}_lenta-{$row['id']}_action-lamp_d-".(($row['display']==0)?"1":"0"); ?>' title='Показать/Скрыть'>
							<button type='button' class='btn btn-default btn-xs'>
								<span class='glyphicon glyphicon-eye-<?php echo (($row['display']==0)?"open":"close"); ?>' aria-hidden='true'></span>
							</button>
						</a>
					</td>
					<td>
						<a href='<?php echo "{$this->structure_page($modules['id'],'chpu_admin')}tpl-table_page-{$page}_lenta-{$row['id']}_action-delete-lent"; ?>' title='Уралить'>
							<button type='button' class='btn btn-default btn-xs'>
								<span class='glyphicon glyphicon-trash' aria-hidden='true'></span>
							</button>
						</a>
					</td>
				</tr>
			<?php
				}
			?>
			</table>
		<?php
			$row = $this->pdo->query("SELECT 	COUNT(*) as col
								FROM 	bk_poligon_info_dog_walking as d, bk_poligon as p
								where 	p.id=d.fk_poligon and
										p.fk_modules=".$modules['id']." and
										d.del=0 and
										p.del=0
								ORDER BY p.name ASC")->fetch();
			echo form::info("Уведомление!","Всего объектов: ".$row['col'],"success");
		} else {
			echo form::info("Уведомление!","Лента пуста","success");
		}
		# --------------------------------------------------- Вывод списка новостей --------------------------------------------------- #
	}
?>


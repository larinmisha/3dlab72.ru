<?php base::test();

    class Defult extends Api {

        /**
         * Получение формы
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function tpl($params) {
            global $cms;
            include $cms->modules['dir'].'/tpl/form.php';
        }

        /**
         * Обновить настройки информера
         * @param  array $params параметры
         */
        public function send($params) {
            global $cms;

            if (($_POST['fio'] == '')||($_POST['phone'] == '')) {
                $data['errors'][] = "Заполены не все поля";
            }

            if ($cms->modules['id'] == '') {
                $data['errors'][] = "не верно указан параметр sid: Модульная привязка";
            }

            if ($_POST['captcha'] != $_SESSION['captcha']) {
                $data['errors'][] = "Введен неправильный проверочный код";
            }

            if ($_POST['ch'] != 'on') {
                $data['errors'][] = "Ознакомьтесь с политикой конфиденциальности";
            }
            if (count($data['errors']) == 0) {
                $row_ = $cms->pdo->prepare("SELECT * FROM bk_feedback WHERE fk_modules=:mid LIMIT 1;");
                $row_->execute([':mid' => $cms->modules['id']]);

                $row = $row_->fetch();
                $message = '';
                $message .= "<b>Форма захвата: </b> ".$params['location']."<br>";
                if ($_POST['fio']  !='') $message .= "<b>ФИО:</b> ".$_POST['fio']."<br>";
                if ($_POST['mail'] !='') $message .= "<b>Электронная почта (e-mail):</b> ".$_POST['mail']."<br>";
                if ($_POST['phone']!='') $message .= "<b>Телефон:</b> ".$_POST['phone']."<br>";
                if ($_POST['text'] !='') $message .= "<b>Сообщение:</b> ".$_POST['text']."<br>";

                if ($params['html'] !='') {
                    $message .= "<b>Блок: </b> ".$params['html']."<br>";
                }

                $message .= "Страница с которой было отправлено сообщение:
                            <a href='".$cms->url($params['node']).$params['get']."' target='_blank'>Перейти</a><br>";


                $row_=$cms->pdo->prepare("INSERT INTO `bk_feedback_sms`(`fk_feedback`, `message`) VALUES (:id, :message);");
                $row_->execute([':id' => $row['id'], ':message' => $message]);
                if ($row['mail']!='') {
                    $sendMail->get(['email'     => [$row['mail']],
                                    'subject'   => "Форма: ".$row['subject'],
                                    'message'   => $message
                                    ]);
                }
                $data['success'] = "Ваше сообщение отправлено";
            }

            return ['data' => $data];
        }

        /**
         * Получение записи
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function select($params) {
            global $cms;

            if ($cms->get_group_cms('dir', 1)) {
                $row_=$cms->pdo->prepare("SELECT * FROM bk_feedback WHERE fk_modules=:mid LIMIT 1");
                $row_->execute([':mid' => $cms->modules['id']]);
                if ($row_->rowCount() > 0) {
                    return $row_->fetch();
                } else {
                    return false;
                }
            }
        }

        /**
         * Получение списка сообщений
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function message($params) {
            global $cms;

            if ($cms->get_group_cms('dir', 1)) {
                $row_=$cms->pdo->prepare("  SELECT s.id, s.message, s.timestamp
                                            FROM    bk_feedback as f,
                                                    bk_feedback_sms as s
                                            WHERE   1=1
                                                    and s.fk_feedback = f.id
                                                    and s.del_user is NULL
                                                    and f.fk_modules = :mid
                                            ORDER BY s.id ASC
                                            ");
                $row_->execute([':mid' => $cms->modules['id']]);
                if ($row_->rowCount() > 0) {
                    while ($row = $row_->fetch()) {
                        $data[] = $row;
                    }
                    return $data;
                } else {
                    return false;
                }
            }
        }

        /**
         * Добавление/изменение записи
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function insert_update($params) {
            global $cms;

            if ($cms->get_group_cms('dir', 1)) {
                $row_=$cms->pdo->prepare("SELECT id FROM  bk_feedback WHERE fk_modules=:mid LIMIT 1");
                $row_->execute([':mid' => $cms->modules['id']]);
                if ($row_->rowCount() == 0) {
                    $row_=$cms->pdo->prepare(" INSERT INTO bk_feedback (fk_modules, mail, subject, title, webkoder)
                                                VALUES (:mid, :mail, :subject, :title, :webkoder)");
                } else {
                    $row_=$cms->pdo->prepare(" UPDATE  bk_feedback
                                                SET     mail        = :mail,
                                                        subject     = :subject,
                                                        title       = :title,
                                                        webkoder    = :webkoder
                                                WHERE   fk_modules  = :mid
                                                LIMIT 1");
                }
                $_POST['text']=str_replace("'","\'",$_POST['text']);
                $row_->execute([':mail'     => $params['mail'],
                                ':subject'  => $params['subject'],
                                ':title'    => $params['title'],
                                ':webkoder' => $params['webkoder'],
                                ':mid'      => $cms->modules['id']
                                ]);
                return 1;
            }
        }

        /**
         * Удаление записи
         * @param  array $params параметры
         * @return array         ответ сервера
         */
        public function delete($params) {
            global $cms;
            if ($cms->get_group_cms('dir', 1)) {
                $row_=$cms->pdo->prepare("UPDATE `bk_feedback_sms` SET del_user=:user WHERE id = :id");
                $row_->execute([':user' => $_SESSION['user']['id'], ':id' => $params['id']]);
                return ['data' => 1];
            } else {
                return ['data' => 0];
            }
        }
    }

    $defult = new Defult();

    if ($defult->getAction()!='') { // вызов через api, иначе вызов напрямую через php
        echo $defult->query()->fetch();
    }

?>

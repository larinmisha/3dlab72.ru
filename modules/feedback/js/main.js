$('body').on('click', '.js-btn-modalForm', function(e) {
    $.ajax({
        type: "POST",
        url: '/api/feedback/raw?collection=defult&action=tpl&sid=5&get='+$(this).data('get'),
        data: {class_html: $(this).data('html'), location: $(this).data('location')}
    }).done(function( msg ) {
        $('.modal_content').html(msg);
        $('.modal_dialog').removeClass('modal-xs modal-sm modal-md modal-lg');
        $('.modal_dialog').addClass('modal-sm');
        $('#myModal').modal();
    });

});

$('body').on('click', '.js-feedback__submit', function(e) {
    var data = main.fetchformData("."+$(this).data('name')+" .js-feedback__form");
    if ($(this).data('html')!='') {
        data.append('html', $('.'+$(this).data('html')).html());
    }
    $('.js-feedback__submit').attr('disabled', true);
    $(' .js-feedback__submit .js-info').removeClass('hidden');
    main.ajaxRequest('/api/feedback/json?collection=defult&action=send&node='+NODE, data)
        .done(function( json ) {
            $('.js-feedback__submit').attr('disabled', !true);
            $(' .js-feedback__submit .js-info').addClass('hidden');
            if ((json)&&(json.data)) {
                if (json.data.errors) {
                    content = '';
                    $.each(json.data.errors, function(e) {
                        content = content + "<li>"+this+"</li>";
                    });
                    $('.js-feedback__info .text').html("<ul>"+content+"</li>");
                } else if (json.data.success) {
                    $('.js-feedback__info .text').html(json.data.success);
                    $(".js-feedback__form, .js-modal-footer").addClass('hidden');
                }
                $('.js-feedback__info').removeClass('hidden');
            }
        });
});

<?php
    base::test();

    include $this->modules['dir']."collections/defult.php";
    if (isset($_POST['save_form_feedback'])) {
        if ($defult->insert_update($_POST_)) {
            echo form::info("Уведомление: ","Успешно сохранено","success");
        }
    }

    $data = $defult->message([]);
    if ($data) {
        foreach ($data as $row) {
            ?>
            <div class="alert alert-info alert-dismissible fade in" role="alert" data-id='<?php echo $row['id']; ?>'>
                <button type="button" class="close js-delete__message" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4>#<?php echo $row['id']; ?> : <?php echo $row['timestamp']; ?></h4>
                <p><?php echo $row['message']; ?></p>
                <p>
                    <button type="button" class="btn btn-default js-delete__message">Скрыть</button>
                </p>
            </div>
            <?php
        }
    }

    $row = $defult->select([]);

    echo form::start($this->url($modules['fk_structure'], 1));
    echo form::header();
    echo form::text("mail <small>(разделитель: запятая)</small>", "mail", $row['mail']);
    echo form::text("Тема письма / Заголовок формы", "subject", $row['subject']);
    echo form::text("От кого письмо, по умолчанию имя сайта если пустое поле", "title", $row['title']);
    echo form::text("Получатель (webkoder@site.ru)", "webkoder", $row['webkoder']);
    ?>
        <button class="btn btn-default btn-xs" type="submit" name='save_form_feedback'>
            <span class="glyphicon glyphicon-floppy-save" aria-hidden="true"></span> Сохранить
        </button>
    <?php
    echo form::footer();
    echo form::end();


?>

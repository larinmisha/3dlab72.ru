<?php base::test(); ?>
<div class="js-form__zacaz">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Заказать звонок</h4>
    </div>

    <div class="modal-body modal_body">
        <div class="js-feedback__info hidden">
            <div class="alert alert-info" role="alert">
              <!-- <span class="glyphicon glyphicon-exclamation-sign"></span> -->
              <span class="text"></span>
            </div>
        </div>
        <form class="js-feedback__form">
            <input type="hidden" name='rzd'       value='<?php echo $params['sid']; ?>'>

            <div class="form-group">
                <label for="fio">Ваше имя:</label>
                <input type="text" id="fio" name="fio" class="form-control">
            </div>

            <!-- <div class="form-group">
                <label for="mail">Электронная почта (e-mail):</label>
                <input type="text" id="mail" name="mail" class="form-control">
            </div> -->

            <div class="form-group">
                <label for="phone">Телефон:</label>
                <input type="text" id="phone" name="phone" class="form-control">
            </div>

            <!-- <div class="form-group has-feedback js-text">
                <label for="text">* Сообщение:</label>
                <textarea id="text" name="text" class="form-control" rows="5"></textarea>
            </div> -->

            <div class="form-group has-feedback js-captcha">
                <label for="captcha">Введите проверочный код:</label>
                <div class="input-group">
                    <span class="input-group-btn">
                        <img src="/api/captcha/" class="b-captcha__image js-captcha">
                        <button type="button" name="recaptcha" class="b-captcha__button btn btn-default js-recaptcha">
                            <i class="glyphicon glyphicon-refresh"></i>
                        </button>
                    </span>
                    <input type="text" name="captcha" class="b-captcha__input form-control js-captcha__input">
                </div>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name='ch'>
                    Ознакомлен с <a href="<?php echo $cms->url(6); ?>" target='_blank'>политикой конфиденциальности</a>
                </label>
            </div>
            <input type="hidden" name ="location"   value="<?php echo $params['location']; ?>">
            <input type="hidden" name ="get"        value="<?php echo $params['get']; ?>">
        </form>
    </div>

    <div class="modal-footer js-modal-footer" style="position: relative;">
        <!-- data-get - набор параметров в get -->
        <button type="button"
                class="btn btn-default js-feedback__submit"
                data-name='js-form__zacaz'
                data-html="<?php echo $params['class_html']; ?>"
            <i class="glyphicon glyphicon-refresh animation-rotate hidden js-icon"></i>&nbsp;Отправить
        </button>
    </div>
</div>

CREATE TABLE IF NOT EXISTS `bk_feedback` (
  `id` int(100) unsigned NOT NULL,
  `fk_modules` int(100) NOT NULL DEFAULT '0',
  `mail` varchar(255) NOT NULL COMMENT 'Список email (через запятую)',
  `subject` varchar(255) DEFAULT NULL COMMENT 'Тема письма / Заголовок формы',
  `title` varchar(255) DEFAULT NULL COMMENT 'От кого письмо',
  `webkoder` varchar(255) DEFAULT NULL COMMENT 'Получатель'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
CREATE TABLE IF NOT EXISTS `bk_feedback_sms` (
  `id` int(100) unsigned NOT NULL,
  `fk_feedback` int(100) NOT NULL DEFAULT '0',
  `message` text NOT NULL COMMENT 'Текст сообщения',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Дата создания',
  `del_user` tinyint(1) DEFAULT NULL COMMENT 'id пользователя удалившего сообщение'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
ALTER TABLE `bk_feedback`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `bk_feedback_sms`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `bk_feedback`
  MODIFY `id` int(100) unsigned NOT NULL AUTO_INCREMENT;
ALTER TABLE `bk_feedback_sms`
  MODIFY `id` int(100) unsigned NOT NULL AUTO_INCREMENT;
